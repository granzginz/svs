﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using LinqSpecs;
using NHibernate;
using NHibernate.Linq;
using NHibernate.Criterion;
using Treemas.Base.Repository;
using Treemas.Credential.Model;
using Treemas.Credential.Interface;
using Treemas.Base.Utilities.Queries;
using Treemas.Base.Utilities;

namespace Treemas.Credential.Repository
{
    public class RoleFunctionRepository : RepositoryController<RoleFunction>, IRoleFunctionRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public RoleFunctionRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }
        public RoleFunction getRoleFunction(long id)
        {
            return transact(() => session.QueryOver<RoleFunction>()
                                                .Where(f => f.Id == id).SingleOrDefault());
        }

        public IList<RoleFunction> getFunctions(long RoleId)
        {
            IEnumerable<RoleFunction> _roleFunction = this.FindAll(new AdHocSpecification<RoleFunction>(s => s.RoleId == RoleId));
            return _roleFunction.ToList();
        }

        public IList<RoleFunction> getRolebyFuncID(long funcId)
        {
            IEnumerable<RoleFunction> _applicationRole = this.FindAll(new AdHocSpecification<RoleFunction>(s => s.FunctionId == funcId));
            return _applicationRole.ToList();
        }
        public bool IsDuplicate(long roleid, long functionid)
        {
            int rowCount = transact(() => session.QueryOver<RoleFunction>()
                                                .Where(f => f.FunctionId == functionid && f.RoleId == roleid).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public PagedResult<RoleFunction> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {
            PagedResult<RoleFunction> paged = new PagedResult<RoleFunction>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.QueryOver<RoleFunction>()
                                                     .OrderBy(Projections.Property(orderColumn)).Asc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() => session.QueryOver<RoleFunction>()
                                                     .OrderBy(x => typeof(RoleFunction).GetProperty(orderColumn)).Desc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }

            paged.TotalItems = Count;
            return paged;
        }

        public int deleteSelected(long roleId, long functionId)
        {
            string hqlSyntax = "delete RoleFunction where RoleId = :RoleId and FunctionId = :FunctionId";
            return session.CreateQuery(hqlSyntax)
                    .SetInt64("RoleId", roleId)
                    .SetInt64("FunctionId", functionId)
                    .ExecuteUpdate();
        }
        
        public RoleFunction getRoleFunc(long roleId, long funcId)
        {
            return transact(() => session.QueryOver<RoleFunction>()
                                                .Where(f => f.RoleId == roleId && f.FunctionId == funcId).SingleOrDefault());
        }
    }
}
