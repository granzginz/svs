﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;
using LinqSpecs;
using NHibernate.Linq;
using Treemas.Base.Repository;
using Treemas.Credential.Model;
using Treemas.Credential.Interface;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using NHibernate.Criterion;

namespace Treemas.Credential.Repository
{
    public class FunctionCrRepository : RepositoryController<AuthorizationFunctionCr>, IFunctionCrRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public FunctionCrRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }

        public void deleteExistingRequest(string functionId)
        {
            string hqlDelete = "delete SEC_FUNCTION_CR where FUNCTION_ID = :FUNCTION_ID and APPROVAL_TYPE = 'U' and APPROVAL_STATUS = 'N' ";
            int deletedEntities = session.CreateQuery(hqlDelete)
                    .SetString("FUNCTION_ID", functionId)
                    .ExecuteUpdate();
        }

        public PagedResult<AuthorizationFunctionCr> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, ApplicationCRFilter filter)
        {
            SimpleExpression status = Restrictions.Eq("ApprovalStatus", filter.ApprovalStatus);
            SimpleExpression apprtype = Restrictions.Eq("ApprovalType", filter.ApprovalType);

            Conjunction conjuction = Restrictions.Conjunction();
            if (!filter.ApprovalStatus.Trim().IsNullOrEmpty()) conjuction.Add(status);
            if (!filter.ApprovalType.Trim().IsNullOrEmpty()) conjuction.Add(apprtype);

            PagedResult<AuthorizationFunctionCr> paged = new PagedResult<AuthorizationFunctionCr>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.QueryOver<AuthorizationFunctionCr>().Where(conjuction)
                                                     .OrderBy(Projections.Property(orderColumn)).Asc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() => session.QueryOver<AuthorizationFunctionCr>().Where(conjuction)
                                                     .OrderBy(Projections.Property(orderColumn)).Desc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }

            paged.TotalItems = Count;
            return paged;
        }

        public AuthorizationFunctionCr getExistingRequest(string functionId)
        {
            return transact(() => session.QueryOver<AuthorizationFunctionCr>()
                                                .Where(f => f.FunctionId == functionId && f.ApprovalType == "U" && f.ApprovalStatus == "N").SingleOrDefault());
        }

        public AuthorizationFunctionCr getFunction(long id)
        {
            return transact(() => session.QueryOver<AuthorizationFunctionCr>()
                                                .Where(f => f.Id == id).SingleOrDefault());
        }

        public IList<AuthorizationFunctionCr> getFunctions(IList<string> ApplicationIds)
        {
            return transact(() =>
                   session.QueryOver<AuthorizationFunctionCr>()
                          .WhereRestrictionOn(val => val.Application)
                          .IsIn(ApplicationIds.ToArray()).List());
        }

        public bool IsDuplicate(string application, string functionid)
        {
            int rowCount = transact(() => session.QueryOver<AuthorizationFunctionCr>()
                                                .Where(f => f.FunctionId == functionid && f.Application == application).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }

        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }
    }
}
