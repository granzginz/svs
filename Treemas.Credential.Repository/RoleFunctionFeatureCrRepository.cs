﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using LinqSpecs;
using NHibernate;
using NHibernate.Linq;
using NHibernate.Criterion;
using Treemas.Base.Repository;
using Treemas.Credential.Model;
using Treemas.Credential.Interface;
using Treemas.Base.Utilities.Queries;
using Treemas.Base.Utilities;

namespace Treemas.Credential.Repository
{
    public class RoleFunctionFeatureCrRepository : RepositoryController<RoleFunctionFeatureCr>, IRoleFunctionFeatureCrRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public RoleFunctionFeatureCrRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }
        public RoleFunctionFeatureCr getRoleFuncFeature(long id)
        {
            return transact(() => session.QueryOver<RoleFunctionFeatureCr>()
                                                .Where(f => f.Id == id).SingleOrDefault());
        }
        public IList<RoleFunctionFeatureCr> getRoleFuncFeaturebyRole(long RoleId)
        {
            IEnumerable<RoleFunctionFeatureCr> _roleFunction = this.FindAll(new AdHocSpecification<RoleFunctionFeatureCr>(s => s.RoleId == RoleId));
            return _roleFunction.ToList();
        }
        public IList<RoleFunctionFeatureCr> getRoleFuncFeatures(long RoleFuncId)
        {
            IEnumerable<RoleFunctionFeatureCr> _roleFunction = this.FindAll(new AdHocSpecification<RoleFunctionFeatureCr>(s => s.RoleFunctionId == RoleFuncId && s.Approved == false));
            return _roleFunction.ToList();
        }
        
        public bool IsDuplicate(long roleid, long functionid)
        {
            int rowCount = transact(() => session.QueryOver<RoleFunctionFeatureCr>()
                                                .Where(f => f.FunctionId == functionid && f.RoleId == roleid).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public PagedResult<RoleFunctionFeatureCr> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {
            PagedResult<RoleFunctionFeatureCr> paged = new PagedResult<RoleFunctionFeatureCr>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.QueryOver<RoleFunctionFeatureCr>()
                                                     .OrderBy(Projections.Property(orderColumn)).Asc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() => session.QueryOver<RoleFunctionFeatureCr>()
                                                     .OrderBy(x => typeof(RoleFunctionFeatureCr).GetProperty(orderColumn)).Desc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }

            paged.TotalItems = Count;
            return paged;
        }

        public int deleteSelected(long roleId, long functionId, long featureId)
        {
            string hqlSyntax = "delete RoleFunctionFeatureCr where RoleId = :RoleId and FunctionId = : FunctionId and FeatureId = :FeatureId and Approved = :Approved";
            return session.CreateQuery(hqlSyntax)
                    .SetInt64("RoleId", roleId)
                    .SetInt64("FunctionId", functionId)
                    .SetInt64("FeatureId", featureId)
                    .SetBoolean("Approved", false)
                    .ExecuteUpdate();
        }
        public int deleteAllbyRoleFuncId(long roleFunctId)
        {
            string hqlSyntax = "delete RoleFunctionFeatureCr where RoleFunctionId = :RoleFunctionId and Approved = :Approved";
            return session.CreateQuery(hqlSyntax)
                    .SetInt64("RoleFunctionId", roleFunctId)
                    .SetBoolean("Approved", false)
                    .ExecuteUpdate();
        }

        public RoleFunctionFeatureCr getRoleFuncFeature(long rolId, long functionId, long featureId)
        {
            return transact(() => session.QueryOver<RoleFunctionFeatureCr>()
                                                .Where(f => f.RoleId == rolId && f.FunctionId == functionId && f.FeatureId == featureId && f.Approved == false).SingleOrDefault());
        }

        public IList<RoleFunctionFeatureCr> getRoleFuncFeatures(long RoleId, long FunctionId)
        {
            IEnumerable<RoleFunctionFeatureCr> _roleFunction = this.FindAll(new AdHocSpecification<RoleFunctionFeatureCr>(s => s.RoleId == RoleId && s.FunctionId == FunctionId && s.Approved == false));
            return _roleFunction.ToList();
        }

        public PagedResult<RoleFunctionFeatureCr> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, ApplicationCRFilter filter)
        {
            SimpleExpression status = Restrictions.Eq("ApprovalStatus", filter.ApprovalStatus);
            SimpleExpression apprtype = Restrictions.Eq("ApprovalType", filter.ApprovalType);

            Conjunction conjuction = Restrictions.Conjunction();
            if (!filter.ApprovalStatus.Trim().IsNullOrEmpty()) conjuction.Add(status);
            if (!filter.ApprovalType.Trim().IsNullOrEmpty()) conjuction.Add(apprtype);

            PagedResult<RoleFunctionFeatureCr> paged = new PagedResult<RoleFunctionFeatureCr>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.QueryOver<RoleFunctionFeatureCr>().Where(conjuction)
                                                     .OrderBy(Projections.Property(orderColumn)).Asc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() => session.QueryOver<RoleFunctionFeatureCr>().Where(conjuction)
                                                     .OrderBy(Projections.Property(orderColumn)).Desc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }

            paged.TotalItems = paged.Items.Count();
            return paged;
        }
    }
}
