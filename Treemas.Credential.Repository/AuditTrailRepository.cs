﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using NHibernate.Criterion;
using Treemas.Base.Repository;
using Treemas.Credential.Model;
using Treemas.Credential.Interface;
using Treemas.Base.Utilities.Queries;
using Treemas.Base.Utilities;
using Newtonsoft.Json;

namespace Treemas.Credential.Repository
{
    public class AuditTrailRepository : RepositoryController<AuditTrail>, IAuditTrailRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        private IAuditTrailRepository _auditRepository;
        public AuditTrailRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }

        public void SaveAuditTrail(string functionName, object auditedObject, string actionby, string action)
        {
            AuditTrail audit = new AuditTrail(0L);
            audit.FunctionName = functionName;
            audit.ObjectName = auditedObject.GetType().Name;
            audit.ObjectValue = JsonConvert.SerializeObject(auditedObject);
            audit.Action = action;
            audit.ActionBy = actionby;
            audit.ActionDateTime = DateTime.Now;
            _auditRepository.Add(audit);
        }

        public PagedResult<AuditTrail> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {
            PagedResult<AuditTrail> paged = new PagedResult<AuditTrail>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.QueryOver<AuditTrail>()
                                                    .OrderBy(Projections.Property(orderColumn)).Asc
                                                    .Skip((pageNumber) * itemsPerPage)
                                                    .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() => session.QueryOver<AuditTrail>()
                                                     .OrderBy(Projections.Property(orderColumn)).Desc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }

            paged.TotalItems = Count;
            return paged;
        }

        public PagedResult<AuditTrail> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            ICriterion specification;

            if (searchColumn == "REQUEST_TIME")
            {
                DateTime reqDate;
                DateTime.TryParseExact(searchValue, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out reqDate);
                DateTime tempDate = reqDate.AddDays(1);
                specification = Restrictions.Between(searchColumn, reqDate, tempDate);
            }
            else
            {
                specification = Restrictions.Like(searchColumn, searchValue, MatchMode.Anywhere);
            }

            PagedResult<AuditTrail> paged = new PagedResult<AuditTrail>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.QueryOver<AuditTrail>()
                                 .Where(specification)
                                 .OrderBy(Projections.Property(orderColumn)).Asc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() =>
                          session.QueryOver<AuditTrail>()
                                 .Where(specification)
                                 .OrderBy(Projections.Property(orderColumn)).Desc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }


            paged.TotalItems = transact(() =>
                         session.QueryOver<AuditTrail>()
                                .Where(specification).RowCount());
            return paged;
        }

        public PagedResult<AuditTrail> FindAllPagedDate(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, DateTime searchValue)
        {

            PagedResult<AuditTrail> paged = new PagedResult<AuditTrail>(pageNumber, itemsPerPage);
            if (searchColumn.StringEqualsIgnoreCase("ActionDateTime"))
            {
                if (orderKey.ToLower() == "asc")
                {
                    paged.Items = transact(() =>
                              session.QueryOver<AuditTrail>()
                                     .Where(f => f.ActionDateTime == searchValue)
                                     .OrderBy(Projections.Property(orderColumn)).Asc
                                     .Skip((pageNumber) * itemsPerPage)
                                     .Take(itemsPerPage).List());
                }
                else
                {
                    paged.Items = transact(() =>
                              session.QueryOver<AuditTrail>()
                                     .Where(f => f.ActionDateTime == searchValue)
                                     .OrderBy(Projections.Property(orderColumn)).Desc
                                     .Skip((pageNumber) * itemsPerPage)
                                     .Take(itemsPerPage).List());
                }
                paged.TotalItems = transact(() =>
                        session.QueryOver<AuditTrail>()
                               .Where(f => f.ActionDateTime == searchValue).RowCount());
            }

            return paged;
        }

        public PagedResult<AuditTrail> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, AuditTrailFilter filter)
        {
            //SimpleExpression FunctionName = Restrictions.Like("FunctionName", filter.FunctionName, MatchMode.Anywhere);
            //SimpleExpression ObjectName = Restrictions.Like("ObjectName", filter.ObjectName, MatchMode.Anywhere);
            SimpleExpression ActionName = Restrictions.Like("Action", filter.ActionName, MatchMode.Anywhere);
            SimpleExpression ActionDateStart = Restrictions.Ge("TimeStamp", filter.ActionDateStart);
            SimpleExpression ActionDateEnd = Restrictions.Le("TimeStamp", filter.ActionDateEnd);

            Conjunction conjuction = Restrictions.Conjunction();
            //if (!filter.FunctionName.IsNullOrEmpty()) conjuction.Add(FunctionName);
            //if (!filter.ObjectName.IsNullOrEmpty()) conjuction.Add(ObjectName);
            if (!filter.ActionName.IsNullOrEmpty()) conjuction.Add(ActionName);
            if (!filter.ActionDateStart.IsNull()) conjuction.Add(ActionDateStart);
            if (!filter.ActionDateEnd.IsNull()) conjuction.Add(ActionDateEnd);

            PagedResult<AuditTrail> paged = new PagedResult<AuditTrail>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.QueryOver<AuditTrail>().Where(conjuction)
                                                     .OrderBy(Projections.Property(orderColumn)).Asc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() => session.QueryOver<AuditTrail>().Where(conjuction)
                                                     .OrderBy(Projections.Property(orderColumn)).Desc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }

            paged.TotalItems = transact(() => session.QueryOver<AuditTrail>().Where(conjuction).RowCount());
            return paged;
        }

        public PagedResult<AuditTrail> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue, DateTime start, DateTime end)
        {
            //SimpleExpression specification;

            //if (searchValue.IsNullOrEmpty())
            //{
            //    specification = Restrictions.Like(searchColumn, searchValue, MatchMode.Anywhere);
            //}
            //else
            //{
            //    specification = Restrictions.Like(searchColumn, searchValue, MatchMode.Exact);
            //}

            //Conjunction conjuction = Restrictions.Conjunction();
            //ICriterion timestamp = null;

            //if (start != DateTime.MinValue && end != DateTime.MinValue)
            //{
            //    string sStart = start.ToString("dd/M/yyyy");
            //    string sEnd = end.ToString("dd/M/yyyy");

            //    var dateStart = DateTime.ParseExact(sStart, "dd/M/yyyy", CultureInfo.InvariantCulture);
            //    var dateEnd = DateTime.ParseExact(sEnd, "dd/M/yyyy", CultureInfo.InvariantCulture);

            //    timestamp = Restrictions.Between("TimeStamp", dateStart, dateEnd);
            //}

            //if (searchValue != null) conjuction.Add(specification);
            //if (start != DateTime.MinValue) conjuction.Add(timestamp);

            ICriterion specification;
            Conjunction conjuction = Restrictions.Conjunction();

            if (!searchValue.IsNullOrEmpty())
            {
                specification = Restrictions.Like(searchColumn, searchValue, MatchMode.Anywhere);
                conjuction.Add(specification);
            }
            else
            {
                specification = Restrictions.IsNotEmpty("ID");
                //conjuction.Add(specification);
            }

            if (start != DateTime.MinValue && end != DateTime.MinValue)
            {
                //specification = Restrictions.Between("TimeStamp", start.ToString("MM/dd/yyyy 00:00:01"), end.ToString("MM/dd/yyyy 23:59:59"));
                ICriterion specification2 = Restrictions.Between("TimeStamp", start, end.AddDays(1));
                conjuction.Add(specification2);
            }

            PagedResult<AuditTrail> paged = new PagedResult<AuditTrail>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                //paged.Items = transact(() => session.QueryOver<AuditTrail>().Where(specification)
                //                                     .OrderBy(Projections.Property(orderColumn)).Asc
                //                                     .Skip((pageNumber) * itemsPerPage)
                //                                     .Take(itemsPerPage).List());
                paged.Items = transact(() => session.QueryOver<AuditTrail>().Where(conjuction)
                                                     .OrderBy(Projections.Property(orderColumn)).Asc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() => session.QueryOver<AuditTrail>().Where(conjuction)
                                                     .OrderBy(Projections.Property(orderColumn)).Desc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }

            paged.TotalItems = transact(() => session.QueryOver<AuditTrail>().Where(conjuction).RowCount());
            return paged;
        }

        //public IList<AuditTrail> FindForExport(AuditTrailFilter filter)
        //{
        //    //SimpleExpression FunctionName = Restrictions.Like("FunctionName", filter.FunctionName, MatchMode.Anywhere);
        //    //SimpleExpression ObjectName = Restrictions.Like("ObjectName", filter.ObjectName, MatchMode.Anywhere);
        //    SimpleExpression ActionName = Restrictions.Like("Action", filter.ActionName, MatchMode.Anywhere);
        //    SimpleExpression ActionDateStart = Restrictions.Ge("ActionDateTime", filter.ActionDateStart);
        //    SimpleExpression ActionDateEnd = Restrictions.Le("ActionDateTime", filter.ActionDateEnd);

        //    Conjunction conjuction = Restrictions.Conjunction();
        //    //if (!filter.FunctionName.IsNullOrEmpty()) conjuction.Add(FunctionName);
        //    //if (!filter.ObjectName.IsNullOrEmpty()) conjuction.Add(ObjectName);
        //    if (!filter.ActionName.IsNullOrEmpty()) conjuction.Add(ActionName);
        //    if (!filter.ActionDateStart.IsNull()) conjuction.Add(ActionDateStart);
        //    if (!filter.ActionDateEnd.IsNull()) conjuction.Add(ActionDateEnd);

        //    //return transact(() => session.QueryOver<AuditTrail>().Where(conjuction).List());

        //    IList<AuditTrail> paged = new List<AuditTrail>();
        //    //if (isAllData == true)
        //    //{
        //        paged = transact(() =>
        //                    session.QueryOver<AuditTrail>()
        //                    .Where(conjuction)
        //                           //.OrderBy(Projections.Property(orderColumn)).Desc
        //                           .List());
        //    //}
        //    //else
        //    //{
        //        //if (orderKey.ToLower() == "asc")
        //        //{
        //        //    paged = transact(() =>
        //        //              session.QueryOver<AuditTrail>()
        //        //                     .Where(conjuction)
        //        //                     .OrderBy(Projections.Property(orderColumn)).Asc
        //        //                     .List());
        //        //}
        //        //else
        //        //{
        //        //    paged = transact(() =>
        //        //              session.QueryOver<AuditTrail>()
        //        //                     .Where(conjuction)
        //        //                     .OrderBy(Projections.Property(orderColumn)).Desc
        //        //                     .List());
        //        //}
        //    //}
        //    return paged;
        //}
        public IList<AuditTrail> FindAllPagedForExport(string searchColumn, string searchValue, DateTime ActionDateTimeStart, DateTime ActionDateTimeEnd)
        {
            Conjunction conjuction = Restrictions.Conjunction();
            ICriterion specification;
            if (!searchValue.IsNullOrEmpty())
            {
                specification = Restrictions.Like(searchColumn, searchValue, MatchMode.Anywhere);
                conjuction.Add(specification);
            }
            if (ActionDateTimeStart != DateTime.MinValue && ActionDateTimeEnd != DateTime.MinValue)
            {
                ICriterion specification2 = Restrictions.Between("TimeStamp", ActionDateTimeStart, ActionDateTimeEnd.AddDays(1));
                conjuction.Add(specification2);
            }
            return transact(() => session.QueryOver<AuditTrail>().Where(conjuction).List());

            //return transact(() => session.QueryOver<ParameterAuditTrail>().Where(conjuction).List());
        }
    }
}
