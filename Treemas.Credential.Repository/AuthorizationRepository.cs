﻿using System.Linq;
using LinqSpecs;
using System.Collections.Generic;
using Treemas.Base.Repository;
using Treemas.Credential.Model;
using Treemas.Credential.Interface;
using NHibernate.Criterion;

namespace Treemas.Credential.Repository
{
    public class AuthorizationRepository : RepositoryController<Authorization>, IAuthorizationRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public AuthorizationRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }

        public IList<Authorization> getUserAuthorization(string username)
        {   
            IEnumerable<Authorization> _userAuthorization = this.FindAll(new AdHocSpecification<Authorization>(s => s.Username == username));
            return _userAuthorization.ToList();
        }

        public IList<Authorization> getUserAuthorizationList(long rid)
        {
            IEnumerable<Authorization> _userAuthorization = this.FindAll(new AdHocSpecification<Authorization>(s => s.Id == rid));
            return _userAuthorization.ToList();
        }
        public Authorization getUserAuthorization(long id)
        {
            return transact(() => session.QueryOver<Authorization>()
                                                .Where(f => f.Id == id).SingleOrDefault());
        }

        public Authorization getUserAuthorizationByUsername(string username)
        {
            return transact(() => session.QueryOver<Authorization>()
                                                .Where(f => f.Username == username).SingleOrDefault());
        }

        public string getDistinctUserAuthorizationByUsername(string username)
        {
            string hqlSyntax = "SELECT DISTINCT Role FROM Authorization where Username = :Username";
            var result = session.CreateQuery(hqlSyntax)
                     .SetString("Username", username).List();

            string role = "";

            foreach(var item in result)
            {
                role = role + item + ", ";
            }
            if (role.Length > 0)
            {
                return role.Substring(0,role.Length-2);
            }else
            {
                return "-";
            }
        }

       

        public int delete(string username, string application, string roleId)
        {

            string hqlSyntax = "delete Authorization where Username = :Username and Application = :Application and Role = :Role";
            return session.CreateQuery(hqlSyntax)
                    .SetString("Username", username)
                    .SetString("Application", application)
                    .SetString("Role", roleId)
                    .ExecuteUpdate();
        }
        public int delete(string username, string application)
        {

            string hqlSyntax = "delete Authorization where Username = :Username and Application = :Application";
            return session.CreateQuery(hqlSyntax)
                    .SetString("Username", username)
                    .SetString("Application", application)
                    .ExecuteUpdate();
        }
    }
}
