﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using LinqSpecs;
using NHibernate;
using NHibernate.Linq;
using NHibernate.Criterion;
using Treemas.Base.Repository;
using Treemas.Credential.Model;
using Treemas.Credential.Interface;
using Treemas.Base.Utilities.Queries;
using Treemas.Base.Utilities;

namespace Treemas.Credential.Repository
{
    public class RoleFunctionFeatureRepository : RepositoryController<RoleFunctionFeature>, IRoleFunctionFeatureRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public RoleFunctionFeatureRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }
        public RoleFunctionFeature getRoleFuncFeature(long id)
        {
            return transact(() => session.QueryOver<RoleFunctionFeature>()
                                                .Where(f => f.Id == id).SingleOrDefault());
        }
        public IList<RoleFunctionFeature> getRoleFuncFeaturebyRole(long RoleId)
        {
            IEnumerable<RoleFunctionFeature> _roleFunction = this.FindAll(new AdHocSpecification<RoleFunctionFeature>(s => s.RoleId == RoleId));
            return _roleFunction.ToList();
        }
        public IList<RoleFunctionFeature> getRoleFuncFeatures(long RoleFuncId)
        {
            IEnumerable<RoleFunctionFeature> _roleFunction = this.FindAll(new AdHocSpecification<RoleFunctionFeature>(s => s.RoleFunctionId == RoleFuncId));
            return _roleFunction.ToList();
        }
        
        public bool IsDuplicate(long roleid, long functionid)
        {
            int rowCount = transact(() => session.QueryOver<RoleFunctionFeature>()
                                                .Where(f => f.FunctionId == functionid && f.RoleId == roleid).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public PagedResult<RoleFunctionFeature> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {
            PagedResult<RoleFunctionFeature> paged = new PagedResult<RoleFunctionFeature>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.QueryOver<RoleFunctionFeature>()
                                                     .OrderBy(Projections.Property(orderColumn)).Asc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() => session.QueryOver<RoleFunctionFeature>()
                                                     .OrderBy(x => typeof(RoleFunctionFeature).GetProperty(orderColumn)).Desc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }

            paged.TotalItems = Count;
            return paged;
        }

        public int deleteSelected(long roleId, long functionId, long featureId)
        {
            string hqlSyntax = "delete RoleFunctionFeature where RoleId = :RoleId and FunctionId = :FunctionId and FeatureId = :FeatureId";
            return session.CreateQuery(hqlSyntax)
                    .SetInt64("RoleId", roleId)
                    .SetInt64("FunctionId", functionId)
                    .SetInt64("FeatureId", featureId)
                    .ExecuteUpdate();
        }
        public int deleteAllbyRoleFuncId(long roleFunctId)
        {
            string hqlSyntax = "delete RoleFunctionFeature where RoleFunctionId = :RoleFunctionId";
            return session.CreateQuery(hqlSyntax)
                    .SetInt64("RoleFunctionId", roleFunctId)
                    .ExecuteUpdate();
        }

        public IList<RoleFunctionFeature> getRoleFuncFeatures(long RoleId, long FunctionId)
        {
            IEnumerable<RoleFunctionFeature> _roleFunction = this.FindAll(new AdHocSpecification<RoleFunctionFeature>(s => s.RoleId == RoleId && s.FunctionId == FunctionId));
            return _roleFunction.ToList();
        }

        public RoleFunctionFeature getRoleFuncFeature(long RoleId, long FunctionId, long FeatureId)
        {
            return transact(() => session.QueryOver<RoleFunctionFeature>()
                                                .Where(f => f.RoleId == RoleId && f.FunctionId == FunctionId && f.FeatureId == FeatureId).SingleOrDefault());
        }
    }
}
