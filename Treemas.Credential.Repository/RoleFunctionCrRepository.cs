﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using LinqSpecs;
using NHibernate;
using NHibernate.Linq;
using NHibernate.Criterion;
using Treemas.Base.Repository;
using Treemas.Credential.Model;
using Treemas.Credential.Interface;
using Treemas.Base.Utilities.Queries;
using Treemas.Base.Utilities;

namespace Treemas.Credential.Repository
{
    public class RoleFunctionCrRepository : RepositoryController<RoleFunctionCr>, IRoleFunctionCrRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public RoleFunctionCrRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }
        public RoleFunctionCr getRoleFunction(long id)
        {
            return transact(() => session.QueryOver<RoleFunctionCr>()
                                                .Where(f => f.Id == id && f.Approved == false).SingleOrDefault());
        }
        public RoleFunctionCr getRoleFunction2(long id)
        {
            return transact(() => session.QueryOver<RoleFunctionCr>()
                                                .Where(f => f.Id == id ).SingleOrDefault());
        }

        public IList<RoleFunctionCr> getFunctions(long RoleId)
        {
            IEnumerable<RoleFunctionCr> _roleFunction = this.FindAll(new AdHocSpecification<RoleFunctionCr>(s => s.RoleId == RoleId && s.Approved == false));
            return _roleFunction.ToList();
        }

        public IList<RoleFunctionCr> getRolebyFuncID(long funcId)
        {
            IEnumerable<RoleFunctionCr> _applicationRole = this.FindAll(new AdHocSpecification<RoleFunctionCr>(s => s.FunctionId == funcId));
            return _applicationRole.ToList();
        }
        public bool IsDuplicate(long roleid, long functionid)
        {
            int rowCount = transact(() => session.QueryOver<RoleFunctionCr>()
                                                .Where(f => f.FunctionId == functionid && f.RoleId == roleid).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public PagedResult<RoleFunctionCr> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {
            PagedResult<RoleFunctionCr> paged = new PagedResult<RoleFunctionCr>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.QueryOver<RoleFunctionCr>()
                                                     .OrderBy(Projections.Property(orderColumn)).Asc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() => session.QueryOver<RoleFunctionCr>()
                                                     .OrderBy(x => typeof(RoleFunctionCr).GetProperty(orderColumn)).Desc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }

            paged.TotalItems = Count;
            return paged;
        }

        public int deleteSelected(long roleId, long functionId)
        {
            string hqlSyntax = "delete RoleFunctionCr where RoleId = :RoleId and FunctionId = :FunctionId and APPROVAL_STATUS = 'N'";
            return session.CreateQuery(hqlSyntax)
                    .SetInt64("RoleId", roleId)
                    .SetInt64("FunctionId", functionId)
                    .ExecuteUpdate();
        }
        
        public RoleFunctionCr getRoleFunc(long roleId, long funcId)
        {
            return transact(() => session.QueryOver<RoleFunctionCr>()
                                                .Where(f => f.RoleId == roleId && f.FunctionId == funcId && f.Approved == false).SingleOrDefault());
        }

        public PagedResult<RoleFunctionCr> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, ApplicationCRFilter filter)
        {
            SimpleExpression status = Restrictions.Eq("ApprovalStatus", filter.ApprovalStatus);
            SimpleExpression apprtype = Restrictions.Eq("ApprovalType", filter.ApprovalType);

            Conjunction conjuction = Restrictions.Conjunction();
            if (!filter.ApprovalStatus.Trim().IsNullOrEmpty()) conjuction.Add(status);
            if (!filter.ApprovalType.Trim().IsNullOrEmpty()) conjuction.Add(apprtype);

            PagedResult<RoleFunctionCr> paged = new PagedResult<RoleFunctionCr>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.QueryOver<RoleFunctionCr>().Where(conjuction)
                                                     .OrderBy(Projections.Property(orderColumn)).Asc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() => session.QueryOver<RoleFunctionCr>().Where(conjuction)
                                                     .OrderBy(Projections.Property(orderColumn)).Desc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }

            paged.TotalItems = paged.Items.Count();
            return paged;
        }

    }
}
