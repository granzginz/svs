﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.Credential.Model;

namespace Treemas.Credential.Repository.Mapping
{
    public class RoleMapping : ClassMapping<Role>
    {
        public RoleMapping()
        {
            this.Table("SEC_ROLE");
            Id<long>(x => x.Id,
                map =>
                {
                    map.Column("ID");
                    map.Generator(Generators.Identity);
                });

            Property<string>(x => x.RoleId ,
                map =>
                {
                    map.Column("ROLE_ID");
                    map.Update(false);
                });

            Property<string>(x => x._Application,
                map =>
                {
                    map.Column("APPLICATION");
                    map.Update(false);
                });

            Property<string>(x => x.Name,
                map =>
                {
                    map.Column("NAME");
                });

            Property<string>(x => x.Description,
                map =>
                {
                    map.Column("DESCRIPTION");
                });

            Property<int>(x => x.SessionTimeout ,
                map =>
                {
                    map.Column("SESSION_TIMEOUT");
                });

            Property<int>(x => x.LockTimeout ,
                map =>
                {
                    map.Column("LOCK_TIMEOUT");
                });

            Property<int>(x => x.LockTimeout,
                map =>
                {
                    map.Column("LOCK_TIMEOUT");
                });
            Property<DateTime?>(x => x.ChangedDate,
               map =>
               {
                   map.Column("CHANGED_DATE");
               });

            Property<DateTime?>(x => x.CreatedDate,
                map =>
                {
                    map.Column("CREATED_DATE");
                    map.Update(false);
                });

            Property<string>(x => x.ChangedBy,
                map =>
                {
                    map.Column("CHANGED_BY");
                });

            Property<string>(x => x.CreatedBy,
                map =>
                {
                    map.Column("CREATED_BY");
                    map.Update(false);
                });
        }
    }
}
