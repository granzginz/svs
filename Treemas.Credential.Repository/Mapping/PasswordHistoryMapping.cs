﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.Credential.Model;

namespace Treemas.Credential.Repository.Mapping
{
    public class PasswordHistoryMapping : ClassMapping<PasswordHistory>
    {
        public PasswordHistoryMapping()
        {
            this.Table("SEC_USER_PASSWORD_HISTORY");
            Id<long>(x => x.Id,
                map =>
                {
                    map.Column("ID");
                    map.Generator(Generators.Identity);
                });

            Property<string>(x => x.Username,
                map =>
                {
                    map.Column("USERNAME");
                });

            Property<string>(x => x.Password,
                map =>
                {
                    map.Column("PASSWORD");
                });
         
            Property<DateTime>(x => x.ChangePasswordDate,
               map =>
               {
                   map.Column("CHANGE_PASSWORD_DATE");
               });
        }
    }
}
