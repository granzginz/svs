﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.Credential.Model;

namespace Treemas.Credential.Repository.Mapping
{
    public class AuthorizationMapping : ClassMapping<Authorization>
    {
        public AuthorizationMapping()
        {
            this.Table("SEC_AUTHORIZATION");
            Id<long>(x => x.Id,
                map =>
                {
                    map.Column("ID");
                    map.Generator(Generators.Identity);
                });

            Property<string>(x => x.Username  ,
                map =>
                {
                    map.Column("USERNAME");
                });

            Property<string>(x => x.Application,
                map =>
                {
                    map.Column("APPLICATION");
                });

            Property<string>(x => x.Role,
                map =>
                {
                    map.Column("ROLE");
                });

            Property<string>(x => x.Function,
                map =>
                {
                    map.Column("[FUNCTION]");
                });

            Property<string>(x => x.Feature,
                map =>
                {
                    map.Column("FEATURE");
                });

            Property<string>(x => x.QualifierKey,
                map =>
                {
                    map.Column("QUALIFIER_KEY");
                });

            Property<string>(x => x.QualifierValue,
                map =>
                {
                    map.Column("QUALIFIER_VALUE");
                });

            Property<DateTime?>(x => x.ChangedDate,
               map =>
               {
                   map.Column("CHANGED_DATE");
               });

            Property<DateTime?>(x => x.CreatedDate,
                map =>
                {
                    map.Column("CREATED_DATE");
                    map.Update(false);
                });

            Property<string>(x => x.ChangedBy,
                map =>
                {
                    map.Column("CHANGED_BY");
                });

            Property<string>(x => x.CreatedBy,
                map =>
                {
                    map.Column("CREATED_BY");
                    map.Update(false);
                });
        }
    }
}
