﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.Credential.Model;

namespace Treemas.Credential.Repository.Mapping
{
    public class FunctionFeatureMapping : ClassMapping<FunctionFeature>
    {
        public FunctionFeatureMapping()
        {
            this.Table("SEC_FUNCTION_FEATURE");
            Id<long>(x => x.Id,
                map =>
                {
                    map.Column("ID");
                    map.Generator(Generators.Identity);
                });

            Property<long>(x => x.FunctionId ,
                map =>
                {
                    map.Column("FUNCTION_ID");
                });

            Property<long>(x => x.FeatureId,
                map =>
                {
                    map.Column("FEATURE_ID");
                });

            Property<DateTime?>(x => x.ChangedDate,
                map =>
                {
                    map.Column("CHANGED_DATE");
                });

            Property<DateTime?>(x => x.CreatedDate,
                map =>
                {
                    map.Column("CREATED_DATE");
                    map.Update(false);
                });

            Property<string>(x => x.ChangedBy,
                map =>
                {
                    map.Column("CHANGED_BY");
                });

            Property<string>(x => x.CreatedBy,
                map =>
                {
                    map.Column("CREATED_BY");
                    map.Update(false);
                });

        }
    }
}
