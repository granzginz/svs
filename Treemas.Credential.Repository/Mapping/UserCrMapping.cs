﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.Credential.Model;

namespace Treemas.Credential.Repository.Mapping
{
    public class UserCrMapping : ClassMapping<UserCr>
    {
        public UserCrMapping()
        {
            this.Table("SEC_USER_CR");

            Id<long>(x => x.Id, map => { map.Column("ID"); map.Generator(Generators.Identity); });
            Property<string>(x => x.Username, map => { map.Column("USERNAME"); map.Update(false); });
            Property<string>(x => x.FullName, map => { map.Column("FULL_NAME"); });
            Property<string>(x => x.FullNameNew, map => { map.Column("FULL_NAME_NEW"); });
            Property<string>(x => x.Password, map => { map.Column("PASSWORD"); });
            Property<string>(x => x.RegNo, map => { map.Column("REG_NO"); });
            Property<bool>(x => x.IsActive, map => { map.Column("IS_ACTIVE"); map.Update(false); });
            Property<int>(x => x.SessionTimeout, map => { map.Column("SESSION_TIMEOUT"); });
            Property<int>(x => x.LockTimeout, map => { map.Column("LOCK_TIMEOUT"); });
            Property<int>(x => x.MaximumConcurrentLogin, map => { map.Column("MAX_CONCURRENT_LOGIN"); });
            Property<bool>(x => x.InActiveDirectory, map => { map.Column("IN_ACTIVE_DIRECTORY"); });
            Property<bool>(x => x.InActiveDirectoryNew, map => { map.Column("IN_ACTIVE_DIRECTORY_NEW"); });
            Property<DateTime>(x => x.PasswordExpirationDate, map => { map.Column("PASSWORD_EXPIRATION_DATE"); map.Update(false); });
            Property<DateTime>(x => x.AccountValidityDate, map => { map.Column("ACCOUNT_VALIDITY_DATE"); map.Update(false); });
            Property<int>(x => x.LoginAttempt, map => { map.Column("LOGIN_ATTEMPT"); });
            Property<DateTime?>(x => x.LastLogin, map => { map.Column("LAST_LOGIN"); map.Update(false); map.Insert(false); });
            Property<bool>(x => x.IsPrinted, map => { map.Column("IS_PRINTED"); map.Update(false); });
            Property<bool>(x => x.Approved, map => { map.Column("APPROVED"); map.Update(false); });
            Property<bool>(x => x.UpdateMenu, map => { map.Column("UPDATE_MENU"); });
            Property<DateTime?>(x => x.ChangedDate, map => { map.Column("CHANGED_DATE"); });
            Property<DateTime?>(x => x.CreatedDate, map => { map.Column("CREATED_DATE"); map.Update(false); });
            Property<string>(x => x.ChangedBy, map => { map.Column("CHANGED_BY"); });
            Property<string>(x => x.CreatedBy, map => { map.Column("CREATED_BY"); map.Update(false); });
            Property<string>(x => x.ApprovedBy, map => { map.Column("APPROVED_BY"); });
            Property<DateTime?>(x => x.ApprovedDate, map => { map.Column("APPROVED_DATE"); });
            Property<string>(x => x.ApprovalType, map => { map.Column("APPROVAL_TYPE"); });
            Property<string>(x => x.ApprovalStatus, map => { map.Column("APPROVAL_STATUS"); });
            Property<string>(x => x.AuthorizedApplicationIdsOld, map => { map.Column("AUTHORIZED_APP_ID_OLD"); });
            Property<string>(x => x.AuthorizedApplicationIdsNew, map => { map.Column("AUTHORIZED_APP_ID_NEW"); });
            Property<string>(x => x.AuthorizedRoleIdsOld, map => { map.Column("AUTHORIZED_ROLE_ID_OLD"); });
            Property<string>(x => x.AuthorizedRoleIdsNew, map => { map.Column("AUTHORIZED_ROLE_ID_NEW"); });
            Property<string>(x => x.BranchCode, map => { map.Column("BRANCH_CODE"); });
            Property<string>(x => x.BranchCodeNew, map => { map.Column("BRANCH_CODE_NEW"); });
        }
    }
}
