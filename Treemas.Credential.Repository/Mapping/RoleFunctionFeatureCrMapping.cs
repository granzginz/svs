﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.Credential.Model;

namespace Treemas.Credential.Repository.Mapping
{
    public class RoleFunctionFeatureCrMapping : ClassMapping<RoleFunctionFeatureCr>
    {
        public RoleFunctionFeatureCrMapping()
        {
            this.Table("SEC_ROLE_FUNCTION_FEATURE_CR");
            Id<long>(x => x.Id,
                map =>
                {
                    map.Column("ID");
                    map.Generator(Generators.Identity);
                });

            Property<long>(x => x.RoleFunctionId,
                map =>
                {
                    map.Column("ROLE_FUNCTION_ID");
                });

            Property<long>(x => x.RoleId,
                map =>
                {
                    map.Column("ROLE_ID");
                });

            Property<long>(x => x.FunctionId,
                map =>
                {
                    map.Column("FUNCTION_ID");
                });

            Property<long>(x => x.FeatureId,
                map =>
                {
                    map.Column("FEATURE_ID");
                });

            Property<DateTime?>(x => x.ChangedDate,
                map =>
                {
                    map.Column("CHANGED_DATE");
                });

            Property<DateTime?>(x => x.CreatedDate,
                map =>
                {
                    map.Column("CREATED_DATE");
                    map.Update(false);
                });

            Property<string>(x => x.ChangedBy,
                map =>
                {
                    map.Column("CHANGED_BY");
                });

            Property<string>(x => x.CreatedBy,
                map =>
                {
                    map.Column("CREATED_BY");
                    map.Update(false);
                });
            Property<bool>(x => x.Approved, map => { map.Column("APPROVED"); });
            Property<string>(x => x.ApprovedBy, map => { map.Column("APPROVED_BY"); });
            Property<DateTime?>(x => x.ApprovedDate, map => { map.Column("APPROVED_DATE"); });
            Property<string>(x => x.ApprovalType, map => { map.Column("APPROVAL_TYPE"); });
            Property<string>(x => x.ApprovalStatus, map => { map.Column("APPROVAL_STATUS"); });
        }
    }
}
