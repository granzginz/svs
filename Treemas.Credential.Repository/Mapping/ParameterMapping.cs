﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.Credential.Model;


namespace Treemas.Credential.Repository.Mapping
{
    public class ParameterMapping : ClassMapping<Parameter>
    {
        public ParameterMapping()
        {
            this.Table("PARAMETER");
            Id<string>(x => x.Id,
               map => { map.Column("PARAM_ID"); });

            Property<string>(x => x.ParamId,
               map => { map.Column("PARAM_ID"); map.Insert(false); map.Update(false); });

            Property<string>(x => x.UserId,
               map => { map.Column("USER_ID");  });

            Property<string>(x => x.Password,
                map => { map.Column("PASSWORD"); });

            Property<string>(x => x.StaggingUrl,
                map => { map.Column("STAGGING_URL"); });

            Property<int>(x => x.LoginAttempt,
                map => { map.Column("LOGIN_ATTEMPT"); });

            Property<int>(x => x.SessionTimeout,
                map => { map.Column("SESSION_TIMEOUT"); });

            Property<DateTime?>(x => x.ChangedDate,
                map => { map.Column("CHANGE_DATE"); });
            
            Property<string>(x => x.ChangedBy,
                map => { map.Column("CHANGED_BY"); });

            Property<string>(x => x.PasswordExp,
                map => { map.Column("PASSWORD_EXPIRED_DATE"); });

            Property<string>(x => x.DomainServer,
                map => { map.Column("DOMAIN_SERVER"); });


            Property<int>(x => x.SizeKTP,
                map => { map.Column("SIZE_KTP"); });

            Property<int>(x => x.SizeSignature,
                map => { map.Column("SIZE_SIGNATURE"); });

            Property<int>(x => x.SizeDocument,
                map => { map.Column("SIZE_DOCUMENT"); });

            Property<int?>(x => x.KeepHistory,
                map => { map.Column("KEEP_HISTORY"); });

            Property<int>(x => x.KeepAuditTrail,
                map => { map.Column("KEEP_AUDIT_TRAIL"); });

            Property<int>(x => x.KeepPendingAppr,
                map => { map.Column("KEEP_PENDING_APPROVAL"); });

            Property<decimal>(x => x.MaxDiffAccount,
                map => { map.Column("MAX_DIFF_ACCOUNT"); });
        }
    }
}
