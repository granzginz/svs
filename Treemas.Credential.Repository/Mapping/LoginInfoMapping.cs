﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.Credential.Model;

namespace Treemas.Credential.Repository.Mapping
{
    public class LoginInfoMapping : ClassMapping<SignonLoginInfo>
    {
        public LoginInfoMapping()
        {
            this.Table("SEC_LOGIN_INFO");

            Id<long>(
                x => x.Id,
                map =>
                {
                    map.Column("ID");
                    map.Generator(Generators.Identity);
                });

            Property<string>(x => x.SessionId, map => { map.Column("SESSION_ID"); });
            Property<string>(x => x.Browser, map => { map.Column("BROWSER"); });
            Property<string>(x => x.BrowserVersion, map => { map.Column("BROWSER_VERSION"); });
            Property<string>(x => x.HostIP, map => { map.Column("HOST_IP"); });
            Property<string>(x => x.Hostname, map => { map.Column("HOST_NAME"); });
            Property<bool>(x => x.IsMobile, map => { map.Column("IS_MOBILE"); });
            Property<DateTime>(x => x.LastActive, map => { map.Column("LAST_ACTIVE"); });
            Property<bool>(x => x.Locked, map => { map.Column("LOCKED"); });
            Property<DateTime?>(x => x.LockTime, map => { map.Column("LOCK_TIME"); map.Insert(false); });
            Property<int>(x => x.LockTimeout, map => { map.Column("LOCK_TIMEOUT"); });
            Property<DateTime>(x => x.LoginTime, map => { map.Column("LOGIN_TIME"); });
            Property<int>(x => x.MaximumLogin, map => { map.Column("MAXIMUM_LOGIN"); });
            Property<int>(x => x.SessionTimeout, map => { map.Column("SESSION_TIMEOUT"); });
            Property<DateTime?>(x => x.UnlockTime, map => { map.Column("UNLOCK_TIME"); map.Insert(false); });
            Property<string>(x => x.Username, map => { map.Column("USERNAME"); });


        }
    }
}
