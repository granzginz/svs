﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.Credential.Model;

namespace Treemas.Credential.Repository.Mapping
{
    public class RoleDocTypeMapping : ClassMapping<RoleDocType>
    {
        public RoleDocTypeMapping()
        {
            this.Table("SEC_ROLE_DOCUMENT_TYPE");
            Id<long>(x => x.Id,
                map =>
                {
                    map.Column("ID");
                    map.Generator(Generators.Identity);
                });

            Property<long>(x => x.RoleId,
                map =>
                {
                    map.Column("ROLE_ID");
                });

            Property<long>(x => x.DocumentTypeId,
                map =>
                {
                    map.Column("DOC_TYPE_ID");
                });
            
            Property<DateTime?>(x => x.ChangedDate,
                map =>
                {
                    map.Column("CHANGED_DATE");
                });

            Property<DateTime?>(x => x.CreatedDate,
                map =>
                {
                    map.Column("CREATED_DATE");
                    map.Update(false);
                });

            Property<string>(x => x.ChangedBy,
                map =>
                {
                    map.Column("CHANGED_BY");
                });

            Property<string>(x => x.CreatedBy,
                map =>
                {
                    map.Column("CREATED_BY");
                    map.Update(false);
                });

        }
    }
}
