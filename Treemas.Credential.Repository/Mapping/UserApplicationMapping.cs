﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.Credential.Model;

namespace Treemas.Credential.Repository.Mapping
{
    public class UserApplicationMapping : ClassMapping<UserApplication>
    {
        public UserApplicationMapping()
        {
            this.Table("SEC_USER_APPLICATION");
            Id<long>(x => x.Id,
                map =>
                {
                    map.Column("ID");
                    map.Generator(Generators.Identity);
                });

            Property<string>(x => x.Username,
                map =>
                {
                    map.Column("USERNAME");
                });

            Property<string>(x => x.Application,
                map =>
                {
                    map.Column("APPLICATION");
                });

            Property<bool>(x => x.IsDefault ,
                map =>
                {
                    map.Column("IS_DEFAULT");
                });

            Property<DateTime?>(x => x.ChangedDate,
               map =>
               {
                   map.Column("CHANGED_DATE");
               });

            Property<DateTime?>(x => x.CreatedDate,
                map =>
                {
                    map.Column("CREATED_DATE");
                    map.Update(false);
                });

            Property<string>(x => x.ChangedBy,
                map =>
                {
                    map.Column("CHANGED_BY");
                });

            Property<string>(x => x.CreatedBy,
                map =>
                {
                    map.Column("CREATED_BY");
                    map.Update(false);
                });
        }
    }
}
