﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;
using LinqSpecs;
using NHibernate.Linq;
using Treemas.Base.Repository;
using Treemas.Credential.Model;
using Treemas.Credential.Interface;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using NHibernate.Criterion;

namespace Treemas.Credential.Repository
{
    public class RoleCrRepository : RepositoryController<RoleCr>, IRoleCrRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public RoleCrRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }

        public void deleteExistingRequest(string roleid)
        {
            string hqlDelete = "delete SEC_ROLE_CR where ROLE_ID = :ROLE_ID and APPROVAL_TYPE = 'U' and APPROVAL_STATUS = 'N' ";
            int deletedEntities = session.CreateQuery(hqlDelete)
                    .SetString("ROLE_ID", roleid)
                    .ExecuteUpdate();
        }

        public PagedResult<RoleCr> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, ApplicationCRFilter filter)
        {
            SimpleExpression status = Restrictions.Eq("ApprovalStatus", filter.ApprovalStatus);
            SimpleExpression apprtype = Restrictions.Eq("ApprovalType", filter.ApprovalType);

            Conjunction conjuction = Restrictions.Conjunction();
            if (!filter.ApprovalStatus.Trim().IsNullOrEmpty()) conjuction.Add(status);
            if (!filter.ApprovalType.Trim().IsNullOrEmpty()) conjuction.Add(apprtype);

            PagedResult<RoleCr> paged = new PagedResult<RoleCr>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.QueryOver<RoleCr>().Where(conjuction)
                                                     .OrderBy(Projections.Property(orderColumn)).Asc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() => session.QueryOver<RoleCr>().Where(conjuction)
                                                     .OrderBy(Projections.Property(orderColumn)).Desc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }

            paged.TotalItems = Count;
            return paged;
        }

        public IList<RoleCr> getApplicationRole(string application)
        {
            throw new NotImplementedException();
        }

        public RoleCr getExistingRequest(string roleid)
        {
            return transact(() => session.QueryOver<RoleCr>()
                                                            .Where(f => f.RoleId == roleid && f.ApprovalType == "U" && f.ApprovalStatus == "N").SingleOrDefault());
        }

        public RoleCr getRole(string id)
        {
            return transact(() => session.QueryOver<RoleCr>()
                                                .Where(f => f.RoleId == id).SingleOrDefault());
        }

        public RoleCr getRole(long id)
        {
            return transact(() => session.QueryOver<RoleCr>()
                                                .Where(f => f.Id == id).SingleOrDefault());
        }

        public bool IsDuplicate(string application, string roleid)
        {
            int rowCount = transact(() => session.QueryOver<RoleCr>()
                                                .Where(f => f.RoleId == roleid && f._Application == application).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
    }
}
