﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using LinqSpecs;
using NHibernate;
using NHibernate.Linq;
using NHibernate.Criterion;
using Treemas.Base.Repository;
using Treemas.Credential.Model;
using Treemas.Credential.Interface;
using Treemas.Base.Utilities.Queries;
using Treemas.Base.Utilities;

namespace Treemas.Credential.Repository
{
    public class RoleDocTypeCrRepository : RepositoryController<RoleDocTypeCr>, IRoleDocTypeCrRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public RoleDocTypeCrRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }
        public RoleDocTypeCr getRoleDocType(long id)
        {
            return transact(() => session.QueryOver<RoleDocTypeCr>()
                                                .Where(f => f.Id == id ).SingleOrDefault());
        }

        public IList<RoleDocTypeCr> getDocTypes(long RoleId)
        {
            IEnumerable<RoleDocTypeCr> _roleDocType = this.FindAll(new AdHocSpecification<RoleDocTypeCr>(s => s.RoleId == RoleId));
            return _roleDocType.ToList();
        }

        public IList<RoleDocTypeCr> getDocTypes2(long RoleId)
        {
            IEnumerable<RoleDocTypeCr> _roleDocType = this.FindAll(new AdHocSpecification<RoleDocTypeCr>(s => s.RoleId == RoleId && s.ApprovalStatus == "N"));
            return _roleDocType.ToList();
        }

        public IList<RoleDocTypeCr> getDocTypes(long RoleId, string appStat)
        {
            IEnumerable<RoleDocTypeCr> _roleDocType = this.FindAll(new AdHocSpecification<RoleDocTypeCr>(s => s.RoleId == RoleId && s.ApprovalStatus == appStat));
            return _roleDocType.ToList();
        }

        public IList<RoleDocTypeCr> getRolebyFuncID(long funcId)
        {
            IEnumerable<RoleDocTypeCr> _applicationRole = this.FindAll(new AdHocSpecification<RoleDocTypeCr>(s => s.DocumentTypeId == funcId));
            return _applicationRole.ToList();
        }
        public bool IsDuplicate(long roleid, long DocTypeid)
        {
            int rowCount = transact(() => session.QueryOver<RoleDocTypeCr>()
                                                .Where(f => f.DocumentTypeId == DocTypeid && f.RoleId == roleid).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public PagedResult<RoleDocTypeCr> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {
            PagedResult<RoleDocTypeCr> paged = new PagedResult<RoleDocTypeCr>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.QueryOver<RoleDocTypeCr>()
                                                     .OrderBy(Projections.Property(orderColumn)).Asc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() => session.QueryOver<RoleDocTypeCr>()
                                                     .OrderBy(x => typeof(RoleDocTypeCr).GetProperty(orderColumn)).Desc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }

            paged.TotalItems = Count;
            return paged;
        }

        public int deleteSelected(long roleId, long DocTypeId)
        {
            string hqlSyntax = "delete RoleDocTypeCr where RoleId = :RoleId and DocumentTypeId = :DocumentTypeId and APPROVAL_STATUS = 'N'";
            return session.CreateQuery(hqlSyntax)
                    .SetInt64("RoleId", roleId)
                    .SetInt64("DocumentTypeId", DocTypeId)
                    .ExecuteUpdate();
        }
        
        public RoleDocTypeCr getRoleDocType(long roleId, long DocTypeId)
        {
            return transact(() => session.QueryOver<RoleDocTypeCr>()
                                                .Where(f => f.RoleId == roleId && f.DocumentTypeId == DocTypeId && f.Approved == false).SingleOrDefault());
        }

        public PagedResult<RoleDocTypeCr> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, ApplicationCRFilter filter)
        {
            SimpleExpression status = Restrictions.Eq("ApprovalStatus", filter.ApprovalStatus);
            SimpleExpression apprtype = Restrictions.Eq("ApprovalType", filter.ApprovalType);

            Conjunction conjuction = Restrictions.Conjunction();
            if (!filter.ApprovalStatus.Trim().IsNullOrEmpty()) conjuction.Add(status);
            if (!filter.ApprovalType.Trim().IsNullOrEmpty()) conjuction.Add(apprtype);

            PagedResult<RoleDocTypeCr> paged = new PagedResult<RoleDocTypeCr>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.QueryOver<RoleDocTypeCr>().Where(conjuction)
                                                     .OrderBy(Projections.Property(orderColumn)).Asc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() => session.QueryOver<RoleDocTypeCr>().Where(conjuction)
                                                     .OrderBy(Projections.Property(orderColumn)).Desc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }

            paged.TotalItems = paged.Items.Count();
            return paged;
        }

    }
}
