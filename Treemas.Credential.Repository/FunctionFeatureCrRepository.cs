﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using LinqSpecs;
using NHibernate.Linq;
using NHibernate.Criterion;
using Treemas.Base.Repository;
using Treemas.Credential.Model;
using Treemas.Credential.Interface;
using Treemas.Base.Utilities.Queries;
using Treemas.Base.Utilities;

namespace Treemas.Credential.Repository
{
    public class FunctionFeatureCrRepository : RepositoryController<FunctionFeatureCr>, IFunctionFeatureCrRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public FunctionFeatureCrRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }

        public FunctionFeatureCr getFunctionFeature(long id)
        {
            return transact(() => session.QueryOver<FunctionFeatureCr>()
                                                .Where(f => f.Id == id).SingleOrDefault());
        }
        
        public IList<FunctionFeatureCr> getFunctionFeatures(long functionid)
        {
            return transact(() => session.QueryOver<FunctionFeatureCr>()
                                                .Where(f => f.FunctionId == functionid && f.Approved == false).List());
        }

        public int deleteSelected(long functionId, long featureId)
        {
            string hqlSyntax = "delete FunctionFeatureCr where FunctionId = :FunctionId and FeatureId = :FeatureId and Approved = false";
            return session.CreateQuery(hqlSyntax)
                    .SetInt64("FunctionId", functionId)
                    .SetInt64("FeatureId", featureId)
                    .ExecuteUpdate();
        }


        public PagedResult<FunctionFeatureCr> FindAllPagedbyFunction(long functionId, int pageNumber, string orderColumn, int itemsPerPage, string orderKey)
        {

            PagedResult<FunctionFeatureCr> paged = new PagedResult<FunctionFeatureCr>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.QueryOver<FunctionFeatureCr>()
                                 .Where(x => x.FunctionId==functionId)
                                 .OrderBy(Projections.Property(orderColumn)).Asc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() =>
                          session.QueryOver<FunctionFeatureCr>()
                                 .Where(x => x.FunctionId == functionId)
                                 .OrderBy(Projections.Property(orderColumn)).Desc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }
            paged.TotalItems = transact(() =>
                          session.QueryOver<FunctionFeatureCr>()
                                 .Where(x => x.FunctionId == functionId)
                                .RowCount());

            return paged;
        }

        public FunctionFeatureCr getFunctionFeature(long functionid, long featureid)
        {
            return transact(() => session.QueryOver<FunctionFeatureCr>()
            .Where(f => f.FunctionId == functionid && f.FeatureId == featureid && f.Approved == false)
            .SingleOrDefault());
        }

        public PagedResult<FunctionFeatureCr> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, ApplicationCRFilter filter)
        {
            SimpleExpression status = Restrictions.Eq("ApprovalStatus", filter.ApprovalStatus);
            SimpleExpression apprtype = Restrictions.Eq("ApprovalType", filter.ApprovalType);

            Conjunction conjuction = Restrictions.Conjunction();
            if (!filter.ApprovalStatus.Trim().IsNullOrEmpty()) conjuction.Add(status);
            if (!filter.ApprovalType.Trim().IsNullOrEmpty()) conjuction.Add(apprtype);

            PagedResult<FunctionFeatureCr> paged = new PagedResult<FunctionFeatureCr>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.QueryOver<FunctionFeatureCr>().Where(conjuction)
                                                     .OrderBy(Projections.Property(orderColumn)).Asc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() => session.QueryOver<FunctionFeatureCr>().Where(conjuction)
                                                     .OrderBy(Projections.Property(orderColumn)).Desc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }

            paged.TotalItems = paged.Items.Count();
            return paged;
        }
    }
}
