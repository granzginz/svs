﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using NHibernate.Criterion;
using Treemas.Base.Repository;
using Treemas.Credential.Model;
using Treemas.Credential.Interface;
using Treemas.Base.Utilities.Queries;
using Treemas.Base.Utilities;

namespace Treemas.Credential.Repository
{
    public class FeatureCrRepository : RepositoryController<AuthorizationFeatureCr>, IFeatureCrRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public FeatureCrRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }

        public void deleteExistingRequest(string featureId)
        {
            string hqlDelete = "delete SEC_FEATURE_CR where FEATURE_ID = :FEATURE_ID and APPROVAL_TYPE = 'U' and APPROVAL_STATUS = 'N' ";
            int deletedEntities = session.CreateQuery(hqlDelete)
                    .SetString("FEATURE_ID", featureId)
                    .ExecuteUpdate();
        }

        public PagedResult<AuthorizationFeatureCr> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, ApplicationCRFilter filter)
        {
            SimpleExpression status = Restrictions.Eq("ApprovalStatus", filter.ApprovalStatus);
            SimpleExpression apprtype = Restrictions.Eq("ApprovalType", filter.ApprovalType);

            Conjunction conjuction = Restrictions.Conjunction();
            if (!filter.ApprovalStatus.Trim().IsNullOrEmpty()) conjuction.Add(status);
            if (!filter.ApprovalType.Trim().IsNullOrEmpty()) conjuction.Add(apprtype);

            PagedResult<AuthorizationFeatureCr> paged = new PagedResult<AuthorizationFeatureCr>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.QueryOver<AuthorizationFeatureCr>().Where(conjuction)
                                                     .OrderBy(Projections.Property(orderColumn)).Asc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() => session.QueryOver<AuthorizationFeatureCr>().Where(conjuction)
                                                     .OrderBy(Projections.Property(orderColumn)).Desc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }

            paged.TotalItems = Count;
            return paged;
        }

        public AuthorizationFeatureCr getExistingRequest(string featureId)
        {
            return transact(() => session.QueryOver<AuthorizationFeatureCr>()
                                                .Where(f => f.FeatureId == featureId && f.ApprovalType == "U" && f.ApprovalStatus == "N").SingleOrDefault());
        }

        public AuthorizationFeatureCr getFeature(long id)
        {
            return transact(() => session.QueryOver<AuthorizationFeatureCr>()
                                               .Where(f => f.Id == id).SingleOrDefault());
        }

        public IList<AuthorizationFeatureCr> getFeatures(IList<string> ApplicationIds)
        {
            return transact(() =>
                   session.QueryOver<AuthorizationFeatureCr>()
                          .WhereRestrictionOn(val => val.Application)
                          .IsIn(ApplicationIds.ToArray()).List());
        }

        public bool IsDuplicate(string application, string featureid)
        {
            int rowCount = transact(() => session.QueryOver<AuthorizationFeatureCr>()
                                                .Where(f => f.FeatureId == featureid && f.Application == application).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }

        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }

    }
}
