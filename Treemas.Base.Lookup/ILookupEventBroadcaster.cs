﻿using System;
using System.Collections.Generic;

namespace Treemas.Base.Lookup
{
    public interface ILookupEventBroadcaster
    {
        void AddEventListener(Action<LookupEvent> action);
        void AddEventListener(ILookupEventListener listener);
        IList<Action<LookupEvent>> GetActionListener();
        IList<ILookupEventListener> GetListeners();
        void RemoveEventListener(Action<LookupEvent> action);
        void RemoveEventListener(ILookupEventListener listener);
    }
}

