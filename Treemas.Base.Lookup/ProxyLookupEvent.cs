﻿using System;

namespace Treemas.Base.Lookup
{
    public class ProxyLookupEvent
    {
        public IProxyLookup Broadcaster { get; set; }

        public ILookup Lookup { get; set; }

        public ProxyLookupEventType Type { get; set; }
    }
}

