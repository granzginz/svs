﻿using System;
using System.Collections.Generic;

namespace Treemas.Base.Lookup
{
    [Serializable]
    public class SimpleLookup : SimpleLookupEventBroadcaster, ILookup, ILookupEventBroadcaster
    {
        private List<object> lookupBag;
        private string name;

        public SimpleLookup() : this("SimpleLookup")
        {
        }

        public SimpleLookup(string name)
        {
            this.name = name;
            this.lookupBag = new List<object>();
        }

        public void Add(object obj)
        {
            if (obj != null)
            {
                this.lookupBag.Add(obj);
                LookupEvent evt = new LookupEvent {
                    Broadcaster = this,
                    Type = LookupEventType.Instance_Added,
                    Instance = obj
                };
                base.BroadcastEvent(evt);
            }
        }

        public void Clear()
        {
            this.lookupBag.Clear();
        }

        public T Get<T>()
        {
            if (this.lookupBag.Count != 0)
            {
                Type type = typeof(T);
                foreach (object obj2 in this.lookupBag)
                {
                    if (type.IsInstanceOfType(obj2))
                    {
                        return (T) obj2;
                    }
                }
            }
            return default(T);
        }

        public IList<object> GetAll() => 
            this.lookupBag;

        public IList<T> GetAll<T>()
        {
            if (this.lookupBag.Count != 0)
            {
                Type type = typeof(T);
                List<T> list = new List<T>();
                foreach (object obj2 in this.lookupBag)
                {
                    if (type.IsInstanceOfType(obj2))
                    {
                        list.Add((T) obj2);
                    }
                }
                if (list.Count > 0)
                {
                    return list;
                }
            }
            return null;
        }

        public string GetName() => 
            this.name;

        public void Remove<T>()
        {
            IList<T> all = this.GetAll<T>();
            if (all != null)
            {
                foreach (T local in all)
                {
                    this.lookupBag.Remove(local);
                    LookupEvent evt = new LookupEvent {
                        Broadcaster = this,
                        Type = LookupEventType.Instance_Removed,
                        Instance = local
                    };
                    base.BroadcastEvent(evt);
                }
            }
        }

        public void Remove(object obj)
        {
            if (obj != null)
            {
                this.lookupBag.Remove(obj);
                LookupEvent evt = new LookupEvent {
                    Broadcaster = this,
                    Type = LookupEventType.Instance_Removed,
                    Instance = obj
                };
                base.BroadcastEvent(evt);
            }
        }

        public void Remove<T>(Predicate<T> matchedCondition)
        {
            IList<T> all = this.GetAll<T>();
            if ((all != null) && (all.Count > 0))
            {
                IList<T> list2 = new List<T>();
                foreach (T local in all)
                {
                    if (matchedCondition(local))
                    {
                        list2.Add(local);
                    }
                }
                foreach (T local2 in list2)
                {
                    this.Remove(local2);
                }
            }
        }
    }
}

