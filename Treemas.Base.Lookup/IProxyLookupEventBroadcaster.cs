﻿using System;

namespace Treemas.Base.Lookup
{
    public interface IProxyLookupEventBroadcaster
    {
        void AddEventListener(Action<ProxyLookupEvent> action);
        void AddEventListener(IProxyLookupEventListener listener);
        void RemoveEventListener(Action<ProxyLookupEvent> action);
        void RemoveEventListener(IProxyLookupEventListener listener);
    }
}

