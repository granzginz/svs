﻿using System;

namespace Treemas.Base.Lookup
{
    public class LookupEvent
    {
        public ILookup Broadcaster { get; set; }

        public object Instance { get; set; }

        public LookupEventType Type { get; set; }
    }
}

