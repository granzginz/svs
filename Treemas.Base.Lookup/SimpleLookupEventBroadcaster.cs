﻿using System;
using System.Collections.Generic;

namespace Treemas.Base.Lookup
{
    [Serializable]
    public class SimpleLookupEventBroadcaster : ILookupEventBroadcaster
    {
        private List<Action<LookupEvent>> actions = new List<Action<LookupEvent>>();
        private List<ILookupEventListener> listeners = new List<ILookupEventListener>();

        public void AddEventListener(Action<LookupEvent> action)
        {
            if ((action != null) && !this.actions.Contains(action))
            {
                this.actions.Add(action);
            }
        }

        public void AddEventListener(ILookupEventListener listener)
        {
            if (!this.listeners.Contains(listener))
            {
                this.listeners.Add(listener);
            }
        }

        protected void BroadcastEvent(LookupEvent evt)
        {
            if (this.listeners.Count > 0)
            {
                IList<ILookupEventListener> list;
                IList<Action<LookupEvent>> list2;
                lock (this.listeners)
                {
                    list = this.listeners.AsReadOnly();
                }
                if (list != null)
                {
                    foreach (ILookupEventListener listener in list)
                    {
                        listener.LookupChanged(evt);
                    }
                }
                lock (this.actions)
                {
                    list2 = this.actions.AsReadOnly();
                }
                if (list2 != null)
                {
                    foreach (Action<LookupEvent> action in list2)
                    {
                        action(evt);
                    }
                }
            }
        }

        public IList<Action<LookupEvent>> GetActionListener() => 
            this.actions.AsReadOnly();

        public IList<ILookupEventListener> GetListeners() => 
            this.listeners.AsReadOnly();

        public void RemoveEventListener(Action<LookupEvent> action)
        {
            if (action != null)
            {
                this.actions.Remove(action);
            }
        }

        public void RemoveEventListener(ILookupEventListener listener)
        {
            this.listeners.Remove(listener);
        }
    }
}

