﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Treemas.Credential.Model;
using UOB.SVS.Interface;
using UOB.SVS.Model;

namespace UOB.SVS.Service
{
    public class SignatureHReqService : ISignatureHReqService
    {
        private ISignatureHReqRepository _signHReqRepo;
        private ISignatureHRepository _signHRepo;
        private ISignatureHHistoryRepository _signHHistoryRepo;

        public SignatureHReqService(ISignatureHReqRepository signHReqRepo, ISignatureHRepository signHRepo,
            ISignatureHHistoryRepository signHHistoryRepo)
        {
            this._signHReqRepo = signHReqRepo;
            this._signHRepo = signHRepo;
            this._signHHistoryRepo = signHHistoryRepo;
        }

        public string SaveSignatureReq(SignatureHRequest signHReq)
        {
            string returnMessage = "";

            try
            {
                using (TransactionScope scope = new TransactionScope())
                {

                    _signHReqRepo.Add(signHReq);
                    if (signHReq.Documents != null)
                    {
                        foreach (SignatureDDocReq item in signHReq.Documents)
                        {
                            _signHReqRepo.AddDocReqD(item);
                        }
                    }
                    
                    if (signHReq.IdentityCards != null)
                    {
                        foreach (SignatureDKTPReq item in signHReq.IdentityCards)
                        {
                            _signHReqRepo.AddKTPReqD(item);
                        }
                    }
                    if (signHReq.Signatures != null)
                    {
                        foreach (SignatureDSignReq item in signHReq.Signatures)
                        {
                            _signHReqRepo.AddSignReqD(item);
                        }
                    }

                    scope.Complete();
                }
            }
            catch (TransactionAbortedException ex)
            {
                returnMessage = ex.Message;
            }
            catch (ApplicationException ex)
            {
                returnMessage = ex.Message;
            }
            return returnMessage;
        }

        public string UpdateSignatureReq(SignatureHRequest signHReq)
        {
            string returnMessage = "";

            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    _signHReqRepo.Save(signHReq);
                    //_signHReqRepo.DeleteKTPReqD(signHReq.AccountNo);
                    //_signHReqRepo.DeleteSignReqD(signHReq.AccountNo);
                    //_signHReqRepo.DeleteDocReqD(signHReq.AccountNo);
                    foreach (SignatureDDocReq item in signHReq.Documents)
                    {
                        _signHReqRepo.DeleteDocReqD(item.ID);
                    }
                    foreach (SignatureDKTPReq item in signHReq.IdentityCards)
                    {
                        _signHReqRepo.DeleteKTPReqD(item.ID);
                    }
                    foreach (SignatureDSignReq item in signHReq.Signatures)
                    {
                        _signHReqRepo.DeleteSignReqD(item.ID);
                    }

                    scope.Complete();
                }
            }
            catch (TransactionAbortedException ex)
            {
                returnMessage = ex.Message;
            }
            catch (ApplicationException ex)
            {
                returnMessage = ex.Message;
            }
            return returnMessage;
        }

        public string CancelSignatureReq(SignatureHRequest signHReq)
        {
            string returnMessage = "";

            try
            {
                using (TransactionScope scope = new TransactionScope())
                {

                    _signHReqRepo.Remove(signHReq);
                    _signHReqRepo.DeleteKTPReqD(signHReq.AccountNo);
                    _signHReqRepo.DeleteSignReqD(signHReq.AccountNo);
                    _signHReqRepo.DeleteDocReqD(signHReq.AccountNo);

                    scope.Complete();
                }
            }
            catch (TransactionAbortedException ex)
            {
                returnMessage = ex.Message;
            }
            catch (ApplicationException ex)
            {
                returnMessage = ex.Message;
            }
            return returnMessage;
        }

        public string ApproveReq(SignatureHRequest signHReq, SignatureHeader svsH, SignatureHHistory svsHistory, User user)
        {
            string returnMessage = "";

            try
            {
                SignatureHeader oldsvsH = new SignatureHeader(svsH.AccountNo);

                using (TransactionScope scope = new TransactionScope())
                {
                    if (signHReq.RequestType == "U" || signHReq.RequestType == "D")
                    {
                        _signHRepo.DeleteSignatureH(svsH.AccountNo);
                        _signHRepo.DeleteKTPDet(svsH.AccountNo);
                        _signHRepo.DeleteSignDet(svsH.AccountNo);
                        _signHRepo.DeleteDocDet(svsH.AccountNo);

                    }

                    if (signHReq.RequestType != "D")
                    {
                        _signHRepo.Save(svsH);
                        foreach (SignatureDDoc item in svsH.Documents)
                        {
                            _signHRepo.AddDocDet(item);
                        }
                        foreach (SignatureDKTP item in svsH.IdentityCards)
                        {
                            _signHRepo.AddKTPDet(item);
                        }
                        foreach (SignatureDSign item in svsH.Signatures)
                        {
                            _signHRepo.AddSignDet(item);
                        }
                    }

                    _signHHistoryRepo.Save(svsHistory);

                    _signHReqRepo.DeleteSignatureHReq(signHReq.AccountNo);
                    _signHReqRepo.DeleteKTPReqD(signHReq.AccountNo);
                    _signHReqRepo.DeleteSignReqD(signHReq.AccountNo);
                    _signHReqRepo.DeleteDocReqD(signHReq.AccountNo);

                    scope.Complete();
                }
            }
            catch (TransactionAbortedException ex)
            {
                returnMessage = ex.Message;
            }
            catch (ApplicationException ex)
            {
                returnMessage = ex.Message;
            }
            return returnMessage;
        }

        public string RejectReq(SignatureHRequest signHReq, User user)
        {
            string returnMessage = "";

            try
            {

                using (TransactionScope scope = new TransactionScope())
                {
                    SignatureHHistory svsHistory = new SignatureHHistory(0L);
                    svsHistory.AccountNo = signHReq.AccountNo;
                    svsHistory.AccountName = signHReq.AccountName;
                    svsHistory.AccountType = signHReq.AccountType;
                    svsHistory.CIFNumber = signHReq.CIFNumber;
                    svsHistory.BranchCode = signHReq.BranchCode;
                    svsHistory.Note = signHReq.Note;
                    svsHistory.RequestType = signHReq.RequestType;
                    svsHistory.RequestDate = signHReq.RequestDate;
                    svsHistory.RequestUser = signHReq.RequestUser;
                    svsHistory.RequestReason = signHReq.RequestReason;
                    svsHistory.RejectBy = user.Username;
                    svsHistory.RejectDate = DateTime.Now;

                    _signHHistoryRepo.Save(svsHistory);


                    _signHReqRepo.Remove(signHReq);
                    _signHReqRepo.DeleteKTPReqD(signHReq.AccountNo);
                    _signHReqRepo.DeleteSignReqD(signHReq.AccountNo);
                    _signHReqRepo.DeleteDocReqD(signHReq.AccountNo);

                    scope.Complete();
                }
            }
            catch (TransactionAbortedException ex)
            {
                returnMessage = ex.Message;
            }
            catch (ApplicationException ex)
            {
                returnMessage = ex.Message;
            }
            return "";
        }

        public string SaveDocument(SignatureDDocReq document)
        {
            string returnMessage = "";

            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    _signHReqRepo.AddDocReqD(document);
                    scope.Complete();
                }
            }
            catch (TransactionAbortedException ex)
            {
                returnMessage = ex.Message;
            }
            catch (ApplicationException ex)
            {
                returnMessage = ex.Message;
            }
            return returnMessage;
        }

        public string SaveSign(SignatureDSignReq sign)
        {
            string returnMessage = "";

            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    _signHReqRepo.AddSignReqD(sign);
                    scope.Complete();
                }
            }
            catch (TransactionAbortedException ex)
            {
                returnMessage = ex.Message;
            }
            catch (ApplicationException ex)
            {
                returnMessage = ex.Message;
            }
            return returnMessage;
        }

        public string SaveIDCard(SignatureDKTPReq idcard)
        {
            string returnMessage = "";

            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    _signHReqRepo.AddKTPReqD(idcard);
                    scope.Complete();
                }
            }
            catch (TransactionAbortedException ex)
            {
                returnMessage = ex.Message;
            }
            catch (ApplicationException ex)
            {
                returnMessage = ex.Message;
            }
            return returnMessage;
        }
    }
}
