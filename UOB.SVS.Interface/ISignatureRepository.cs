﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using UOB.SVS.Model;

namespace UOB.SVS.Interface
{
    public interface ISignatureRepository : IBaseRepository<SignatureMaster>
    {
        SignatureMaster getSignatureMaster(string id);
        IList<SignatureMaster> getSignatureMasters(IList<string> signatureIds);
        bool IsDuplicate(string accno);
        PagedResult<SignatureMaster> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<SignatureMaster> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
    }
}
