﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using UOB.SVS.Model;

namespace UOB.SVS.Interface
{
    public interface ISignatureHHistoryRepository : IBaseRepository<SignatureHHistory>
    {
        SignatureHHistory getSignatureHHistory(string id);
        IList<SignatureHHistory> getSignatureHHistorys(IList<string> signatureHIds);
        bool IsDuplicate(string accno);
        PagedResult<SignatureHHistory> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<SignatureHHistory> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
    }
}
