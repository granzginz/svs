﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Treemas.Credential.Model;
using UOB.SVS.Model;

namespace UOB.SVS.Interface
{
    public interface ISignatureHReqService
    {
        string SaveSignatureReq(SignatureHRequest signHReq);
        string ApproveReq(SignatureHRequest signHReq, SignatureHeader signH, SignatureHHistory signHistory, User user);
        string UpdateSignatureReq(SignatureHRequest signHReq);
        string RejectReq(SignatureHRequest signHReq, User user);
        string CancelSignatureReq(SignatureHRequest signHReq);

        string SaveDocument(SignatureDDocReq document);
        string SaveSign(SignatureDSignReq sign);
        string SaveIDCard(SignatureDKTPReq idcard);

    }
}
