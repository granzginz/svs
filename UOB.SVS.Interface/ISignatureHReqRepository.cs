﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.Credential.Model;
using UOB.SVS.Model;

namespace UOB.SVS.Interface
{
    public interface ISignatureHReqRepository : IBaseRepository<SignatureHRequest>
    {
        SignatureHRequest getSignatureHReq(string id);
        IList<SignatureHRequest> getSignatureHReqList(IList<string> signatureHReqIds);
        bool IsDuplicate(string accno);
        PagedResult<SignatureHRequest> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<SignatureHRequest> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, SVSReqFilter filter);
        PagedResult<SignatureHRequest> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, SVSReqFilter filter, User user);
        PagedResult<SignatureHRequest> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
        PagedResult<SignatureHRequest> FindAllPagedCreate(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, SVSReqFilter filter);

        void DeleteSignatureHReq(string ID);

        IList<SignatureDKTPReq> getKTPReqD(string accountNo);
        void AddKTPReqD(SignatureDKTPReq item);
        void DeleteKTPReqD(string Id);
        void DeleteKTPReqD(long Id);


        IList<SignatureDSignReq> getSignReqD(string accountNo);
        void AddSignReqD(SignatureDSignReq item);
        void DeleteSignReqD(string Id);
        void DeleteSignReqD(long Id);


        IList<SignatureDDocReq> getDocReqD(string accountNo);
        void AddDocReqD(SignatureDDocReq item);
        void DeleteDocReqD(string Id);
        void DeleteDocReqD(long Id);

    }
}
