﻿using LinqSpecs;
using System.Collections.Generic;
using Treemas.Credential.Model;
using Treemas.Base.Utilities.Queries;
using UOB.SVS.Model;
namespace UOB.SVS.Interface
{
    public interface IVFunctionFeatureRepository: IBaseRepository<VFunctionFeature>
    {
        PagedResult<VFunctionFeature> FindAllPagedbyFunction(long functionId, int pageNumber, string orderColumn, int itemsPerPage, string orderKey, string searchColumn, string searchValue);
    }
}
