﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using UOB.SVS.Model;

namespace UOB.SVS.Interface
{
    public interface IBranchRepository : IBaseRepository<Branch>
    {
        Branch getBranch(string branchCode);
        Branch getBranch(long id);
        IList<Branch> getBranchs();
        IList<Branch> getBranchs(IList<string> branchCode);
        bool IsDuplicate(string branchCode);
        PagedResult<Branch> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<Branch> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
        
    }
}
