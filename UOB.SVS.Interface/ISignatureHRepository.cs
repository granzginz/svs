﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using UOB.SVS.Model;

namespace UOB.SVS.Interface
{
    public interface ISignatureHRepository : IBaseRepository<SignatureHeader>
    {
        SignatureHeader getSignatureHeader(string id);
        IList<SignatureHeader> getSignatureHeaders(IList<string> signatureHIds);
        bool IsDuplicate(string accno);
        PagedResult<SignatureHeader> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, SVSReqFilter filter);
        PagedResult<SignatureHeader> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);

        void DeleteSignatureH(string ID);

        void AddKTPDet(SignatureDKTP item);
        IList<SignatureDKTP> getKTPDet(string accountNo);
        void DeleteKTPDet(string ID);

        void AddSignDet(SignatureDSign item);
        IList<SignatureDSign> getSignDet(string accountNo);
        void DeleteSignDet(string ID);

        void AddDocDet(SignatureDDoc item);
        IList<SignatureDDoc> getDocDet(string accountNo);
        void DeleteDocDet(string ID);
    }
}
