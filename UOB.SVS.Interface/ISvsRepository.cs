﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using UOB.SVS.Model;

namespace UOB.SVS.Interface
{
    public interface ISvsRepository : IBaseRepository<Svs>
    {
        Svs getSvs(long id);
        IList<Svs> getSvss(IList<string> SvsIds);
        bool IsDuplicate(string accno, long id);
        PagedResult<Svs> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<Svs> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
    }
}
