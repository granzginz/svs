﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using UOB.SVS.Model;

namespace UOB.SVS.Interface
{
    public interface IAccountNoViewRepository : IBaseRepository<AccountNoView>
    {
        bool IsDuplicate(string accno);
    }
}
