﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using UOB.SVS.Model;

namespace UOB.SVS.Interface
{
    public interface IDocumentTypeRepository : IBaseRepository<DocumentType>
    {
        DocumentType getDocumentType(long id);
        IList<DocumentType> getDocumentTypes(IList<string> doctype);
        bool IsDuplicate(string doctype);
        PagedResult<DocumentType> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<DocumentType> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
    }
}
