﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.Credential.Model;
using UOB.SVS.Model;

namespace UOB.SVS.Interface
{
    public interface IDocumentTypeCrRepository : IBaseRepository<DocumentTypeCr>
    {
        DocumentTypeCr getDocType(string docTypeId);
        DocumentTypeCr getDocType(long id);
        DocumentTypeCr getDocTypeCr(long docTypeId, ApprovalType type);
        IList<DocumentTypeCr> getDocTypes(IList<string> docTypeId);
        bool IsDuplicate(string docTypeId);
        PagedResult<DocumentTypeCr> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<DocumentTypeCr> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey,  ApplicationCRFilter filter);
        PagedResult<DocumentTypeCr> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
    }
}
