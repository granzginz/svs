﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UOB.SVS.Models
{
    public class ParameterView
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public string Password { get; set; }
        public string DukcapilUrl { get; set; }
        public string StaggingUrl { get; set; }
        public string ClientIPAddress { get; set; }
        public string PasswordExp { get; set; }
        public string Validity { get; set; }
        public int NikExpiredDuration { get; set; }
        public int SessionTimeout { get; set; }
        public int LoginAttempt { get; set; }

        public int SizeKTP { get; set; }
        public int SizeSignature { get; set; }
        public int SizeDocument { get; set; }
        public int KeepHistory { get; set; }
        public int KeepAuditTrail { get; set; }
        public int KeepPendingAppr { get; set; }
        public decimal MaxDiffAccount { get; set; }
    }
}