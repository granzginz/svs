﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UOB.SVS.Models
{
    public class RoleDocTypeSelectHelper
    {
        public bool selected { get; set; }
        public long DocumentTypeId { get; set; }
    }
}