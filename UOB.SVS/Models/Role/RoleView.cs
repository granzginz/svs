﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UOB.SVS.Models
{
    public class RoleView
    {
        public long Id { get; set; }
        public string Application { get; set; }
        public string RoleId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int SessionTimeout { get; set; }
        public int LockTimeout { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
    }
}