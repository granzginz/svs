﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UOB.SVS.Models
{
    public class ApplicationCrView
    {
        public string Id { get; set; }
        public string ApprovalID { get; set; }
        public string ApplicationId { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }
        public string RequestType { get; set; }
        public string Runtime { get; set; }
        public string Description { get; set; }
        public string CSSColor { get; set; }
        public string Icon { get; set; }
        public string RequestStatus { get; set; }
        public string ApprovalFuntion { get; set; }
        public bool Approved { get; set; }
        public string ApprovedBy { get; set; }
        public string ApprovedDate { get; set; }
        public string ApprovalType { get; set; }
        public string ApprovalStatus { get; set; }
        public string MakerUser { get; set; }
        public string MakerDate { get; set; }
        public string ApprovalIDBefore { get; set; }
        public string ApplicationIdBefore { get; set; }
        public string TypeBefore { get; set; }
        public string NameBefore { get; set; }
        public string RequestTypeBefore { get; set; }
        public string RuntimeBefore { get; set; }
        public string DescriptionBefore { get; set; }
        public string CSSColorBefore { get; set; }
        public string IconBefore { get; set; }
        public long FunctionId { get; set; }
        public long RoleId { get; set; }
        public long DocTypeid { get; set; }
        public long RoleDocTypeid { get; set; }
    }
}