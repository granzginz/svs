﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UOB.SVS.Models
{
    public class ApplicationView
    {
        public long Id { get; set; }
        public string ApplicationId { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Runtime { get; set; }
        public string Description { get; set; }
        public string CSSColor { get; set; }
        public string Icon { get; set; }
    }
}