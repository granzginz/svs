﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UOB.SVS.Models
{
    public class AuditTrailView
    {
        public long Id { get; set; }
        public string TimeStamp { get; set; }
        public string TimeStampString { get; set; }
        public string FunctionName { get; set; }
        public string ObjectName { get; set; }
        public string ObjectValueBefore { get; set; }
        public string ObjectValueAfter { get; set; }
        public string UserMaker { get; set; }
        public DateTime? RequestDate { get; set; }
        public string RequestDateString { get; set; }
        public string UserChecker { get; set; }
        public DateTime? ApproveDate { get; set; }
        public string ApproveDateString { get; set; }
        public string Action { get; set; }
        public string TimeStampStart { get; set; }
        public string TimeStampEnd { get; set; }
    }
}