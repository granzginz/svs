﻿
namespace UOB.SVS.Models
{
    public class SvsView
    {
        public long ID { get; set; }
        public string ACCOUNT_NUMBER { get; set; }
        public string CUSTOMER_NAME { get; set; }
        public string NOTE { get; set; }
        public string SIGNATUREVal { get; set; }
        public string KTP { get; set; }
        public string NOTE_KTP { get; set; }
        public string PENGENAL { get; set; }
        public string ADD_BY { get; set; }
        public string DATE_ADD { get; set; }
        public string UPD_BY { get; set; }
        public string DATE_UPD { get; set; }
        public string DEL_BY { get; set; }
        public string DATE_DEL { get; set; }
        public string APP_ADD_BY { get; set; }
        public string DATE_APP_ADD { get; set; }
        public string APP_UP_BY { get; set; }
        public string DATE_APP_UPD { get; set; }
        public string APP_DEL_BY { get; set; }
        public string DATE_APP_DEL { get; set; }
        public string NOT_APP_ADD_BY { get; set; }
        public string DATE_NOT_APP_ADD { get; set; }
        public string NOT_APP_UPD_BY { get; set; }
        public string DATE_NOT_APP_UPD { get; set; }
        public string NOT_APP_DEL_BY { get; set; }
        public string DATE_NOT_APP_DELL { get; set; }
        public string STATUS_FLAG { get; set; }
        public string STATUS_AKTIF { get; set; }
        public string OLD_NOTE { get; set; }
        public string OLD_NOTE_KTP { get; set; }
        public string FSIGNATURE { get; set; }
        public string FKTP { get; set; }
        public string FNOTE { get; set; }
        public string ZSIGNATURE { get; set; }
        public string ZOLD_SIGNATURE { get; set; }
        public string ZKTP { get; set; }
        public string ZOLD_KTP { get; set; }
        public string OLD_SIGNATURE { get; set; }
        public string OLD_KTP { get; set; }
        public string OLD_ACC_NUMBER { get; set; }
    }
}