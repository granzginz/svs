﻿using System;
using System.Web;
using System.Collections.Generic;

namespace UOB.SVS.Models
{
    public class SignatureHReqView
    {
        public string AccountNo { get; set; }
        public string CopiedAccountNo { get; set; }
        public string AccountName { get; set; }
        public string AccountNameOld { get; set; }
        public string AccountType { get; set; }
        public string AccountTypeOld { get; set; }
        public string CIFNumber { get; set; }
        public string CIFNumberOld { get; set; }
        public string BranchCode { get; set; }
        public string BranchCodeOld { get; set; }
        public string Note { get; set; }
        public string NoteOld { get; set; }
        public string RequestType { get; set; }
        public string RequestTypeString {
            get
            {
                if (RequestType == "C")
                {
                    return "Create";
                }
                if (RequestType == "U")
                {
                    return "Update";
                }
                if (RequestType == "D")
                {
                    return "Delete";
                }
                return "";
            }
        }
        public DateTime? RequestDate { get; set; }
        public string RequestDateString { get; set; }
        public string RequestUser { get; set; }
        public string RequestReason { get; set; }
        public string RejectNote { get; set; }
        public bool IsRejected { get; set; }
        public List<Signature> Signatures { get; set; }
        public List<IdentityCard> IdentityCards { get; set; }
        public List<Document> Documents { get; set; }
        public List<DeleteFormat> DeletedIdentityCards { get; set; }
        public List<DeleteFormat> DeletedSignatures { get; set; }
        public List<DeleteFormat> DeletedDocuments { get; set; }
    }

    public class Signature
    {
        public long ID { get; set; }

        public int Index { get; set; }
        public string ImageType { get; set; }
        public string CroppedBlob { get; set; }
        public string ImageTypeOld { get; set; }
        public string CroppedBlobOld { get; set; }
        public string AccountNo { get; set; }
        public Boolean IsNew { get; set; }
    }

    public class IdentityCard
    {
        public long ID { get; set; }

        public int Index { get; set; }
        public string ImageType { get; set; }
        public string CroppedBlob { get; set; }
        public string ImageTypeOld { get; set; }
        public string CroppedBlobOld { get; set; }
        public string AccountNo { get; set; }
        public Boolean IsNew { get; set; }
    }

    public class Document
    {
        public long ID { get; set; }
        public int Index { get; set; }
        public string FileType { get; set; }
        public string FileBlob { get; set; }
        public string DocumentType { get; set; }
        public string FileTypeOld { get; set; }
        public string FileBlobOld { get; set; }
        public string DocumentTypeOld { get; set; }
        public string AccountNo { get; set; }
        public Boolean IsNew { get; set; }
    }

    public class DeleteFormat
    {
        public long ID { get; set; }
    }

    public class DocumentForm
    {
        public long ID { get; set; }
        public int Index { get; set; }
        public string FileType { get; set; }
        public string FileBlob { get; set; }
        public string DocumentType { get; set; }
        public string FileTypeOld { get; set; }
        public string FileBlobOld { get; set; }
        public string DocumentTypeOld { get; set; }
        public string AccountNo { get; set; }
        public Boolean IsNew { get; set; }
        
    }
}