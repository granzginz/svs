﻿using System;
using System.Web;
using System.Collections.Generic;

namespace UOB.SVS.Models
{
    public class SignatureHView
    {
        public string AccountNo { get; set; }
        public string AccountName { get; set; }
        public string AccountType { get; set; }
        public string CIFNumber { get; set; }
        public string BranchCode { get; set; }
        public string Note { get; set; }
        public List<Signature> Signatures { get; set; }
        public List<IdentityCard> IdentityCards { get; set; }
        public List<Document> Documents { get; set; }

    }
}