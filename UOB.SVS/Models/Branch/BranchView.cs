﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UOB.SVS.Models
{
    public class BranchView
    {
        public long Id { get; set; }
        public string BranchCode { get; set; }
        public string Description { get; set; }
        public string MainBranch { get; set; }
        public bool isMainBranch{ get; set; }
        public string MainBranchID { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ChangedBy { get; set; }
        public string ChangedDate { get; set; }
    }
}