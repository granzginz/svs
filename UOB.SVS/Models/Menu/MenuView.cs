﻿
using System.Collections.Generic;
using Treemas.Credential.Model;

namespace UOB.SVS.Models
{
    public class MenuView
    {
        public long Id { get; set; }
        public string MenuId { get; set; }
        public string MenuName { get; set; }
        public string Application { get; set; }
        public string FunctionId { get; set; }
        public string MenuUrl { get; set; }
        public int MenuLevel { get; set; }
        public int MenuOrder { get; set; }
        public string MenuIcon { get; set; }
        public bool Active { get; set; }

        public string ParentMenu { get; set; }
        public IList<Menu> ChildMenu { get; set; }

    }
}