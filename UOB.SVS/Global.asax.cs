﻿using System;
using System.Web.Mvc;
using System.Web.Routing;
using System.Reflection;
using Treemas.Base.Web.Platform;
using Treemas.Credential;
using Treemas.Credential.Repository;
using Treemas.Credential.Interface;
using Treemas.Base.Utilities;
using Treemas.Base.Lookup;
using SimpleInjector;
using SimpleInjector.Integration.Web;
using SimpleInjector.Integration.Web.Mvc;
using UOB.SVS.Interface;
using UOB.SVS.Repository;
using UOBISecurity;
using UOB.SVS.Service;

namespace UOB.SVS
{
    public class SVS : WebApplication
    {
        public SVS() : base("SVS")
        {
            //SystemSettings.Instance.Name = "e-KTP Inquiry";
            SystemSettings.Instance.Alias = "SVS";
            SystemSettings.Instance.Security.EnableAuthentication = true;
            SystemSettings.Instance.Security.IgnoreAuthorization = true;
            SystemSettings.Instance.Security.EnableSingleSignOn = false;
            SystemSettings.Instance.Logging.Enabled = false;
        }

        protected override void Startup()
        {
            // Integration With UOBISecurity
            Encryptor enc = new Encryptor();
            ApplicationRegistryHandler appReg = new ApplicationRegistryHandler();

            string keyReg;
            string appName = SystemSettings.Instance.UOBISetting.EncryptionAppName;
            string uobikey = SystemSettings.Instance.UOBISetting.UOBIKey;
            string halfkey = SystemSettings.Instance.UOBISetting.EncryptionHalfKey;
            appName = appName.Trim().Replace(" ", "");
            keyReg = appReg.ReadFromRegistry(@"Software\" + appName, "Key");
            
            var hashKey = enc.Decrypt(halfkey, keyReg, uobikey);
            Encryption.Instance.SetHashKey(hashKey);


            RouteCollection routes = RouteTable.Routes;
            RouteConfig.RegisterRoutes(routes);

            this.BindInterface();
        }

        private void BindInterface()
        {
            var container = new Container();
            container.Options.DefaultScopedLifestyle = new WebRequestLifestyle();

            container.Register<ISessionAuthentication, SessionAuthentication>(Lifestyle.Scoped);
            container.Register<ISSOSessionStorage, SSOSessionStorage>(Lifestyle.Scoped);

            // Register your types, for instance:
            container.Register<IApplicationRepository, ApplicationRepository>(Lifestyle.Scoped);
            container.Register<IAuditTrailLog, AuditTrailLog>(Lifestyle.Scoped);
            container.Register<IAuditTrailRepository, AuditTrailRepository>(Lifestyle.Scoped);
            container.Register<IAuthorizationRepository, AuthorizationRepository>(Lifestyle.Scoped);
            container.Register<IFeatureRepository, FeatureRepository>(Lifestyle.Scoped);
            container.Register<IFunctionFeatureRepository, FunctionFeatureRepository>(Lifestyle.Scoped);
            container.Register<IFunctionRepository, FunctionRepository>(Lifestyle.Scoped);            
            container.Register<ILoginInfoRepository, LoginInfoRepository>(Lifestyle.Scoped);            
            container.Register<IMenuRepository, MenuRepository>(Lifestyle.Scoped);
            container.Register<IParameterRepository, ParameterRepository>(Lifestyle.Scoped);
            container.Register<IRoleFunctionFeatureRepository, RoleFunctionFeatureRepository>(Lifestyle.Scoped);
            container.Register<IRoleFunctionRepository, RoleFunctionRepository>(Lifestyle.Scoped);
            container.Register<IRoleRepository, RoleRepository>(Lifestyle.Scoped);
            container.Register<IUserAccount, UserAccount>(Lifestyle.Scoped);
            container.Register<IUserApplicationRepository, UserApplicationRepository>(Lifestyle.Scoped);
            container.Register<IUserCrRepository, UserCrRepository>(Lifestyle.Scoped);
            container.Register<IUserRepository, UserRepository>(Lifestyle.Scoped);
            container.Register<IBranchRepository, BranchRepository>(Lifestyle.Scoped);
            container.Register<IDocumentTypeRepository, DocumentTypeRepository>(Lifestyle.Scoped);
            container.Register<IBranchCrRepository, BranchCrRepository>(Lifestyle.Scoped);
            container.Register<IAccountMasterRepository, AccountMasterRepository>(Lifestyle.Scoped);

            container.Register<ISvsRepository, SvsRepository>(Lifestyle.Scoped);
            container.Register<ISignatureHReqRepository, SignatureHReqRepository>(Lifestyle.Scoped);
            container.Register<IAccountNoViewRepository, AccountNoViewRepository>(Lifestyle.Scoped);
            container.Register<ISignatureHReqService, SignatureHReqService>(Lifestyle.Scoped);
            container.Register<ISignatureHRepository, SignatureHRepository>(Lifestyle.Scoped);
            container.Register<ISignatureHHistoryRepository, SignatureHHistoryRepository>(Lifestyle.Scoped);
            container.Register<IVApprovalRepository, VApprovalRepository>(Lifestyle.Scoped);
            container.Register<IVFunctionFeatureRepository, VFunctionFeatureRepository>(Lifestyle.Scoped);

            //approval credential
            container.Register<IApplicationCrRepository, ApplicationCrRepository>(Lifestyle.Scoped);
            container.Register<IFunctionCrRepository, FunctionCrRepository>(Lifestyle.Scoped);
            container.Register<IFeatureCrRepository, FeatureCrRepository>(Lifestyle.Scoped);
            container.Register<IRoleCrRepository, RoleCrRepository>(Lifestyle.Scoped);
            container.Register<IMenuCrRepository, MenuCrRepository>(Lifestyle.Scoped);
            container.Register<IFunctionFeatureCrRepository, FunctionFeatureCrRepository>(Lifestyle.Scoped);
            container.Register<IRoleFunctionCrRepository, RoleFunctionCrRepository>(Lifestyle.Scoped);
            container.Register<IRoleFunctionFeatureCrRepository, RoleFunctionFeatureCrRepository>(Lifestyle.Scoped);
            container.Register<IRoleDocTypeRepository, RoleDocTypeRepository>(Lifestyle.Scoped);
            container.Register<IRoleDocTypeCrRepository, RoleDocTypeCrRepository>(Lifestyle.Scoped);
            container.Register<IDocumentTypeCrRepository, DocumentTypeCrRepository>(Lifestyle.Scoped);

            //new audit trail
            container.Register<IAppAuditTrailLog, AppAuditTrailLog>(Lifestyle.Scoped);
            container.Register<IAppAuditTrailRepository, AppAuditTrailRepository>(Lifestyle.Scoped);

            container.Register<IParameterAuditTrail, ParameterAuditTrailLog>(Lifestyle.Scoped);
            container.Register<IParameterAuditTrailRepository, ParameterAuditTrailRepository>(Lifestyle.Scoped);
            container.Register<IUserAuditTrailLog, UserAuditTrailLog>(Lifestyle.Scoped);
            container.Register<IUserAuditTrailRepository, UserAuditTrailRepository>(Lifestyle.Scoped);


            // This is an extension method from the integration package.
            container.RegisterMvcControllers(Assembly.GetExecutingAssembly());

            container.Verify();

            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));
            
            // Override setting parameter
            var paramRepo = container.GetInstance<IParameterRepository>();
            var param = paramRepo.getParameter();
            
            SystemSettings.Instance.Security.SessionTimeout = param.SessionTimeout;
            SystemSettings.Instance.Security.LoginAttempt = param.LoginAttempt;
            SystemSettings.Instance.Security.DomainServer = param.DomainServer;
        }
    }
}
