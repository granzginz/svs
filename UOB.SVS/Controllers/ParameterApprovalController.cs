﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using Treemas.Credential.Model;
using UOB.SVS.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using UOB.SVS.Resources;

namespace UOB.SVS.Controllers
{
    public class ParameterApprovalController : PageController
    {
        private IParameterRepository _parameterRepository;
        private IParameterAuditTrail _auditRepository;
        public ParameterApprovalController(ISessionAuthentication sessionAuthentication, IParameterRepository parameterRepository, IParameterAuditTrail auditRepository) 
            :base(sessionAuthentication)
        {
            this._parameterRepository = parameterRepository;
            this._auditRepository = auditRepository;
            Settings.Title = ParameterResources.ApprovalPageTitle;

        }
        protected override void Startup()
        {
            ParameterHistory param = _parameterRepository.GetChangesRequest();

            if (param != null)
            {
                param.UserId = Encryption.Instance.DecryptText(param.UserId);
                param.Password = Encryption.Instance.DecryptText(param.Password);
                param.NewUserId = Encryption.Instance.DecryptText(param.NewUserId);
                param.NewPassword = Encryption.Instance.DecryptText(param.NewPassword);

                ViewData["ParameterHistory"] = param;
            }            
        }

        // POST: Function/Edit/5
        [HttpPost]
        public ActionResult Approve(string ApprovalStatus)
        {
            ViewData["ActionName"] = "Approve";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                Parameter paramData = _parameterRepository.getParameter();
                ParameterHistory histData = _parameterRepository.GetChangesRequest();
                
                //_auditRepository.SaveAuditTrail(Settings.ModuleName, paramData, user.Username, "BeforeEdit");
                if (ApprovalStatus == ApprovalStatusState.D.ToString())
                {
                    histData.ApprovalStatus = ApprovalStatusState.D.ToString();
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, paramData, histData, histData.ChangedBy, histData.ChangedDate, user.Username, "Reject");

                }
                else if (ApprovalStatus == ApprovalStatusState.A.ToString())
                {
                    histData.ApprovalStatus = ApprovalStatusState.A.ToString();

                    paramData.UserId = histData.NewUserId;
                    paramData.Password = histData.NewPassword;
                    //paramData.DukcapilUrl = histData.NewDukcapilUrl;
                    paramData.StaggingUrl = histData.NewStaggingUrl;
                    //paramData.ClientIPAddress = histData.NewClientIPAddress;
                    paramData.SessionTimeout = (histData.NewSessionTimeout <= 0) ? 30 : histData.NewSessionTimeout;
                    paramData.LoginAttempt = (histData.NewLoginAttempt <= 0) ? 3 : histData.NewLoginAttempt;
                    paramData.PasswordExp = histData.NewPasswordExp;
                    //Penambahan Rifki
                    paramData.SizeKTP = histData.NewSizeKTP;
                    paramData.SizeSignature = histData.NewSizeSignature;
                    paramData.SizeDocument = histData.NewSizeDocument;
                    paramData.KeepHistory = histData.NewKeepHistory;
                    paramData.KeepAuditTrail = histData.NewKeepAuditTrail;
                    paramData.KeepPendingAppr = histData.NewKeepPendingAppr;
                    paramData.MaxDiffAccount = histData.NewMaxDiffAccount;
                    //paramData.Validity = histData.NewValidity;
                    //paramData.NIKExpiredDuration = histData.NewNIKExpiredDuration;
                    _parameterRepository.Save(paramData);
                }

                histData.ApprovedBy = user.Username;
                histData.ApprovedDate = DateTime.Now;
                _parameterRepository.SaveHistory(histData);

                _auditRepository.SaveAuditTrail(Settings.ModuleName, histData, paramData, paramData.ChangedBy, paramData.ChangedDate, user.Username, "Edit");
                //_auditRepository.SaveAuditTrail(Settings.ModuleName, histData, user.Username, "ChangeApproval");
                //_auditRepository.SaveAuditTrail(Settings.ModuleName, paramData, user.Username, "AfterEdit");
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(ParameterResources.Validation_Success));
                return RedirectToRoute(new { controller = "ParameterApproval", action="Index" });

            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView();

        }
        
        private ViewResult CreateView()
        {
            ParameterHistory param = _parameterRepository.GetChangesRequest();
            param.UserId = Encryption.Instance.DecryptText(param.UserId);
            param.Password = Encryption.Instance.DecryptText(param.Password);
            param.NewUserId = Encryption.Instance.DecryptText(param.NewUserId);
            param.NewPassword = Encryption.Instance.DecryptText(param.NewPassword);

            ViewData["ParameterHistory"] = param;
            return View("ParameterApproval");
        }

    }
}