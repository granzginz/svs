﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using Treemas.Credential.Model;
using UOB.SVS.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using UOB.SVS.Resources;

namespace UOB.SVS.Controllers
{
    public class RoleFuncController : PageController
    {
        private IRoleRepository _roleRepository;
        private IApplicationRepository _appRepository;
        private IAppAuditTrailLog _auditRepository;
        private IRoleCrRepository _roleCrRepository;
        public RoleFuncController(ISessionAuthentication sessionAuthentication, IRoleRepository roleRepository,
            IApplicationRepository appRepository, IAppAuditTrailLog auditRepository, IRoleCrRepository roleCrRepository) : base(sessionAuthentication)
        {
            this._roleRepository = roleRepository;
            this._appRepository = appRepository;
            this._auditRepository = auditRepository;
            this._roleCrRepository = roleCrRepository;
            Settings.ModuleName = "Role Function";
            Settings.Title = RoleResources.PageTitleRoleFunction;
        }

        protected override void Startup()
        {
        }

        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.   
                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.   
                PagedResult<Role> data;
                if (searchValue.IsNullOrEmpty())
                {
                    data = _roleRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir);
                }
                else
                {
                    data = _roleRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchValue);
                }

                var items = data.Items.ToList().ConvertAll<RoleView>(new Converter<Role, RoleView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public RoleView ConvertFrom(Role item)
        {
            RoleView returnItem = new RoleView();
            returnItem.Application = item._Application;
            returnItem.Description = item.Description;
            returnItem.SessionTimeout = item.SessionTimeout;
            returnItem.LockTimeout = item.LockTimeout;
            returnItem.RoleId = item.RoleId;
            returnItem.Id = item.Id;
            returnItem.Name = item.Name;
            return returnItem;
        }

        public SelectListItem ConvertFrom(Application item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.Name;
            returnItem.Value = item.ApplicationId;
            return returnItem;
        }

        // GET: Function/Create
        public ActionResult Create()
        {
            ViewData["ActionName"] = "Create";
            RoleView data = new RoleView();
            return CreateView(data);
        }

        private ViewResult CreateView(RoleView data)
        {
            ViewData["ApplicationMaster"] = createApplicationSelect(data.Application);
            return View("Detail", data);
        }

        private IList<SelectListItem> createApplicationSelect(string selected)
        {
            IList<SelectListItem> dataList = _appRepository.FindAll().ToList().ConvertAll<SelectListItem>(new Converter<Application, SelectListItem>(ConvertFrom));
            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected).Selected = true;
            }
            return dataList;
        }

        // POST: Role/Create
        [HttpPost]
        public ActionResult Create(RoleView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                message = validateData(data, "Create");
                if (message.IsNullOrEmpty())
                {
                    //Role newData = new Role(0L);
                    RoleCr newData = new RoleCr(0L);
                    newData._Application = data.Application;
                    newData.RoleId = data.RoleId;
                    newData.Name = data.Name;
                    newData.Description = data.Description;
                    newData.SessionTimeout = data.SessionTimeout;
                    newData.LockTimeout = data.LockTimeout;
                    newData.ChangedBy = user.Username;
                    newData.ChangedDate = DateTime.Now;
                    newData.CreatedBy = user.Username;
                    newData.CreatedDate = DateTime.Now;
                    newData.ApprovalType = "C";
                    newData.ApprovalStatus = "N";
                    newData.Approved = false;
                    //_roleRepository.Add(newData);
                    _roleCrRepository.Add(newData);
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(SvsResources.Validation_Approval));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        private string validateData(RoleView data, string mode)
        {
            if (data.Application == "")
                return RoleResources.Validation_SelectApplication;

            if (data.Name == "")
                return RoleResources.Validation_FillRoleName;

            if (data.RoleId == "")
                return RoleResources.Validation_FillRoleId;

            if (data.SessionTimeout <= 0)
                return RoleResources.Validation_SessionTimeout;

            if (data.LockTimeout <= 0)
                return RoleResources.Validation_LockTimeout;

            if (mode == "Create")
            {
                if (_roleRepository.IsDuplicate(data.Application, data.RoleId))
                    return RoleResources.Validation_DuplicateData;
            }
            return "";
        }

        // GET: Role/Edit/5
        public ActionResult Edit(long id)
        {
            ViewData["ActionName"] = "Edit";
            Role autoRole = _roleRepository.getRole(id);
            RoleView data = ConvertFrom(autoRole);

            return CreateView(data);
        }
        // POST: Role/Edit/5
        [HttpPost]
        public ActionResult Edit(RoleView data)
        {
            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                message = validateData(data, "Edit");
                if (message.IsNullOrEmpty())
                {
                    //Role newData = new Role(data.Id);
                    RoleCr newData = new RoleCr(0L);
                    newData._Application = data.Application;
                    newData.RoleId = data.RoleId;
                    newData.Name = data.Name;
                    newData.Description = data.Description;
                    newData.SessionTimeout = data.SessionTimeout;
                    newData.LockTimeout = data.LockTimeout;
                    newData.ChangedBy = user.Username;
                    newData.ChangedDate = DateTime.Now;
                    newData.CreatedBy = user.Username;
                    newData.CreatedDate = DateTime.Now;
                    newData.ApprovalType = "U";
                    newData.ApprovalStatus = "N";
                    newData.Approved = false;

                    //_roleRepository.Save(newData);
                    _roleCrRepository.Add(newData);
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(SvsResources.Validation_Approval));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }


        // POST: Role/Delete/5
        [HttpPost]
        public ActionResult Delete(long Id)
        {
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                Role roleData = _roleRepository.getRole(Id);
                RoleCr newData = new RoleCr(0L);
                newData._Application = roleData._Application;
                newData.RoleId = roleData.RoleId;
                newData.Name = roleData.Name;
                newData.Description = roleData.Description;
                newData.SessionTimeout = roleData.SessionTimeout;
                newData.LockTimeout = roleData.LockTimeout;
                newData.ChangedBy = user.Username;
                newData.ChangedDate = DateTime.Now;
                newData.CreatedBy = user.Username;
                newData.CreatedDate = DateTime.Now;
                newData.ApprovalType = "D";
                newData.ApprovalStatus = "N";
                newData.Approved = false;

                //_roleRepository.Save(newData);
                //Role newData = new Role(Id);
                //_roleRepository.Remove(newData);
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(SvsResources.Validation_Approval));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return Edit(Id);
        }
    }
}