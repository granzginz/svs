﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using Treemas.Credential.Model;
using UOB.SVS.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using UOB.SVS.Resources;

namespace UOB.SVS.Controllers
{
    public class ParameterController : PageController
    {
        private IParameterRepository _parameterRepository;
        private IParameterAuditTrail _auditRepository;
        public ParameterController(ISessionAuthentication sessionAuthentication, IParameterRepository parameterRepository, IParameterAuditTrail auditRepository) 
            :base(sessionAuthentication)
        {
            this._parameterRepository = parameterRepository;
            this._auditRepository = auditRepository;
            Settings.Title = ParameterResources.PageTitle;

        }
        protected override void Startup()
        {
            Parameter param = _parameterRepository.getParameter();
            param.UserId = Encryption.Instance.DecryptText(param.UserId);
            param.Password = Encryption.Instance.DecryptText(param.Password);
            bool InChangeRequest = _parameterRepository.InChangesRequest(param.Id);

            if (InChangeRequest)
                ScreenMessages.Submit(ScreenMessage.Info(ParameterResources.ChangesRequestInProgress));

            ViewData["Parameter"] = param;
            ViewData["InChangeRequest"] = InChangeRequest;
        }

        private string validateData(ParameterView data, string mode)
        {

            if (data.UserId.IsNullOrEmpty())
                return ParameterResources.Validation_FillUserId;
            if (mode == "Create")
            {
                if (_parameterRepository.IsDuplicate(data.UserId))
                    return ParameterResources.Validation_DuplicateData;
            }
            return "";
        }

        // POST: Function/Edit/5
        [HttpPost]
        public ActionResult Edit(ParameterView data)
        {
            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                message = validateData(data, "Edit");
                if (message.IsNullOrEmpty())
                {

                    Parameter paramData = _parameterRepository.getParameter();

                    ParameterHistory HistData = new ParameterHistory(0L);
                    HistData.ParamId = paramData.ParamId;
                    HistData.UserId = paramData.UserId;
                    HistData.Password = paramData.Password;
                    //HistData.DukcapilUrl = paramData.DukcapilUrl;
                    HistData.StaggingUrl = paramData.StaggingUrl;
                    //HistData.ClientIPAddress = paramData.ClientIPAddress;
                    HistData.SessionTimeout = paramData.SessionTimeout;
                    HistData.LoginAttempt = paramData.LoginAttempt;
                    HistData.PasswordExp = paramData.PasswordExp;
                    //Penambahan Rifki
                    HistData.SizeKTP = paramData.SizeKTP;
                    HistData.SizeSignature = paramData.SizeSignature;
                    HistData.SizeDocument = paramData.SizeDocument;
                    HistData.KeepHistory = paramData.KeepHistory;
                    HistData.KeepAuditTrail = paramData.KeepAuditTrail;
                    HistData.KeepPendingAppr = paramData.KeepPendingAppr;
                    HistData.MaxDiffAccount = paramData.MaxDiffAccount;
                    //HistData.Validity = paramData.Validity;
                    //HistData.NIKExpiredDuration = paramData.NIKExpiredDuration;
                    
                    HistData.NewUserId = Encryption.Instance.EncryptText(data.UserId);
                    HistData.NewPassword = Encryption.Instance.EncryptText(data.Password);
                    //HistData.NewDukcapilUrl = data.DukcapilUrl;
                    HistData.NewStaggingUrl = data.StaggingUrl;
                    //HistData.NewClientIPAddress = data.ClientIPAddress;
                    HistData.NewSessionTimeout = (data.SessionTimeout <= 0) ? 30 : data.SessionTimeout;
                    HistData.NewLoginAttempt= (data.LoginAttempt <= 0) ? 3 : data.LoginAttempt;
                    HistData.NewPasswordExp = data.PasswordExp;
                    //Penambahan Rifki
                    HistData.NewSizeKTP = data.SizeKTP;
                    HistData.NewSizeSignature = data.SizeSignature;
                    HistData.NewSizeDocument = data.SizeDocument;
                    HistData.NewKeepHistory = data.KeepHistory;
                    HistData.NewKeepAuditTrail = data.KeepAuditTrail;
                    HistData.NewKeepPendingAppr = data.KeepPendingAppr;
                    HistData.NewMaxDiffAccount = data.MaxDiffAccount;

                    //HistData.NewValidity = data.Validity;
                    //HistData.NewNIKExpiredDuration = data.NikExpiredDuration;
                    HistData.ApprovalStatus = ApprovalStatusState.R.ToString();
                    HistData.ChangedBy = user.Username;
                    HistData.ChangedDate = DateTime.Now;

                    _parameterRepository.AddHistory(HistData);
                    

                    paramData.UserId = Encryption.Instance.EncryptText(data.UserId);
                    paramData.Password = Encryption.Instance.EncryptText(data.Password);
                    //paramData.DukcapilUrl = data.DukcapilUrl;
                    paramData.StaggingUrl = data.StaggingUrl;
                    //paramData.ClientIPAddress = data.ClientIPAddress;
                    paramData.SessionTimeout = (data.SessionTimeout <= 0) ? 30 : data.SessionTimeout;
                    paramData.LoginAttempt = (data.LoginAttempt <= 0) ? 3 : data.LoginAttempt;
                    paramData.PasswordExp = data.PasswordExp;
                    //paramData.Validity = data.Validity;
                    //paramData.NIKExpiredDuration = data.NikExpiredDuration;
                    
                    ViewData["Parameter"] = paramData;
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                //ScreenMessages.Submit(ScreenMessage.Success(ParameterResources.Validation_RequestSuccess));
                return RedirectToRoute(new { controller = "Parameter", action="Index" });

            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);

        }
        
        private ViewResult CreateView(ParameterView data)
        {
            Parameter param = _parameterRepository.getParameter();
            param.UserId = Encryption.Instance.DecryptText(param.UserId);
            param.Password = Encryption.Instance.DecryptText(param.Password);
            bool InChangeRequest = _parameterRepository.InChangesRequest(param.Id);

           

            ViewData["InChangeRequest"] = InChangeRequest;
            ViewData["Parameter"] = param;
            return View("Parameter", data);
        }

    }
}