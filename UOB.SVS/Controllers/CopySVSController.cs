﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using UOB.SVS.Models;
using Treemas.Base.Utilities;
using UOB.SVS.Resources;
using UOB.SVS.Model;
using UOB.SVS.Interface;
using Treemas.Base.Utilities.Queries;
using System.Linq;
using Treemas.Credential.Model;


namespace UOB.SVS.Controllers
{
    public class CopySVSController : PageController
    {
        private ISignatureHRepository _hRepository;
        private ISignatureHReqRepository _signatureHReqRepository;
        private ISignatureHReqService _signatureHReqService;
        private IAccountNoViewRepository _accNoVwRepo;
        private IAuditTrailLog _auditRepository;
        private IBranchRepository _branchRepository;
        private IDocumentTypeRepository _docTypeRepository;
        private IAccountMasterRepository _accountMasterRepository;

        private SignatureHeader reqH;
        private static SignatureDKTP reqKTP;
        private static SignatureDSign reqTTD;
        private static SignatureDDoc reqDoc;

        private static string oldCIFNo_Copy;
        private static string oldAccountNo_Copy;

        public CopySVSController(ISessionAuthentication sessionAuthentication, ISignatureHRepository hRepository,
            ISignatureHReqRepository signatureHReqRepository, IBranchRepository branchRepository, IDocumentTypeRepository docTypeRepository,
            ISignatureHReqService signatureHReqService, IAccountNoViewRepository accNoVwRepo, IAuditTrailLog auditRepository, IAccountMasterRepository accountMasterRepository) : base(sessionAuthentication)
        {
            this._hRepository = hRepository;
            this._signatureHReqRepository = signatureHReqRepository;
            this._signatureHReqService = signatureHReqService;
            this._accNoVwRepo = accNoVwRepo;
            this._auditRepository = auditRepository;
            this._branchRepository = branchRepository;
            this._docTypeRepository = docTypeRepository;
            this._accountMasterRepository = accountMasterRepository;
            Settings.ModuleName = "CopySVS";
            Settings.Title = "Copy SVS";
        }

        protected override void Startup()
        {

        }

        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.
                User user = Lookup.Get<User>();

                SVSReqFilter filter = new SVSReqFilter();
                //filter.Branches = getBranches();
                string CIFNo = Request.QueryString.GetValues("CIFNo")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                filter.CIFNumber = CIFNo;
                // Loading.   
                PagedResult<SignatureHeader> data;

                data = _hRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, filter);
                var items = data.Items.ToList().ConvertAll<SignatureHReqView>(new Converter<SignatureHeader, SignatureHReqView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }


        public SignatureHReqView ConvertFrom(SignatureHeader item)
        {
            User user = Lookup.Get<User>();

            SignatureHReqView returnItem = new SignatureHReqView();
            returnItem.AccountNo = item.AccountNo;
            returnItem.AccountName = item.AccountName;
            returnItem.AccountType = item.AccountType;
            returnItem.CIFNumber = item.CIFNumber;
            returnItem.BranchCode = item.BranchCode;
            returnItem.Note = item.Note;
            returnItem.RequestType = "U";
            returnItem.RequestDate = DateTime.Now;
            returnItem.RequestUser = user.Username;
            returnItem.RequestReason = "";

            int index = 1;
            if (item.Documents != null)
            {
                IList<Document> docs = new List<Document>();
                foreach (SignatureDDoc row in item.Documents)
                {
                    Document doc = new Document();
                    doc.ID = row.Id;
                    doc.Index = index++;
                    doc.DocumentType = row.DocumentType;
                    doc.FileBlob = row.FileBlob;
                    doc.FileType = row.FileType;
                    doc.AccountNo = row.AccountNo;
                    doc.IsNew = true;
                    docs.Add(doc);
                }
                returnItem.Documents = docs.ToList();
            }

            if (item.IdentityCards != null)
            {
                index = 1;
                IList<IdentityCard> idcards = new List<IdentityCard>();
                foreach (SignatureDKTP row in item.IdentityCards)
                {
                    IdentityCard idcard = new IdentityCard();
                    idcard.ID = row.Id;
                    idcard.Index = index++;
                    idcard.CroppedBlob = row.KTP;
                    idcard.ImageType = row.ImageType;
                    idcard.AccountNo = row.AccountNo;
                    idcard.IsNew = true;
                    idcards.Add(idcard);
                }
                returnItem.IdentityCards = idcards.ToList();
            }

            if (item.Signatures != null)
            {
                index = 1;
                IList<Signature> signs = new List<Signature>();
                foreach (SignatureDSign row in item.Signatures)
                {
                    Signature sign = new Signature();
                    sign.ID = row.Id;
                    sign.Index = index++;
                    sign.CroppedBlob = row.Signature;
                    sign.ImageType = row.ImageType;
                    sign.AccountNo = row.AccountNo;
                    sign.IsNew = true;
                    signs.Add(sign);
                }
                returnItem.Signatures = signs.ToList();
            }

            return returnItem;
        }


        public SignatureHReqView ConvertFromReq(SignatureHRequest item)
        {
            SignatureHReqView returnItem = new SignatureHReqView();
            returnItem.AccountNo = item.AccountNo;
            returnItem.AccountName = item.AccountName;
            returnItem.AccountType = item.AccountType;
            returnItem.CIFNumber = item.CIFNumber;
            returnItem.BranchCode = item.BranchCode;
            returnItem.Note = item.Note;
            returnItem.RequestType = item.RequestType;
            returnItem.RequestDate = item.RequestDate;
            returnItem.RequestUser = item.RequestUser;
            returnItem.RequestReason = item.RequestReason;

            int index = 1;
            if (item.Documents != null)
            {
                IList<Document> docs = new List<Document>();
                foreach (SignatureDDocReq row in item.Documents)
                {
                    Document doc = new Document();
                    doc.ID = row.Id;
                    doc.Index = index++;
                    doc.DocumentType = row.DocumentType;
                    doc.FileBlob = row.FileBlob;
                    doc.FileType = row.FileType;
                    doc.AccountNo = row.AccountNo;
                    doc.IsNew = false;
                    docs.Add(doc);
                }
                returnItem.Documents = docs.ToList();
            }

            if (item.IdentityCards != null)
            {
                index = 1;
                IList<IdentityCard> idcards = new List<IdentityCard>();
                foreach (SignatureDKTPReq row in item.IdentityCards)
                {
                    IdentityCard idcard = new IdentityCard();
                    idcard.ID = row.Id;
                    idcard.Index = index++;
                    idcard.CroppedBlob = row.KTP;
                    idcard.ImageType = row.ImageType;
                    idcard.AccountNo = row.AccountNo;
                    idcard.IsNew = false;
                    idcards.Add(idcard);
                }
                returnItem.IdentityCards = idcards.ToList();
            }

            if (item.Signatures != null)
            {
                index = 1;
                IList<Signature> signs = new List<Signature>();
                foreach (SignatureDSignReq row in item.Signatures)
                {
                    Signature sign = new Signature();
                    sign.ID = row.Id;
                    sign.Index = index++;
                    sign.CroppedBlob = row.Signature;
                    sign.ImageType = row.ImageType;
                    sign.AccountNo = row.AccountNo;
                    sign.IsNew = false;
                    signs.Add(sign);
                }
                returnItem.Signatures = signs.ToList();
            }
            return returnItem;
        }


        //public ActionResult Edit(string id)
        //{
        //    Parameter param = Lookup.Get<Parameter>();

        //    ViewData["Parameter"] = param;
        //    ViewData["ActionName"] = "Edit";
        //    SignatureHRequest reqH = _signatureHReqRepository.getSignatureHReq(id);
        //    reqH.IdentityCards = _signatureHReqRepository.getKTPReqD(reqH.AccountNo);
        //    reqH.Documents = _signatureHReqRepository.getDocReqD(reqH.AccountNo);
        //    reqH.Signatures = _signatureHReqRepository.getSignReqD(reqH.AccountNo);
        //    SignatureHReqView data = ConvertFromReq(reqH);

        //    oldAccountNo_Copy = data.AccountNo;
        //    oldCIFNo_Copy = data.CIFNumber;

        //    return CreateView(data);
        //}


            

        public ActionResult Edit(string m)
        {
            User user = Lookup.Get<User>();
            Parameter param = Lookup.Get<Parameter>();
            ViewData["ActionName"] = "Copy";
            ViewData["Parameter"] = param;

            SignatureHeader reqH = _hRepository.getSignatureHeader(m);
            if (reqH.IsNull())
            {
                ScreenMessages.Submit(ScreenMessage.Error("Account number does not exist."));
                return RedirectToAction("Index");
            }
            var branches = getBranches();
            if (user.BranchCode != "000" && !branches.Exists(x => x.BranchCode == reqH.BranchCode))
            {
                ScreenMessages.Submit(ScreenMessage.Error("Account number does not belong to your branch."));
                return RedirectToAction("Index");
            }
            reqH.IdentityCards = _hRepository.getKTPDet(reqH.AccountNo);
            reqH.Documents = _hRepository.getDocDet(reqH.AccountNo);
            reqH.Signatures = _hRepository.getSignDet(reqH.AccountNo);

            SignatureHReqView data = ConvertFrom(reqH);

            oldAccountNo_Copy = data.AccountNo;
            oldCIFNo_Copy = data.CIFNumber;

            return CreateView(data);
        }

        [HttpPost]
        public ActionResult Edit(SignatureHReqView m)
        {
            User user = Lookup.Get<User>();
            Parameter param = Lookup.Get<Parameter>();
            ViewData["ActionName"] = "Copy";
            ViewData["Parameter"] = param;

            SignatureHeader reqH = _hRepository.getSignatureHeader(m.AccountNo);
            if (reqH.IsNull())
            {
                ScreenMessages.Submit(ScreenMessage.Error("Account number does not exist."));
                return RedirectToAction("Index");
            }
            var branches = getBranches();
            if (user.BranchCode != "000" && !branches.Exists(x => x.BranchCode == reqH.BranchCode))
            {
                ScreenMessages.Submit(ScreenMessage.Error("Account number does not belong to your branch."));
                return RedirectToAction("Index");
            }
            reqH.IdentityCards = _hRepository.getKTPDet(reqH.AccountNo);
            reqH.Documents = _hRepository.getDocDet(reqH.AccountNo);
            reqH.Signatures = _hRepository.getSignDet(reqH.AccountNo);

            SignatureHReqView data = ConvertFrom(reqH);


            oldAccountNo_Copy = data.AccountNo;
            oldCIFNo_Copy = data.CIFNumber;

            return CreateView(data);
        }

        // GET: Function/Create
        public ActionResult Copy()
        {
            Parameter param = Lookup.Get<Parameter>();

            ViewData["ActionName"] = "Copy";
            ViewData["Parameter"] = param;
            SignatureHReqView data = new SignatureHReqView();
            return CreateView(data);
        }

        [HttpPost]
        public ActionResult Copy(SignatureHReqView data)
        {
            ViewData["ActionName"] = "Copy";
            User user = Lookup.Get<User>();

            string message = "";
            string returnCode = "";
            string warningmessage = "";
            string AccountNo = "";

            try
            {
                AccountMaster accMaster = _accountMasterRepository.getAccount(data.AccountNo);

                message = validateData(data, "Create", accMaster, user);
                if (message.IsNullOrEmpty())
                {
                    SignatureHRequest newData = new SignatureHRequest("");
                    AccountNo = data.AccountNo;
                    newData.AccountNo = data.AccountNo;
                    newData.AccountName = data.AccountName;
                    newData.AccountType = data.AccountType;
                    newData.CIFNumber = data.CIFNumber;
                    newData.BranchCode = data.BranchCode;
                    newData.Note = data.Note;
                    newData.RequestType = "C";
                    newData.RequestDate = DateTime.Now;
                    newData.RequestUser = user.Username;
                    newData.RequestReason = "Copy SVS";
                    newData.IsRejected = false;

                    warningmessage = validateAccount(data, accMaster, user);
                    message = _signatureHReqService.SaveSignatureReq(newData);

                    // Hapus detail data, biar audit trailnya ngak penuh
                    newData.Documents = null;
                    newData.Signatures = null;
                    newData.IdentityCards = null;
                }
            }
            catch (Exception e)
            {
                message = e.Message;
                returnCode = "ERR";
                //ScreenMessages.Submit(ScreenMessage.Error(message));
            }

            if (message.IsNullOrEmpty())
            {
                returnCode = "OK";
                ScreenMessages.Submit(ScreenMessage.Success(SvsResources.Validation_Approval));
            }
            else
            {
                returnCode = "ERR";
                ScreenMessages.Submit(ScreenMessage.Error(message));
            }

            JsonResult result = new JsonResult();
            result = this.Json(new
            {
                message = message,
                returnCode = returnCode,
                warningMessage = warningmessage,
                AccountNo = AccountNo
            });

            return result;
        }




        private ViewResult CreateView(SignatureHReqView data)
        {
            ViewData["BranchMaster"] = createBranchSelect(data.BranchCode);
            ViewData["DocTypeMaster"] = createDocTypeSelect("");
            ViewData["AccTypeMaster"] = createAccountTypeSelect(data.AccountType);
            return View("Detail", data);
        }

        //// POST: Delete
        //[HttpPost]
        //public ActionResult Update(SignatureHReqView data)
        //{
        //    ViewData["ActionName"] = "Update";
        //    User user = Lookup.Get<User>();
            
        //    string message = "";
        //    string returnCode = "";
        //    string warningmessage = "";
        //    string AccountNo = "";

        //    try
        //    {
        //        AccountMaster accMaster = _accountMasterRepository.getAccount(data.AccountNo);

        //        message = validateData(data, "Create", accMaster, user);
        //        if (message.IsNullOrEmpty())
        //        {
        //            SignatureHRequest newData = new SignatureHRequest("");
        //            AccountNo = data.AccountNo;
        //            newData.AccountNo = data.AccountNo;
        //            newData.AccountName = data.AccountName;
        //            newData.AccountType = data.AccountType;
        //            newData.CIFNumber = data.CIFNumber;
        //            newData.BranchCode = data.BranchCode;
        //            newData.Note = data.Note;
        //            newData.RequestType = "C";
        //            newData.RequestDate = DateTime.Now;
        //            newData.RequestUser = user.Username;
        //            newData.RequestReason = "Copy SVS";
        //            newData.IsRejected = false;

        //            warningmessage = validateAccount(data, accMaster, user);
        //            message = _signatureHReqService.SaveSignatureReq(newData);

        //            // Hapus detail data, biar audit trailnya ngak penuh
        //            newData.Documents = null;
        //            newData.Signatures = null;
        //            newData.IdentityCards = null;
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        message = e.Message;
        //        returnCode = "ERR";
        //        //ScreenMessages.Submit(ScreenMessage.Error(message));
        //    }

        //    if (message.IsNullOrEmpty())
        //    {
        //        returnCode = "OK";
        //        ScreenMessages.Submit(ScreenMessage.Success(SvsResources.Validation_Approval));
        //    }
        //    else
        //    {
        //        returnCode = "ERR";
        //        ScreenMessages.Submit(ScreenMessage.Error(message));
        //    }

        //    JsonResult result = new JsonResult();
        //    result = this.Json(new
        //    {
        //        message = message,
        //        returnCode = returnCode,
        //        warningMessage = warningmessage,
        //        AccountNo = AccountNo
        //    });

        //    return result;
        //}

        private string validateData(SignatureHReqView data, string mode, AccountMaster accMaster, User user)
        {
            user = Lookup.Get<User>();

            var CIFExist = _accountMasterRepository.getAccountbyCIF(data.CIFNumber);

            if (user.BranchCode != "000")
            {

                if (user.BranchCode != data.BranchCode)
                    return SvsResources.Validation_BranchCodeNotMatch;
            }

            if (data.AccountNo == "")
                return SvsResources.Validation_FillAccountNo;

            if (data.AccountName == "")
                return SvsResources.Validation_FillCustName;

            if (CIFExist == null)
                return "CIF is not exists in Account Master!";

            if (oldCIFNo_Copy != data.CIFNumber)
                return "CIF is different!";

            if (mode == "Create")
            {
                if (_accNoVwRepo.IsDuplicate(data.AccountNo))
                    return SvsResources.Validation_DuplicateData;
            }

            return "";
        }

        private string validateAccount(SignatureHReqView data, AccountMaster accMaster, User user)
        {
            if (accMaster == null)
            {
                return SvsResources.Validation_AccountMasterNotFound;
            }

            string message = "";

            if (accMaster.AccountType.Trim() != data.AccountType)
                message = SvsResources.Validation_AccountTypeNotMatch;

            if (accMaster.BranchCode != data.BranchCode)
                message = message == "" ? SvsResources.Validation_BranchCodeNotMatch : ", " + SvsResources.Validation_BranchCodeNotMatch;

            if (accMaster.CIFNo != data.CIFNumber)
                message = message == "" ? SvsResources.Validation_CIFNumberNotMatch : ", " + SvsResources.Validation_CIFNumberNotMatch;

            return "";
        }

        #region Dropdownlist

        private IList<SelectListItem> createBranchSelect(string selected)
        {
            //IList<SelectListItem> dataList = getBranches().ConvertAll<SelectListItem>(new Converter<Branch, SelectListItem>(ConvertFrom));
            IList<SelectListItem> dataList = _branchRepository.FindAll().ToList().ConvertAll<SelectListItem>(new Converter<Branch, SelectListItem>(ConvertFrom));
            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected).Selected = true;
            }
            return dataList;
        }

        public List<Branch> getBranches()
        {
            User user = Lookup.Get<User>();
            IList<string> branchs = new List<string>();
            if (user.BranchCode == "000")
            {
                return _branchRepository.getBranchs().ToList();
            }
            else
            {
                branchs.Add(user.BranchCode);
                return _branchRepository.getBranchs(branchs).ToList();
            }
        }

        public SelectListItem ConvertFrom(Branch item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.BranchCode;
            returnItem.Value = item.BranchCode;
            return returnItem;
        }

        private IList<SelectListItem> createDocTypeSelect(string selected)
        {
            IList<SelectListItem> dataList = _docTypeRepository.FindAll().ToList().ConvertAll<SelectListItem>(new Converter<DocumentType, SelectListItem>(ConvertFrom));
            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected).Selected = true;
            }
            return dataList;
        }

        public SelectListItem ConvertFrom(DocumentType item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.Description;
            returnItem.Value = item.DocType;
            return returnItem;
        }

        private IList<SelectListItem> createAccountTypeSelect(string selected)
        {
            IList<SelectListItem> dataList = new List<SelectListItem>();
            dataList.Insert(0, new SelectListItem() { Value = "CS", Text = "CA/SA" });
            dataList.Insert(1, new SelectListItem() { Value = "TD", Text = "TD" });

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected.ToString()).Selected = true;
            }
            return dataList;
        }

        #endregion
    }
}