﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using UOB.SVS.Models;
using Treemas.Base.Utilities;
using UOB.SVS.Resources;
using UOB.SVS.Model;
using UOB.SVS.Interface;
using Treemas.Base.Utilities.Queries;
using System.Linq;
using Treemas.Credential.Model;
using SpreadsheetLight;
using System.Data;

namespace UOB.SVS.Controllers
{
    public class PurgingSVSController : PageController
    {
        private ISignatureHRepository _hRepository;
        private IAuditTrailLog _auditRepository;
        private IBranchRepository _branchRepository;
        private IDocumentTypeRepository _docTypeRepository;

        public PurgingSVSController(ISessionAuthentication sessionAuthentication, ISignatureHRepository hRepository,
            IBranchRepository branchRepository, IDocumentTypeRepository docTypeRepository,
            IAuditTrailLog auditRepository) : base(sessionAuthentication)
        {
            this._hRepository = hRepository;
            this._auditRepository = auditRepository;
            this._branchRepository = branchRepository;
            this._docTypeRepository = docTypeRepository;
            Settings.ModuleName = "SVS Purging Data";
            Settings.Title = "SVS";
        }

        protected override void Startup()
        {

        }

        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.   
                SVSReqFilter filter = new SVSReqFilter();
                //filter.TanggalStart = Convert.ToDateTime(Request.QueryString.GetValues("TanggalStart")[0]);
                //filter.TanggalEnd = Convert.ToDateTime(Request.QueryString.GetValues("TanggalEnd")[0]);
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.   
                PagedResult<SignatureHeader> data;
                //if (!searchValue.IsNullOrEmpty())
                //{
                //    if (searchColumn == "AccountNo")
                //        filter.AccountNo = searchValue;
                //    else if (searchColumn == "AccountName")
                //        filter.AccountName = searchValue;
                //    else if (searchColumn == "CIFNumber")
                //        filter.CIFNumber = searchValue;
                //}

                data = _hRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, filter);
                var items = data.Items.ToList().ConvertAll<SignatureHView>(new Converter<SignatureHeader, SignatureHView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public SignatureHView ConvertFrom(SignatureHeader item)
        {
            SignatureHView returnItem = new SignatureHView();
            returnItem.AccountNo = item.AccountNo;
            returnItem.AccountName = item.AccountName;
            returnItem.AccountType = item.AccountType;
            returnItem.CIFNumber = item.CIFNumber;
            returnItem.BranchCode = item.BranchCode;
            returnItem.Note = item.Note;

            int index = 1;
            if (item.Documents != null)
            {
                IList<Document> docs = new List<Document>();
                foreach (SignatureDDoc row in item.Documents)
                {
                    Document doc = new Document();
                    doc.Index = index++;
                    doc.DocumentType = row.DocumentType;
                    doc.FileBlob = row.FileBlob;
                    doc.FileType = row.FileType;
                    docs.Add(doc);
                }
                returnItem.Documents = docs.ToList();
            }

            if (item.IdentityCards != null)
            {
                index = 1;
                IList<IdentityCard> idcards = new List<IdentityCard>();
                foreach (SignatureDKTP row in item.IdentityCards)
                {
                    IdentityCard idcard = new IdentityCard();
                    idcard.Index = index++;
                    idcard.CroppedBlob = row.KTP;
                    idcard.ImageType = row.ImageType;
                    idcards.Add(idcard);
                }
                returnItem.IdentityCards = idcards.ToList();
            }

            if (item.Signatures != null)
            {
                index = 1;
                IList<Signature> signs = new List<Signature>();
                foreach (SignatureDSign row in item.Signatures)
                {
                    Signature sign = new Signature();
                    sign.Index = index++;
                    sign.CroppedBlob = row.Signature;
                    sign.ImageType = row.ImageType;
                    signs.Add(sign);
                }
                returnItem.Signatures = signs.ToList();
            }
            return returnItem;
        }


        public ActionResult Edit(string id)
        {
            SignatureHeader signH = _hRepository.getSignatureHeader(id);
            if (signH.IsNull())
            {
                ScreenMessages.Submit(ScreenMessage.Error("Account number does not exist."));
                return RedirectToAction("Index");
            }
            signH.IdentityCards = _hRepository.getKTPDet(signH.AccountNo);
            signH.Documents = _hRepository.getDocDet(signH.AccountNo);
            signH.Signatures = _hRepository.getSignDet(signH.AccountNo);
            SignatureHView data = ConvertFrom(signH);

            return CreateView(data);
        }

        private IList<SelectListItem> createBranchSelect(string selected)
        {
            IList<SelectListItem> dataList = _branchRepository.FindAll().ToList().ConvertAll<SelectListItem>(new Converter<Branch, SelectListItem>(ConvertFrom));
            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected).Selected = true;
            }
            return dataList;
        }

        private IList<SelectListItem> createAccountTypeSelect(string selected)
        {
            IList<SelectListItem> dataList = new List<SelectListItem>();
            dataList.Insert(0, new SelectListItem() { Value = "CA/SA", Text = "CA/SA" });
            dataList.Insert(1, new SelectListItem() { Value = "TD", Text = "TD" });
            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected).Selected = true;
            }
            return dataList;
        }

        public SelectListItem ConvertFrom(Branch item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.BranchCode;
            returnItem.Value = item.BranchCode;
            return returnItem;
        }
        private IList<SelectListItem> createDocTypeSelect(string selected)
        {
            IList<SelectListItem> dataList = _docTypeRepository.FindAll().ToList().ConvertAll<SelectListItem>(new Converter<DocumentType, SelectListItem>(ConvertFrom));
            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected).Selected = true;
            }
            return dataList;
        }

        public SelectListItem ConvertFrom(DocumentType item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.Description;
            returnItem.Value = item.DocType;
            return returnItem;
        }

        private ViewResult CreateView(SignatureHView data)
        {
            ViewData["BranchMaster"] = createBranchSelect("");
            ViewData["DocTypeMaster"] = createDocTypeSelect("");
            ViewData["AccTypeList"] = createAccountTypeSelect("");
            return View("Detail", data);
        }

        public ActionResult Export()
        {
            var headerList = _hRepository.FindAll();

            IList<SignatureHView> items = headerList.ToList().ConvertAll<SignatureHView>(new Converter<SignatureHeader, SignatureHView>(ConvertFrom));

            DataTable dt = items.ListToDataTable();
            //List<UserView> list = new List<UserView>();
           
            MemoryStream ms = new MemoryStream();
            using (SLDocument sl = new SLDocument())
            {
                sl.ImportDataTable(1, 1, dt, true);
                sl.SelectWorksheet("Worksheet");
                sl.SaveAs(ms);
            }

            ms.Position = 0;
            string fName = "SVS_Purging_Data.xls";
            return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fName);
        }
    }
}