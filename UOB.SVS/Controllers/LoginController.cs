﻿using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using Treemas.Base.Utilities;
using Treemas.Credential.Model;

namespace UOB.SVS.Controllers
{
    public class LoginController : LoginPageController
    {
        private IUserAccount _userAccount;

        public LoginController(IUserAccount userAccount, ISessionAuthentication sessionAuthentication,
            ISSOSessionStorage ssoSessionStorage, IParameterRepository InquiryParameter) : base(userAccount, sessionAuthentication, ssoSessionStorage, InquiryParameter)
        {
            this._userAccount = userAccount;
        }

        protected override void Startup()
        {
        }

    }
}