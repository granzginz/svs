﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using UOB.SVS.Models;
using Treemas.Base.Utilities;
using UOB.SVS.Resources;
using UOB.SVS.Model;
using UOB.SVS.Interface;
using Treemas.Base.Utilities.Queries;
using System.Linq;
using Treemas.Credential.Model;


namespace UOB.SVS.Controllers
{
    public class WaitingApprovalController : PageController
    {
        private ISignatureHReqRepository _hReqRepository;
        private ISignatureHReqService _signatureHReqService;
        private IAuditTrailLog _auditRepository;
        private IBranchRepository _branchRepository;
        private IDocumentTypeRepository _docTypeRepository;
        private IAccountNoViewRepository _accNoVwRepo;
        private IAccountMasterRepository _accountMasterRepository;

        public WaitingApprovalController(ISessionAuthentication sessionAuthentication, ISignatureHReqRepository hReqRepository,
            IBranchRepository branchRepository, IDocumentTypeRepository docTypeRepository, ISignatureHReqService signatureHReqService,
            IAccountNoViewRepository accNoVwRepo, IAuditTrailLog auditRepository, IAccountMasterRepository accountMasterRepository) : base(sessionAuthentication)
        {
            this._hReqRepository = hReqRepository;
            this._signatureHReqService = signatureHReqService;
            this._auditRepository = auditRepository;
            this._branchRepository = branchRepository;
            this._docTypeRepository = docTypeRepository;
            this._accNoVwRepo = accNoVwRepo;
            this._accountMasterRepository = accountMasterRepository;
            Settings.ModuleName = "WaitingApprooval";
            Settings.Title = "SVS Waiting Approval";
        }

        protected override void Startup()
        {

        }

        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            try
            {
                User user = Lookup.Get<User>();

                // Initialization.   
                SVSReqFilter filter = new SVSReqFilter();
                filter.Branches = getBranches();
                filter.RequestUser = user.Username;
                filter.RequestType = Request.QueryString.GetValues("reqType")[0];
                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.   
                PagedResult<SignatureHRequest> data;
                if (!searchValue.IsNullOrEmpty())
                {
                    if (searchColumn == "AccountNumber")
                        filter.AccountNo = searchValue;
                    else if (searchColumn == "AccountName")
                        filter.AccountName = searchValue;
                }

                data = _hReqRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, filter);
                var items = data.Items.ToList().ConvertAll<SignatureHReqView>(new Converter<SignatureHRequest, SignatureHReqView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public SignatureHReqView ConvertFrom(SignatureHRequest item)
        {
            SignatureHReqView returnItem = new SignatureHReqView();
            returnItem.AccountNo = item.AccountNo;
            returnItem.AccountName = item.AccountName;
            returnItem.AccountType = item.AccountType;
            returnItem.CIFNumber = item.CIFNumber;
            returnItem.BranchCode = item.BranchCode;
            returnItem.Note = item.Note;
            returnItem.RequestType = item.RequestType;
            returnItem.RequestDate = item.RequestDate;
            returnItem.RequestDateString = item.RequestDate.Value.ToString("dd/MM/yyyy");
            returnItem.RequestUser = item.RequestUser;
            returnItem.RequestReason = item.RequestReason;
            returnItem.IsRejected = item.IsRejected;

            int index = 1;
            if (item.Documents != null)
            {
                IList<Document> docs = new List<Document>();
                foreach (SignatureDDocReq row in item.Documents)
                {
                    Document doc = new Document();
                    doc.ID = row.Id;
                    doc.Index = index++;
                    doc.DocumentType = row.DocumentType;
                    doc.FileBlob = row.FileBlob;
                    doc.FileType = row.FileType;
                    doc.AccountNo = row.AccountNo;
                    doc.IsNew = false;
                    docs.Add(doc);
                }
                returnItem.Documents = docs.ToList();
            }

            if (item.IdentityCards != null)
            {
                index = 1;
                IList<IdentityCard> idcards = new List<IdentityCard>();
                foreach (SignatureDKTPReq row in item.IdentityCards)
                {
                    IdentityCard idcard = new IdentityCard();
                    idcard.ID = row.Id;
                    idcard.Index = index++;
                    idcard.CroppedBlob = row.KTP;
                    idcard.ImageType = row.ImageType;
                    idcard.AccountNo = row.AccountNo;
                    idcard.IsNew = false;
                    idcards.Add(idcard);
                }
                returnItem.IdentityCards = idcards.ToList();
            }

            if (item.Signatures != null)
            {
                index = 1;
                IList<Signature> signs = new List<Signature>();
                foreach (SignatureDSignReq row in item.Signatures)
                {
                    Signature sign = new Signature();
                    sign.ID = row.Id;
                    sign.Index = index++;
                    sign.CroppedBlob = row.Signature;
                    sign.ImageType = row.ImageType;
                    sign.AccountNo = row.AccountNo;
                    sign.IsNew = false;
                    signs.Add(sign);
                }
                returnItem.Signatures = signs.ToList();
            }
            return returnItem;
        }


        public ActionResult Edit(string id)
        {
            Parameter param = Lookup.Get<Parameter>();

            ViewData["Parameter"] = param;
            ViewData["ActionName"] = "Edit";
            SignatureHRequest reqH = _hReqRepository.getSignatureHReq(id);
            reqH.IdentityCards = _hReqRepository.getKTPReqD(reqH.AccountNo);
            reqH.Documents = _hReqRepository.getDocReqD(reqH.AccountNo);
            reqH.Signatures = _hReqRepository.getSignReqD(reqH.AccountNo);
            SignatureHReqView data = ConvertFrom(reqH);

            return CreateView(data);
        }

        private ViewResult CreateView(SignatureHReqView data)
        {
            ViewData["BranchMaster"] = createBranchSelect(data.BranchCode);
            ViewData["DocTypeMaster"] = createDocTypeSelect("");
            ViewData["AccTypeMaster"] = createAccountTypeSelect(data.AccountType);
            if (data.RequestType == "D")
            {
                return View("View", data);
            } else
            {
                return View("Detail", data);
            }
        }

        // POST: Role/Edit
        [HttpPost]
        public ActionResult Edit(SignatureHReqView data)
        {
            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            string message = "";
            string returnCode = "";
            string warningmessage = "";
            string AccountNo = "";
            try
            {
                AccountMaster accMaster = _accountMasterRepository.getAccount(data.AccountNo);

                message = validateData(data, "Edit");
                if (message.IsNullOrEmpty())
                {

                    SignatureHRequest reqH = _hReqRepository.getSignatureHReq(data.AccountNo);
                    if (reqH.IsNull())
                    {
                        message = "NF";
                        returnCode = "NF";
                        ScreenMessages.Submit(ScreenMessage.Error("Data Has Been Approved!"));
                    }
                    else
                    {
                        AccountNo = data.AccountNo;
                        reqH.AccountName = data.AccountName;
                        reqH.AccountType = data.AccountType;
                        reqH.CIFNumber = data.CIFNumber;
                        reqH.BranchCode = data.BranchCode;
                        reqH.Note = data.Note;
                        reqH.RequestDate = DateTime.Now;

                        IList<SignatureDDocReq> docs = new List<SignatureDDocReq>();
                        if (data.DeletedDocuments != null)
                        {
                            foreach (DeleteFormat item in data.DeletedDocuments)
                            {
                                SignatureDDocReq doc = new SignatureDDocReq(item.ID);
                                docs.Add(doc);
                            }
                        }
                        reqH.Documents = docs;

                        IList<SignatureDKTPReq> idcards = new List<SignatureDKTPReq>();
                        if (data.DeletedIdentityCards != null)
                        {
                            foreach (DeleteFormat item in data.DeletedIdentityCards)
                            {
                                SignatureDKTPReq idcard = new SignatureDKTPReq(item.ID);
                                idcards.Add(idcard);
                            }
                        }
                        reqH.IdentityCards = idcards;

                        IList<SignatureDSignReq> signs = new List<SignatureDSignReq>();
                        if (data.DeletedSignatures != null)
                        {
                            foreach (DeleteFormat item in data.DeletedSignatures)
                            {
                                SignatureDSignReq sign = new SignatureDSignReq(item.ID);
                                signs.Add(sign);
                            }
                        }
                        reqH.Signatures = signs;

                        warningmessage = validateAccount(data, accMaster);
                        message = _signatureHReqService.UpdateSignatureReq(reqH);

                        // Hapus detail data, biar audit trailnya ngak penuh
                        reqH.Documents = null;
                        reqH.Signatures = null;
                        reqH.IdentityCards = null;
                    }
                }
            }
            catch (Exception e)
            {
                message = e.Message;
                returnCode = "ERR";
                //ScreenMessages.Submit(ScreenMessage.Error(message));
            }

            if (message.IsNullOrEmpty())
            {
                returnCode = "OK";
                ScreenMessages.Submit(ScreenMessage.Success(SvsResources.Validation_Approval));
            }
            else
            {
                if (returnCode != "NF")
                {
                    returnCode = "ERR";
                }
                //ScreenMessages.Submit(ScreenMessage.Error(message));
            }

            JsonResult result = new JsonResult();
            result = this.Json(new
            {
                message = message,
                returnCode = returnCode,
                warningMessage = warningmessage,
                AccountNo = AccountNo
            });

            return result;
        }
        
        private string validateData(SignatureHReqView data, string mode)
        {
            if (data.AccountNo == "")
                return SvsResources.Validation_FillAccountNo;

            if (data.AccountName == "")
                return SvsResources.Validation_FillCustName;

            return "";
        }

        private string validateAccount(SignatureHReqView data, AccountMaster accMaster)
        {
            if (accMaster == null)
            {
                return SvsResources.Validation_AccountMasterNotFound;
            }

            string message = "";

            if (accMaster.AccountType != data.AccountType)
                message = SvsResources.Validation_AccountTypeNotMatch;

            if (accMaster.BranchCode != data.BranchCode)
                message = message == "" ? SvsResources.Validation_BranchCodeNotMatch : ", " + SvsResources.Validation_BranchCodeNotMatch;

            if (accMaster.CIFNo != data.CIFNumber)
                message = message == "" ? SvsResources.Validation_CIFNumberNotMatch : ", " + SvsResources.Validation_CIFNumberNotMatch;

            return "";
        }

        //Cancel submitted data
        [HttpPost]
        public ActionResult Reject(string Id)
        {
            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            string message = "";
            string returnCode = "";

            try
            {
                //message = validateData(FixedAssetMaster, "Edit");
                if (message.IsNullOrEmpty())
                {
                    SignatureHRequest reqH = _hReqRepository.getSignatureHReq(Id);

                    message = _signatureHReqService.CancelSignatureReq(reqH);

                    // Hapus detail data, biar audit trailnya ngak penuh
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(SvsResources.Validation_Cancel));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            //ini kalau ga ke index
            //CollectScreenMessages();
            //return CreateView(data); //ini akan problem gara2 si dropdown di disable, harus kasih hidden valuenya si dropdown di detail

            //ini kalau ke index
            //CollectScreenMessages(); ga perlu karena ke index otomatis collect, kalo ada ini malah ga keluar messagenya by Ivan
            return RedirectToAction("Index");
        }

        #region Dropdownlist
        private IList<SelectListItem> createBranchSelect(string selected)
        {
            IList<SelectListItem> dataList = getBranches().ConvertAll<SelectListItem>(new Converter<Branch, SelectListItem>(ConvertFrom));
            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected).Selected = true;
            }
            return dataList;
        }

        public List<Branch> getBranches()
        {
            User user = Lookup.Get<User>();
            IList<string> branchs = new List<string>();
            if (user.BranchCode == "000")
            {
                return _branchRepository.getBranchs().ToList();
            }
            else
            {
                branchs.Add(user.BranchCode);
                return _branchRepository.getBranchs(branchs).ToList();
            }
        }

        public SelectListItem ConvertFrom(Branch item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.BranchCode;
            returnItem.Value = item.BranchCode;
            return returnItem;
        }

        private IList<SelectListItem> createAccountTypeSelect(string selected)
        {
            IList<SelectListItem> dataList = new List<SelectListItem>();
            dataList.Insert(0, new SelectListItem() { Value = "CS", Text = "CA/SA" });
            dataList.Insert(1, new SelectListItem() { Value = "TD", Text = "TD" });
            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected).Selected = true;
            }
            return dataList;
        }

        private IList<SelectListItem> createDocTypeSelect(string selected)
        {
            IList<SelectListItem> dataList = _docTypeRepository.FindAll().ToList().ConvertAll<SelectListItem>(new Converter<DocumentType, SelectListItem>(ConvertFrom));
            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected).Selected = true;
            }
            return dataList;
        }

        public SelectListItem ConvertFrom(DocumentType item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.Description;
            returnItem.Value = item.DocType;
            return returnItem;
        }
        #endregion
        
    }
}