﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using Treemas.Credential.Model;
using UOB.SVS.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using UOB.SVS.Resources;

namespace UOB.SVS.Controllers
{
    public class ApplicationController : PageController
    {
        private IApplicationRepository _appRepository;
        private IApplicationCrRepository _appcrRepository;
        private IAppAuditTrailLog _auditRepository;
        public ApplicationController(ISessionAuthentication sessionAuthentication,
            IApplicationRepository appRepository, IApplicationCrRepository appcrRepository, IAppAuditTrailLog auditRepository) : base(sessionAuthentication)
        {
            this._appRepository = appRepository;
            this._auditRepository = auditRepository;
            this._appcrRepository = appcrRepository;
            Settings.ModuleName = "Application";
            Settings.Title = ApplicationResources.PageTitle;
        }

        protected override void Startup()
        {
        }

        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.   
                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.   
                PagedResult<Application> data;
                if (searchValue.IsNullOrEmpty())
                {
                    data = _appRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir);
                }
                else
                {
                    data = _appRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchValue);
                }

                var items = data.Items.ToList().ConvertAll<ApplicationView>(new Converter<Application, ApplicationView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public ApplicationView ConvertFrom(Application item)
        {
            ApplicationView returnItem = new ApplicationView();
            returnItem.ApplicationId = item.ApplicationId;
            returnItem.Type = item.Type;
            returnItem.Runtime = item.Runtime;
            returnItem.Description = item.Description;
            returnItem.CSSColor = item.CSSColor;
            returnItem.Icon = item.Icon;
            returnItem.Id = item.Id;
            returnItem.Name = item.Name;
            return returnItem;
        }
        
        // GET: Function/Create
        public ActionResult Create()
        {
            ViewData["ActionName"] = "Create";
            ApplicationView data = new ApplicationView();
            return CreateView(data);
        }

        private ViewResult CreateView(ApplicationView data)
        {
            return View("Detail", data);
        }
        
        // POST: application/Create
        [HttpPost]
        public ActionResult Create(ApplicationView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                message = validateData(data, "Create");
                if (message.IsNullOrEmpty())
                {
                    //Application newData = new Application(0L);
                    ApplicationCr newData = new ApplicationCr(0L); //new modified by fahmi
                    newData.ApplicationId = data.ApplicationId;
                    newData.Name = data.Name;
                    newData.Type = data.Type;
                    newData.Runtime = data.Runtime;
                    newData.Description = data.Description;
                    newData.CSSColor = data.CSSColor;
                    newData.Icon = data.Icon;
                    newData.ChangedBy = user.Username;
                    newData.ChangedDate = DateTime.Now;
                    newData.CreatedBy = user.Username;
                    newData.CreatedDate = DateTime.Now;
                    //new modified by fahmi, approval additional field
                    newData.ApprovalType = "C";
                    newData.ApprovalStatus = "N";
                    newData.Approved = false;
                    //_appRepository.Add(newData);
                    _appcrRepository.Add(newData);//new modified by fahmi
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "CreateRequest");
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(SvsResources.Validation_Approval));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        private string validateData(ApplicationView data, string mode)
        {
            if (data.ApplicationId == "")
                return ApplicationResources.Validation_FillApplicationId;

            if (data.Name == "")
                return ApplicationResources.Validation_FillApplicationName;

            if (data.Type == "")
                return ApplicationResources.Validation_FillApplicationType;

            if (data.Runtime == "")
                return ApplicationResources.Validation_FillApplicationRuntime;

            if (mode == "Create")
            {
                if (_appRepository.IsDuplicate(data.ApplicationId))
                    return ApplicationResources.Validation_DuplicateData;
            }
            return "";
        }

        // GET: application/Edit/5
        public ActionResult Edit(long id)
        {
            ViewData["ActionName"] = "Edit";
            Application autoFeat = _appRepository.getApplication(id);
            ApplicationView data = ConvertFrom(autoFeat);

            return CreateView(data);
        }

        // POST: application/Edit/5
        [HttpPost]
        public ActionResult Edit(ApplicationView data)
        {
            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                message = validateData(data, "Edit");
                if (message.IsNullOrEmpty())
                {
                    //new additional by fahmi
                    ApplicationCr appCr = _appcrRepository.getExistingRequest(data.ApplicationId);
                    if (!appCr.IsNull())
                    {
                        _appcrRepository.deleteExistingRequest(data.ApplicationId);
                    }

                    //Application newData = new Application(data.Id);
                    ApplicationCr newData = new ApplicationCr(0L); //new modified by fahmi
                    //_auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "BeforeEdit");


                    newData.ApplicationId = data.ApplicationId;
                    newData.Name = data.Name;
                    newData.Type = data.Type;
                    newData.Runtime = data.Runtime;
                    newData.Description = data.Description;
                    newData.CSSColor = data.CSSColor;
                    newData.Icon = data.Icon;
                    newData.ChangedBy = user.Username;
                    newData.ChangedDate = DateTime.Now;
                    newData.CreatedBy = user.Username;
                    newData.CreatedDate = DateTime.Now;
                    //new modified by fahmi, approval additional field
                    newData.ApprovalType = "U";
                    newData.ApprovalStatus = "N";
                    newData.Approved = false;
                    //_appRepository.Add(newData);
                    _appcrRepository.Add(newData);//new modified by fahmi

                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "UpdateRequest");
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(SvsResources.Validation_Approval));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }


        // POST: application/Delete/5
        [HttpPost]
        public ActionResult Delete(long Id)
        {
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                Application appData = _appRepository.getApplication(Id);
                ApplicationCr newData = new ApplicationCr(0L); //new modified by fahmi

                newData.ApplicationId = appData.ApplicationId;
                newData.Name = appData.Name;
                newData.Type = appData.Type;
                newData.Runtime = appData.Runtime;
                newData.Description = appData.Description;
                newData.CSSColor = appData.CSSColor;
                newData.Icon = appData.Icon;
                newData.ChangedBy = user.Username;
                newData.ChangedDate = DateTime.Now;
                newData.CreatedBy = user.Username;
                newData.CreatedDate = DateTime.Now;
                //new modified by fahmi, approval additional field
                newData.ApprovalType = "D";
                newData.ApprovalStatus = "N";
                newData.Approved = false;
                //_appRepository.Add(newData);
                _appcrRepository.Add(newData);//new modified by fahmi

                _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "DeleteRequest");

                //_appRepository.Remove(newData);

                //_auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Delete");
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(SvsResources.Validation_Approval));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return Edit(Id);
        }
    }
}