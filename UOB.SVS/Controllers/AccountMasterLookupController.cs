﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using UOB.SVS.Models;
using Treemas.Base.Utilities;
using UOB.SVS.Resources;
using UOB.SVS.Model;
using UOB.SVS.Interface;
using Treemas.Base.Utilities.Queries;
using System.Linq;
using Treemas.Credential.Model;


namespace UOB.SVS.Controllers
{
    public class AccountMasterLookupController : PageController
    {
        private IAccountMasterRepository _hAccountMasterRepository;
        private IAuditTrailLog _auditRepository;
        private IBranchRepository _branchRepository;

        public AccountMasterLookupController(ISessionAuthentication sessionAuthentication, IAccountMasterRepository _hAccountMasterRepository,
            IBranchRepository branchRepository, IAuditTrailLog auditRepository) : base(sessionAuthentication)
        {
            this._hAccountMasterRepository = _hAccountMasterRepository;
            this._auditRepository = auditRepository;
            this._branchRepository = branchRepository;
            Settings.ModuleName = "Account Master Lookup";
            Settings.Title = "Account Master Lookup";
        }

        protected override void Startup()
        {

        }

        public ActionResult NoAuthCheckSearch()
        {
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.   
                SVSReqFilter filter = new SVSReqFilter();
                User user = Lookup.Get<User>();
                filter.Branches = getBranches();
                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                filter.AccountNo = "";
                filter.CIFNumber = "";
                // Loading.   
                PagedResult<AccountMaster> data;
                if (!searchValue.IsNullOrEmpty())
                {
                    if (searchColumn == "AccountNo")
                        filter.AccountNo = searchValue;
                    else if (searchColumn == "CIFNumber")
                        filter.CIFNumber = searchValue;
                }

                data = _hAccountMasterRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, filter,user);

                var items = data.Items.ToList().ConvertAll<AccountMasterReqView>(new Converter<AccountMaster, AccountMasterReqView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public AccountMasterReqView ConvertFrom(AccountMaster item)
        {
            AccountMasterReqView returnItem = new AccountMasterReqView();
            returnItem.AccountNo = item.AccountNo;
            returnItem.AccountName = item.AccountName;
            returnItem.AccountType = item.AccountType;
            returnItem.CIFNo       = item.CIFNo;
            returnItem.BranchCode  = item.BranchCode ;
            returnItem.Status      = item.Status;
            returnItem.DateOpened  = item.DateOpened ;
            return returnItem;
        }

        public List<Branch> getBranches()
        {
            User user = Lookup.Get<User>();
            IList<string> branchs = new List<string>();
            if (user.BranchCode == "000")
            {
                return _branchRepository.getBranchs().ToList();
            }
            else
            {
                branchs.Add(user.BranchCode);
                return _branchRepository.getBranchs(branchs).ToList();
            }
        }


    }
}