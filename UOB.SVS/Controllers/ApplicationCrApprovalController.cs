﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using Treemas.Credential.Model;
using UOB.SVS.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using UOB.SVS.Resources;
using System.Globalization;
using Treemas.Base.Resources;
using UOB.SVS.Interface;
using UOB.SVS.Model;

namespace UOB.SVS.Controllers
{
    public class ApplicationCrApprovalController : PageController
    {
        private IApplicationRepository _appRepository;
        private IApplicationCrRepository _appCrReporsitory;
        private IAppAuditTrailLog _auditRepository;
        private IFunctionRepository _funcRepository;
        private IFunctionCrRepository _functionCrRepository;
        private IFeatureRepository _featureRepository;
        private IFeatureCrRepository _featureCrRepository;
        private IRoleRepository _roleRepository;
        private IRoleCrRepository _roleCrRepository;
        private IMenuRepository _menuRepository;
        private IMenuCrRepository _menuCrRepository;
        private IUserRepository _userRepository;
        private IBranchRepository _branchRepository;
        private IBranchCrRepository _branchCrRepository;
        private IFunctionFeatureRepository _funcfeatRepository;
        private IFunctionFeatureCrRepository _funcfeatCrRepository;
        private IRoleFunctionRepository _rolefuncRepository;
        private IRoleFunctionCrRepository _rolefuncCrRepository;
        private IRoleFunctionFeatureCrRepository _rolefuncfeatCrRepository;
        private IRoleFunctionFeatureRepository _rolefuncfeatRepository;
        private IDocumentTypeCrRepository _documentTypeCrRepository;
        private IDocumentTypeRepository _documentTypeRepository;
        private IRoleDocTypeCrRepository _roleDocumentTypeCrRepository;
        private IRoleDocTypeRepository _roleDocumentTypeRepository;
        private IVApprovalRepository _VApprovalRepository;

        public ApplicationCrApprovalController(ISessionAuthentication sessionAuthentication, IFeatureRepository featureRepository,
            IApplicationRepository appRepository, IApplicationCrRepository appCrReporsitory, IFunctionRepository funcRepository,
        IFunctionCrRepository functionCrRepository, IAppAuditTrailLog auditRepository, IFeatureCrRepository featureCrRepository, IRoleRepository roleRepository,
        IRoleCrRepository roleCrRepository, IMenuRepository menuRepository, IMenuCrRepository menuCrRepository, IUserRepository userRepository,
        IBranchRepository branchRepository, IBranchCrRepository branchCrRepository, IFunctionFeatureRepository funcfeatRepository, IFunctionFeatureCrRepository funcfeatCrRepository,
        IRoleFunctionRepository rolefuncRepository, IRoleFunctionCrRepository rolefuncCrRepository, IRoleFunctionFeatureCrRepository rolefuncfeatCrRepository,
        IDocumentTypeCrRepository documentTypeCrRepository, IDocumentTypeRepository documentTypeRepository, IRoleDocTypeCrRepository roleDocumentTypeCrRepository, IRoleDocTypeRepository roleDocumentTypeRepository,
        IVApprovalRepository VApprovalRepository,
        IRoleFunctionFeatureRepository rolefuncfeatRepository) : base(sessionAuthentication)
        {
            this._appRepository = appRepository;
            this._auditRepository = auditRepository;
            this._appCrReporsitory = appCrReporsitory;
            this._functionCrRepository = functionCrRepository;
            this._funcRepository = funcRepository;
            this._featureRepository = featureRepository;
            this._featureCrRepository = featureCrRepository;
            this._roleRepository = roleRepository;
            this._roleCrRepository = roleCrRepository;
            this._menuRepository = menuRepository;
            this._menuCrRepository = menuCrRepository;
            this._userRepository = userRepository;
            this._branchRepository = branchRepository;
            this._branchCrRepository = branchCrRepository;
            this._funcfeatCrRepository = funcfeatCrRepository;
            this._funcfeatRepository = funcfeatRepository;
            this._rolefuncRepository = rolefuncRepository;
            this._rolefuncCrRepository = rolefuncCrRepository;
            this._rolefuncfeatCrRepository = rolefuncfeatCrRepository;
            this._rolefuncfeatRepository = rolefuncfeatRepository;
            this._documentTypeRepository = documentTypeRepository;
            this._documentTypeCrRepository = documentTypeCrRepository;
            this._roleDocumentTypeCrRepository = roleDocumentTypeCrRepository;
            this._roleDocumentTypeRepository = roleDocumentTypeRepository;
            this._VApprovalRepository = VApprovalRepository;

            Settings.ModuleName = "Application Changes Approval";
            Settings.Title = "Application Changes Approval";
        }
        protected override void Startup()
        {
            ViewData["ApprovalFunctionList"] = createApprovalFunctionSelect("");
            ViewData["ApprovalStatusList"] = createApprovalStatusSelect("");
            ViewData["ApprovalTypeList"] = createApprovalTypeSelect("");
            //ApplicationCr appCr = _appCrReporsitory.getChangesRequest();

            //if (appCr != null)
            //{
            //    param.UserId = Encryption.Instance.DecryptText(param.UserId);
            //    param.Password = Encryption.Instance.DecryptText(param.Password);
            //    param.NewUserId = Encryption.Instance.DecryptText(param.NewUserId);
            //    param.NewPassword = Encryption.Instance.DecryptText(param.NewPassword);

            //    ViewData["ParameterHistory"] = param;
            //}
        }

        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.   
                ApplicationCRFilter appFilter = new ApplicationCRFilter();
                appFilter.ApprovalType = Request.QueryString.GetValues("ApprovalType")[0];
                //appFilter.ApprovalStatus = Request.QueryString.GetValues("ApprovalStatus")[0];
                appFilter.ApprovalFuntion = Request.QueryString.GetValues("ApprovalFuntion")[0];
                string mode = Request.QueryString.GetValues("Mode")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.  

                PagedResult<VApproval> appData = _VApprovalRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, appFilter);

                //            
                var items = appData.Items.ToList().ConvertAll<ApplicationCrView>(new Converter<VApproval, ApplicationCrView>(ConvertFrom));



                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = appData.TotalItems,
                    recordsFiltered = appData.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }
        
        #region ConvertFromList

        public ApplicationCrView ConvertFrom(VApproval item)
        {
            ApplicationCrView returnItem = new ApplicationCrView();
            returnItem.Id = item.Id.ToString().Trim();
            returnItem.ApprovalFuntion = item.ApprovalFuntion;
            returnItem.RequestType = item.ApprovalTypeEnum.ToDescription();
            returnItem.ApprovalStatus = item.ApprovalStatus.Trim() == "N" ? "New Request" : item.ApprovalStatus.Trim() == "A" ? "Approved" : "Rejected";
            returnItem.MakerDate = item.MakerDate;//item.MakerDate.IsNull() ? null : Convert.ToDateTime(item.MakerDate).ToString("dd/MM/yyyy");
            returnItem.MakerUser = item.MakerUser;
            return returnItem;
        }


        public ApplicationCrView ConvertFrom(ApplicationCr item)
        {
            ApplicationCrView returnItem = new ApplicationCrView();
            returnItem.Id = "A" + item.Id.ToString().Trim();
            returnItem.ApprovalFuntion = "Application";
            returnItem.RequestType = item.ApprovalTypeEnum.ToDescription();
            returnItem.ApprovalStatus = item.ApprovalStatus.Trim() == "N" ? "New Request" : item.ApprovalStatus.Trim() == "A" ? "Approved" : "Rejected";
            returnItem.MakerDate = item.CreatedDate.IsNull() ? null : Convert.ToDateTime(item.CreatedDate).ToString("dd/MM/yyyy");
            returnItem.MakerUser = item.CreatedBy;
            return returnItem;
        }
        public ApplicationCrView ConvertFrom(AuthorizationFunctionCr item)
        {
            ApplicationCrView returnItem = new ApplicationCrView();
            returnItem.Id = "F" + item.Id.ToString().Trim();
            returnItem.ApprovalFuntion = "Function";
            returnItem.RequestType = item.ApprovalTypeEnum.ToDescription();
            returnItem.ApprovalStatus = item.ApprovalStatus.Trim() == "N" ? "New Request" : item.ApprovalStatus.Trim() == "A" ? "Approved" : "Rejected";
            returnItem.MakerDate = item.CreatedDate.IsNull() ? null : Convert.ToDateTime(item.CreatedDate).ToString("dd/MM/yyyy");
            returnItem.MakerUser = item.CreatedBy;
            return returnItem;
        }
        public ApplicationCrView ConvertFrom(AuthorizationFeatureCr item)
        {
            ApplicationCrView returnItem = new ApplicationCrView();
            returnItem.Id = "E" + item.Id.ToString().Trim();
            returnItem.ApprovalFuntion = "Feature";
            returnItem.RequestType = item.ApprovalTypeEnum.ToDescription();
            returnItem.ApprovalStatus = item.ApprovalStatus.Trim() == "N" ? "New Request" : item.ApprovalStatus.Trim() == "A" ? "Approved" : "Rejected";
            returnItem.MakerDate = item.CreatedDate.IsNull() ? null : Convert.ToDateTime(item.CreatedDate).ToString("dd/MM/yyyy");
            returnItem.MakerUser = item.CreatedBy;
            return returnItem;
        }
        public ApplicationCrView ConvertFrom(RoleCr item)
        {
            ApplicationCrView returnItem = new ApplicationCrView();
            returnItem.Id = "R" + item.Id.ToString().Trim();
            returnItem.ApprovalFuntion = "Role";
            returnItem.RequestType = item.ApprovalTypeEnum.ToDescription();
            returnItem.ApprovalStatus = item.ApprovalStatus.Trim() == "N" ? "New Request" : item.ApprovalStatus.Trim() == "A" ? "Approved" : "Rejected";
            returnItem.MakerDate = item.CreatedDate.IsNull() ? null : Convert.ToDateTime(item.CreatedDate).ToString("dd/MM/yyyy");
            returnItem.MakerUser = item.CreatedBy;
            return returnItem;
        }
        public ApplicationCrView ConvertFrom(MenuCr item)
        {
            ApplicationCrView returnItem = new ApplicationCrView();
            returnItem.Id = "M" + item.Id.ToString().Trim();
            returnItem.ApprovalFuntion = "Menu";
            returnItem.RequestType = item.ApprovalTypeEnum.ToDescription();
            returnItem.ApprovalStatus = item.ApprovalStatus.Trim() == "N" ? "New Request" : item.ApprovalStatus.Trim() == "A" ? "Approved" : "Rejected";
            returnItem.MakerDate = item.CreatedDate.IsNull() ? null : Convert.ToDateTime(item.CreatedDate).ToString("dd/MM/yyyy");
            returnItem.MakerUser = item.CreatedBy;
            return returnItem;
        }
        public ApplicationCrView ConvertFrom(BranchCr item)
        {
            ApplicationCrView returnItem = new ApplicationCrView();
            returnItem.Id = "B" + item.Id.ToString().Trim();
            returnItem.ApprovalFuntion = "Branch";
            returnItem.RequestType = item.ApprovalTypeEnum.ToDescription();
            returnItem.ApprovalStatus = item.ApprovalStatus.Trim() == "N" ? "New Request" : item.ApprovalStatus.Trim() == "A" ? "Approved" : "Rejected";
            returnItem.MakerDate = item.CreatedDate.IsNull() ? null : Convert.ToDateTime(item.CreatedDate).ToString("dd/MM/yyyy");
            returnItem.MakerUser = item.CreatedBy;
            return returnItem;
        }
        public ApplicationCrView ConvertFrom(DocumentTypeCr item)
        {
            ApplicationCrView returnItem = new ApplicationCrView();
            returnItem.Id = "D" + item.Id.ToString().Trim();
            returnItem.DocTypeid = item.DocTypeId;
            returnItem.ApprovalFuntion = "DocumentType";
            returnItem.RequestType = item.ApprovalTypeEnum.ToDescription();
            returnItem.ApprovalStatus = item.ApprovalStatus.Trim() == "N" ? "New Request" : item.ApprovalStatus.Trim() == "A" ? "Approved" : "Rejected";
            returnItem.MakerDate = item.CreatedDate.IsNull() ? null : Convert.ToDateTime(item.CreatedDate).ToString("dd/MM/yyyy");
            returnItem.MakerUser = item.CreatedBy;
            return returnItem;
        }
        public ApplicationCrView ConvertFrom(RoleDocTypeCr item)
        {
            ApplicationCrView returnItem = new ApplicationCrView();
            returnItem.Id = "L" + item.Id.ToString().Trim();
            returnItem.RoleDocTypeid = item.RoleId;
            returnItem.ApprovalFuntion = "RoleDocumentType";
            returnItem.RequestType = item.ApprovalTypeEnum.ToDescription();
            returnItem.ApprovalStatus = item.ApprovalStatus.Trim() == "N" ? "New Request" : item.ApprovalStatus.Trim() == "A" ? "Approved" : "Rejected";
            returnItem.MakerDate = item.CreatedDate.IsNull() ? null : Convert.ToDateTime(item.CreatedDate).ToString("dd/MM/yyyy");
            returnItem.MakerUser = item.CreatedBy;
            return returnItem;
        }
        public ApplicationCrView ConvertFrom(FunctionFeatureCr item)
        {
            ApplicationCrView returnItem = new ApplicationCrView();
            returnItem.Id = "T" + item.Id.ToString().Trim();
            returnItem.ApprovalFuntion = "Feature of Function";
            returnItem.RequestType = item.ApprovalTypeEnum.ToDescription();
            returnItem.ApprovalStatus = item.ApprovalStatus.Trim() == "N" ? "New Request" : item.ApprovalStatus.Trim() == "A" ? "Approved" : "Rejected";
            returnItem.MakerDate = item.CreatedDate.IsNull() ? null : Convert.ToDateTime(item.CreatedDate).ToString("dd/MM/yyyy");
            returnItem.MakerUser = item.CreatedBy;
            return returnItem;
        }
        #endregion

        #region CreateSelected
        private IList<SelectListItem> createApprovalFunctionSelect(string selected)
        {
            IList<SelectListItem> dataList = new List<SelectListItem>();
            dataList.Insert(0, new SelectListItem() { Value = "", Text = "" });
            dataList.Insert(1, new SelectListItem() { Value = "Role", Text = "Role" });
            dataList.Insert(2, new SelectListItem() { Value = "Role Function", Text = "Function of Role" });
            dataList.Insert(3, new SelectListItem() { Value = "Role Function Feature", Text = "Feature of Role Function" });
            dataList.Insert(4, new SelectListItem() { Value = "Menu", Text = "Menu" });
            dataList.Insert(5, new SelectListItem() { Value = "Branch", Text = "Branch" });
            dataList.Insert(6, new SelectListItem() { Value = "Document Type", Text = "Document Type" });
            dataList.Insert(7, new SelectListItem() { Value = "Role Document Type", Text = "Role Document Type" });
            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected.ToString()).Selected = true;
            }
            return dataList;
        }
        private IList<SelectListItem> createApprovalTypeSelect(string selected)
        {
            IList<SelectListItem> dataList = new List<SelectListItem>();
            dataList.Insert(0, new SelectListItem() { Value = "", Text = "" });
            dataList.Insert(1, new SelectListItem() { Value = "C", Text = "Create" });
            dataList.Insert(2, new SelectListItem() { Value = "U", Text = "Update" });
            dataList.Insert(3, new SelectListItem() { Value = "D", Text = "Delete" });

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected.ToString()).Selected = true;
            }
            return dataList;
        }
        private IList<SelectListItem> createApprovalActionSelect(string selected)
        {
            IList<SelectListItem> dataList = new List<SelectListItem>();
            dataList.Insert(0, new SelectListItem() { Value = "", Text = "" });
            dataList.Insert(1, new SelectListItem() { Value = "A", Text = "Approve" });
            dataList.Insert(2, new SelectListItem() { Value = "R", Text = "Reject" });

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected.ToString()).Selected = true;
            }
            return dataList;
        }
        private IList<SelectListItem> createApprovalStatusSelect(string selected)
        {
            IList<SelectListItem> dataList = new List<SelectListItem>();
            dataList.Insert(0, new SelectListItem() { Value = "", Text = "" });
            dataList.Insert(1, new SelectListItem() { Value = "N", Text = "New Request" });
            dataList.Insert(2, new SelectListItem() { Value = "R", Text = "Rejected" });
            dataList.Insert(3, new SelectListItem() { Value = "A", Text = "Approved" });

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected.ToString()).Selected = true;
            }
            return dataList;
        }
        #endregion

        public ActionResult Approve(string id)
        {
            string appFunction = id.Substring(0, 1);
            long appId = Convert.ToInt32(id.Substring(1, id.Length - 1));
            switch (appFunction)
            {
                case "A":
                    ViewData["ActionName"] = "Approve";
                    ViewData["ApprovalActionList"] = createApprovalActionSelect("");
                    ApplicationCr appCr = _appCrReporsitory.getApplication(appId);
                    ApplicationCrView appCrData = ConvertFromApprove(appCr);
                    ViewData["appCrData"] = appCrData;
                    return View("DetailApp", appCrData);
                case "F":
                    ViewData["ActionName"] = "ApproveFunction";
                    ViewData["ApprovalActionList"] = createApprovalActionSelect("");
                    AuthorizationFunctionCr functCr = _functionCrRepository.getFunction(appId);
                    FunctionCrView functCrData = ConvertFromApprove(functCr);
                    ViewData["functionCrData"] = functCrData;
                    ViewData["ApplicationMaster"] = createApplicationSelect(functCrData.ApplicationID);
                    return View("DetailFunction", functCrData);
                case "E":
                    ViewData["ActionName"] = "ApproveFeature";
                    ViewData["ApprovalActionList"] = createApprovalActionSelect("");
                    AuthorizationFeatureCr featureCr = _featureCrRepository.getFeature(appId);
                    FeatureCrView featureCrData = ConvertFromApprove(featureCr);
                    ViewData["featureCrData"] = featureCrData;
                    ViewData["ApplicationMaster"] = createApplicationSelect(featureCrData.ApplicationID);
                    return View("DetailFeature", featureCrData);
                case "R":
                    ViewData["ActionName"] = "ApproveRole";
                    ViewData["ApprovalActionList"] = createApprovalActionSelect("");
                    RoleCr roleCr = _roleCrRepository.getRole(appId);
                    RoleCrView roleCrData = ConvertFromApprove(roleCr);
                    ViewData["roleCrData"] = roleCrData;
                    ViewData["ApplicationMaster"] = createApplicationSelect(roleCrData.ApplicationID);
                    return View("DetailRole", roleCrData);
                case "M":
                    ViewData["ActionName"] = "ApproveMenu";
                    ViewData["ApprovalActionList"] = createApprovalActionSelect("");
                    MenuCr menuCr = _menuCrRepository.getForEditMode(appId);
                    MenuCrView menuCrData = ConvertFromApprove(menuCr);
                    ViewData["menuCrData"] = menuCrData;
                    return View("DetailMenu", menuCrData);
                case "B":
                    ViewData["ActionName"] = "ApproveBranch";
                    ViewData["ApprovalActionList"] = createApprovalActionSelect("");
                    BranchCr branchCr = _branchCrRepository.getBranch(appId);
                    BranchCrView branchCrData = ConvertFromApprove(branchCr);
                    ViewData["branchCrData"] = branchCrData;
                    return View("DetailBranch", branchCrData);
                case "D":
                    ViewData["ActionName"] = "ApproveDocType";
                    ViewData["ApprovalActionList"] = createApprovalActionSelect("");
                    DocumentTypeCr DocTypeCr = _documentTypeCrRepository.getDocType(appId);
                    DocumentTypeCrView DocTypeCrData = ConvertFromApprove(DocTypeCr);
                    ViewData["DocTypeCrData"] = DocTypeCrData;
                    return View("DetailDocType", DocTypeCrData);
                case "L":
                    ViewData["ActionName"] = "ApproveRolDocTypeList";
                    ViewData["ApprovalActionList"] = createApprovalActionSelect("");
                    RoleDocTypeCr RolDocTypeCr = _roleDocumentTypeCrRepository.getRoleDocType(appId);
                    RoleDocTypeCrView RoleDocTypeCrData = ConvertFromApprove(RolDocTypeCr);
                    ViewData["RoleDocTypeCrData"] = RoleDocTypeCrData;
                    return View("DetailRoleDocType", RoleDocTypeCrData);
                case "T":
                    ViewData["ActionName"] = "ApproveFunctionFeature";
                    ViewData["ApprovalActionList"] = createApprovalActionSelect("");
                    FunctionFeatureCr ffCr = _funcfeatCrRepository.getFunctionFeature(appId);
                    FunctionFeatureCrView ffCrData = ConvertFromApprove(ffCr);
                    ViewData["ffCrData"] = ffCrData;
                    return View("DetailFunctionFeature", ffCrData);
                case "O":
                    ViewData["ActionName"] = "ApproveRoleFunction";
                    ViewData["ApprovalActionList"] = createApprovalActionSelect("");
                    RoleFunctionCr rfCr = _rolefuncCrRepository.getRoleFunction2(appId);
                    RoleFunctionCrView rfCrData = ConvertFromApprove(rfCr);
                    ViewData["rfCrData"] = rfCrData;
                    return View("DetailRoleFunction", rfCrData);
                case "U":
                    ViewData["ActionName"] = "ApproveRoleFunctionFeature";
                    ViewData["ApprovalActionList"] = createApprovalActionSelect("");
                    RoleFunctionFeatureCr rffCr = _rolefuncfeatCrRepository.getRoleFuncFeature(appId);
                    RoleFunctionCrView rffCrData = ConvertFromApprove(rffCr);
                    ViewData["rffCrData"] = rffCrData;
                    return View("DetailRoleFunctionFeature", rffCrData);
                default:
                    return View("Detail");
            }
        }

        #region ConvertFromApproval
        public ApplicationCrView ConvertFromApprove(ApplicationCr item)
        {
            Application app = _appRepository.getApplication(item.ApplicationId);
            User usr = _userRepository.GetUser(item.CreatedBy);

            ApplicationCrView returnItem = new ApplicationCrView();
            returnItem.ApplicationId = item.ApplicationId;
            returnItem.Type = item.Type;
            returnItem.Runtime = item.Runtime;
            returnItem.Description = item.Description;
            returnItem.CSSColor = item.CSSColor;
            returnItem.Icon = item.Icon;
            returnItem.Id = item.Id.ToString().Trim();
            returnItem.Name = item.Name;
            returnItem.ApprovalType = item.ApprovalType;
            returnItem.Approved = item.Approved;

            if (item.ApprovalType == "U")
            {
                returnItem.ApplicationIdBefore = app.ApplicationId;
                returnItem.TypeBefore = app.Type;
                returnItem.RuntimeBefore = app.Runtime;
                returnItem.DescriptionBefore = app.Description;
                returnItem.CSSColorBefore = app.CSSColor;
                returnItem.IconBefore = app.Icon;
                returnItem.NameBefore = app.Name;
            }

            returnItem.MakerUser = usr.FullName;
            returnItem.MakerDate = Convert.ToDateTime(item.CreatedDate).ToString("dd/MM/yyyy HH:mm:ss");

            return returnItem;
        }
        public FunctionCrView ConvertFromApprove(AuthorizationFunctionCr item)
        {
            Application app = _appRepository.getApplication(item.Application);
            AuthorizationFunction funct = _funcRepository.getFunction(item.FunctionId);
            User usr = _userRepository.GetUser(item.CreatedBy);

            FunctionCrView returnItem = new FunctionCrView();
            returnItem.Application = app.Name;
            returnItem.ApplicationID = item.Application;
            returnItem.Description = item.Description;
            returnItem.FunctionId = item.FunctionId;
            returnItem.Id = item.Id.ToString().Trim();
            returnItem.Name = item.Name;
            returnItem.ApprovalType = item.ApprovalType;
            returnItem.Approved = item.Approved;
            returnItem.MakerUser = usr.FullName;
            returnItem.MakerDate = Convert.ToDateTime(item.CreatedDate).ToString("dd/MM/yyyy HH:mm:ss");
            if (item.ApprovalType == "U")
            {
                returnItem.ApplicationBefore = funct.Name;
                returnItem.ApplicationIDBefore = funct.Application;
                returnItem.DescriptionBefore = funct.Description;
                returnItem.FunctionIdBefore = funct.FunctionId;
                returnItem.NameBefore = funct.Name;
            }
            return returnItem;
        }
        public FeatureCrView ConvertFromApprove(AuthorizationFeatureCr item)
        {
            User usr = _userRepository.GetUser(item.CreatedBy);
            Application app = _appRepository.getApplication(item.Application);
            AuthorizationFeature feature = _featureRepository.getFeature(item.FeatureId);

            FeatureCrView returnItem = new FeatureCrView();
            returnItem.Application = app.Name;
            returnItem.ApplicationID = item.Application;
            returnItem.Description = item.Description;
            returnItem.FeatureId = item.FeatureId;
            returnItem.Id = item.Id.ToString().Trim();
            returnItem.Name = item.Name;
            returnItem.ApprovalType = item.ApprovalType;
            returnItem.Approved = item.Approved;
            if (item.ApprovalType == "U")
            {
                returnItem.ApplicationBefore = feature.Name;
                returnItem.ApplicationIDBefore = feature.Application;
                returnItem.DescriptionBefore = feature.Description;
                returnItem.FeatureIdBefore = feature.FeatureId;
                returnItem.NameBefore = feature.Name;
            }
            returnItem.MakerUser = usr.FullName;
            returnItem.MakerDate = Convert.ToDateTime(item.CreatedDate).ToString("dd/MM/yyyy HH:mm:ss");
            return returnItem;
        }
        public RoleCrView ConvertFromApprove(RoleCr item)
        {
            Application app = _appRepository.getApplication(item._Application);
            User usr = _userRepository.GetUser(item.CreatedBy);
            Role role = _roleRepository.getRole(item.RoleId);

            RoleCrView returnItem = new RoleCrView();
            returnItem.Application = app.Name;
            returnItem.ApplicationID = item._Application;
            returnItem.Description = item.Description;
            returnItem.RoleId = item.RoleId;
            returnItem.Id = item.Id.ToString().Trim();
            returnItem.Name = item.Name;
            returnItem.SessionTimeout = item.SessionTimeout;
            returnItem.LockTimeout = item.LockTimeout;
            returnItem.ApprovalType = item.ApprovalType;
            returnItem.Approved = item.Approved;
            returnItem.MakerUser = usr.FullName;
            returnItem.MakerDate = Convert.ToDateTime(item.CreatedDate).ToString("dd/MM/yyyy HH:mm:ss");
            if (item.ApprovalType == "U")
            {
                returnItem.ApplicationBefore = role.Name;
                returnItem.ApplicationIDBefore = role._Application;
                returnItem.DescriptionBefore = role.Description;
                returnItem.RoleIdBefore = role.RoleId;
                returnItem.NameBefore = role.Name;
                returnItem.SessionTimeoutBefore = role.SessionTimeout;
                returnItem.LockTimeoutBefore = role.LockTimeout;
            }
            return returnItem;
        }
        public MenuCrView ConvertFromApprove(MenuCr item)
        {
            Application app = _appRepository.getApplication(item.Application);
            AuthorizationFunction func = _funcRepository.getFunction(item.FunctionId);
            User usr = _userRepository.GetUser(item.CreatedBy);

            MenuCrView returnItem = new MenuCrView();
            //string spaces = createSpaces(item.MenuLevel);
            returnItem.Id = item.Id.ToString().Trim();
            returnItem.MenuId = item.MenuId;
            returnItem.MenuName = item.MenuName;
            returnItem.ParentMenu = item.ParentMenu;
            returnItem.FunctionId = item.FunctionId;
            returnItem.FunctionName = func.IsNull()? "-" : func.Name;
            returnItem.MenuUrl = item.MenuUrl;
            returnItem.MenuLevel = item.MenuLevel;
            returnItem.MenuOrder = item.MenuOrder;
            returnItem.MenuIcon = item.MenuIcon;
            returnItem.Active = item.Active;
            returnItem.Application = item.Application;
            returnItem.ApprovalType = item.ApprovalType;
            returnItem.Approved = item.Approved;
            returnItem.MakerUser = usr.FullName;
            returnItem.MakerDate = Convert.ToDateTime(item.CreatedDate).ToString("dd/MM/yyyy HH:mm:ss");
            if (item.ApprovalType == "U")
            {
                Menu menu = _menuRepository.getForEditMode(item.MenuId);
                AuthorizationFunction funcb = _funcRepository.getFunction(menu.FunctionId);
                returnItem.MenuIdBefore = menu.MenuId;
                returnItem.MenuNameBefore = menu.MenuName;
                returnItem.ParentMenuBefore = menu.ParentMenu;
                returnItem.FunctionIdBefore = menu.FunctionId;
                returnItem.FunctionNameBefore = funcb.Name;
                returnItem.MenuUrlBefore = menu.MenuUrl;
                returnItem.MenuLevelBefore = menu.MenuLevel;
                returnItem.MenuOrderBefore = menu.MenuOrder;
                returnItem.MenuIconBefore = menu.MenuIcon;
                returnItem.ActiveBefore = menu.Active;
                returnItem.ApplicationBefore = menu.Application;
            }

            return returnItem;
        }
        private string createSpaces(int level)
        {
            string spaces = "";
            for (int i = 0; i < level; i++)
            {
                spaces += "&nbsp;&nbsp;&nbsp;";
            }
            return spaces;
        }
        public BranchCrView ConvertFromApprove(BranchCr item)
        {
            User usr = _userRepository.GetUser(item.CreatedBy);
            Branch brc = _branchRepository.getBranch(item.BranchCode);
            Branch mainb1 = _branchRepository.getBranch(item.MainBranch);

            BranchCrView returnItem = new BranchCrView();
            returnItem.Id = item.Id.ToString().Trim();
            returnItem.BranchCode = item.BranchCode;
            returnItem.Description = item.Description;
            returnItem.MainBranchID = item.MainBranch;
            if (!mainb1.IsNull())
                returnItem.MainBranch = mainb1.Description;
            if (returnItem.MainBranchID == returnItem.BranchCode)
                returnItem.MainBranch = returnItem.Description;
            returnItem.CreatedDate = item.CreatedDate.IsNull() ? null : Convert.ToDateTime(item.CreatedDate).ToString("dd/MM/yyyy");
            returnItem.CreatedBy = item.CreatedBy;
            returnItem.ChangedDate = item.ChangedDate.IsNull() ? null : Convert.ToDateTime(item.ChangedDate).ToString("dd/MM/yyyy");
            returnItem.ChangedBy = item.ChangedBy;
            returnItem.ApprovalType = item.ApprovalType;
            returnItem.Approved = item.Approved;
            returnItem.MakerUser = usr.FullName;
            returnItem.MakerDate = Convert.ToDateTime(item.CreatedDate).ToString("dd/MM/yyyy HH:mm:ss");
            if (item.ApprovalType == "U")
            {
                Branch mainb2 = _branchRepository.getBranch(brc.MainBranch);

                returnItem.BranchCodeBefore = brc.BranchCode;
                returnItem.DescriptionBefore = brc.Description;
                returnItem.MainBranchBefore = mainb2.Description;

                if (returnItem.MainBranchBefore == returnItem.BranchCodeBefore)
                    returnItem.MainBranchBefore = returnItem.DescriptionBefore;
            }
            return returnItem;
        }

        public DocumentTypeCrView ConvertFromApprove(DocumentTypeCr item)
        {
            User usr = _userRepository.GetUser(item.CreatedBy);
            DocumentType brc = _documentTypeRepository.getDocumentType(item.DocTypeId);
            //DocumentType mainb1 = _documentTypeRepository.get(item.MainBranch);

            DocumentTypeCrView returnItem = new DocumentTypeCrView();
            returnItem.Id = item.Id.ToString().Trim();
            returnItem.DocType = item.DocType;
            returnItem.DocTypeId = item.DocTypeId;
            returnItem.Description = item.Description;
            returnItem.CreatedDate = item.CreatedDate.IsNull() ? null : Convert.ToDateTime(item.CreatedDate).ToString("dd/MM/yyyy");
            returnItem.CreatedBy = item.CreatedBy;
            returnItem.ChangedDate = item.ChangedDate.IsNull() ? null : Convert.ToDateTime(item.ChangedDate).ToString("dd/MM/yyyy");
            returnItem.ChangedBy = item.ChangedBy;
            returnItem.ApprovalType = item.ApprovalType;
            returnItem.Approved = item.Approved;
            returnItem.MakerUser = usr.FullName;
            returnItem.MakerDate = Convert.ToDateTime(item.CreatedDate).ToString("dd/MM/yyyy HH:mm:ss");
            if (item.ApprovalType == "U")
            {
                returnItem.DocTypeBefore = brc.DocType;
                returnItem.DescriptionBefore = brc.Description;
            }
            return returnItem;
        }

        //public RoleDocTypeCrView ConvertFromApprove(RoleDocTypeCr item)
        //{
        //    User usr = _userRepository.GetUser(item.CreatedBy);
        //    //DocumentType brc = _documentTypeRepository.getDocumentType(item.RoleId);
        //    //DocumentType mainb1 = _documentTypeRepository.get(item.MainBranch);

        //    RoleDocTypeCrView returnItem = new RoleDocTypeCrView();
        //    returnItem.Id = item.Id.ToString().Trim();
        //    returnItem.RoleId = item.RoleId.ToString();
        //    returnItem.DocumentTypeId = item.DocumentTypeId.ToString();
        //    returnItem.CreatedDate = item.CreatedDate.IsNull() ? null : Convert.ToDateTime(item.CreatedDate).ToString("dd/MM/yyyy");
        //    returnItem.CreatedBy = item.CreatedBy;
        //    returnItem.ChangedDate = item.ChangedDate.IsNull() ? null : Convert.ToDateTime(item.ChangedDate).ToString("dd/MM/yyyy");
        //    returnItem.ChangedBy = item.ChangedBy;
        //    returnItem.ApprovalType = item.ApprovalType;
        //    returnItem.Approved = item.Approved;
        //    returnItem.MakerUser = usr.FullName;
        //    returnItem.MakerDate = Convert.ToDateTime(item.CreatedDate).ToString("dd/MM/yyyy HH:mm:ss");
        //    if (item.ApprovalType == "U")
        //    {
        //        //returnItem.DocTypeBefore = brc.DocType;
        //        //returnItem.DescriptionBefore = brc.Description;
        //    }
        //    return returnItem;
        //}
        public FunctionFeatureCrView ConvertFromApprove(FunctionFeatureCr item)
        {
            User usr = _userRepository.GetUser(item.CreatedBy);
            AuthorizationFunction function = _funcRepository.getFunction(item.FunctionId);
            IList<FunctionFeature> oldFF = _funcfeatRepository.getFunctionFeatures(item.FunctionId);
            IList<FunctionFeatureCr> newFF = _funcfeatCrRepository.getFunctionFeatures(item.FunctionId);

            FunctionFeatureCrView returnItem = new FunctionFeatureCrView();
            returnItem.Id = item.Id.ToString().Trim();
            returnItem.FunctionId = item.FunctionId.ToString().Trim();
            returnItem.Description = function.Description;
            returnItem.FunctionName = function.Name;
            returnItem.OldFeatureList = new List<FeatureView>(oldFF.Count);
            returnItem.NewFeatureList = new List<FeatureView>(oldFF.Count + newFF.Count);

            foreach (FunctionFeature ff in oldFF)
            {
                FeatureView fv = new FeatureView();
                AuthorizationFeature af = _featureRepository.getFeature(ff.FeatureId);
                fv.Id = af.Id;
                fv.FeatureId = af.FeatureId;
                fv.Description = af.Description;
                fv.Name = af.Name;

                returnItem.OldFeatureList.Add(fv);
                returnItem.NewFeatureList.Add(fv);
            }
            foreach (FunctionFeatureCr nf in newFF)
            {
                FunctionFeature ofv = oldFF.AsEnumerable().Where(f => f.FeatureId == nf.FeatureId).SingleOrDefault();
                FeatureView rm = returnItem.NewFeatureList.AsEnumerable().Where(f => f.Id == nf.FeatureId).SingleOrDefault();
                AuthorizationFeature af = _featureRepository.getFeature(nf.FeatureId);
                FeatureView fv = new FeatureView();

                fv.Id = af.Id;
                fv.FeatureId = af.FeatureId;
                fv.Description = af.Description;
                fv.Name = af.Name;

                if (!ofv.IsNull())
                {
                    if (nf.ApprovalType == "D")
                    {
                        returnItem.NewFeatureList.Remove(rm);
                    }
                    else if (nf.ApprovalType == "C")
                    {
                        returnItem.NewFeatureList.Add(fv);
                    }
                }
                else
                {
                    returnItem.NewFeatureList.Add(fv);
                }
            }

            returnItem.ApprovalType = item.ApprovalType;
            returnItem.Approved = item.Approved;
            returnItem.MakerUser = usr.FullName;
            returnItem.MakerDate = Convert.ToDateTime(item.CreatedDate).ToString("dd/MM/yyyy HH:mm:ss");

            return returnItem;
        }
        public RoleFunctionCrView ConvertFromApprove(RoleFunctionCr item)
        {
            User usr = _userRepository.GetUser(item.CreatedBy);
            RoleFunctionCrView returnItem = new RoleFunctionCrView();
            Role role = _roleRepository.getRole(item.RoleId);
            IList<RoleFunction> oldRF = _rolefuncRepository.getFunctions(item.RoleId);
            IList<RoleFunctionCr> newRF = _rolefuncCrRepository.getFunctions(item.RoleId);

            returnItem.Id = item.Id.ToString().Trim();
            returnItem.RoleId = item.RoleId.ToString().Trim();
            returnItem.Name = !role.IsNull() ? role.Name : "";
            returnItem.ApprovalType = item.ApprovalType;
            returnItem.Approved = item.Approved;
            returnItem.MakerUser = usr.FullName;
            returnItem.MakerDate = Convert.ToDateTime(item.CreatedDate).ToString("dd/MM/yyyy HH:mm:ss");
            returnItem.OldFunctionList = new List<FunctionView>(oldRF.Count);
            returnItem.NewFunctionList = new List<FunctionView>(oldRF.Count + newRF.Count);

            foreach (RoleFunction orf in oldRF)
            {
                FunctionView fv = new FunctionView();
                AuthorizationFunction af = _funcRepository.getFunction(orf.FunctionId);
                fv.FunctionId = af.FunctionId;
                fv.Name = af.Name;
                fv.Description = af.Description;

                returnItem.OldFunctionList.Add(fv);
                returnItem.NewFunctionList.Add(fv);
            }
            foreach (RoleFunctionCr nrf in newRF)
            {
                FunctionView fv = new FunctionView();
                AuthorizationFunction af = _funcRepository.getFunction(nrf.FunctionId);
                RoleFunction orf = oldRF.AsEnumerable().Where(f => f.FunctionId == nrf.FunctionId).SingleOrDefault();
                FunctionView rm = returnItem.NewFunctionList.AsEnumerable().Where(f => f.Id == nrf.FunctionId).SingleOrDefault();

                fv.FunctionId = af.FunctionId;
                fv.Name = af.Name;
                fv.Description = af.Description;

                if (!orf.IsNull())
                {
                    if (nrf.ApprovalType == "D")
                    {
                        returnItem.NewFunctionList.Remove(rm);
                    }
                    else if (nrf.ApprovalType == "C")
                    {
                        returnItem.NewFunctionList.Add(fv);
                    }
                }
                else
                {
                    returnItem.NewFunctionList.Add(fv);
                }
            }

            return returnItem;
        }

        public RoleDocTypeCrView ConvertFromApprove(RoleDocTypeCr item)
        {
            User usr = _userRepository.GetUser(item.CreatedBy);
            RoleDocTypeCrView returnItem = new RoleDocTypeCrView();
            Role role = _roleRepository.getRole(item.RoleId);
            IList<RoleDocType> oldRF = _roleDocumentTypeRepository.getDocTypes(item.RoleId);
            IList<RoleDocTypeCr> newRF = _roleDocumentTypeCrRepository.getDocTypes2(item.RoleId);

            returnItem.Id = item.Id.ToString().Trim();
            returnItem.RoleId = item.RoleId.ToString().Trim();
            returnItem.Name = role.Name;
            returnItem.ApprovalType = item.ApprovalType;
            returnItem.Approved = item.Approved;
            returnItem.MakerUser = usr.FullName;
            returnItem.MakerDate = Convert.ToDateTime(item.CreatedDate).ToString("dd/MM/yyyy HH:mm:ss");
            returnItem.OldFunctionList = new List<DocumentTypeView>(oldRF.Count);
            returnItem.NewFunctionList = new List<DocumentTypeView>(oldRF.Count + newRF.Count);

            foreach (RoleDocType orf in oldRF)
            {
                DocumentTypeView fv = new DocumentTypeView();
                DocumentType af = _documentTypeRepository.getDocumentType(orf.DocumentTypeId);
                fv.Id = af.Id;
                fv.DocType = af.DocType;
                fv.Description = af.Description;

                returnItem.OldFunctionList.Add(fv);
                returnItem.NewFunctionList.Add(fv);
            }
            foreach (RoleDocTypeCr nrf in newRF)
            {
                DocumentTypeView fv = new DocumentTypeView();
                DocumentType af = _documentTypeRepository.getDocumentType(nrf.DocumentTypeId);
                RoleDocType orf = oldRF.AsEnumerable().Where(f => f.DocumentTypeId == nrf.DocumentTypeId).SingleOrDefault();
                DocumentTypeView rm = returnItem.NewFunctionList.AsEnumerable().Where(f => f.Id == nrf.DocumentTypeId).SingleOrDefault();

                fv.Id = af.Id;
                fv.DocType = af.DocType;
                fv.Description = af.Description;

                if (!orf.IsNull())
                {
                    if (nrf.ApprovalType == "D")
                    {
                        returnItem.NewFunctionList.Remove(rm);
                    }
                    else if (nrf.ApprovalType == "C")
                    {
                        returnItem.NewFunctionList.Add(fv);
                    }
                }
                else
                {
                    returnItem.NewFunctionList.Add(fv);
                }
            }

            return returnItem;
        }

        public RoleFunctionCrView ConvertFromApprove(RoleFunctionFeatureCr item)
        {
            User usr = _userRepository.GetUser(item.CreatedBy);
            RoleFunctionCrView returnItem = new RoleFunctionCrView();
            Role role = _roleRepository.getRole(item.RoleId);
            AuthorizationFunction funct = _funcRepository.getFunction(item.FunctionId);

            IList<RoleFunctionFeature> oldRF = _rolefuncfeatRepository.getRoleFuncFeaturebyRole(item.RoleId);
            IList<RoleFunctionFeatureCr> newRF = _rolefuncfeatCrRepository.getRoleFuncFeaturebyRole(item.RoleId);

            returnItem.Id = item.Id.ToString().Trim();
            returnItem.RoleId = item.RoleId.ToString().Trim();
            returnItem.Name = role.Name;
            returnItem.ApprovalType = item.ApprovalType;
            returnItem.Approved = item.Approved;
            returnItem.MakerUser = usr.FullName;
            returnItem.MakerDate = Convert.ToDateTime(item.CreatedDate).ToString("dd/MM/yyyy HH:mm:ss");
            returnItem.OldFunctionList = new List<FunctionView>(oldRF.Count);
            returnItem.NewFunctionList = new List<FunctionView>(oldRF.Count + newRF.Count);

            foreach (RoleFunctionFeature orf in oldRF)
            {
                FunctionView fv = new FunctionView();
                AuthorizationFunction af = _funcRepository.getFunction(orf.FunctionId);

                fv.FunctionId = af.FunctionId;
                fv.Name = af.Name;
                fv.Id = af.Id;

                FunctionView cfv = returnItem.OldFunctionList.AsEnumerable().Where(f => f.Id == fv.Id).SingleOrDefault();

                if (cfv.IsNull())
                {
                    returnItem.OldFunctionList.Add(fv);
                    returnItem.NewFunctionList.Add(fv);
                }
            }
            foreach (RoleFunctionFeatureCr nrf in newRF)
            {
                FunctionView fv = new FunctionView();
                AuthorizationFunction af = _funcRepository.getFunction(nrf.FunctionId);
                RoleFunctionFeature orf = oldRF.AsEnumerable().Where(f => f.RoleId == nrf.RoleId && f.FunctionId == nrf.FunctionId && f.FeatureId == nrf.FeatureId).SingleOrDefault();
                FunctionView rm = returnItem.NewFunctionList.AsEnumerable().Where(f => f.Id == nrf.FunctionId).SingleOrDefault();

                fv.FunctionId = af.FunctionId;
                fv.Name = af.Name;
                fv.Id = af.Id;

                FunctionView cfv = returnItem.NewFunctionList.AsEnumerable().Where(f => f.Id == fv.Id).SingleOrDefault();

                if (cfv.IsNull())
                {
                    if (!orf.IsNull())
                    {
                        if (nrf.ApprovalType == "D")
                        {
                            returnItem.NewFunctionList.Remove(rm);
                        }
                        else if (nrf.ApprovalType == "C")
                        {
                            returnItem.NewFunctionList.Add(fv);
                        }
                    }
                    else
                    {
                        returnItem.NewFunctionList.Add(fv);
                    }
                }
            }

            foreach (FunctionView fv in returnItem.OldFunctionList)
            {
                IList<RoleFunctionFeature> oldRFF = _rolefuncfeatRepository.getRoleFuncFeatures(Convert.ToInt32(returnItem.RoleId), fv.Id);
                IList<RoleFunctionFeatureCr> newRFF = _rolefuncfeatCrRepository.getRoleFuncFeatures(Convert.ToInt32(returnItem.RoleId), fv.Id);
                fv.OldFeatureList = new List<FeatureView>(oldRFF.Count);
                fv.NewFeatureList = new List<FeatureView>(newRFF.Count + oldRFF.Count);

                foreach (RoleFunctionFeature orf in oldRFF)
                {
                    AuthorizationFeature af = _featureRepository.getFeature(orf.FeatureId);
                    FeatureView ofv = new FeatureView();

                    ofv.FeatureId = af.FeatureId;
                    ofv.Name = af.Name;
                    ofv.Id = af.Id;

                    FeatureView cfv = fv.OldFeatureList.AsEnumerable().Where(f => f.Id == ofv.Id).SingleOrDefault();
                    if (cfv.IsNull())
                    {
                        fv.OldFeatureList.Add(ofv);
                        fv.NewFeatureList.Add(ofv);
                    }
                }
                foreach (RoleFunctionFeatureCr nrf in newRFF)
                {
                    FeatureView nfv = new FeatureView();
                    AuthorizationFeature af = _featureRepository.getFeature(nrf.FeatureId);
                    RoleFunctionFeature orf = oldRFF.AsEnumerable().Where(f => f.RoleId == nrf.RoleId && f.FunctionId == nrf.FunctionId && f.FeatureId == nrf.FeatureId).SingleOrDefault();
                    FeatureView rm = fv.NewFeatureList.AsEnumerable().Where(f => f.Id == nrf.FeatureId).SingleOrDefault();

                    nfv.FeatureId = af.FeatureId;
                    nfv.Name = af.Name;
                    nfv.Id = af.Id;

                    FeatureView cfv = fv.NewFeatureList.AsEnumerable().Where(f => f.Id == nfv.Id).SingleOrDefault();

                    if (cfv.IsNull())
                    {
                        if (!orf.IsNull())
                        {
                            if (nrf.ApprovalType == "D")
                            {
                                fv.NewFeatureList.Remove(rm);
                            }
                            else if (nrf.ApprovalType == "C")
                            {
                                fv.NewFeatureList.Add(nfv);
                            }
                        }
                        else
                        {
                            fv.NewFeatureList.Add(nfv);
                        }
                    }
                }
            }

            foreach (FunctionView fv in returnItem.NewFunctionList)
            {
                IList<RoleFunctionFeature> oldRFF = _rolefuncfeatRepository.getRoleFuncFeatures(Convert.ToInt32(returnItem.RoleId), fv.Id);
                IList<RoleFunctionFeatureCr> newRFF = _rolefuncfeatCrRepository.getRoleFuncFeatures(Convert.ToInt32(returnItem.RoleId), fv.Id);
                fv.OldFeatureList = new List<FeatureView>(oldRFF.Count);
                fv.NewFeatureList = new List<FeatureView>(newRFF.Count + oldRFF.Count);

                foreach (RoleFunctionFeature orf in oldRFF)
                {
                    AuthorizationFeature af = _featureRepository.getFeature(orf.FeatureId);
                    FeatureView ofv = new FeatureView();

                    ofv.FeatureId = af.FeatureId;
                    ofv.Name = af.Name;
                    ofv.Id = af.Id;

                    FeatureView cfv = fv.OldFeatureList.AsEnumerable().Where(f => f.Id == ofv.Id).SingleOrDefault();
                    if (cfv.IsNull())
                    {
                        fv.OldFeatureList.Add(ofv);
                        fv.NewFeatureList.Add(ofv);
                    }
                }
                foreach (RoleFunctionFeatureCr nrf in newRFF)
                {
                    FeatureView nfv = new FeatureView();
                    AuthorizationFeature af = _featureRepository.getFeature(nrf.FeatureId);
                    RoleFunctionFeature orf = oldRFF.AsEnumerable().Where(f => f.RoleId == nrf.RoleId && f.FunctionId == nrf.FunctionId && f.FeatureId == nrf.FeatureId).SingleOrDefault();
                    FeatureView rm = fv.NewFeatureList.AsEnumerable().Where(f => f.Id == nrf.FeatureId).SingleOrDefault();

                    nfv.FeatureId = af.FeatureId;
                    nfv.Name = af.Name;
                    nfv.Id = af.Id;

                    //FeatureView cfv = fv.NewFeatureList.AsEnumerable().Where(f => f.Id == nfv.Id).SingleOrDefault();

                    //if (cfv.IsNull())
                    //{
                    if (!orf.IsNull())
                    {
                        if (nrf.ApprovalType == "D")
                        {
                            fv.NewFeatureList.Remove(rm);
                        }
                        else if (nrf.ApprovalType == "C")
                        {
                            fv.NewFeatureList.Add(nfv);
                        }
                    }
                    else
                    {
                        fv.NewFeatureList.Add(nfv);
                    }
                    //}
                }
            }

            return returnItem;
        }

        #endregion

        #region Approval
        [HttpPost]
        public ActionResult Approve(ApplicationCrView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                ApplicationCr appCr = _appCrReporsitory.getApplication(Convert.ToInt32(data.Id.Substring(1, data.Id.Length - 1)));
                appCr.Approved = true;
                appCr.ApprovedBy = user.Username;
                appCr.ApprovedDate = DateTime.Now;
                appCr.ApprovalStatus = data.ApprovalStatus;
                _appCrReporsitory.Save(appCr);

                if (data.ApprovalStatus == "A")
                {
                    if (data.ApprovalType == "C")
                    {
                        Application newData = new Application(0L);
                        newData.ApplicationId = data.ApplicationId;
                        newData.Name = data.Name;
                        newData.Type = data.Type;
                        newData.Runtime = data.Runtime;
                        newData.Description = data.Description;
                        newData.CSSColor = data.CSSColor;
                        newData.Icon = data.Icon;
                        newData.CreatedBy = appCr.CreatedBy;
                        newData.CreatedDate = appCr.CreatedDate;

                        _appRepository.Add(newData);
                    }
                    else if (data.ApprovalType == "D")
                    {
                        Application appDelete = _appRepository.getApplication(data.ApplicationId);
                        _appRepository.Remove(appDelete);

                    }
                    else
                    {
                        Application appUpdate = _appRepository.getApplication(data.ApplicationId);

                        appUpdate.ApplicationId = data.ApplicationId;
                        appUpdate.Name = data.Name;
                        appUpdate.Type = data.Type;
                        appUpdate.Runtime = data.Runtime;
                        appUpdate.Description = data.Description;
                        appUpdate.CSSColor = data.CSSColor;
                        appUpdate.Icon = data.Icon;
                        appUpdate.ChangedBy = appCr.CreatedBy;
                        appUpdate.ChangedDate = appCr.CreatedDate;

                        _appRepository.Save(appUpdate);
                    }
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(ApplicationResources.Validation_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        [HttpPost]
        public ActionResult ApproveFunction(FunctionCrView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                AuthorizationFunctionCr appCr = _functionCrRepository.getFunction(Convert.ToInt32(data.Id.Substring(1, data.Id.Length - 1)));
                appCr.Approved = true;
                appCr.ApprovedBy = user.Username;
                appCr.ApprovedDate = DateTime.Now;
                appCr.ApprovalStatus = data.ApprovalStatus;
                _functionCrRepository.Save(appCr);
                if (data.ApprovalStatus == "A")
                {
                    if (data.ApprovalType == "C")
                    {
                        AuthorizationFunction newData = new AuthorizationFunction(0L);
                        newData.Application = data.ApplicationID;
                        newData.FunctionId = data.FunctionId;
                        newData.Name = data.Name;
                        newData.Description = data.Description;
                        newData.ChangedBy = user.Username;
                        newData.ChangedDate = DateTime.Now;
                        newData.CreatedBy = user.Username;
                        newData.CreatedDate = DateTime.Now;
                        _funcRepository.Add(newData);
                    }
                    else if (data.ApprovalType == "D")
                    {
                        AuthorizationFunction appDelete = _funcRepository.getFunction(data.FunctionId);
                        _funcRepository.Remove(appDelete);

                    }
                    else
                    {
                        AuthorizationFunction appUpdate = _funcRepository.getFunction(data.FunctionId);
                        appUpdate.Application = data.ApplicationID;
                        appUpdate.FunctionId = data.FunctionId;
                        appUpdate.Name = data.Name;
                        appUpdate.Description = data.Description;
                        appUpdate.ChangedBy = user.Username;
                        appUpdate.ChangedDate = DateTime.Now;
                        _funcRepository.Save(appUpdate);
                    }
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(FunctionResources.Validation_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        [HttpPost]
        public ActionResult ApproveFeature(FeatureCrView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                AuthorizationFeatureCr appCr = _featureCrRepository.getFeature(Convert.ToInt32(data.Id.Substring(1, data.Id.Length - 1)));
                appCr.Approved = true;
                appCr.ApprovedBy = user.Username;
                appCr.ApprovedDate = DateTime.Now;
                appCr.ApprovalStatus = data.ApprovalStatus;
                _featureCrRepository.Save(appCr);

                if (data.ApprovalStatus == "A")
                {
                    if (data.ApprovalType == "C")
                    {
                        AuthorizationFeature newData = new AuthorizationFeature(0L);
                        newData.Application = data.ApplicationID;
                        newData.FeatureId = data.FeatureId;
                        newData.Name = data.Name;
                        newData.Description = data.Description;
                        newData.ChangedBy = user.Username;
                        newData.ChangedDate = DateTime.Now;
                        newData.CreatedBy = user.Username;
                        newData.CreatedDate = DateTime.Now;
                        _featureRepository.Add(newData);
                    }
                    else if (data.ApprovalType == "D")
                    {
                        AuthorizationFeature delData = _featureRepository.getFeature(data.FeatureId);
                        _featureRepository.Remove(delData);

                    }
                    else
                    {
                        AuthorizationFeature newData = _featureRepository.getFeature(data.FeatureId);

                        newData.Application = data.ApplicationID;
                        newData.FeatureId = data.FeatureId;
                        newData.Name = data.Name;
                        newData.Description = data.Description;
                        newData.ChangedBy = user.Username;
                        newData.ChangedDate = DateTime.Now;

                        _featureRepository.Save(newData);
                    }
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(FeatureResources.Validation_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        [HttpPost]
        public ActionResult ApproveRole(RoleCrView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                RoleCr appCr = _roleCrRepository.getRole(Convert.ToInt32(data.Id.Substring(1, data.Id.Length - 1)));
                appCr.Approved = true;
                appCr.ApprovedBy = user.Username;
                appCr.ApprovedDate = DateTime.Now;
                appCr.ApprovalStatus = data.ApprovalStatus;
                _roleCrRepository.Save(appCr);

                if (data.ApprovalStatus == "A")
                {
                    if (data.ApprovalType == "C")
                    {
                        Role oldData = new Role(0L);
                        Role newData = new Role(0L);
                        newData._Application = data.ApplicationID;
                        newData.RoleId = data.RoleId;
                        newData.Name = data.Name;
                        newData.Description = data.Description;
                        newData.SessionTimeout = data.SessionTimeout;
                        newData.LockTimeout = data.LockTimeout;
                        newData.ChangedBy = user.Username;
                        newData.ChangedDate = DateTime.Now;
                        newData.CreatedBy = user.Username;
                        newData.CreatedDate = DateTime.Now;

                        _roleRepository.Add(newData);
                        //_auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Create");
                        //_auditRepository.SaveAuditTrail(Settings.ModuleName, oldData, newData, newData.CreatedBy, newData.CreatedDate, user.Username, "Create");
                    }
                    else if (data.ApprovalType == "D")
                    {
                        Role oldData = new Role(0L);
                        Role newData = _roleRepository.getRole(data.RoleId);
                        //_auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Delete");
                        //_auditRepository.SaveAuditTrail(Settings.ModuleName, oldData, newData, newData.CreatedBy, newData.CreatedDate, user.Username, "Delete");
                        _roleRepository.Remove(newData);
                    }
                    else
                    {
                        Role oldData = _roleRepository.getRole(data.RoleId);
                        Role newData = _roleRepository.getRole(data.RoleId);

                        newData._Application = data.ApplicationID;
                        newData.RoleId = data.RoleId;
                        newData.Name = data.Name;
                        newData.Description = data.Description;
                        newData.SessionTimeout = data.SessionTimeout;
                        newData.LockTimeout = data.LockTimeout;
                        newData.ChangedBy = user.Username;
                        newData.ChangedDate = DateTime.Now;

                        //_auditRepository.SaveAuditTrail(Settings.ModuleName, oldData, newData, newData.CreatedBy, newData.CreatedDate, user.Username, "Edit");
                        _roleRepository.Save(newData);
                    }
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(RoleResources.Validation_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        [HttpPost]
        public ActionResult ApproveMenu(MenuCrView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                MenuCr appCr = _menuCrRepository.getForEditMode(Convert.ToInt32(data.Id.Substring(1, data.Id.Length - 1)));
                appCr.Approved = true;
                appCr.ApprovedBy = user.Username;
                appCr.ApprovedDate = DateTime.Now;
                appCr.ApprovalStatus = data.ApprovalStatus;
                _menuCrRepository.Save(appCr);
                if (data.ApprovalStatus == "A")
                {
                    if (data.ApprovalType == "C")
                    {
                        Menu oldData = new Menu(0L);
                        Menu newData = new Menu(0L);
                        newData.MenuId = data.MenuId;
                        newData.MenuName = data.MenuName;
                        newData.ParentMenu = "0";
                        newData.Application = data.Application;
                        newData.FunctionId = data.FunctionId;
                        newData.MenuLevel = 0;
                        newData.MenuUrl = data.MenuUrl;
                        newData.MenuOrder = data.MenuOrder;
                        newData.MenuIcon = data.MenuIcon;
                        newData.Active = data.Active;
                        newData.ChangedBy = user.Username;
                        newData.ChangedDate = DateTime.Now;
                        newData.CreatedBy = user.Username;
                        newData.CreatedDate = DateTime.Now;

                        _menuRepository.Add(newData);
                        //_auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Create");
                        //_auditRepository.SaveAuditTrail(Settings.ModuleName, oldData, newData, newData.CreatedBy, newData.CreatedDate, user.Username, "Create");
                        _userRepository.setUpdateMenu(true);
                    }
                    else if (data.ApprovalType == "N")
                    {
                        Menu oldData = new Menu(0L);
                        Menu newData = new Menu(0L);
                        newData.Application = data.Application;
                        newData.ParentMenu = data.ParentMenu;
                        newData.MenuId = data.MenuId;
                        newData.MenuName = data.MenuName;
                        newData.FunctionId = data.FunctionId;
                        newData.MenuUrl = data.MenuUrl;
                        newData.MenuLevel = 1;
                        newData.MenuOrder = data.MenuOrder;
                        newData.MenuIcon = data.MenuIcon;
                        newData.Active = data.Active;
                        newData.ChangedBy = user.Username;
                        newData.ChangedDate = DateTime.Now;
                        newData.CreatedBy = user.Username;
                        newData.CreatedDate = DateTime.Now;
                        _menuRepository.Add(newData);

                        //_auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Add");
                        //_auditRepository.SaveAuditTrail(Settings.ModuleName, oldData, newData, newData.CreatedBy, newData.CreatedDate, user.Username, "Add");
                        _userRepository.setUpdateMenu(true);
                    }
                    else if (data.ApprovalType == "U")
                    {
                        Menu oldData = _menuRepository.getForEditMode(data.MenuId);
                        Menu newData = _menuRepository.getForEditMode(data.MenuId);

                        newData.Application = data.Application;
                        newData.ParentMenu = data.ParentMenu;
                        newData.MenuId = data.MenuId;
                        newData.MenuName = data.MenuName;
                        newData.FunctionId = data.FunctionId;
                        newData.MenuUrl = data.MenuUrl;
                        newData.MenuOrder = data.MenuOrder;
                        newData.MenuIcon = data.MenuIcon;
                        newData.Active = data.Active;
                        newData.ChangedBy = user.Username;
                        newData.ChangedDate = DateTime.Now;
                        _menuRepository.Save(newData);
                        //_auditRepository.SaveAuditTrail(Settings.ModuleName, oldData, newData, newData.CreatedBy, newData.CreatedDate, user.Username, "Edit");
                        _userRepository.setUpdateMenu(true);
                    }
                    else if (data.ApprovalType == "D")
                    {
                        Menu oldData = new Menu(0L);
                        Menu newData = _menuRepository.getForEditMode(data.MenuId);
                        _menuRepository.Remove(newData);
                        //_auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Delete");
                        //_auditRepository.SaveAuditTrail(Settings.ModuleName, oldData, newData, newData.CreatedBy, newData.CreatedDate, user.Username, "Delete");
                        _userRepository.setUpdateMenu(true);
                    }
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(MenuResources.Validation_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        [HttpPost]
        public ActionResult ApproveBranch(BranchCrView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                BranchCr appCr = _branchCrRepository.getBranch(Convert.ToInt32(data.Id.Substring(1, data.Id.Length - 1)));
                appCr.Approved = true;
                appCr.ApprovedBy = user.Username;
                appCr.ApprovedDate = DateTime.Now;
                appCr.ApprovalStatus = data.ApprovalStatus;
                _branchCrRepository.Save(appCr);

                if (data.ApprovalStatus == "A")
                {
                    if (data.ApprovalType == "C")
                    {
                        Branch oldData = new Branch(0L);
                        Branch newData = new Branch(0L);
                        newData.BranchCode = data.BranchCode;
                        newData.Description = data.Description;
                        newData.MainBranch = data.MainBranchID;

                        newData.ChangedBy = user.Username;
                        newData.ChangedDate = DateTime.Now;
                        newData.CreatedBy = user.Username;
                        newData.CreatedDate = DateTime.Now;

                        _branchRepository.Add(newData);
                        //_auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Create");
                        //_auditRepository.SaveAuditTrail(Settings.ModuleName, oldData, newData, newData.CreatedBy, newData.CreatedDate, user.Username, "Create");
                    }
                    else if (data.ApprovalType == "U")
                    {
                        Branch oldData = _branchRepository.getBranch(data.BranchCode);
                        Branch newData = _branchRepository.getBranch(data.BranchCode);

                        newData.Description = data.Description;
                        newData.MainBranch = data.MainBranchID;

                        newData.ChangedBy = user.Username;
                        newData.ChangedDate = DateTime.Now;
                        _branchRepository.Save(newData);
                        //_auditRepository.SaveAuditTrail(Settings.ModuleName, oldData, newData, newData.CreatedBy, newData.CreatedDate, user.Username, "Edit");
                    }
                    else if (data.ApprovalType == "D")
                    {
                        Branch oldData = new Branch(0L);
                        Branch newData = _branchRepository.getBranch(data.BranchCode);
                        _branchRepository.Remove(newData);
                        //_auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Delete");
                        //_auditRepository.SaveAuditTrail(Settings.ModuleName, oldData, newData, newData.CreatedBy, newData.CreatedDate, user.Username, "Delete");
                    }
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(BranchResources.Validation_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        [HttpPost]
        public ActionResult ApproveDocType(DocumentTypeCrView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                DocumentTypeCr appCr = _documentTypeCrRepository.getDocType(Convert.ToInt32(data.Id.Substring(1, data.Id.Length - 1)));
                appCr.Approved = true;
                appCr.ApprovedBy = user.Username;
                appCr.ApprovedDate = DateTime.Now;
                appCr.ApprovalStatus = data.ApprovalStatus;
                _documentTypeCrRepository.Save(appCr);

                if (data.ApprovalStatus == "A")
                {
                    if (data.ApprovalType == "C")
                    {
                        DocumentType oldData = new DocumentType(0L);
                        DocumentType newData = new DocumentType(0L);
                        newData.DocType = data.DocType;
                        newData.Description = data.Description;

                        newData.ChangedBy = user.Username;
                        newData.ChangedDate = DateTime.Now;
                        newData.CreatedBy = user.Username;
                        newData.CreatedDate = DateTime.Now;

                        _documentTypeRepository.Add(newData);
                        //_auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Create");
                        //_auditRepository.SaveAuditTrail(Settings.ModuleName, oldData, newData, newData.CreatedBy, newData.CreatedDate, user.Username, "Create");
                    }
                    else if (data.ApprovalType == "U")
                    {
                        DocumentType oldData = _documentTypeRepository.getDocumentType(data.DocTypeId);
                        DocumentType newData = _documentTypeRepository.getDocumentType(data.DocTypeId);

                        newData.Description = data.Description;
                        newData.DocType = data.DocType;

                        newData.ChangedBy = user.Username;
                        newData.ChangedDate = DateTime.Now;
                        _documentTypeRepository.Save(newData);
                        //_auditRepository.SaveAuditTrail(Settings.ModuleName, oldData, newData, newData.CreatedBy, newData.CreatedDate, user.Username, "Edit");
                    }
                    else if (data.ApprovalType == "D")
                    {
                        DocumentType oldData = new DocumentType(0L);
                        DocumentType newData = _documentTypeRepository.getDocumentType(data.DocTypeId);
                        _documentTypeRepository.Remove(newData);
                        //_auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Delete");
                        //_auditRepository.SaveAuditTrail(Settings.ModuleName, oldData, newData, newData.CreatedBy, newData.CreatedDate, user.Username, "Delete");
                    }
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(SvsResources.Validation_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }
        
        [HttpPost]
        public ActionResult ApproveRolDocType(RoleDocTypeCrView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                RoleDocTypeCr appCr = _roleDocumentTypeCrRepository.getRoleDocType(Convert.ToInt32(data.Id.Substring(1, data.Id.Length - 1)));
                appCr.Approved = true;
                appCr.ApprovedBy = user.Username;
                appCr.ApprovedDate = DateTime.Now;
                appCr.ApprovalStatus = data.ApprovalStatus;
                _roleDocumentTypeCrRepository.Save(appCr);

                if (data.ApprovalStatus == "A")
                {
                    if (data.ApprovalType == "C")
                    {
                        RoleDocType oldData = new RoleDocType(0L);
                        RoleDocType newData = new RoleDocType(0L);
                        newData.RoleId =long.Parse(data.RoleId);
                        newData.DocumentTypeId = long.Parse(data.DocumentTypeId);

                        newData.ChangedBy = user.Username;
                        newData.ChangedDate = DateTime.Now;
                        newData.CreatedBy = user.Username;
                        newData.CreatedDate = DateTime.Now;

                        _roleDocumentTypeRepository.Add(newData);
                        //_auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Create");
                        //_auditRepository.SaveAuditTrail(Settings.ModuleName, oldData, newData, newData.CreatedBy, newData.CreatedDate, user.Username, "Create");
                    }
                    else if (data.ApprovalType == "U")
                    {
                        RoleDocType oldData = _roleDocumentTypeRepository.getRoleDocType(long.Parse(data.Id));
                        RoleDocType newData = _roleDocumentTypeRepository.getRoleDocType(long.Parse(data.Id));

                        newData.RoleId = long.Parse(data.RoleId);
                        newData.DocumentTypeId = long.Parse(data.DocumentTypeId);

                        newData.ChangedBy = user.Username;
                        newData.ChangedDate = DateTime.Now;
                        _roleDocumentTypeRepository.Save(newData);
                        //_auditRepository.SaveAuditTrail(Settings.ModuleName, oldData, newData, newData.CreatedBy, newData.CreatedDate, user.Username, "Edit");
                    }
                    else if (data.ApprovalType == "D")
                    {
                        RoleDocType oldData = new RoleDocType(0L);
                        RoleDocType newData = _roleDocumentTypeRepository.getRoleDocType(long.Parse(data.RoleId),long.Parse(data.DocumentTypeId));
                        _roleDocumentTypeRepository.Remove(newData);
                        //_auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Delete");
                        //_auditRepository.SaveAuditTrail(Settings.ModuleName, oldData, newData, newData.CreatedBy, newData.CreatedDate, user.Username, "Delete");
                    }
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(SvsResources.Validation_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }
        [HttpPost]
        public ActionResult ApproveFunctionFeature(FunctionFeatureCrView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                if (data.ApprovalStatus == "A")
                {
                    IList<FunctionFeatureCr> newFF = _funcfeatCrRepository.getFunctionFeatures(Convert.ToInt32(data.FunctionId));
                    foreach (FunctionFeatureCr fcr in newFF)
                    {
                        FunctionFeatureCr appCr = _funcfeatCrRepository.getFunctionFeature(fcr.Id);
                        appCr.Approved = true;
                        appCr.ApprovedBy = user.Username;
                        appCr.ApprovedDate = DateTime.Now;
                        appCr.ApprovalStatus = data.ApprovalStatus;
                        _funcfeatCrRepository.Save(appCr);

                        if (fcr.ApprovalType == "C")
                        {
                            FunctionFeature oldData = new FunctionFeature(0L);
                            FunctionFeature newData = new FunctionFeature(0L);
                            newData.FeatureId = fcr.FeatureId;
                            newData.FunctionId = fcr.FunctionId;
                            newData.ChangedBy = user.Username;
                            newData.ChangedDate = DateTime.Now;
                            newData.CreatedBy = user.Username;
                            newData.CreatedDate = DateTime.Now;

                            _funcfeatRepository.Add(newData);
                            //_auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Create");
                            //_auditRepository.SaveAuditTrail(Settings.ModuleName, oldData, newData, newData.CreatedBy, newData.CreatedDate, user.Username, "Create");
                        }
                        else if (data.ApprovalType == "D")
                        {
                            FunctionFeature oldData = new FunctionFeature(0L);
                            FunctionFeature newData = _funcfeatRepository.getFunctionFeature(fcr.FunctionId, fcr.FeatureId);
                            _funcfeatRepository.deleteSelected(fcr.FunctionId, fcr.FeatureId);
                            //_auditRepository.SaveAuditTrail(Settings.ModuleName, new { fcr.FunctionId, fcr.FeatureId }, user.Username, "Delete");
                            //_auditRepository.SaveAuditTrail(Settings.ModuleName, oldData, newData, newData.CreatedBy, newData.CreatedDate, user.Username, "Delete");
                        }
                    }
                }
                else
                {
                    IList<FunctionFeatureCr> newFF = _funcfeatCrRepository.getFunctionFeatures(Convert.ToInt32(data.FunctionId));
                    foreach (FunctionFeatureCr fcr in newFF)
                    {
                        FunctionFeatureCr appCr = _funcfeatCrRepository.getFunctionFeature(fcr.Id);
                        appCr.Approved = true;
                        appCr.ApprovedBy = user.Username;
                        appCr.ApprovedDate = DateTime.Now;
                        appCr.ApprovalStatus = data.ApprovalStatus;
                        _funcfeatCrRepository.Save(appCr);
                    }
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(FunctionResources.Validation_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        [HttpPost]
        public ActionResult ApproveRolDocTypeList(RoleDocTypeCrView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                if (data.ApprovalStatus == "A")
                {
                    IList<RoleDocTypeCr> newRF = _roleDocumentTypeCrRepository.getDocTypes2(Convert.ToInt32(data.RoleId));
                    foreach (RoleDocTypeCr rfcr in newRF)
                    {
                        RoleDocTypeCr appCr = _roleDocumentTypeCrRepository.getRoleDocType(rfcr.Id);
                        appCr.Approved = true;
                        appCr.ApprovedBy = user.Username;
                        appCr.ApprovedDate = DateTime.Now;
                        appCr.ApprovalStatus = data.ApprovalStatus;
                        _roleDocumentTypeCrRepository.Save(appCr);

                        if (rfcr.ApprovalType == "C")
                        {
                            RoleDocType oldData = new RoleDocType(0L);
                            RoleDocType newData = new RoleDocType(0L);
                            newData.RoleId = rfcr.RoleId;
                            newData.DocumentTypeId = rfcr.DocumentTypeId;
                            newData.ChangedBy = user.Username;
                            newData.ChangedDate = DateTime.Now;
                            newData.CreatedBy = user.Username;
                            newData.CreatedDate = DateTime.Now;

                            _roleDocumentTypeRepository.Add(newData);
                            //_auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Create");
                            //_auditRepository.SaveAuditTrail(Settings.ModuleName, oldData, newData, newData.CreatedBy, newData.CreatedDate, user.Username, "Create");
                        }
                        else if (data.ApprovalType == "D")
                        {
                            _roleDocumentTypeRepository.deleteSelected(rfcr.RoleId, rfcr.DocumentTypeId);
                            //_auditRepository.SaveAuditTrail(Settings.ModuleName, new { rfcr.RoleId, rfcr.DocumentTypeId }, user.Username, "Delete");
                        }
                    }
                }
                else
                {
                    IList<RoleDocTypeCr> newRF = _roleDocumentTypeCrRepository.getDocTypes2(Convert.ToInt32(data.RoleId));
                    foreach (RoleDocTypeCr rfcr in newRF)
                    {
                        RoleDocTypeCr appCr = _roleDocumentTypeCrRepository.getRoleDocType(rfcr.Id);
                        //_auditRepository.SaveAuditTrail(Settings.ModuleName, appCr, user.Username, "BeforeEdit");
                        appCr.Approved = true;
                        appCr.ApprovedBy = user.Username;
                        appCr.ApprovedDate = DateTime.Now;
                        appCr.ApprovalStatus = data.ApprovalStatus;
                        _roleDocumentTypeCrRepository.Save(appCr);
                    }
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(RoleResources.Validation_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        [HttpPost]
        public ActionResult ApproveRoleFunction(RoleFunctionCrView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                if (data.ApprovalStatus == "A")
                {
                    IList<RoleFunctionCr> newRF = _rolefuncCrRepository.getFunctions(Convert.ToInt32(data.RoleId));
                    foreach (RoleFunctionCr rfcr in newRF)
                    {
                        RoleFunctionCr appCr = _rolefuncCrRepository.getRoleFunction(rfcr.Id);
                        //_auditRepository.SaveAuditTrail(Settings.ModuleName, appCr, user.Username, "BeforeEdit");
                        appCr.Approved = true;
                        appCr.ApprovedBy = user.Username;
                        appCr.ApprovedDate = DateTime.Now;
                        appCr.ApprovalStatus = data.ApprovalStatus;
                        _rolefuncCrRepository.Save(appCr);
                        //_auditRepository.SaveAuditTrail(Settings.ModuleName, appCr, user.Username, "AfterEdit");

                        if (rfcr.ApprovalType == "C")
                        {
                            RoleFunction oldData = new RoleFunction(0L);
                            RoleFunction newData = new RoleFunction(0L);
                            newData.RoleId = rfcr.RoleId;
                            newData.FunctionId = rfcr.FunctionId;
                            newData.ChangedBy = user.Username;
                            newData.ChangedDate = DateTime.Now;
                            newData.CreatedBy = user.Username;
                            newData.CreatedDate = DateTime.Now;

                            _rolefuncRepository.Add(newData);
                            //_auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Create");
                            //_auditRepository.SaveAuditTrail(Settings.ModuleName, oldData, newData, newData.CreatedBy, newData.CreatedDate, user.Username, "Create");
                        }
                        else if (data.ApprovalType == "D")
                        {
                            RoleFunction oldData = new RoleFunction(0L);
                            RoleFunction newData = _rolefuncRepository.getRoleFunc(rfcr.RoleId, rfcr.FunctionId);
                            _rolefuncRepository.deleteSelected(rfcr.RoleId, rfcr.FunctionId);
                            //_auditRepository.SaveAuditTrail(Settings.ModuleName, new { rfcr.RoleId, rfcr.FunctionId }, user.Username, "Delete");
                            //_auditRepository.SaveAuditTrail(Settings.ModuleName, oldData, newData, newData.CreatedBy, newData.CreatedDate, user.Username, "Delete");
                        }
                    }
                }
                else
                {
                    IList<RoleFunctionCr> newRF = _rolefuncCrRepository.getFunctions(Convert.ToInt32(data.RoleId));
                    foreach (RoleFunctionCr rfcr in newRF)
                    {
                        RoleFunctionCr appCr = _rolefuncCrRepository.getRoleFunction(rfcr.Id);
                        //_auditRepository.SaveAuditTrail(Settings.ModuleName, appCr, user.Username, "BeforeEdit");
                        appCr.Approved = true;
                        appCr.ApprovedBy = user.Username;
                        appCr.ApprovedDate = DateTime.Now;
                        appCr.ApprovalStatus = data.ApprovalStatus;
                        _rolefuncCrRepository.Save(appCr);
                    }
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(RoleResources.Validation_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        [HttpPost]
        public ActionResult ApproveRoleFunctionFeature(RoleFunctionCrView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                if (data.ApprovalStatus == "A")
                {
                    IList<RoleFunctionFeatureCr> newRFF = _rolefuncfeatCrRepository.getRoleFuncFeaturebyRole(Convert.ToInt32(data.RoleId));
                    foreach (RoleFunctionFeatureCr rfcr in newRFF)
                    {
                        RoleFunctionFeatureCr appCr = _rolefuncfeatCrRepository.getRoleFuncFeature(rfcr.Id);
                        //_auditRepository.SaveAuditTrail(Settings.ModuleName, appCr, user.Username, "BeforeEdit");
                        appCr.Approved = true;
                        appCr.ApprovedBy = user.Username;
                        appCr.ApprovedDate = DateTime.Now;
                        appCr.ApprovalStatus = data.ApprovalStatus;
                        _rolefuncfeatCrRepository.Save(appCr);
                        //_auditRepository.SaveAuditTrail(Settings.ModuleName, appCr, user.Username, "AfterEdit");

                        if (rfcr.ApprovalType == "C")
                        {
                            RoleFunctionFeature oldData = new RoleFunctionFeature(0L);
                            RoleFunctionFeature newData = new RoleFunctionFeature(0L);
                            newData.RoleId = rfcr.RoleId;
                            newData.FunctionId = rfcr.FunctionId;
                            newData.FeatureId = rfcr.FeatureId;
                            newData.RoleFunctionId = rfcr.RoleFunctionId;
                            newData.ChangedBy = user.Username;
                            newData.ChangedDate = DateTime.Now;
                            newData.CreatedBy = user.Username;
                            newData.CreatedDate = DateTime.Now;

                            _rolefuncfeatRepository.Add(newData);
                            //_auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Create");
                            //_auditRepository.SaveAuditTrail(Settings.ModuleName, oldData, newData, newData.CreatedBy, newData.CreatedDate, user.Username, "Create");
                        }
                        else if (data.ApprovalType == "D")
                        {
                            RoleFunctionFeature oldData = new RoleFunctionFeature(0L);
                            RoleFunctionFeature newData = _rolefuncfeatRepository.getRoleFuncFeature(rfcr.RoleId, rfcr.FunctionId, rfcr.FeatureId);
                            _rolefuncfeatRepository.deleteSelected(rfcr.RoleId, rfcr.FunctionId, rfcr.FeatureId);
                            //_auditRepository.SaveAuditTrail(Settings.ModuleName, new { rfcr.RoleId, rfcr.FunctionId, rfcr.FeatureId }, user.Username, "Delete");
                            //_auditRepository.SaveAuditTrail(Settings.ModuleName, oldData, newData, newData.CreatedBy, newData.CreatedDate, user.Username, "Delete");
                        }
                    }
                }
                else
                {
                    IList<RoleFunctionFeatureCr> newRFF = _rolefuncfeatCrRepository.getRoleFuncFeaturebyRole(Convert.ToInt32(data.RoleId));
                    foreach (RoleFunctionFeatureCr rfcr in newRFF)
                    {
                        RoleFunctionFeatureCr appCr = _rolefuncfeatCrRepository.getRoleFuncFeature(rfcr.Id);
                        //_auditRepository.SaveAuditTrail(Settings.ModuleName, appCr, user.Username, "BeforeEdit");
                        appCr.Approved = true;
                        appCr.ApprovedBy = user.Username;
                        appCr.ApprovedDate = DateTime.Now;
                        appCr.ApprovalStatus = data.ApprovalStatus;
                        _rolefuncfeatCrRepository.Save(appCr);
                    }
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(RoleResources.Validation_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return View("DetailRoleFunctionFeature", data);
        }

        private ViewResult CreateView(ApplicationCrView data)
        {
            return View("DetailApp", data);
        }
        private ViewResult CreateView(FunctionCrView data)
        {
            return View("DetailFuntion", data);
        }
        private ViewResult CreateView(FeatureCrView data)
        {
            return View("DetailFeature", data);
        }
        private ViewResult CreateView(RoleCrView data)
        {
            return View("DetailRole", data);
        }
        private ViewResult CreateView(MenuCrView data)
        {
            return View("DetailMenu", data);
        }
        private ViewResult CreateView(BranchCrView data)
        {
            return View("DetailBranch", data);
        }
        private ViewResult CreateView(DocumentTypeCrView data)
        {
            return View("DetailDocType", data);
        }
        private ViewResult CreateView(RoleDocTypeCrView data)
        {
            return View("DetailRoleDocType", data);
        }
        private ViewResult CreateView(FunctionFeatureCrView data)
        {
            return View("DetailFunctionFeature", data);
        }
        private ViewResult CreateView(RoleFunctionCrView data)
        {
            return View("DetailRoleFunction", data);
        }
        #endregion

        private IList<SelectListItem> createApplicationSelect(string selected)
        {
            IList<SelectListItem> dataList = _appRepository.FindAll().ToList().ConvertAll<SelectListItem>(new Converter<Application, SelectListItem>(ConvertFrom));
            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected).Selected = true;
            }
            return dataList;
        }

        private IList<SelectListItem> createFunctionSelect(string selected)
        {
            IList<SelectListItem> dataList = _funcRepository.FindAll().ToList().ConvertAll<SelectListItem>(new Converter<AuthorizationFunction, SelectListItem>(ConvertFrom));
            dataList.Insert(0, new SelectListItem { Text = "", Value = "" });
            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected).Selected = true;
            }
            return dataList;
        }

        private IList<SelectListItem> createParentMenuSelect(string selected)
        {
            IList<SelectListItem> dataList = _menuRepository.FindAll().Where(x => x.ParentMenu == "0" && x.Active == true).ToList().ConvertAll<SelectListItem>(new Converter<Menu, SelectListItem>(ConvertFromMenu));
            dataList.Insert(0, new SelectListItem { Text = "", Value = "" });
            if (!selected.IsNullOrEmpty())
            {
                if (Convert.ToInt16(selected) != 0)
                {
                    dataList.FindElement(item => item.Value == selected).Selected = true;
                }

            }
            return dataList;
        }

        private IList<SelectListItem> createMainBranchSelect(string selected)
        {
            IList<Branch> branchs = _branchRepository.getBranchs();
            var gb = branchs.AsEnumerable().GroupBy(g => g.MainBranch);

            List<Branch> mainbranchs = new List<Branch>();

            foreach (IGrouping<string, Branch> b in gb)
            {
                Branch nb = branchs.AsEnumerable().Where(t => t.BranchCode == b.Key).SingleOrDefault();
                if (!nb.IsNull())
                {
                    if (!mainbranchs.Contains(nb))
                        mainbranchs.Add(nb);
                }
            }

            IList<SelectListItem> dataList = mainbranchs.ToList().ConvertAll<SelectListItem>(new Converter<Branch, SelectListItem>(ConvertFromSelect));

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected).Selected = true;
            }
            return dataList;
        }

        public SelectListItem ConvertFromSelect(Branch item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.Description;
            returnItem.Value = item.BranchCode;
            return returnItem;
        }
        public SelectListItem ConvertFrom(Application item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.Name;
            returnItem.Value = item.ApplicationId;
            return returnItem;
        }

        public SelectListItem ConvertFrom(AuthorizationFunction item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.Name;
            returnItem.Value = item.FunctionId;
            return returnItem;
        }
        public SelectListItem ConvertFromMenu(Menu item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.MenuName;
            returnItem.Value = item.Id.ToString();
            return returnItem;
        }

    }
}