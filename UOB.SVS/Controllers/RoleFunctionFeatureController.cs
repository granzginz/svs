﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using Treemas.Credential.Model;
using UOB.SVS.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using UOB.SVS.Resources;
using UOB.SVS.Interface;
using UOB.SVS.Model;

namespace UOB.SVS.Controllers
{
    public class RoleFunctionFeatureController : PageController
    {
        private IRoleRepository _roleRepository;
        private IFunctionRepository _funcRepository;
        private IRoleFunctionRepository _rolefuncRepository;
        private IFeatureRepository _featRepository;
        private IRoleFunctionFeatureRepository _rolefuncfeatRepository;
        private IFunctionFeatureRepository _funcfeatRepository;
        private IAppAuditTrailLog _auditRepository;
        private IRoleFunctionCrRepository _rolefuncCrRepository;
        private IRoleFunctionFeatureCrRepository _rolefuncfeatCrRepository;
        private IVFunctionFeatureRepository _vfunctfeatRepository;

        public IList<RoleFunctionFeature> roleFuncFeatList;
        public IList<RoleFunctionFeatureCr> roleFuncFeatCrList;
        public List<RoleFunctionFeatSelectHelper> roleFunctFeatSelectList;

        public RoleFunctionFeatureController(ISessionAuthentication sessionAuthentication, IRoleRepository roleRepository,
            IRoleFunctionRepository rolefuncRepository, IFunctionRepository funcRepository, IFeatureRepository featRepository,
            IRoleFunctionFeatureRepository rolefuncfeatRepository, IFunctionFeatureRepository funcfeatRepository, IAppAuditTrailLog auditRepository,
            IRoleFunctionCrRepository rolefuncCrRepository, IRoleFunctionFeatureCrRepository rolefuncfeatCrRepository, IVFunctionFeatureRepository vfunctfeatRepository)
            : base(sessionAuthentication)
        {
            this._roleRepository = roleRepository;
            this._rolefuncRepository = rolefuncRepository;
            this._funcRepository = funcRepository;
            this._featRepository = featRepository;
            this._rolefuncfeatRepository = rolefuncfeatRepository;
            this._funcfeatRepository = funcfeatRepository;
            this._auditRepository = auditRepository;
            this._rolefuncCrRepository = rolefuncCrRepository;
            this._rolefuncfeatCrRepository = rolefuncfeatCrRepository;
            this._vfunctfeatRepository = vfunctfeatRepository;

            Settings.Title = RoleResources.PageTitleRoleFunctionFeature;
            Settings.ModuleName = "Role Function Feature";
        }
        protected override void Startup()
        {
            var returnpage = new { controller = "RoleFunction", action = "Index" };
            try
            {
                //int RoleFuncid = Convert.ToInt32(Request.RequestContext.RouteData.Values["Id"]);
                //if (RoleFuncid < 1)
                //    Response.RedirectToRoute(returnpage);

                //RoleFunctionCr roleFunc = null;
                //AuthorizationFunction Func = null;

                //roleFunc = _rolefuncCrRepository.getRoleFunction(RoleFuncid);
                //if (roleFunc.IsNull())
                //    Response.RedirectToRoute(returnpage);

                //Func = _funcRepository.getFunction(roleFunc.FunctionId);
                //Lookup.Remove<RoleFunction>();
                //Lookup.Add(roleFunc);
                //ViewData["RoleFunction"] = roleFunc;
                //ViewData["Function"] = Func;
                //Role role = _roleRepository.getRole(roleFunc.RoleId);

                #region OldCode
                int RoleFuncid = Convert.ToInt32(Request.RequestContext.RouteData.Values["Id"]);
                int RoleFuncCrid = Convert.ToInt32(Request.RequestContext.RouteData.Values["Id"]);
                RoleFunction roleFunc = null;
                RoleFunctionCr roleFuncCr = null;

                AuthorizationFunction Func = null;
                if (RoleFuncid < 1)
                    Response.RedirectToRoute(returnpage);

                roleFunc = _rolefuncRepository.getRoleFunction(RoleFuncid);
                roleFuncCr = _rolefuncCrRepository.getRoleFunction(RoleFuncid);

                Func = _funcRepository.getFunction(roleFunc.FunctionId);
                if (roleFunc.IsNull())
                    Response.RedirectToRoute(returnpage);

                Role role = _roleRepository.getRole(roleFunc.RoleId);

                Lookup.Remove<RoleFunction>();
                Lookup.Add(roleFunc);
                Lookup.Remove<RoleFunctionCr>();
                Lookup.Add(roleFuncCr);

                ViewData["RoleFunction"] = roleFunc;
                ViewData["RoleFunctionCr"] = roleFuncCr;
                ViewData["Function"] = Func;
                ViewData["Role"] = role;

                roleFunctFeatSelectList = new List<RoleFunctionFeatSelectHelper>();
                Session.Add("rfflist", roleFunctFeatSelectList);
                //Role role = _roleRepository.getRole(roleFunc.RoleId);
                #endregion
            }
            catch
            {
                Response.RedirectToRoute(returnpage);
            }

        }
        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            try
            {
                RoleFunction roleFunc = Lookup.Get<RoleFunction>();
                RoleFunctionCr roleFuncCr = Lookup.Get<RoleFunctionCr>();

                if (roleFunc.IsNull() && roleFuncCr.IsNull())
                {
                    if (Request.QueryString.GetValues("Id").IsNullOrEmpty())
                        return null;
                    long Id = long.Parse(Request.QueryString.GetValues("Id")[0]);
                    roleFunc = _rolefuncRepository.getRoleFunction(Id);
                    roleFuncCr = _rolefuncCrRepository.getRoleFunction(Id);
                    Lookup.Remove<RoleFunction>();
                    Lookup.Add(roleFunc);
                    Lookup.Remove<RoleFunctionCr>();
                    Lookup.Add(roleFuncCr);
                }

                long roleId = roleFunc.IsNull() ? roleFuncCr.RoleId : roleFunc.RoleId;
                long functionId = roleFunc.IsNull() ? roleFuncCr.FunctionId : roleFunc.FunctionId;
                Role role = _roleRepository.getRole(roleId);

                // Initialization.   
                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                roleFuncFeatList = _rolefuncfeatRepository.getRoleFuncFeatures(roleId, functionId);
                roleFuncFeatCrList = _rolefuncfeatCrRepository.getRoleFuncFeatures(roleId, functionId);

                // Loading.   
                //PagedResult<FunctionFeature> data;
                PagedResult<VFunctionFeature> vdata;

                //if (searchValue.IsNullOrEmpty())
                //{
                //    data = _funcfeatRepository.FindAllPagedbyFunction(roleFunc.FunctionId, pageNumber, sortColumn, pageSize, sortColumnDir);
                //}
                //else
                //{
                vdata = _vfunctfeatRepository.FindAllPagedbyFunction(roleFunc.FunctionId, pageNumber, sortColumn, pageSize, sortColumnDir, searchColumn, searchValue);
                //foreach (VFunctionFeature vff in vdata.Items)
                //{
                //    FunctionFeature ff = new FunctionFeature(vff.Id);
                //    ff.FunctionId = vff.FunctionId;
                //    ff.FeatureId = vff.FeatureId;
                //    ffData.Add(ff);
                //}
                //}

                var items = vdata.Items.ToList().ConvertAll<RoleFunctionFeatureView>(new Converter<VFunctionFeature, RoleFunctionFeatureView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = vdata.TotalItems,
                    recordsFiltered = vdata.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }
        public RoleFunctionFeatureView ConvertFrom(VFunctionFeature item)
        {
            RoleFunction roleFunc = Lookup.Get<RoleFunction>();
            AuthorizationFeature feat = null;
            feat = _featRepository.getFeature(item.FeatureId);
            RoleFunctionFeatureView returnItem = new RoleFunctionFeatureView();
            var found = roleFuncFeatList.FindElement(x => x.FeatureId == item.FeatureId);
            var foundCr = roleFuncFeatCrList.FindElement(x => x.FeatureId == item.FeatureId);

            if (found.IsNull() && foundCr.IsNull())
            {
                returnItem.selected = false;
            }
            else
            {
                returnItem.selected = true;
                returnItem.Id = found.IsNull() ? foundCr.Id : found.Id;
                returnItem.RoleId = found.IsNull() ? foundCr.RoleId : found.RoleId;
                returnItem.FunctionId = found.IsNull() ? foundCr.FunctionId : found.FunctionId;
                returnItem.RoleFunctionId = found.IsNull() ? foundCr.RoleFunctionId : found.RoleFunctionId;
            }
            returnItem.FeatureId = item.FeatureId;
            returnItem.Name = feat.Name;
            return returnItem;
        }

        public ActionResult CreateTemp(RoleFunctionFeatSelectHelper select)
        {
            JsonResult result = new JsonResult();
            List<RoleFunctionFeatSelectHelper> cfflist = (List<RoleFunctionFeatSelectHelper>)Session["rfflist"];

            RoleFunctionFeatSelectHelper rff = cfflist.Find(f => f.FeatureId == select.FeatureId);

            if (!rff.IsNull())
            {
                if (rff.selected != select.selected)
                {
                    cfflist.Remove(rff);
                }
            }
            else
            {
                cfflist.Add(select);
            }


            Session["rfflist"] = cfflist;

            result = this.Json(new
            {
                message = "ok",
            }, JsonRequestBehavior.AllowGet);

            // Return info.   
            return result;
        }

        public ActionResult Create()
        {
            JsonResult result = new JsonResult();
            string message = "";
            long Id = 0;
            try
            {
                List<RoleFunctionFeatSelectHelper> cfflist = (List<RoleFunctionFeatSelectHelper>)Session["rfflist"];

                foreach (RoleFunctionFeatSelectHelper select in cfflist)
                {
                    RoleFunction roleFunc = Lookup.Get<RoleFunction>();
                    RoleFunctionCr roleFuncCr = Lookup.Get<RoleFunctionCr>();

                    long roleId = roleFunc.IsNull() ? roleFuncCr.RoleId : roleFunc.RoleId;
                    long functionId = roleFunc.IsNull() ? roleFuncCr.FunctionId : roleFunc.FunctionId;

                    RoleFunctionFeatureCr roleffCr = _rolefuncfeatCrRepository.getRoleFuncFeature(roleId, functionId, select.FeatureId);

                    if (roleFunc.IsNull())
                    {
                        roleFunc = new RoleFunction(roleFuncCr.Id);

                        roleFunc.FunctionId = roleFuncCr.FunctionId;
                        roleFunc.RoleId = roleFuncCr.RoleId;
                    }

                    if (select.selected == true)
                    {
                        if (roleffCr.IsNull())
                        {
                            Id = Insert(select.FeatureId, roleFunc);
                        }
                        else
                        {
                            if (roleffCr.ApprovalType == "D")
                            {
                                DeleteCr(roleFunc.RoleId, roleFunc.FunctionId, select.FeatureId);
                            }
                        }
                    }
                    else
                    {
                        if (!roleffCr.IsNull())
                        {
                            if (roleffCr.ApprovalType == "C")
                            {
                                message = DeleteCr(roleFunc.RoleId, roleFunc.FunctionId, select.FeatureId);
                            }
                        }
                        else
                        {
                            message = Delete(roleFunc.Id, roleFunc.RoleId, roleFunc.FunctionId, select.FeatureId);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            result = this.Json(new
            {
                message = RoleResources.Validation_Success,
                Id = Id
            }, JsonRequestBehavior.AllowGet);

            // Return info.   
            return result;
        }


        private long Insert(long featureId, RoleFunction roleFunc)
        {
            User user = Lookup.Get<User>();
            long returnId = -1;
            try
            {
                RoleFunctionFeatureCr newData = new RoleFunctionFeatureCr(0L);
                newData.FeatureId = featureId;
                newData.RoleFunctionId = roleFunc.Id;
                newData.FunctionId = roleFunc.FunctionId;
                newData.RoleId = roleFunc.RoleId;
                newData.ChangedBy = user.Username;
                newData.ChangedDate = DateTime.Now;
                newData.CreatedBy = user.Username;
                newData.CreatedDate = DateTime.Now;
                newData.ApprovalType = "C";
                newData.ApprovalStatus = "N";
                newData.Approved = false;

                _rolefuncfeatCrRepository.Add(newData);
                returnId = newData.Id;
            }
            catch (Exception e)
            {
                returnId = -1;
            }
            return returnId;
        }

        public string Delete(long roleFuncId, long roleId, long functionId, long featureId)
        {
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                //_rolefuncfeatRepository.deleteSelected(roleFuncId, featureId);
                RoleFunctionFeatureCr newData = new RoleFunctionFeatureCr(0L);
                newData.FeatureId = featureId;
                newData.RoleFunctionId = roleFuncId;
                newData.FunctionId = functionId;
                newData.RoleId = roleId;
                newData.ChangedBy = user.Username;
                newData.ChangedDate = DateTime.Now;
                newData.CreatedBy = user.Username;
                newData.CreatedDate = DateTime.Now;
                newData.ApprovalType = "D";
                newData.ApprovalStatus = "N";
                newData.Approved = false;

                _rolefuncfeatCrRepository.Add(newData);
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            return message;
        }

        public string DeleteCr(long roleId, long functionId, long featureId)
        {
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                _rolefuncfeatCrRepository.deleteSelected(roleId, functionId, featureId);
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            return message;
        }
    }
}