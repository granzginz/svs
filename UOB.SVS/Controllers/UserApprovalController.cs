﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using Treemas.Credential.Model;
using UOB.SVS.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using UOB.SVS.Resources;
using System.Globalization;
using System.Text.RegularExpressions;
using Microsoft.Reporting.WebForms;
using System.IO;
using LinqSpecs;

namespace UOB.SVS.Controllers
{
    public class UserApprovalController : PageController
    {
        private IUserRepository _userRepository;
        private IUserCrRepository _userCrRepository;
        private IApplicationRepository _appRepository;
        private IUserAuditTrailLog _auditRepository;
        private IAuthorizationRepository _authRepository;
        private IRoleRepository _roleRepository;
        private IRoleFunctionFeatureRepository _roleFuncFeatRepository;
        private IFeatureRepository _featRepository;
        private IFunctionRepository _funcRepository;
        private IUserApplicationRepository _userApplicationRepository;

        public UserApprovalController(ISessionAuthentication sessionAuthentication, IUserRepository userRepository, IUserCrRepository userCrRepository,
            IApplicationRepository appRepository, IUserAuditTrailLog auditRepository, IAuthorizationRepository authRepository, IRoleRepository roleRepository,
            IRoleFunctionFeatureRepository roleFunctionFeatureRepository, IFeatureRepository featureRepository, IFunctionRepository functionRepository,
            IUserApplicationRepository userApplicationRepository) : base(sessionAuthentication)
        {
            this._userRepository = userRepository;
            this._userCrRepository = userCrRepository;
            this._appRepository = appRepository;
            this._auditRepository = auditRepository;
            this._authRepository = authRepository;
            this._roleRepository = roleRepository;
            this._roleFuncFeatRepository = roleFunctionFeatureRepository;
            this._featRepository = featureRepository;
            this._funcRepository = functionRepository;
            this._userApplicationRepository = userApplicationRepository;
            Settings.ModuleName = "UserApproval";
            Settings.Title = UserResources.PageTitle_UserApproval;
        }

        protected override void Startup()
        {
            ViewData["ApprovalTypeList"] = createApprovalTypeSelect("");
            //long uid;
            //long.TryParse(Request.RequestContext.RouteData.Values["id"].ToString(), out uid);
            //UserView userData = null;
            //if (uid.ToString().IsNullOrEmpty())
            //    Response.RedirectToRoute(new { controller = "User", action = "Index" });


            //userData = ConvertFrom(_userRepository.GetUserById(uid));

            //if (userData.IsNull())
            //    Response.RedirectToRoute(new { controller = "User", action = "Index" });

            //Lookup.Remove<UserView>();
            //Lookup.Add(userData);
            //ViewData["UserData"] = userData;
            //ViewData["ActionName"] = "Activate";
        }

        public ActionResult Search()
        {
            User user = Lookup.Get<User>();

            JsonResult result = new JsonResult();
            try
            {
                // Initialization.   
                string mode = Request.QueryString.GetValues("Mode")[0];
                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.   
                PagedResult<UserCr> data;
                if (searchValue.IsNullOrEmpty())
                {
                    data = _userCrRepository.FindAllPagedApproval(pageNumber, pageSize, sortColumn, sortColumnDir, user.Username);
                }
                else
                {
                    if (searchColumn.ToLower().Contains("date"))
                    {
                        DateTime searchDate = DateTime.ParseExact(searchValue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        data = _userCrRepository.FindAllPagedDateApproval(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchDate, user.Username);
                    }
                    else
                    {
                        data = _userCrRepository.FindAllPagedApproval(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchValue, user.Username);
                    }

                }

                var items = data.Items.ToList().ConvertAll<UserView>(new Converter<UserCr, UserView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public UserView ConvertFrom(UserCr item)
        {

            UserView returnItem = new UserView();
            returnItem.Id = item.Id;
            returnItem.Username = item.Username;
            returnItem.PasswordExpirationDate = item.PasswordExpirationDate.ToString("dd/MM/yyyy");
            returnItem.AccountValidityDate = item.AccountValidityDate.ToString("dd/MM/yyyy");
            returnItem.RegNo = item.RegNo;
            returnItem.SessionTimeout = item.SessionTimeout;
            returnItem.LockTimeout = item.LockTimeout;
            returnItem.MaxConLogin = item.MaximumConcurrentLogin;
            ////old
            //returnItem.FullName = item.FullName;
            //returnItem.sInActiveDirectory = item.InActiveDirectory ? YesNo.Y.ToDescription() : YesNo.N.ToDescription();
            //returnItem.InActiveDirectory = item.InActiveDirectory;
            //returnItem.IsActive = item.IsActive;
            //returnItem.Applications = item.AuthorizedApplicationIdsOld;
            //returnItem.Roles = item.AuthorizedRoleIdsOld;

            ////new
            //returnItem.FullNameNew = item.FullNameNew;
            //returnItem.sInActiveDirectoryNew = item.InActiveDirectoryNew ? YesNo.Y.ToDescription() : YesNo.N.ToDescription();
            //returnItem.InActiveDirectoryNew = item.InActiveDirectoryNew;
            //returnItem.ApplicationsNew = item.AuthorizedApplicationIdsNew;
            //returnItem.RolesNew = item.AuthorizedRoleIdsNew;

            switch (item.ApprovalType)
            {
                case "C":
                    //new
                    returnItem.FullName = item.FullName;
                    returnItem.FullNameNew = item.FullName;
                    returnItem.sInActiveDirectoryNew = item.InActiveDirectoryNew ? YesNo.Y.ToDescription() : YesNo.N.ToDescription();
                    returnItem.InActiveDirectoryNew = item.InActiveDirectoryNew;
                    returnItem.ApplicationsNew = item.AuthorizedApplicationIdsNew;
                    returnItem.RolesNew = item.AuthorizedRoleIdsNew;
                    returnItem.BranchCodeNew = item.BranchCode;
                    break;
                case "U":
                    //old
                    returnItem.FullName = item.FullName;
                    returnItem.sInActiveDirectory = item.InActiveDirectory ? YesNo.Y.ToDescription() : YesNo.N.ToDescription();
                    returnItem.InActiveDirectory = item.InActiveDirectory;
                    returnItem.IsActive = item.IsActive;
                    returnItem.Applications = item.AuthorizedApplicationIdsOld;
                    returnItem.Roles = item.AuthorizedRoleIdsOld;
                    returnItem.BranchCode = item.BranchCode;

                    //new
                    returnItem.FullNameNew = item.FullNameNew;
                    returnItem.sInActiveDirectoryNew = item.InActiveDirectoryNew ? YesNo.Y.ToDescription() : YesNo.N.ToDescription();
                    returnItem.InActiveDirectoryNew = item.InActiveDirectoryNew;
                    returnItem.ApplicationsNew = item.AuthorizedApplicationIdsNew;
                    returnItem.RolesNew = item.AuthorizedRoleIdsNew.IsNull()? item.AuthorizedRoleIdsOld : item.AuthorizedRoleIdsNew;
                    returnItem.BranchCodeNew = item.BranchCodeNew;
                    break;
                case "D":
                    //old
                    returnItem.FullName = item.FullName;
                    returnItem.sInActiveDirectory = item.InActiveDirectory ? YesNo.Y.ToDescription() : YesNo.N.ToDescription();
                    returnItem.InActiveDirectory = item.InActiveDirectory;
                    returnItem.IsActive = item.IsActive;
                    returnItem.Applications = item.AuthorizedApplicationIdsOld;
                    returnItem.Roles = item.AuthorizedRoleIdsOld;
                    returnItem.BranchCode = item.BranchCode;
                    break;
                case "R":
                    //old
                    returnItem.FullName = item.FullName;
                    returnItem.sInActiveDirectory = item.InActiveDirectory ? YesNo.Y.ToDescription() : YesNo.N.ToDescription();
                    returnItem.InActiveDirectory = item.InActiveDirectory;
                    returnItem.IsActive = item.IsActive;
                    returnItem.Applications = item.AuthorizedApplicationIdsOld;
                    returnItem.Roles = item.AuthorizedRoleIdsOld;
                    returnItem.BranchCode = item.BranchCode;

                    returnItem.FullNameNew = item.FullName;
                    returnItem.sInActiveDirectoryNew = item.InActiveDirectory ? YesNo.Y.ToDescription() : YesNo.N.ToDescription();
                    returnItem.InActiveDirectoryNew = item.InActiveDirectory;
                    returnItem.ApplicationsNew = item.AuthorizedApplicationIdsOld;
                    returnItem.RolesNew = item.AuthorizedRoleIdsOld;
                    returnItem.BranchCodeNew = item.BranchCode;
                    break;
            }


            if (item.ApprovalType == ApprovalType.C.ToString())
            {
                returnItem.ApprovalType = ApprovalType.C.ToDescription();
            }
            else if (item.ApprovalType == ApprovalType.U.ToString())
            {
                returnItem.ApprovalType = ApprovalType.U.ToDescription();
            }
            else if (item.ApprovalType == ApprovalType.D.ToString())
            {
                returnItem.ApprovalType = ApprovalType.D.ToDescription();
            }
            else if (item.ApprovalType == ApprovalType.R.ToString())
            {
                returnItem.ApprovalType = ApprovalType.R.ToDescription();
            }

            returnItem.ChangedBy = item.ChangedBy;
            returnItem.ChangedDate = item.ChangedDate.IsNull() ? item.CreatedDate.ToString() : item.ChangedDate.ToString();

            return returnItem;
        }


        public ActionResult Approve(long id)
        {
            ViewData["ActionName"] = "Approve";
            UserCr user = _userCrRepository.GetUserById(id);

            UserView data = ConvertFrom(user);
            ViewData["UserData"] = data;


            return View("DetailEdit", data);

        }

        [HttpPost]
        public ActionResult Approve(UserView data)
        {
            ViewData["ActionName"] = "Activate";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                if (message.IsNullOrEmpty())
                {
                    _userCrRepository.setApproved(data.Id, user.Username, DateTime.Now, ApprovalStatusState.A.ToString());

                    UserCr userdata = _userCrRepository.GetUserById(data.Id);
                    //_auditRepository.SaveAuditTrail(Settings.ModuleName, userdata, user.Username, "Approve");

                    if (data.ApprovalStatus == "A")
                    {
                        switch (userdata.ApprovalTypeEnum)
                        {
                            case ApprovalType.C:
                                User oldData = new User(0L);

                                User newData = new User(0L);

                                newData.Username = userdata.Username;
                                newData.Password = userdata.Password;
                                newData.PasswordExpirationDate = userdata.PasswordExpirationDate;
                                newData.AccountValidityDate = userdata.AccountValidityDate;
                                newData.FullName = userdata.FullName;
                                newData.RegNo = userdata.RegNo;
                                newData.SessionTimeout = userdata.SessionTimeout;
                                newData.LockTimeout = userdata.LockTimeout;
                                newData.MaximumConcurrentLogin = userdata.MaximumConcurrentLogin;
                                newData.IsActive = userdata.IsActive;
                                newData.InActiveDirectory = userdata.InActiveDirectory;
                                newData.IsPrinted = userdata.IsPrinted;
                                newData.Approved = userdata.Approved;
                                newData.CreatedBy = userdata.CreatedBy;
                                newData.CreatedDate = userdata.CreatedDate;
                                newData.Username = userdata.Username;
                                newData.BranchCode = userdata.BranchCode;

                                newData.ChangedBy = user.Username;
                                newData.ChangedDate = DateTime.Now;
                                newData.UpdateMenu = true;

                                //Check for any application change
                                //User newData2 = _userRepository.GetUser(userdata.Username);
                                if (userdata.AuthorizedApplicationIdsOld != null)
                                {
                                    string[] appsOld = userdata.AuthorizedApplicationIdsOld.Replace(" ", "").Split(',');
                                    string[] appsNew = userdata.AuthorizedApplicationIdsNew.Replace(" ", "").Split(',');
                                    var q = from a in appsOld
                                            join b in appsNew on a equals b
                                            select a;
                                    bool isAppsEqual = appsOld.Length == appsNew.Length && q.Count() == appsOld.Length;
                                    if (!isAppsEqual)
                                    {
                                        //newData2.UpdateMenu = true; //Will be saved later with conditional Roles changes.
                                         // delete all authorization by username
                                        foreach (string appId in appsOld)
                                        {
                                            UserApplication userApp = _userApplicationRepository.getUserApplication(userdata.Username, appId);
                                            if (!userApp.IsNull())
                                            {
                                                _userApplicationRepository.Remove(userApp);
                                            }
                                        }
                                        //// begin new insert
                                        //foreach (string appId in appsNew)
                                        //{
                                        //    if (appId.IsNullOrEmpty())
                                        //    {
                                        //        continue;
                                        //    }
                                        //    UserApplication newUserAppData = new UserApplication(0L);
                                        //    newUserAppData.Username = userdata.Username;
                                        //    newUserAppData.Application = appId;
                                        //    newUserAppData.IsDefault = false;
                                        //    newUserAppData.ChangedBy = null;
                                        //    newUserAppData.ChangedDate = null;
                                        //    newUserAppData.CreatedBy = userdata.CreatedBy;
                                        //    newUserAppData.CreatedDate = userdata.CreatedDate;
                                        //    _userApplicationRepository.Add(newUserAppData);
                                        //}
                                    }                                    
                                }

                                // Check for any Role change
                                if (userdata.AuthorizedRoleIdsOld != null)
                                {
                                    string[] rolesOld = userdata.AuthorizedRoleIdsOld.Split(',');
                                    string[] rolesNew = userdata.AuthorizedRoleIdsNew.Split(',');

                                    var qRole = from a in rolesOld
                                                join b in rolesNew on a equals b
                                                select a;

                                    bool isRolesEqual = rolesOld.Length == rolesNew.Length && qRole.Count() == rolesOld.Length;
                                    if (!isRolesEqual)
                                    {
                                        //newData2.UpdateMenu = true; //Will be saved later with conditional Roles changes.

                                        // delete all authorization by username
                                        foreach (string roleId in rolesOld)
                                        {
                                            Role role = _roleRepository.GetOne(new AdHocSpecification<Role>(s => s.RoleId == roleId));

                                            if (!role.IsNull())
                                            {
                                                //_authRepository.delete(userdata.Username, role._Application, roleId);
                                                _authRepository.delete(userdata.Username, role._Application);
                                            }
                                        }

                                        // begin new insert
                                        foreach (string roleId in rolesNew)
                                        {
                                            Role role = _roleRepository.GetOne(new AdHocSpecification<Role>(s => s.RoleId == roleId));
                                            if (role.IsNull())
                                            {
                                                continue;
                                            }

                                            IList<RoleFunctionFeature> roleFuncFeat = _roleFuncFeatRepository.getRoleFuncFeaturebyRole(role.Id);
                                            foreach (var item in roleFuncFeat)
                                            {
                                                AuthorizationFeature feat = _featRepository.getFeature(item.FeatureId);
                                                AuthorizationFunction func = _funcRepository.getFunction(item.FunctionId);
                                                try
                                                {
                                                    Authorization newAuthorizationData = new Authorization(0L);
                                                    newAuthorizationData.Username = userdata.Username;
                                                    newAuthorizationData.Application = role._Application;
                                                    newAuthorizationData.Role = role.RoleId;
                                                    newAuthorizationData.Function = func.FunctionId;
                                                    newAuthorizationData.Feature = feat.Name;
                                                    newAuthorizationData.QualifierKey = "";
                                                    newAuthorizationData.QualifierValue = "";
                                                    newAuthorizationData.ChangedBy = user.Username;
                                                    newAuthorizationData.ChangedDate = DateTime.Now;
                                                    newAuthorizationData.CreatedBy = user.Username;
                                                    newAuthorizationData.CreatedDate = DateTime.Now;
                                                    _authRepository.Add(newAuthorizationData);
                                                }
                                                catch (Exception e)
                                                {

                                                }
                                            }
                                        }
                                    }
                                }

                                _userRepository.Add(newData);

                                //_auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Create");
                                _auditRepository.SaveAuditTrail(Settings.ModuleName, oldData, newData, userdata.CreatedBy, userdata.CreatedDate, user.Username, "Create");
                                //_auditRepository.SaveAuditTrail(Settings.ModuleName, "User",oldData, newData, userdata.CreatedBy, userdata.CreatedDate, user.Username, "Create");
                                break;

                            case ApprovalType.U:
                                User oldDataUpdate = _userRepository.GetUser(userdata.Username);
                                User newData3 = _userRepository.GetUser(userdata.Username);

                                newData3.FullName = userdata.FullNameNew;
                                newData3.InActiveDirectory = userdata.InActiveDirectoryNew;
                                newData3.Approved = userdata.Approved;
                                newData3.BranchCode = userdata.BranchCodeNew;
                                newData3.ChangedBy = user.Username;
                                newData3.ChangedDate = DateTime.Now; //apakah changed date = approval date

                                //Check for any application change
                                if (userdata.AuthorizedApplicationIdsOld != null)
                                {
                                    string[] appsOld = userdata.AuthorizedApplicationIdsOld.Replace(" ", "").Split(',');
                                    string[] appsNew = userdata.AuthorizedApplicationIdsNew.Replace(" ", "").Split(',');
                                    var q = from a in appsOld
                                            join b in appsNew on a equals b
                                            select a;
                                    bool isAppsEqual = appsOld.Length == appsNew.Length && q.Count() == appsOld.Length;
                                    if (!isAppsEqual)
                                    {
                                        newData3.UpdateMenu = true; //Will be saved later with conditional Roles changes.
                                                                    // delete all authorization by username
                                        foreach (string appId in appsOld)
                                        {
                                            UserApplication userApp = _userApplicationRepository.getUserApplication(userdata.Username, appId);
                                            if (!userApp.IsNull())
                                            {
                                                _userApplicationRepository.Remove(userApp);
                                            }
                                        }
                                        //// begin new insert
                                        //foreach (string appId in appsNew)
                                        //{
                                        //    if (appId.IsNullOrEmpty())
                                        //    {
                                        //        continue;
                                        //    }
                                        //    UserApplication newUserAppData = new UserApplication(0L);
                                        //    newUserAppData.Username = userdata.Username;
                                        //    newUserAppData.Application = appId;
                                        //    newUserAppData.IsDefault = false;
                                        //    newUserAppData.ChangedBy = null;
                                        //    newUserAppData.ChangedDate = null;
                                        //    newUserAppData.CreatedBy = userdata.CreatedBy;
                                        //    newUserAppData.CreatedDate = userdata.CreatedDate;
                                        //    _userApplicationRepository.Add(newUserAppData);
                                        //}
                                    }

                                    // Check for any Role change
                                    //string[] rolesOld = userdata.AuthorizedRoleIdsOld.Replace(" ", "").Split(',');
                                    //string[] rolesNew = userdata.AuthorizedRoleIdsNew.Replace(" ", "").Split(',');
                                    if (userdata.AuthorizedRoleIdsOld != null)
                                    {
                                        string[] rolesOld = userdata.AuthorizedRoleIdsOld.Split(',');
                                        string[] rolesNew = userdata.AuthorizedRoleIdsNew.Split(',');

                                        var qRole = from a in rolesOld
                                                    join b in rolesNew on a equals b
                                                    select a;

                                        bool isRolesEqual = rolesOld.Length == rolesNew.Length && qRole.Count() == rolesOld.Length;
                                        if (!isRolesEqual)
                                        {
                                            newData3.UpdateMenu = true; //Will be saved later with conditional Roles changes.

                                            // delete all authorization by username
                                            foreach (string roleId in rolesOld)
                                            {
                                                Role role = _roleRepository.GetOne(new AdHocSpecification<Role>(s => s.RoleId == roleId));

                                                if (!role.IsNull())
                                                {
                                                    //_authRepository.delete(userdata.Username, role._Application, roleId);
                                                    _authRepository.delete(userdata.Username, role._Application);
                                                }
                                            }

                                            // begin new insert
                                            foreach (string roleId in rolesNew)
                                            {
                                                Role role = _roleRepository.GetOne(new AdHocSpecification<Role>(s => s.RoleId == roleId));
                                                if (role.IsNull())
                                                {
                                                    continue;
                                                }

                                                IList<RoleFunctionFeature> roleFuncFeat = _roleFuncFeatRepository.getRoleFuncFeaturebyRole(role.Id);
                                                foreach (var item in roleFuncFeat)
                                                {
                                                    AuthorizationFeature feat = _featRepository.getFeature(item.FeatureId);
                                                    AuthorizationFunction func = _funcRepository.getFunction(item.FunctionId);
                                                    try
                                                    {
                                                        Authorization newAuthorizationData = new Authorization(0L);
                                                        newAuthorizationData.Username = userdata.Username;
                                                        newAuthorizationData.Application = role._Application;
                                                        newAuthorizationData.Role = role.RoleId;
                                                        newAuthorizationData.Function = func.FunctionId;
                                                        newAuthorizationData.Feature = feat.Name;
                                                        newAuthorizationData.QualifierKey = "";
                                                        newAuthorizationData.QualifierValue = "";
                                                        newAuthorizationData.ChangedBy = user.Username;
                                                        newAuthorizationData.ChangedDate = DateTime.Now;
                                                        newAuthorizationData.CreatedBy = user.Username;
                                                        newAuthorizationData.CreatedDate = DateTime.Now;
                                                        _authRepository.Add(newAuthorizationData);
                                                    }
                                                    catch (Exception e)
                                                    {

                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                _userRepository.Save(newData3);
                                //_auditRepository.SaveAuditTrail(Settings.ModuleName, newData3, user.Username, "AfterEdit");
                                _auditRepository.SaveAuditTrail(Settings.ModuleName, oldDataUpdate, newData3, userdata.CreatedBy, userdata.CreatedDate, user.Username, "Update");
                                //_auditRepository.SaveAuditTrail(Settings.ModuleName, "User",oldDataUpdate, newData2, userdata.CreatedBy, userdata.CreatedDate, user.Username, "Update");

                                break;

                            case ApprovalType.D:

                                User usrdata = _userRepository.GetUser(userdata.Username);
                                User emptyDataDelete = new User(0L);

                                _auditRepository.SaveAuditTrail(Settings.ModuleName, usrdata, emptyDataDelete, userdata.CreatedBy, userdata.CreatedDate, user.Username, "Delete");
                                //_auditRepository.SaveAuditTrail(Settings.ModuleName,"User", usrdata, emptyDataDelete, userdata.CreatedBy, userdata.CreatedDate, user.Username, "Delete");
                                //_auditRepository.SaveAuditTrail(Settings.ModuleName, usrdata, user.Username, "Delete");
                                _userRepository.deleteAll(usrdata.Username);
                                _userRepository.Remove(usrdata);

                                break;

                            case ApprovalType.R:

                                User newDataR = _userRepository.GetUser(userdata.Username);


                                newDataR.Password = userdata.Password;
                                newDataR.Approved = true;

                                newDataR.PasswordExpirationDate = userdata.PasswordExpirationDate;
                                newDataR.AccountValidityDate = userdata.AccountValidityDate;

                                newDataR.ChangedBy = user.Username;
                                newDataR.ChangedDate = DateTime.Now; //apakah changed date = approval date

                                newDataR.LastLogin = userdata.LastLogin;

                                _userRepository.Save(newDataR);
                                //_auditRepository.SaveAuditTrail(Settings.ModuleName, newDataR, user.Username, "ResetPassword");

                                break;
                        }
                    }
                    else if (data.ApprovalStatus == "D")
                    {
                        //UserCr userdata = _userCrRepository.GetUserById(data.Id);
                        UserCr userCr = _userCrRepository.GetUserById(data.Id);
                        User userBefore = _userRepository.GetUser(userCr.Username);
                        if (userBefore.IsNull())
                            userBefore = new User(0L);
                        _auditRepository.SaveAuditTrail(Settings.ModuleName, userBefore, userCr, userdata.CreatedBy, userdata.CreatedDate, user.Username, "Reject");

                        //_auditRepository.SaveAuditTrail(Settings.ModuleName, userdata, user.Username, "Reject");
                        _userCrRepository.deleteUserCr(userdata.Username, data.Id);
                        //_userCrRepository.Remove(userdata);

                    }
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }


            if (data.ApprovalStatus == "A")
            {
                if (message.IsNullOrEmpty())
                {
                    ScreenMessages.Submit(ScreenMessage.Success("Request has been approved"));
                    return RedirectToRoute(new { controller = "UserApproval", action = "Index" });
                }
            }
            else if (data.ApprovalStatus == "D")
            {
                if (message.IsNullOrEmpty())
                {
                    ScreenMessages.Submit(ScreenMessage.Success("Request has been rejected"));
                    return RedirectToAction("Index");
                }
            }

            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return RedirectToRoute(new { controller = "UserApproval", action = "Index" });
        }

        // POST: Feature/Delete/5
        [HttpPost]
        public ActionResult Reject(long Id)
        {
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                UserCr userdata = _userCrRepository.GetUserById(Id);
                User userOld = _userRepository.GetUser(userdata.Username);
                //_auditRepository.SaveAuditTrail(Settings.ModuleName, userdata, user.Username, "Reject");
                //_userCrRepository.deleteAll(userdata.Username);
                //_userCrRepository.Remove(userdata);
                _auditRepository.SaveAuditTrail(Settings.ModuleName, userOld, userdata, userdata.CreatedBy, userdata.CreatedDate, user.Username, "Reject");

                //_auditRepository.SaveAuditTrail(Settings.ModuleName, userdata, user.Username, "Reject");
                _userCrRepository.deleteUserCr(userdata.Username, Id);
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(UserResources.Delete_Rejected));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return Approve(Id);
        }

        private IList<SelectListItem> createApprovalTypeSelect(string selected)
        {
            IList<SelectListItem> dataList = new List<SelectListItem>();
            dataList.Insert(0, new SelectListItem() { Value = "", Text = "" });
            dataList.Insert(1, new SelectListItem() { Value = "C", Text = "Create" });
            dataList.Insert(2, new SelectListItem() { Value = "U", Text = "Update" });
            dataList.Insert(3, new SelectListItem() { Value = "D", Text = "Delete" });
            dataList.Insert(4, new SelectListItem() { Value = "R", Text = "Reset" });

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected.ToString()).Selected = true;
            }
            return dataList;
        }

    }
}