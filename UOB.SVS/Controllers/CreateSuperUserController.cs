﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using Treemas.Credential.Model;
using UOB.SVS.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using UOB.SVS.Resources;
using System.Text.RegularExpressions;

namespace UOB.SVS.Controllers
{
    public class CreateSuperUserController : PageController
    {
        private IUserRepository _userRepository;
        private IAuditTrailLog _auditRepository;
        private IUserAccount _userAccount;

        public CreateSuperUserController(ISessionAuthentication sessionAuthentication, IUserRepository userRepository, IAuditTrailLog auditRepository,
            IUserAccount userAccount) : base(sessionAuthentication)
        {
            this._userRepository = userRepository;
            this._auditRepository = auditRepository;
            this._userAccount = userAccount;
            Settings.Title = UserResources.PageTitleChangePassword;
            Settings.ModuleName = "CreateSuperUser";
        }
        // GET: Home
        protected override void Startup()
        {
            User user = Lookup.Get<User>();

            ViewData["User"] = user;
            ViewData["ActionName"] = "Create";
        }

        [HttpPost]
        public ActionResult Create(string Password, string ConfirmPassword)
        {
            ViewData["ActionName"] = "Create";
            string message = "";
            try
            {
                if (Password != ConfirmPassword)
                    message = "Password did not match";
                if (message.IsNullOrEmpty())
                {
                    var user = _userAccount.CreateSuperUser(SystemSettings.Instance.Runtime.SuperUser, Password, SystemSettings.Instance.Name);
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success("Super user has been created"));
                return RedirectToRoute(new { controller = "Login", action = "Logout" });
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();

            return View("CreateSuperUser");
        }



    }
}