﻿using Treemas.Credential.Model;
using Treemas.Credential.Interface;
using Treemas.Base.Web.Platform;
using UOB.SVS.Resources;

namespace UOB.SVS.Controllers
{
    public class HomeController : PageController
    {
        private IUserRepository _userRepository;

        public HomeController(ISessionAuthentication sessionAuthentication, IUserRepository userRepository) : base(sessionAuthentication)
        {
            this._userRepository = userRepository;
            Settings.Title = ApplicationLabel.PageTitle;
        }
        // GET: Home
        protected override void Startup()
        {
            User user = null;
            Settings.Title = ApplicationLabel.PageTitle;

            if (!SystemSettings.Instance.Security.EnableAuthentication)
                user = _userRepository.GetUser(SystemSettings.Instance.Security.SimulatedAuthenticatedUser.Username);
            else
                user = Lookup.Get<User>();

            ViewData["User"] = user;
        }
    }
}