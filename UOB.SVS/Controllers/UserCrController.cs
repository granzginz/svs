﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using Treemas.Credential.Model;
using UOB.SVS.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using UOB.SVS.Resources;
using System.Globalization;
using System.Text.RegularExpressions;
using Microsoft.Reporting.WebForms;
using System.IO;
using SpreadsheetLight;
using System.Data;

namespace UOB.SVS.Controllers
{
    public class UserCrController : PageController
    {
        //private IList<UserRole> userRoles;
        private IUserRepository _userRepository;
        private IApplicationRepository _appRepository;
        private IUserCrRepository _auditRepository;

        public UserCrController(ISessionAuthentication sessionAuthentication, IUserRepository userRepository,
            IApplicationRepository appRepository, IUserCrRepository auditRepository) : base(sessionAuthentication)
        {
            this._userRepository = userRepository;
            this._appRepository = appRepository;
            this._auditRepository = auditRepository;

            Settings.ModuleName = "AuditTrail";
            Settings.Title = "User Management Audit Trail";
        }
        protected override void Startup()
        {

        }

        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.   
                AuditTrailFilter filter = new AuditTrailFilter();
                filter.ActionDateStart = Request.QueryString.GetValues("DateFrom")[0].ToDateTime();
                filter.ActionDateEnd = Request.QueryString.GetValues("DateEnd")[0].ToDateTime();
                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;
                filter.searchColumn = searchColumn;
                filter.searchValue = searchValue;

                // Loading.   
                PagedResult<UserCr> data;
                if (searchValue.IsNullOrEmpty())
                {
                    data = _auditRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, filter);
                }
                else
                {
                    data = _auditRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchValue, filter);

                }
                                
                var items = data.Items.ToList().ConvertAll<UserView>(new Converter<UserCr, UserView>(ConvertFrom));

                
                Lookup.Remove<AuditTrailFilter>();
                Lookup.Add(filter);


                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public UserView ConvertFrom(UserCr item)
        {
            //var ar = _auditRepository
            UserView returnItem = new UserView();
            returnItem.Id = item.Id;
            returnItem.Username = item.Username;
            returnItem.FullName = item.FullName;
            returnItem.FullNameNew = item.FullNameNew;
            returnItem.ApprovalType = item.ApprovalTypeEnum.ToDescription();
            returnItem.CreatedBy = item.CreatedBy;
            returnItem.CreatedDate = item.CreatedDate.ToString();
            returnItem.ChangedBy = item.ChangedBy;
            returnItem.ChangedDate = item.ChangedDate.ToString();
            returnItem.ApprovedBy = item.ApprovedBy;
            returnItem.ApprovedDate = item.ApprovedDate.ToString();
            returnItem.BranchCode = item.BranchCode;
            returnItem.BranchCodeNew = item.BranchCodeNew;
            returnItem.Roles = item.AuthorizedRoleIdsOld;
            returnItem.RolesNew = item.AuthorizedRoleIdsNew;
            returnItem.Applications = item.AuthorizedApplicationIdsOld;
            returnItem.ApplicationsNew = item.AuthorizedApplicationIdsNew;
            //returnItem.YesNo = "";

            return returnItem;
        }

        public ActionResult Export()
        {
            AuditTrailFilter filter = Lookup.Get<AuditTrailFilter>();

            var auditList = _auditRepository.FindExport(filter);
            
            IList<UserView> items = auditList.ToList().ConvertAll<UserView>(new Converter<UserCr, UserView>(ConvertFrom));

            DataTable dt = items.ListToDataTable();
            //List<UserView> list = new List<UserView>();

            dt.Columns.Remove("Id");
            dt.Columns.Remove("RegNo");
            dt.Columns.Remove("Password");
            dt.Columns.Remove("OldPassword");
            dt.Columns.Remove("ConfirmPassword");
            dt.Columns.Remove("PasswordExpirationDate");
            dt.Columns.Remove("AccountValidityDate");
            dt.Columns.Remove("InActiveDirectory");
            dt.Columns.Remove("InActiveDirectoryNew");
            dt.Columns.Remove("sInActiveDirectory");
            dt.Columns.Remove("sInActiveDirectoryNew");
            dt.Columns.Remove("SessionTimeout");
            dt.Columns.Remove("LockTimeout");
            dt.Columns.Remove("MaxConLogin");
            dt.Columns.Remove("IsActive");
            dt.Columns.Remove("IsPrinted");
            dt.Columns.Remove("Approved");
            dt.Columns.Remove("passExp");
            dt.Columns.Remove("AccValidity");
            dt.Columns.Remove("LastLogin");
            dt.Columns.Remove("YesNo");
            dt.Columns.Remove("ApprovalStatus");

            MemoryStream ms = new MemoryStream();
            using (SLDocument sl = new SLDocument())
            {
                sl.ImportDataTable(1, 1, dt, true);
                sl.SelectWorksheet("Worksheet");
                sl.SaveAs(ms);
            }

            ms.Position = 0;
            string fName = "SVS_Audit_Trail.xls";
            return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fName);
        }

    }
}