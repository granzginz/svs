﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using Treemas.Credential.Model;
using UOB.SVS.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using UOB.SVS.Resources;
using LinqSpecs;

namespace UOB.SVS.Controllers
{
    public class UserApplicationRoleController : PageController
    {
        private IUserApplicationRepository _userAppRepository;
        private IRoleFunctionFeatureRepository _roleFuncFeatRepository;
        private IFunctionRepository _funcRepository;
        private IRoleRepository _roleRepository;
        private IFeatureRepository _featRepository;
        private IAuthorizationRepository _authRepository;
        private IList<Authorization> authList;
        private IUserRepository _userRepository;
        private IAuditTrailLog _auditRepository;
        private IUserCrRepository _userCrRepository;
        private IUserApplicationRepository _userApplicationRepository;

        public UserApplicationRoleController(ISessionAuthentication sessionAuthentication, IUserApplicationRepository userAppRepository, IFeatureRepository featRepository,
            IRoleFunctionFeatureRepository roleFuncFeatRepository, IFunctionRepository funcRepository, IRoleRepository roleRepository, IAuthorizationRepository authRepository,
            IUserRepository userRepository, IAuditTrailLog auditRepository, IUserCrRepository userCrRepository, IUserApplicationRepository userApplicationRepository)
            : base(sessionAuthentication)
        {
            this._userAppRepository = userAppRepository;
            this._roleFuncFeatRepository = roleFuncFeatRepository;
            this._funcRepository = funcRepository;
            this._featRepository = featRepository;
            this._roleRepository = roleRepository;
            this._authRepository = authRepository;
            this._userRepository = userRepository;
            this._auditRepository = auditRepository;
            this._userCrRepository = userCrRepository;
            this._userApplicationRepository = userApplicationRepository;

            Settings.Title = "UserApplication";
            Settings.ModuleName = UserResources.PageTitle;
        }
        protected override void Startup()
        {
            var returnpage = new { controller = "User", action = "Index" };
            try
            {
                long rid = Convert.ToInt64(Request.RequestContext.RouteData.Values["Id"]);

                UserApplication userapp = null;
                if (rid.IsNull())
                    Response.RedirectToRoute(returnpage);
                if (rid < 0)
                    Response.RedirectToRoute(returnpage);

                userapp = _userAppRepository.getUserApplication(rid);
                if (userapp.IsNull())
                    Response.RedirectToRoute(returnpage);

                User user = _userRepository.GetUser(userapp.Username);
                if (user == null)
                {
                    UserCr usercr = _userCrRepository.GetUser(userapp.Username);
                    if (usercr != null)
                    {
                        user = new User(usercr.Id);
                        user.Username = usercr.Username;
                        user.PasswordExpirationDate = usercr.PasswordExpirationDate;//.ToString("dd/MM/yyyy");
                        user.AccountValidityDate = usercr.AccountValidityDate;//.ToString("dd/MM/yyyy");
                        user.FullName = usercr.FullName;
                        user.RegNo = usercr.RegNo;
                        user.SessionTimeout = SystemSettings.Instance.Security.SessionTimeout; //item.SessionTimeout;
                        user.LockTimeout = 10;//item.LockTimeout;
                        user.InActiveDirectory = usercr.InActiveDirectory;
                        user.IsActive = usercr.IsActive;
                        user.IsPrinted = usercr.IsPrinted;
                        user.Approved = usercr.Approved;
                        user.CreatedDate = usercr.CreatedDate;//.Value.ToString("dd/MM/yyyy");
                        user.CreatedBy = usercr.CreatedBy;
                        user.BranchCode = usercr.BranchCode;

                    }
                }
                Lookup.Remove<UserApplication>();
                Lookup.Add(userapp);
                ViewData["UserApp"] = userapp;
                ViewData["vUser"] = user;
            }
            catch
            {
                Response.RedirectToRoute(returnpage);
            }

        }
        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            try
            {
                UserApplication userApp = Lookup.Get<UserApplication>();
                if (userApp.IsNull())
                {
                    if (Request.QueryString.GetValues("Id").IsNullOrEmpty())
                        return null;
                    long Id = long.Parse(Request.QueryString.GetValues("Id")[0]);
                    userApp = _userAppRepository.getUserApplication(Id);
                    Lookup.Remove<UserApplication>();
                    Lookup.Add(userApp);
                }

                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                authList = _authRepository.getUserAuthorization(userApp.Username);

                // Loading.   
                PagedResult<Role> data;
                if (searchValue.IsNullOrEmpty())
                {
                    data = _roleRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir);
                }
                else
                {
                    data = _roleRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchValue);
                }

                var items = data.Items.ToList().ConvertAll<AuthorizationView>(new Converter<Role, AuthorizationView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }
        public AuthorizationView ConvertFrom(Role item)
        {
            AuthorizationView returnItem = new AuthorizationView();
            int index = authList.FindIndex(x => x.Role.Equals(item.RoleId));
            if (index == -1)
            {
                returnItem.Selected = false;
                returnItem.Id = item.Id;
            }
            else
            {
                returnItem.Id = item.Id;
                returnItem.Selected = true;
            }
            returnItem.Role = item.RoleId;

            return returnItem;
        }

        [HttpPost]
        public ActionResult Create(AuthorizationHelper data)
        {
            User user = Lookup.Get<User>();
            JsonResult result = new JsonResult();
            try
            {

                if (data.Selected == true)
                {
                    Insert(data);
                }
                else
                {
                    Delete(data);
                }
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        [HttpPost]
        public JsonResult Edit(string userName, string roles)
        {
            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            string message = "";

            bool isAdmin = false;

            if (user.Username == SystemSettings.Instance.Runtime.SuperUser)
            {
                isAdmin = true;
            }
            else
            {
                isAdmin = false;
            }

            try
            {
                User oldData = _userRepository.GetUser(userName);
                if (!oldData.IsNull())
                {
                    UserCr usrcr = _userCrRepository.GetUser(userName, ApprovalType.U);
                    if (!usrcr.IsNull())
                    {
                        throw new Exception(UserResources.Validation_AlreadyRequest);
                    }

                    //get authorized applications
                    IList<UserApplication> authorizedApplicationIdsList = _userAppRepository.getUserApplication(userName);
                    string authorizedApplicationIdsString = string.Empty;
                    foreach (UserApplication app in authorizedApplicationIdsList)
                    {
                        authorizedApplicationIdsString += app.Application.ToString() + ",";
                    }
                    authorizedApplicationIdsString = authorizedApplicationIdsString.Substring(0, authorizedApplicationIdsString.Length - 1);

                    string authorizedRoles = _authRepository.getDistinctUserAuthorizationByUsername(userName);
                    authorizedRoles = authorizedRoles == "-" ? string.Empty : authorizedRoles;


                    UserCr newData = new UserCr(0L);
                    newData.Username = oldData.Username;
                    newData.FullName = oldData.FullName;
                    newData.FullNameNew = oldData.FullName;
                    newData.AuthorizedApplicationIdsOld = "";// authorizedApplicationIdsString;
                    newData.AuthorizedApplicationIdsNew = authorizedApplicationIdsString;
                    newData.AuthorizedRoleIdsOld = authorizedRoles;
                    newData.AuthorizedRoleIdsNew = roles;
                    newData.RegNo = oldData.RegNo;
                    newData.SessionTimeout = oldData.SessionTimeout;
                    newData.LockTimeout = oldData.LockTimeout;
                    newData.MaximumConcurrentLogin = oldData.MaximumConcurrentLogin;

                    newData.LastLogin = oldData.LastLogin;
                    newData.LoginAttempt = oldData.LoginAttempt;

                    newData.InActiveDirectory = oldData.InActiveDirectory;
                    newData.InActiveDirectoryNew = oldData.InActiveDirectory;
                    newData.IsActive = oldData.IsActive;

                    newData.IsPrinted = oldData.IsPrinted;
                    newData.Approved = false;

                    newData.ApprovalType = ApprovalType.U.ToString();

                    newData.Password = oldData.Password;
                    newData.PasswordExpirationDate = oldData.PasswordExpirationDate;
                    newData.AccountValidityDate = oldData.AccountValidityDate;

                    newData.CreatedDate = oldData.CreatedDate;
                    newData.CreatedBy = oldData.CreatedBy;
                    newData.ChangedBy = user.Username;
                    newData.ChangedDate = DateTime.Now;

                    newData.BranchCode = oldData.BranchCode;
                    newData.BranchCodeNew = oldData.BranchCode;
                    _userCrRepository.Add(newData);
                }
                else
                {
                    UserCr usrcr = _userCrRepository.GetUser(userName, ApprovalType.U);
                    if (!usrcr.IsNull())
                    {
                        throw new Exception(UserResources.Validation_AlreadyRequest);
                    }
                    usrcr = _userCrRepository.GetUser(userName, ApprovalType.C);

                    //get authorized applications
                    IList<UserApplication> authorizedApplicationIdsList = _userAppRepository.getUserApplication(userName);
                    string authorizedApplicationIdsString = string.Empty;
                    foreach (UserApplication app in authorizedApplicationIdsList)
                    {
                        authorizedApplicationIdsString += app.Application.ToString() + ",";
                    }
                    authorizedApplicationIdsString = authorizedApplicationIdsString.Substring(0, authorizedApplicationIdsString.Length - 1);

                    string authorizedRoles = _authRepository.getDistinctUserAuthorizationByUsername(userName);
                    authorizedRoles = authorizedRoles == "-" ? string.Empty : authorizedRoles;
                    usrcr.AuthorizedApplicationIdsOld = "";// authorizedApplicationIdsString;
                    usrcr.AuthorizedApplicationIdsNew = authorizedApplicationIdsString;
                    usrcr.AuthorizedRoleIdsOld = "";// authorizedRoles;
                    usrcr.AuthorizedRoleIdsNew = roles;
                    _userCrRepository.Save(usrcr);
                }

                if (isAdmin)
                {
                    UserCr usrcr = _userCrRepository.GetUserforAdmin(userName, ApprovalType.C);
                    User newData2 = _userRepository.GetUser(userName);

                    newData2.FullName = usrcr.FullNameNew;
                    newData2.InActiveDirectory = usrcr.InActiveDirectoryNew;
                    newData2.Approved = usrcr.Approved;
                    newData2.ChangedBy = usrcr.ChangedBy;
                    newData2.ChangedDate = DateTime.Now; //apakah changed date = approval date

                    string[] appsOld = usrcr.AuthorizedApplicationIdsOld.Split(',');
                    string[] appsNew = usrcr.AuthorizedApplicationIdsNew.Split(',');

                    var q = from a in appsOld
                            join b in appsNew on a equals b
                            select a;
                    bool isAppsEqual = appsOld.Length == appsNew.Length && q.Count() == appsOld.Length;
                    if (!isAppsEqual)
                    {
                        newData2.UpdateMenu = true; //Will be saved later with conditional Roles changes.
                                                    // delete all authorization by username
                        foreach (string appId in appsOld)
                        {
                            UserApplication userApp = _userApplicationRepository.getUserApplication(usrcr.Username, appId);
                            if (!userApp.IsNull())
                            {
                                _userApplicationRepository.Remove(userApp);
                            }
                        }
                        //// begin new insert
                        //foreach (string appId in appsNew)
                        //{
                        //    if (appId.IsNullOrEmpty())
                        //    {
                        //        continue;
                        //    }
                        //    UserApplication newUserAppData = new UserApplication(0L);
                        //    newUserAppData.Username = usrcr.Username;
                        //    newUserAppData.Application = appId;
                        //    newUserAppData.IsDefault = false;
                        //    newUserAppData.ChangedBy = null;
                        //    newUserAppData.ChangedDate = null;
                        //    newUserAppData.CreatedBy = usrcr.CreatedBy;
                        //    newUserAppData.CreatedDate = usrcr.CreatedDate;
                        //    _userApplicationRepository.Add(newUserAppData);
                        //}
                    }

                    // Check for any Role change
                    //string[] rolesOld = userdata.AuthorizedRoleIdsOld.Replace(" ", "").Split(',');
                    //string[] rolesNew = userdata.AuthorizedRoleIdsNew.Replace(" ", "").Split(',');

                    string[] rolesOld = usrcr.AuthorizedRoleIdsOld.Split(',');
                    string[] rolesNew = usrcr.AuthorizedRoleIdsNew.Split(',');

                    var qRole = from a in rolesOld
                                join b in rolesNew on a equals b
                                select a;

                    bool isRolesEqual = rolesOld.Length == rolesNew.Length && qRole.Count() == rolesOld.Length;
                    if (!isRolesEqual)
                    {
                        newData2.UpdateMenu = true; //Will be saved later with conditional Roles changes.

                        // delete all authorization by username
                        foreach (string roleId in rolesOld)
                        {
                            Role role = _roleRepository.GetOne(new AdHocSpecification<Role>(s => s.RoleId == roleId));

                            if (!role.IsNull())
                            {
                                //_authRepository.delete(usrcr.Username, role._Application, roleId);
                                _authRepository.delete(usrcr.Username, role._Application);
                            }
                        }

                        // begin new insert
                        foreach (string roleId in rolesNew)
                        {
                            Role role = _roleRepository.GetOne(new AdHocSpecification<Role>(s => s.RoleId == roleId));
                            if (role.IsNull())
                            {
                                continue;
                            }

                            IList<RoleFunctionFeature> roleFuncFeat = _roleFuncFeatRepository.getRoleFuncFeaturebyRole(role.Id);
                            foreach (var item in roleFuncFeat)
                            {
                                AuthorizationFeature feat = _featRepository.getFeature(item.FeatureId);
                                AuthorizationFunction func = _funcRepository.getFunction(item.FunctionId);
                                try
                                {
                                    Authorization newAuthorizationData = new Authorization(0L);
                                    newAuthorizationData.Username = usrcr.Username;
                                    newAuthorizationData.Application = role._Application;
                                    newAuthorizationData.Role = role.RoleId;
                                    newAuthorizationData.Function = func.FunctionId;
                                    newAuthorizationData.Feature = feat.FeatureId;
                                    newAuthorizationData.QualifierKey = "";
                                    newAuthorizationData.QualifierValue = "";
                                    newAuthorizationData.ChangedBy = user.Username;
                                    newAuthorizationData.ChangedDate = DateTime.Now;
                                    newAuthorizationData.CreatedBy = user.Username;
                                    newAuthorizationData.CreatedDate = DateTime.Now;
                                    _authRepository.Add(newAuthorizationData);
                                }
                                catch (Exception e)
                                {

                                }
                            }
                        }
                    }

                    _userCrRepository.setApproved(usrcr.Id, user.Username, DateTime.Now, ApprovalStatusState.A.ToString());
                    _userRepository.Save(newData2);

                }


            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                //ScreenMessages.Submit(ScreenMessage.Success(SvsResources.Validation_Approval));
                //return RedirectToAction("Index");
                return this.Json(RoleResources.Validation_Success, JsonRequestBehavior.AllowGet);
            }
            return this.Json(message, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Insert(AuthorizationHelper data)
        {
            User user = Lookup.Get<User>();
            UserApplication userapp = Lookup.Get<UserApplication>();
            Role role = _roleRepository.getRole(data.RoleId);

            IList<RoleFunctionFeature> roleFuncFeat = _roleFuncFeatRepository.getRoleFuncFeaturebyRole(data.RoleId);

            foreach (var item in roleFuncFeat)
            {
                AuthorizationFeature feat = _featRepository.getFeature(item.FeatureId);
                AuthorizationFunction func = _funcRepository.getFunction(item.FunctionId);
                try
                {
                    Authorization newData = new Authorization(0L);
                    newData.Username = userapp.Username;
                    newData.Application = userapp.Application;
                    newData.Role = role.RoleId;
                    newData.Function = func.FunctionId;
                    newData.Feature = feat.Name;
                    newData.QualifierKey = "";
                    newData.QualifierValue = "";
                    newData.ChangedBy = user.Username;
                    newData.ChangedDate = DateTime.Now;
                    newData.CreatedBy = user.Username;
                    newData.CreatedDate = DateTime.Now;
                    _authRepository.Add(newData);
                    _userRepository.setUpdateMenu(userapp.Username, true);
                }
                catch (Exception e)
                {

                }
            }
            return RedirectToAction("Search");
        }

        [HttpPost]
        public ActionResult Delete(AuthorizationHelper data)
        {
            User user = Lookup.Get<User>();
            Role role = _roleRepository.getRole(data.RoleId);
            UserApplication userapp = Lookup.Get<UserApplication>();
            string message = "";
            try
            {
                _authRepository.delete(userapp.Username, userapp.Application, role.RoleId);
                _userRepository.setUpdateMenu(userapp.Username, true);
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            return RedirectToAction("Search");
        }

    }
}