﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using UOB.SVS.Models;
using Treemas.Base.Utilities;
using UOB.SVS.Resources;
using UOB.SVS.Model;
using UOB.SVS.Interface;
using Treemas.Base.Utilities.Queries;
using System.Linq;
using Treemas.Credential.Model;


namespace UOB.SVS.Controllers
{
    public class DeleteSVSController : PageController
    {
        private ISignatureHRepository _hRepository;
        private ISignatureHReqRepository _signatureHReqRepository;
        private ISignatureHReqService _signatureHReqService;
        private IAccountNoViewRepository _accNoVwRepo;
        private IAuditTrailLog _auditRepository;
        private IBranchRepository _branchRepository;
        private IDocumentTypeRepository _docTypeRepository;

        public DeleteSVSController(ISessionAuthentication sessionAuthentication, ISignatureHRepository hRepository,
            ISignatureHReqRepository signatureHReqRepository,
            ISignatureHReqService signatureHReqService, IAccountNoViewRepository accNoVwRepo, IAuditTrailLog auditRepository,
            IBranchRepository branchRepository, IDocumentTypeRepository docTypeRepository) : base(sessionAuthentication)
        {
            this._hRepository = hRepository;
            this._signatureHReqRepository = signatureHReqRepository;
            this._signatureHReqService = signatureHReqService;
            this._accNoVwRepo = accNoVwRepo;
            this._auditRepository = auditRepository;
            this._branchRepository = branchRepository;
            this._docTypeRepository = docTypeRepository;
            Settings.ModuleName = "DeleteSVS";
            Settings.Title = "Delete SVS";
        }

        protected override void Startup()
        {

        }

        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.
                User user = Lookup.Get<User>();

                SVSReqFilter filter = new SVSReqFilter();
                filter.Branches = getBranches();
                string CIFNo = Request.QueryString.GetValues("CIFNo")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                filter.CIFNumber = CIFNo;
                // Loading.   
                PagedResult<SignatureHeader> data;

                data = _hRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, filter);
                var items = data.Items.ToList().ConvertAll<SignatureHReqView>(new Converter<SignatureHeader, SignatureHReqView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public SignatureHReqView ConvertFrom(SignatureHeader item)
        {
            User user = Lookup.Get<User>();

            SignatureHReqView returnItem = new SignatureHReqView();
            returnItem.AccountNo = item.AccountNo;
            returnItem.AccountName = item.AccountName;
            returnItem.AccountType = item.AccountType;
            returnItem.CIFNumber = item.CIFNumber;
            returnItem.BranchCode = item.BranchCode;
            returnItem.Note = item.Note;
            returnItem.RequestType = "U";
            returnItem.RequestDate = DateTime.Now;
            returnItem.RequestUser = user.Username;
            returnItem.RequestReason = "";

            int index = 1;
            if (item.Documents != null)
            {
                IList<Document> docs = new List<Document>();
                foreach (SignatureDDoc row in item.Documents)
                {
                    Document doc = new Document();
                    doc.ID = row.Id;
                    doc.Index = index++;
                    doc.DocumentType = row.DocumentType;
                    doc.FileBlob = row.FileBlob;
                    doc.FileType = row.FileType;
                    doc.AccountNo = row.AccountNo;
                    doc.IsNew = true;
                    docs.Add(doc);
                }
                returnItem.Documents = docs.ToList();
            }

            if (item.IdentityCards != null)
            {
                index = 1;
                IList<IdentityCard> idcards = new List<IdentityCard>();
                foreach (SignatureDKTP row in item.IdentityCards)
                {
                    IdentityCard idcard = new IdentityCard();
                    idcard.ID = row.Id;
                    idcard.Index = index++;
                    idcard.CroppedBlob = row.KTP;
                    idcard.ImageType = row.ImageType;
                    idcard.AccountNo = row.AccountNo;
                    idcard.IsNew = true;
                    idcards.Add(idcard);
                }
                returnItem.IdentityCards = idcards.ToList();
            }

            if (item.Signatures != null)
            {
                index = 1;
                IList<Signature> signs = new List<Signature>();
                foreach (SignatureDSign row in item.Signatures)
                {
                    Signature sign = new Signature();
                    sign.ID = row.Id;
                    sign.Index = index++;
                    sign.CroppedBlob = row.Signature;
                    sign.ImageType = row.ImageType;
                    sign.AccountNo = row.AccountNo;
                    sign.IsNew = true;
                    signs.Add(sign);
                }
                returnItem.Signatures = signs.ToList();
            }

            return returnItem;
        }

        public ActionResult Edit(string id)
        {
            User user = Lookup.Get<User>();
            Parameter param = Lookup.Get<Parameter>();
            ViewData["ActionName"] = "Copy";
            ViewData["Parameter"] = param;

            SignatureHeader reqH = _hRepository.getSignatureHeader(id);
            if (reqH.IsNull())
            {
                ScreenMessages.Submit(ScreenMessage.Error("Account number does not exist."));
                return RedirectToAction("Index");
            }
            var branches = getBranches();
            if (user.BranchCode != "000" && !branches.Exists(x => x.BranchCode == reqH.BranchCode))
            {
                ScreenMessages.Submit(ScreenMessage.Error("Account number does not belong to your branch."));
                return RedirectToAction("Index");
            }
            reqH.IdentityCards = _hRepository.getKTPDet(reqH.AccountNo);
            reqH.Documents = _hRepository.getDocDet(reqH.AccountNo);
            reqH.Signatures = _hRepository.getSignDet(reqH.AccountNo);

            SignatureHReqView data = ConvertFrom(reqH);

            return CreateView(data);
        }

        [HttpPost]
        public ActionResult Edit(SignatureHReqView m)
        {
            User user = Lookup.Get<User>();
            Parameter param = Lookup.Get<Parameter>();
            ViewData["ActionName"] = "Copy";
            ViewData["Parameter"] = param;

            SignatureHeader reqH = _hRepository.getSignatureHeader(m.AccountNo);
            if (reqH.IsNull())
            {
                ScreenMessages.Submit(ScreenMessage.Error("Account number does not exist."));
                return RedirectToAction("Index");
            }

            var branches = getBranches();
            if (user.BranchCode != "000" && !branches.Exists(x => x.BranchCode == reqH.BranchCode))
            {
                ScreenMessages.Submit(ScreenMessage.Error("Account number does not belong to your branch."));
                return RedirectToAction("Index");
            }
            reqH.IdentityCards = _hRepository.getKTPDet(reqH.AccountNo);
            reqH.Documents = _hRepository.getDocDet(reqH.AccountNo);
            reqH.Signatures = _hRepository.getSignDet(reqH.AccountNo);

            SignatureHReqView data = ConvertFrom(reqH);

            return CreateView(data);
        }

        private ViewResult CreateView(SignatureHReqView data)
        {
            ViewData["BranchMaster"] = createBranchSelect(data.BranchCode);
            ViewData["DocTypeMaster"] = createDocTypeSelect("");
            ViewData["AccTypeMaster"] = createAccountTypeSelect(data.AccountType);
            return View("Detail", data);
        }


        // POST: Role/Delete/5
        [HttpPost]
        public ActionResult Delete(string Id)
        {
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                SignatureHRequest existingSHQ = _signatureHReqRepository.getSignatureHReq(Id);
                if (!existingSHQ.IsNull())
                {
                    if (existingSHQ.RequestType == "D" && existingSHQ.IsRejected == false)
                    {
                        ScreenMessages.Submit(ScreenMessage.Error("Delete failed, account number " + Id.Trim() + " is awaiting for DELETE approval"));
                        return RedirectToAction("Index");
                    }
                    else if (existingSHQ.RequestType == "U" && existingSHQ.IsRejected == false)
                    {
                        ScreenMessages.Submit(ScreenMessage.Error("Delete failed, account number " + Id.Trim() + " is awaiting for UPDATE approval"));
                        return RedirectToAction("Index");
                    }

                }

                SignatureHeader reqH = _hRepository.getSignatureHeader(Id);
                reqH.IdentityCards = _hRepository.getKTPDet(reqH.AccountNo);
                reqH.Documents = _hRepository.getDocDet(reqH.AccountNo);
                reqH.Signatures = _hRepository.getSignDet(reqH.AccountNo);

                SignatureHRequest newData = new SignatureHRequest("");
                newData.AccountNo = reqH.AccountNo;
                newData.AccountName = reqH.AccountName;
                newData.AccountType = reqH.AccountType;
                newData.CIFNumber = reqH.CIFNumber;
                newData.BranchCode = reqH.BranchCode;
                newData.Note = reqH.Note;
                newData.RequestType = "D";
                newData.RequestDate = DateTime.Now;
                newData.RequestUser = user.Username;
                newData.RequestReason = "Delete SVS";
                //newData.doc1 = byteDoc1;
                IList<SignatureDDocReq> docs = new List<SignatureDDocReq>();
                if (reqH.Documents != null)
                {
                    foreach (SignatureDDoc item in reqH.Documents)
                    {
                        SignatureDDocReq doc = new SignatureDDocReq(0);
                        doc.AccountNo = reqH.AccountNo;
                        doc.DocumentType = item.DocumentType;
                        doc.FileBlob = item.FileBlob;
                        doc.FileType = item.FileType;
                        doc.RequestType = "D";
                        doc.RequestDate = DateTime.Now;
                        doc.RequestUser = user.Username;
                        doc.RequestReason = "Delete SVS";
                        docs.Add(doc);
                    }
                }
                newData.Documents = docs;

                IList<SignatureDKTPReq> idcards = new List<SignatureDKTPReq>();
                if (reqH.IdentityCards != null)
                {
                    foreach (SignatureDKTP item in reqH.IdentityCards)
                    {
                        SignatureDKTPReq idcard = new SignatureDKTPReq(0);
                        idcard.AccountNo = reqH.AccountNo;
                        idcard.KTP = item.KTP;
                        idcard.ImageType = item.ImageType;
                        idcard.RequestType = "D";
                        idcard.RequestDate = DateTime.Now;
                        idcard.RequestUser = user.Username;
                        idcard.RequestReason = "Delete SVS";
                        idcards.Add(idcard);
                    }
                }
                newData.IdentityCards = idcards;

                IList<SignatureDSignReq> signs = new List<SignatureDSignReq>();
                if (reqH.Signatures != null)
                {
                    foreach (SignatureDSign item in reqH.Signatures)
                    {
                        SignatureDSignReq sign = new SignatureDSignReq(0);
                        sign.AccountNo = reqH.AccountNo;
                        sign.Signature = item.Signature;
                        sign.ImageType = item.ImageType;
                        sign.RequestType = "D";
                        sign.RequestDate = DateTime.Now;
                        sign.RequestUser = user.Username;
                        sign.RequestReason = "Delete SVS";
                        signs.Add(sign);
                    }
                }
                newData.Signatures = signs;

                message = _signatureHReqService.SaveSignatureReq(newData);

                // Hapus detail data, biar audit trailnya ngak penuh
                newData.Documents = null;
                newData.Signatures = null;
                newData.IdentityCards = null;
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(SvsResources.Validation_Approval));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            //CollectScreenMessages(); ga perlu karena ke index otomatis collect, kalo ada ini malah ga keluar messagenya by Ivan
            return RedirectToAction("Index");
        }

        #region Dropdownlist

        private IList<SelectListItem> createBranchSelect(string selected)
        {
            IList<SelectListItem> dataList = getBranches().ConvertAll<SelectListItem>(new Converter<Branch, SelectListItem>(ConvertFrom));
            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected).Selected = true;
            }
            return dataList;
        }

        public List<Branch> getBranches()
        {
            User user = Lookup.Get<User>();
            IList<string> branchs = new List<string>();
            if (user.BranchCode == "000")
            {
                return _branchRepository.getBranchs().ToList();
            }
            else
            {
                branchs.Add(user.BranchCode);
                return _branchRepository.getBranchs(branchs).ToList();
            }
        }

        public SelectListItem ConvertFrom(Branch item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.BranchCode;
            returnItem.Value = item.BranchCode;
            return returnItem;
        }

        private IList<SelectListItem> createDocTypeSelect(string selected)
        {
            IList<SelectListItem> dataList = _docTypeRepository.FindAll().ToList().ConvertAll<SelectListItem>(new Converter<DocumentType, SelectListItem>(ConvertFrom));
            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected).Selected = true;
            }
            return dataList;
        }

        public SelectListItem ConvertFrom(DocumentType item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.Description;
            returnItem.Value = item.DocType;
            return returnItem;
        }

        private IList<SelectListItem> createAccountTypeSelect(string selected)
        {
            IList<SelectListItem> dataList = new List<SelectListItem>();
            dataList.Insert(0, new SelectListItem() { Value = "CS", Text = "CA/SA" });
            dataList.Insert(1, new SelectListItem() { Value = "TD", Text = "TD" });

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected.ToString()).Selected = true;
            }
            return dataList;
        }

        #endregion

    }
}