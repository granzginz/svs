﻿using System;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using UOB.SVS.Models;
using Treemas.Base.Utilities;
using UOB.SVS.Resources;
using UOB.SVS.Model;
using UOB.SVS.Interface;
using Treemas.Base.Utilities.Queries;
using System.Linq;
using Treemas.Credential.Model;


namespace UOB.SVS.Controllers
{
    public class SvsController : PageController
    {
        private ISvsRepository _SvsRepo;
        private IParameterRepository _InquiryParameter;
        private IAuditTrailLog _auditRepository;

        public SvsController(ISessionAuthentication sessionAuthentication, ISvsRepository SvsRepo,
            IParameterRepository InquiryParameter, IAuditTrailLog auditRepository) : base(sessionAuthentication)
        {
            this._SvsRepo = SvsRepo;
            this._InquiryParameter = InquiryParameter;

            this._auditRepository = auditRepository;
            Settings.ModuleName = "Signature";
            Settings.Title = SvsResources.PageTitle;
        }

        protected override void Startup()
        {
        }

        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.   
                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.   
                PagedResult<Svs> data;
                if (searchValue.IsNullOrEmpty())
                {
                    data = _SvsRepo.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir);
                }
                else
                {
                    data = _SvsRepo.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchValue);
                }

                var items = data.Items.ToList().ConvertAll<SvsView>(new Converter<Svs, SvsView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public SvsView ConvertFrom(Svs item)
        {
            SvsView returnItem = new SvsView();
            returnItem.ACCOUNT_NUMBER = item.ACCOUNT_NUMBER;
            returnItem.CUSTOMER_NAME = item.CUSTOMER_NAME;
            returnItem.NOTE = item.NOTE;
            returnItem.ID = item.ID;
            returnItem.SIGNATUREVal = item.SIGNATURE;
            ViewBag.Base64String = item.SIGNATURE;
            return returnItem;
        }

        public ActionResult Create()
        {
            ViewData["ActionName"] = "Create";
            SvsView data = new SvsView();
            return CreateView(data);
        }

        // POST: Role/Create
        [HttpPost]
        public ActionResult Create(SvsView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                message = validateData(data, "Create");
                if (message.IsNullOrEmpty())
                {
                    Svs newData = new Svs(0L);
                    newData.ACCOUNT_NUMBER = data.ACCOUNT_NUMBER;
                    newData.CUSTOMER_NAME = data.CUSTOMER_NAME;
                    newData.NOTE = data.NOTE;
                    newData.SIGNATURE = data.SIGNATUREVal;

                    _SvsRepo.Add(newData);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Create");
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(SvsResources.Validation_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        private ViewResult CreateView(SvsView data)
        {
            return View("Detail", data);
        }

        private ViewResult CreateViewDisplay(SvsView data)
        {
            return View("Display", data);
        }

        // GET: /Edit/{ID}
        public ActionResult Edit(long id)
        {
            ViewData["ActionName"] = "Edit";
            Svs sign = _SvsRepo.getSvs(id);
            SvsView data = ConvertFrom(sign);

            return CreateView(data);
        }

        // POST: /Edit/{ID}
        [HttpPost]
        public ActionResult Edit(SvsView data)
        {
            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                message = validateData(data, "Edit");
                if (message.IsNullOrEmpty())
                {
                    Svs newData = new Svs(data.ID);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "BeforeEdit");

                    newData.ID = data.ID;
                    newData.ACCOUNT_NUMBER = data.ACCOUNT_NUMBER;
                    newData.CUSTOMER_NAME = data.CUSTOMER_NAME;
                    newData.NOTE = data.NOTE;
                    newData.SIGNATURE = data.SIGNATUREVal;

                    //_auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "AfterEdit");
                    _SvsRepo.Save(newData);
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(SvsResources.Validation_Success));
                return RedirectToAction("Index");
            }

            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        // POST: /Delete/{ID}
        [HttpPost]
        public ActionResult Delete(long Id)
        {
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                Svs newData = new Svs(Id);
                _SvsRepo.Remove(newData);
                _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Delete");
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(SvsResources.Delete_Success));
                return RedirectToAction("Index");
            }

            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return Edit(Id);
        }

        // GET: /Edit/{ID}
        public ActionResult Display(long id)
        {
            ViewData["ActionName"] = "Display";
            Svs sign = _SvsRepo.getSvs(id);
            SvsView data = ConvertFrom(sign);

            return CreateViewDisplay(data);
        }

        private string validateData(SvsView data, string mode)
        {
            if (data.ACCOUNT_NUMBER == "")
                return SvsResources.Validation_FillAccountNo;

            if (data.CUSTOMER_NAME == "")
                return SvsResources.Validation_FillCustName;

            if (mode == "Create")
            {
                if (_SvsRepo.IsDuplicate(data.ACCOUNT_NUMBER, data.ID))
                    return SvsResources.Validation_DuplicateData;
            }

            return "";
        }
    }
}