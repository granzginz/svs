﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using UOB.SVS.Models;
using Treemas.Base.Utilities;
using UOB.SVS.Resources;
using UOB.SVS.Model;
using UOB.SVS.Interface;
using Treemas.Base.Utilities.Queries;
using System.Linq;
using Treemas.Credential.Model;


namespace UOB.SVS.Controllers
{
    public class ApproveSVSController : PageController
    {
        private ISignatureHReqRepository _hReqRepository;
        private ISignatureHRepository _hRepository;
        private ISignatureHReqService _signatureHReqService;
        private IAuditTrailLog _auditRepository;
        private IBranchRepository _branchRepository;
        private IDocumentTypeRepository _docTypeRepository;
        private IUserRepository _userRepository;

        public ApproveSVSController(ISessionAuthentication sessionAuthentication, ISignatureHReqRepository hReqRepository,
            IBranchRepository branchRepository, IDocumentTypeRepository docTypeRepository, ISignatureHRepository hRepository,
            ISignatureHReqService signatureHReqService, IAuditTrailLog auditRepository, IUserRepository userRepository) : base(sessionAuthentication)
        {
            this._hReqRepository = hReqRepository;
            this._auditRepository = auditRepository;
            this._branchRepository = branchRepository;
            this._signatureHReqService = signatureHReqService;
            this._docTypeRepository = docTypeRepository;
            this._hRepository = hRepository;
            this._userRepository = userRepository;
            Settings.ModuleName = "ApproveSVS";
            Settings.Title = "Approval SVS";
        }

        protected override void Startup()
        {

        }

        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.   
                SVSReqFilter filter = new SVSReqFilter();
                User user = Lookup.Get<User>();
                filter.Branches = getBranches();
                filter.ApprovalType = "ApproveSVS";
                filter.RequestType = Request.QueryString.GetValues("reqType")[0];
                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.   
                PagedResult<SignatureHRequest> data;
                if (!searchValue.IsNullOrEmpty())
                {
                    if (searchColumn == "AccountNumber")
                        filter.AccountNo = searchValue;
                    else if (searchColumn == "AccountName")
                        filter.AccountName = searchValue;
                }

                data = _hReqRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, filter, user);
                var items = data.Items.ToList().ConvertAll<SignatureHReqView>(new Converter<SignatureHRequest, SignatureHReqView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public SignatureHReqView ConvertFrom(SignatureHRequest item)
        {
            SignatureHReqView returnItem = new SignatureHReqView();
            returnItem.AccountNo = item.AccountNo;
            returnItem.AccountName = item.AccountName;
            returnItem.AccountType = item.AccountType;
            returnItem.CIFNumber = item.CIFNumber;
            returnItem.BranchCode = item.BranchCode;
            returnItem.Note = item.Note;
            returnItem.RequestType = item.RequestType;
            returnItem.RequestDate = item.RequestDate;
            returnItem.RequestUser = item.RequestUser;
            returnItem.RequestReason = item.RequestReason;
            returnItem.RequestDateString = item.RequestDate.Value.ToString("dd/MM/yyyy");

            int index = 1;
            if (item.Documents != null)
            {
                IList<Document> docs = new List<Document>();
                foreach (SignatureDDocReq row in item.Documents)
                {
                    Document doc = new Document();
                    doc.Index = index++;
                    doc.DocumentType = row.DocumentType;
                    doc.FileBlob = row.FileBlob;
                    doc.FileType = row.FileType;
                    docs.Add(doc);
                }
                returnItem.Documents = docs.ToList();
            }

            if (item.IdentityCards != null)
            {
                index = 1;
                IList<IdentityCard> idcards = new List<IdentityCard>();
                foreach (SignatureDKTPReq row in item.IdentityCards)
                {
                    IdentityCard idcard = new IdentityCard();
                    idcard.Index = index++;
                    idcard.CroppedBlob = row.KTP;
                    idcard.ImageType = row.ImageType;
                    idcards.Add(idcard);
                }
                returnItem.IdentityCards = idcards.ToList();
            }

            if (item.Signatures != null)
            {
                index = 1;
                IList<Signature> signs = new List<Signature>();
                foreach (SignatureDSignReq row in item.Signatures)
                {
                    Signature sign = new Signature();
                    sign.Index = index++;
                    sign.CroppedBlob = row.Signature;
                    sign.ImageType = row.ImageType;
                    signs.Add(sign);
                }
                returnItem.Signatures = signs.ToList();
            }

            

            return returnItem;
        }
        public SignatureHReqView ConvertFromHeader(SignatureHeader header)
        {
            SignatureHReqView returnItem = new SignatureHReqView();
            returnItem.AccountNo = header.AccountNo;
            returnItem.AccountNameOld = header.AccountName;
            returnItem.AccountTypeOld = header.AccountType;
            returnItem.CIFNumberOld = header.CIFNumber;
            returnItem.BranchCodeOld = header.BranchCode;
            returnItem.NoteOld = header.Note;
            
            int index = 1;

            if (header.Documents != null)
            {
                IList<Document> docs = new List<Document>();
                foreach (SignatureDDoc row in header.Documents)
                {
                    Document doc = new Document();
                    doc.Index = index++;
                    doc.DocumentTypeOld = row.DocumentType;
                    doc.FileBlobOld = row.FileBlob;
                    doc.FileTypeOld = row.FileType;
                    docs.Add(doc);
                }
                returnItem.Documents = docs.ToList();
            }

            if (header.IdentityCards != null)
            {
                index = 1;
                IList<IdentityCard> idcards = new List<IdentityCard>();
                foreach (SignatureDKTP row in header.IdentityCards)
                {
                    IdentityCard idcard = new IdentityCard();
                    idcard.Index = index++;
                    idcard.CroppedBlobOld = row.KTP;
                    idcard.ImageTypeOld = row.ImageType;
                    idcards.Add(idcard);
                }
                returnItem.IdentityCards = idcards.ToList();
            }

            if (header.Signatures != null)
            {
                index = 1;
                IList<Signature> signs = new List<Signature>();
                foreach (SignatureDSign row in header.Signatures)
                {
                    Signature sign = new Signature();
                    sign.Index = index++;
                    sign.CroppedBlobOld = row.Signature;
                    sign.ImageTypeOld = row.ImageType;
                    signs.Add(sign);
                }
                returnItem.Signatures = signs.ToList();
            }

            return returnItem;
        }

        public ActionResult Edit(string id)
        {
            Parameter param = Lookup.Get<Parameter>();
            ViewData["Parameter"] = param;
            SignatureHRequest reqH = _hReqRepository.getSignatureHReq(id);
            reqH.IdentityCards = _hReqRepository.getKTPReqD(reqH.AccountNo);
            reqH.Documents = _hReqRepository.getDocReqD(reqH.AccountNo);
            reqH.Signatures = _hReqRepository.getSignReqD(reqH.AccountNo);

            User userReq = _userRepository.GetUser(reqH.RequestUser);
            SignatureHReqView data = ConvertFrom(reqH);

            SignatureHeader reqHeader = _hRepository.getSignatureHeader(id);
            if (reqHeader != null)
            {
                reqHeader.IdentityCards = _hRepository.getKTPDet(reqH.AccountNo);
                reqHeader.Documents = _hRepository.getDocDet(reqH.AccountNo);
                reqHeader.Signatures = _hRepository.getSignDet(reqH.AccountNo);

                SignatureHReqView dataheader = ConvertFromHeader(reqHeader);
                ViewData["DataHeader"] = dataheader;
            }
            
            ViewData["SVSData"] = data;
            ViewData["UserReq"] = userReq;

            return CreateView(data);
        }

        private ViewResult CreateView(SignatureHReqView data)
        {
            User user = Lookup.Get<User>();

            ViewData["UserData"] = user;
            ViewData["BranchMaster"] = createBranchSelect("");
            ViewData["DocTypeMaster"] = createDocTypeSelect("");
            ViewData["AccTypeList"] = createAccountTypeSelect("");
            

            if (data.RequestType == "U")
            {
                return View("DetailEdit", data);
            }
            else
            {
                return View("Detail", data);
            }
        }


        [HttpPost]
        public ActionResult Approve(SignatureHReqView data)
        {
            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            string message = "";
            SignatureHRequest reqH = _hReqRepository.getSignatureHReq(data.AccountNo);
            User userReq = _userRepository.GetUser(reqH.RequestUser);

            try
            {
                if (userReq.BranchCode != user.BranchCode)
                {
                    message = SvsResources.Validation_BranchCodeNotMatch;
                }
                if (!(data.RequestDate==reqH.RequestDate))
                {
                    message = "Already change";
                }
                if (message.IsNullOrEmpty())
                {

                    reqH.IdentityCards = _hReqRepository.getKTPReqD(reqH.AccountNo);
                    reqH.Documents = _hReqRepository.getDocReqD(reqH.AccountNo);
                    reqH.Signatures = _hReqRepository.getSignReqD(reqH.AccountNo);

                    SignatureHReqView dataReq = ConvertFrom(reqH);

                    SignatureHeader oldsvsH = new SignatureHeader(data.AccountNo);
                    oldsvsH.IdentityCards = _hRepository.getKTPDet(oldsvsH.AccountNo);
                    oldsvsH.Documents = _hRepository.getDocDet(oldsvsH.AccountNo);
                    oldsvsH.Signatures = _hRepository.getSignDet(oldsvsH.AccountNo);

                    SignatureHRequest newData = new SignatureHRequest("");
                    newData.AccountNo = dataReq.AccountNo;
                    newData.AccountName = dataReq.AccountName;
                    newData.AccountType = dataReq.AccountType;
                    newData.CIFNumber = dataReq.CIFNumber;
                    newData.BranchCode = dataReq.BranchCode;
                    newData.Note = dataReq.Note;
                    newData.RequestType = dataReq.RequestType;
                    newData.RequestDate = dataReq.RequestDate;
                    newData.RequestUser = dataReq.RequestUser;
                    newData.RequestReason = dataReq.RequestReason;

                    SignatureHeader svsH = new SignatureHeader("");
                    svsH.AccountNo = dataReq.AccountNo;
                    svsH.AccountName = dataReq.AccountName;
                    svsH.AccountType = dataReq.AccountType;
                    svsH.CIFNumber = dataReq.CIFNumber;
                    svsH.BranchCode = dataReq.BranchCode;
                    svsH.Note = dataReq.Note;

                    IList<SignatureDDoc> docs = new List<SignatureDDoc>();

                    if (dataReq.Documents != null)
                    {
                        foreach (Document item in dataReq.Documents)
                        {
                            SignatureDDoc doc = new SignatureDDoc(0L);
                            doc.AccountNo = dataReq.AccountNo;
                            doc.DocumentType = item.DocumentType;
                            doc.FileBlob = item.FileBlob;
                            doc.FileType = item.FileType;
                            docs.Add(doc);
                        }
                    }
                    svsH.Documents = docs;

                    IList<SignatureDKTP> idcards = new List<SignatureDKTP>();
                    if (dataReq.Documents != null)
                    {
                        foreach (IdentityCard item in dataReq.IdentityCards)
                        {
                            SignatureDKTP idcard = new SignatureDKTP(0L);
                            idcard.AccountNo = dataReq.AccountNo;
                            idcard.KTP = item.CroppedBlob;
                            idcard.ImageType = item.ImageType;
                            idcards.Add(idcard);
                        }
                    }
                    svsH.IdentityCards = idcards;

                    IList<SignatureDSign> signs = new List<SignatureDSign>();
                    if (dataReq.Documents != null)
                    {
                        foreach (Signature item in dataReq.Signatures)
                        {
                            SignatureDSign sign = new SignatureDSign(0L);
                            sign.AccountNo = dataReq.AccountNo;
                            sign.Signature = item.CroppedBlob;
                            sign.ImageType = item.ImageType;
                            signs.Add(sign);
                        }
                    }
                    svsH.Signatures = signs;

                    SignatureHHistory svsHistory = new SignatureHHistory(0L);
                    svsHistory.AccountNo = dataReq.AccountNo;
                    svsHistory.AccountName = dataReq.AccountName;
                    svsHistory.AccountType = dataReq.AccountType;
                    svsHistory.CIFNumber = dataReq.CIFNumber;
                    svsHistory.BranchCode = dataReq.BranchCode;
                    svsHistory.Note = dataReq.Note;
                    svsHistory.RequestType = dataReq.RequestType;
                    svsHistory.RequestDate = dataReq.RequestDate;
                    svsHistory.RequestUser = dataReq.RequestUser;
                    svsHistory.RequestReason = dataReq.RequestReason;
                    svsHistory.ApproveBy = user.Username;
                    svsHistory.ApproveDate = DateTime.Now;

                    message = _signatureHReqService.ApproveReq(newData, svsH, svsHistory, user);

                    // Hapus detail data, biar audit trailnya ngak penuh
                    svsH.Documents = null;
                    svsH.Signatures = null;
                    svsH.IdentityCards = null;

                    oldsvsH.Documents = null;
                    oldsvsH.Signatures = null;
                    oldsvsH.IdentityCards = null;

                    //_auditRepository.SaveAuditTrail(Settings.ModuleName, svsH, user.Username, "Approve");
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, oldsvsH, svsH, dataReq.RequestUser, dataReq.RequestDate, user.Username, dataReq.RequestType);

                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {

                ScreenMessages.Submit(ScreenMessage.Success(SvsResources.Validation_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            //ini kalau ga ke index
            //CollectScreenMessages();
            //return CreateView(data); //ini akan problem gara2 si dropdown di disable, harus kasih hidden valuenya si dropdown di detail

            //ini kalau ke index
            //CollectScreenMessages(); ga perlu karena ke index otomatis collect, kalo ada ini malah ga keluar messagenya by Ivan
            return RedirectToAction("Index");
        }

        //[HttpPost]
        public ActionResult Reject(string Id)
        {
            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            string message = "";

            try
            {
                if (message.IsNullOrEmpty())
                {
                    SignatureHRequest reqH = _hReqRepository.getSignatureHReq(Id);
                    reqH.IsRejected = true;
                    reqH.IdentityCards = _hReqRepository.getKTPReqD(reqH.AccountNo);
                    reqH.Documents = _hReqRepository.getDocReqD(reqH.AccountNo);
                    reqH.Signatures = _hReqRepository.getSignReqD(reqH.AccountNo);

                    SignatureHeader oldsvsH = _hRepository.getSignatureHeader(reqH.AccountNo);
                    if (oldsvsH.IsNull())
                        oldsvsH = new SignatureHeader(reqH.AccountNo);

                    message = _signatureHReqService.RejectReq(reqH, user);

                    // Hapus detail data, biar audit trailnya ngak penuh
                    reqH.Documents = null;
                    reqH.Signatures = null;
                    reqH.IdentityCards = null;

                    oldsvsH.Documents = null;
                    oldsvsH.Signatures = null;
                    oldsvsH.IdentityCards = null;

                    //_auditRepository.SaveAuditTrail(Settings.ModuleName, reqH, user.Username, "Reject");
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, oldsvsH, reqH, reqH.RequestUser, reqH.RequestDate, user.Username, "Reject");

                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {

                ScreenMessages.Submit(ScreenMessage.Success(SvsResources.Validation_Reject));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            //ini kalau ga ke index
            //CollectScreenMessages();
            //return CreateView(data); //ini akan problem gara2 si dropdown di disable, harus kasih hidden valuenya si dropdown di detail

            //ini kalau ke index
            //CollectScreenMessages(); ga perlu karena ke index otomatis collect, kalo ada ini malah ga keluar messagenya by Ivan
            return RedirectToAction("Index");
        }

        public List<Branch> getBranches()
        {
            User user = Lookup.Get<User>();
            IList<string> branchs = new List<string>();
            if (user.BranchCode == "000")
            {
                return _branchRepository.getBranchs().ToList();
            }
            else
            {
                branchs.Add(user.BranchCode);
                return _branchRepository.getBranchs(branchs).ToList();
            }
        }

        #region Dropdownlist

        private IList<SelectListItem> createBranchSelect(string selected)
        {
            IList<SelectListItem> dataList = _branchRepository.FindAll().ToList().ConvertAll<SelectListItem>(new Converter<Branch, SelectListItem>(ConvertFrom));
            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected).Selected = true;
            }
            return dataList;
        }

        private IList<SelectListItem> createAccountTypeSelect(string selected)
        {
            IList<SelectListItem> dataList = new List<SelectListItem>();
            dataList.Insert(0, new SelectListItem() { Value = "CS", Text = "CA/SA" });
            dataList.Insert(1, new SelectListItem() { Value = "TD", Text = "TD" });
            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected).Selected = true;
            }
            return dataList;
        }

        public SelectListItem ConvertFrom(Branch item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.BranchCode;
            returnItem.Value = item.BranchCode;
            return returnItem;
        }
        private IList<SelectListItem> createDocTypeSelect(string selected)
        {
            IList<SelectListItem> dataList = _docTypeRepository.FindAll().ToList().ConvertAll<SelectListItem>(new Converter<DocumentType, SelectListItem>(ConvertFrom));
            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected).Selected = true;
            }
            return dataList;
        }

        public SelectListItem ConvertFrom(DocumentType item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.Description;
            returnItem.Value = item.DocType;
            return returnItem;
        }

        #endregion
    }
}