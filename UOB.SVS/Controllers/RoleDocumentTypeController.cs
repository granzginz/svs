﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using Treemas.Credential.Model;
using UOB.SVS.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using UOB.SVS.Interface;
using UOB.SVS.Resources;
using UOB.SVS.Model;

namespace UOB.SVS.Controllers
{
    public class RoleDocumentTypeController : PageController
    {
        private IRoleRepository _roleRepository;
        private IDocumentTypeRepository _docTypeRepository;
        private IRoleDocTypeRepository _roleDocTypeRepository;
        private IAppAuditTrailLog _auditRepository;
        private IRoleDocTypeCrRepository _roleDocTypeCrRepository;

        public IList<RoleDocType> roledocTypeList;
        public IList<RoleDocTypeCr> roledocTypeCrList;
        //int i = 0;
        static int Roleid;
        public RoleDocumentTypeController(ISessionAuthentication sessionAuthentication, IRoleRepository roleRepository, IRoleDocTypeRepository roleDocTypeRepository,
            IDocumentTypeRepository docTypeRepository, IAppAuditTrailLog auditRepository, IRoleDocTypeCrRepository roleDocTypeCrRepository)
            : base(sessionAuthentication)
        {
            this._roleRepository = roleRepository;
            this._roleDocTypeRepository = roleDocTypeRepository;
            this._docTypeRepository = docTypeRepository;
            this._auditRepository = auditRepository;
            this._roleDocTypeCrRepository = roleDocTypeCrRepository;
            Settings.Title = RoleResources.PageTitleRoleDocType;
            Settings.ModuleName = "Role";
        }
        protected override void Startup()
        {
            var returnpage = new { controller = "Role", action = "Index" };
            try
            {
                Roleid = Convert.ToInt32(Request.RequestContext.RouteData.Values["Id"]);
                Role role = null;
                if (Roleid < 1)
                    Response.RedirectToRoute(returnpage);
                role = _roleRepository.getRole(Roleid);
                if (role.IsNull())
                    Response.RedirectToRoute(returnpage);
                Lookup.Remove<Role>();
                Lookup.Add(role);
                ViewData["Role"] = role;
                //Search();
            }
            catch
            {
                Response.RedirectToRoute(returnpage);
            }

        }
        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            try
            {
                Role role = Lookup.Get<Role>();
                //RoleDocType master = new RoleDocType(0);
                //master = _roleDocTypeCrRepository.getDocTypes(Roleid);
                if (role.IsNull())
                {
                    if (Request.QueryString.GetValues("Id").IsNullOrEmpty())
                        return null;
                    long Id = long.Parse(Request.QueryString.GetValues("Id")[0]);
                    role = _roleRepository.getRole(Id);
                    Lookup.Remove<Role>();
                    Lookup.Add(role);
                }
                // Initialization.   
                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                roledocTypeList = _roleDocTypeRepository.getDocTypes(role.Id);
                roledocTypeCrList = _roleDocTypeCrRepository.getDocTypes(role.Id);

                // Loading.   
                PagedResult<DocumentType> data;
                if (searchValue.IsNullOrEmpty())
                {
                    data = _docTypeRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir);
                }
                else
                {
                    data = _docTypeRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchValue);
                }

                var items = data.Items.ToList().ConvertAll<RoleDocTypeView>(new Converter<DocumentType, RoleDocTypeView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }
        public RoleDocTypeView ConvertFrom(DocumentType item)
        {
            RoleDocTypeView returnItem = new RoleDocTypeView();
            var found = roledocTypeList.FindElement(x => x.DocumentTypeId == item.Id);
            //var foundCr = roledocTypeCrList.FindElement(x => x.DocumentTypeId == item.Id);

            //if (found.IsNull() && foundCr.IsNull())
            if (found.IsNull() )
            {
                    returnItem.selected = false;
            }
            else
            {
                returnItem.selected = true;
                //returnItem.Id = found.IsNull() ? foundCr.Id : found.Id;
                //returnItem.DocumentTypeId = found.IsNull() ? foundCr.RoleId : found.RoleId;
            }
            returnItem.Id = item.Id;
            returnItem.DocumentTypeId = item.Id;
            returnItem.DocType = item.DocType;
            returnItem.Description = item.Description;
            return returnItem;
        }

        [HttpPost]
        public JsonResult Edit(string userName, string apps)
        {
            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            string message = "";


            try
            {
                //UserCr usrcr = _userCrRepository.GetUser(userName, ApprovalType.U);
                //if (!usrcr.IsNull())
                //{
                //    throw new Exception(UserResources.Validation_AlreadyRequest);
                //}

                //User oldData = _userRepository.GetUser(userName);

                //get authorized applications

                Role coverage = _roleRepository.getRole(long.Parse(userName));
                IList<RoleDocType> authorizedApplicationIdsList = _roleDocTypeRepository.getDocTypes(long.Parse(userName));

                IList<RoleDocTypeCr> authorizedApplicationIdsListCR = _roleDocTypeCrRepository.getDocTypes(long.Parse(userName),"N");

                string authorizedApplicationIdsString = string.Empty;
                foreach (RoleDocType app in authorizedApplicationIdsList)
                {
                    authorizedApplicationIdsString += app.DocumentTypeId.ToString() + ",";
                }
                if (!authorizedApplicationIdsString.IsNullOrEmpty())
                {
                    authorizedApplicationIdsString = authorizedApplicationIdsString.Remove(authorizedApplicationIdsString.LastIndexOf(','), 1);
                }

                string authorizedApplicationIdsStringCR = string.Empty;
                foreach (RoleDocTypeCr app in authorizedApplicationIdsListCR)
                {
                    authorizedApplicationIdsStringCR += app.DocumentTypeId.ToString() + ",";
                }
                if (!authorizedApplicationIdsStringCR.IsNullOrEmpty())
                {
                    authorizedApplicationIdsStringCR = authorizedApplicationIdsStringCR.Remove(authorizedApplicationIdsStringCR.LastIndexOf(','), 1);
                }

                string[] appsCR = authorizedApplicationIdsStringCR.Split(',');
                string[] appsOld = authorizedApplicationIdsString.Split(',');
                string[] appsNew = apps.Split(',');
                string appsUpdate = string.Empty;
                string appsInsert = string.Empty;
                bool cek = false;

                for (int i = 0; i < appsCR.Length; i++)
                {
                    for (int ii = 0; ii < appsNew.Length; ii++)
                    {
                        if (appsNew[ii].Trim() == appsCR[i].Trim() && appsCR[i].Trim()!="")
                        {
                            cek = true;
                            break;
                        }
                    }
                    if (cek)
                    {
                        break;
                        //appsUpdate += appsOld[i] + ",";
                    }
                }
                if (cek)
                {
                    throw new Exception("There is already the same request that need approval");
                }

                var q = from a in appsOld
                        join b in appsNew on a equals b
                        select a;

                bool isAppsEqual = appsOld.Length == appsNew.Length && q.Count() == appsOld.Length;
                if (isAppsEqual)
                {
                    throw new Exception("No application changes being made!");
                }

                for (int i = 0; i < appsOld.Length; i++)
                {
                    for (int ii = 0; ii < appsNew.Length; ii++)
                    {
                        if (appsNew[ii].Trim() == appsOld[i].Trim() || appsOld[i].Trim() == "")
                        {
                            cek = false;
                            break;
                        }
                        else
                        {
                            cek = true;
                        }
                    }
                    if (cek)
                    {
                        appsUpdate += appsOld[i] + ",";
                    }
                }
                if (!appsUpdate.IsNullOrEmpty())
                {
                    appsUpdate = appsUpdate.Remove(appsUpdate.LastIndexOf(','), 1);
                }

                string[] appsU = appsUpdate.Split(',');

                for (int i = 0; i < appsNew.Length; i++)
                {
                    for (int ii = 0; ii < appsOld.Length; ii++)
                    {
                        if (appsOld[ii].Trim() == appsNew[i].Trim())
                        {
                            cek = false;
                            break;
                        }
                        else
                        {
                            cek = true;
                        }
                    }
                    if (cek)
                    {
                        appsInsert += appsNew[i] + ",";
                    }
                }
                if (!appsInsert.IsNullOrEmpty())
                {
                    appsInsert = appsInsert.Remove(appsInsert.LastIndexOf(','), 1);
                }

                string[] appsI = appsInsert.Split(',');
                //string authorizedRoles = _authRepository.getDistinctUserAuthorizationByUsername(userName);





                //_dispatcherMasterCrRepo.Add(newData);
                if (!isAppsEqual)
                {
                    RoleDocTypeCr newData = new RoleDocTypeCr(0L);

                    newData.RoleId = coverage.Id;


                    newData.CreatedBy = user.Username;
                    newData.CreatedDate = DateTime.Now;
                    newData.ApprovalStatus = "N";
                    newData.Approved = false;

                    foreach (string appId in appsU)
                    {
                        if (appId.Trim() == "")
                            break;
                        newData.ApprovalType = ApprovalType.D.ToString();
                        newData.DocumentTypeId = long.Parse(appId);
                        _roleDocTypeCrRepository.Add(newData);
                    }
                    // begin new insert
                    foreach (string appId in appsI)
                    {
                        if (appId.Trim() == "")
                            break;
                        newData.ApprovalType = ApprovalType.C.ToString();
                        newData.DocumentTypeId = long.Parse(appId);
                        _roleDocTypeCrRepository.Add(newData);
                    }
                }



            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                return this.Json(ApplicationResources.Validation_Success, JsonRequestBehavior.AllowGet);
            }
            return this.Json(message, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create(RoleDocTypeSelectHelper select)
        {
            JsonResult result = new JsonResult();
            User user = Lookup.Get<User>();
            string message = "";
            //string messageRFF = "";
            long Id = 0;
            //long IdRoledocTypeFeat = 0;
            try
            {
                Role role = Lookup.Get<Role>();

                DocumentType docType = null;
                RoleDocType roledocType = _roleDocTypeRepository.getRoleDocType(role.Id, select.DocumentTypeId);
                RoleDocTypeCr roledocTypecr = _roleDocTypeCrRepository.getRoleDocType(role.Id, select.DocumentTypeId);

                //IList<DocTypeFeature> docTypeFeatlist = null;
                if (select.selected == true)
                {
                    if (roledocTypecr.IsNull())
                    {
                        docType = _docTypeRepository.getDocumentType(select.DocumentTypeId);
                        Id = Insert(docType, role.Id);

                        //if (!roledocType.IsNull())
                        //{
                        //docTypeFeatlist = _docTypefeatRepository.getDocTypeFeatures(select.DocTypeId);
                        //roledocType.RoleId = role.Id;
                        //roledocType.DocTypeId = select.DocTypeId;
                        //foreach (var a in docTypeFeatlist)
                        //{
                        //    IdRoledocTypeFeat = InsertRoledocTypeFeatCr(Id, role.Id, select.DocTypeId, a.FeatureId);
                        //}
                        //}
                    }
                    else
                    {
                        if (roledocTypecr.ApprovalType == "D")
                            message = DeleteCr(role.Id, select.DocumentTypeId);
                    }
                }
                else
                {
                    if (!roledocTypecr.IsNull())
                    {
                        if (roledocTypecr.ApprovalType == "C")
                        {
                            message = DeleteCr(role.Id, select.DocumentTypeId);
                        }
                    }
                    else
                    {
                        if (!roledocType.IsNull())
                        {
                            //messageRFF = DeleteRoledocTypeFeat(roledocType.Id);
                            message = Delete(role.Id, select.DocumentTypeId);
                        }
                    }



                }

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            result = this.Json(new
            {
                message = message,
                Id = Id
            }, JsonRequestBehavior.AllowGet);

            // Return info.   
            return result;
        }


        private long Insert(DocumentType data, long roleId)
        {
            User user = Lookup.Get<User>();
            long returnId = -1;
            try
            {
                //RoleDocType newData = new RoleDocType(0L);
                RoleDocTypeCr newData = new RoleDocTypeCr(0L);
                newData.DocumentTypeId = data.Id;
                newData.RoleId = roleId;
                newData.ChangedBy = user.Username;
                newData.ChangedDate = DateTime.Now;
                newData.CreatedBy = user.Username;
                newData.CreatedDate = DateTime.Now;
                newData.ApprovalType = "C";
                newData.ApprovalStatus = "N";
                newData.Approved = false;

                //_roledocTypeRepository.Add(newData);
                _roleDocTypeCrRepository.Add(newData);
                returnId = newData.Id;
            }
            catch (Exception e)
            {
                returnId = -1;
            }
            return returnId;
        }

        //private long InsertRoledocTypeFeat(long rfId, long roleId, long docTypeId, long featId)
        //{
        //    User user = Lookup.Get<User>();
        //    long returnId = -1;
        //    try
        //    {
        //        RoleDocTypeFeature newData = new RoleDocTypeFeature(0L);
        //        newData.FeatureId = featId;
        //        newData.RoleDocTypeId = rfId;
        //        newData.DocTypeId = docTypeId;
        //        newData.RoleId = roleId;
        //        newData.ChangedBy = user.Username;
        //        newData.ChangedDate = DateTime.Now;
        //        newData.CreatedBy = user.Username;
        //        newData.CreatedDate = DateTime.Now;
        //        _roledocTypefeatRepository.Add(newData);
        //        returnId = newData.Id;

        //    }
        //    catch (Exception e)
        //    {
        //        returnId = -1;
        //    }
        //    return returnId;
        //}

        //private long InsertRoledocTypeFeatCr(long rfId, long roleId, long docTypeId, long featId)
        //{
        //    User user = Lookup.Get<User>();
        //    long returnId = -1;
        //    try
        //    {
        //        RoleDocTypeFeatureCr newData = new RoleDocTypeFeatureCr(0L);
        //        newData.FeatureId = featId;
        //        newData.RoleDocTypeId = rfId;
        //        newData.DocTypeId = docTypeId;
        //        newData.RoleId = roleId;
        //        newData.ChangedBy = user.Username;
        //        newData.ChangedDate = DateTime.Now;
        //        newData.CreatedBy = user.Username;
        //        newData.CreatedDate = DateTime.Now;
        //        newData.ApprovalType = "C";
        //        newData.ApprovalStatus = "N";
        //        newData.Approved = false;

        //        _roledocTypefeatCrRepository.Add(newData);

        //        returnId = newData.Id;

        //    }
        //    catch (Exception e)
        //    {
        //        returnId = -1;
        //    }
        //    return returnId;
        //}

        public string Delete(long roleId, long DocTypeId)
        {
            string message = "";
            User user = Lookup.Get<User>();
            try
            {
                //RoleDocType newData = new RoleDocType(0L);
                RoleDocTypeCr newData = new RoleDocTypeCr(0L);
                newData.DocumentTypeId = DocTypeId;
                newData.RoleId = roleId;
                newData.ChangedBy = user.Username;
                newData.ChangedDate = DateTime.Now;
                newData.CreatedBy = user.Username;
                newData.CreatedDate = DateTime.Now;
                newData.ApprovalType = "D";
                newData.ApprovalStatus = "N";
                newData.Approved = false;

                _roleDocTypeCrRepository.Add(newData);
                //_roledocTypeRepository.deleteSelected(roleId, DocTypeId);
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            return message;
        }

        public string DeleteCr(long roleId, long DocTypeId)
        {
            string message = "";
            try
            {
                _roleDocTypeCrRepository.deleteSelected(roleId, DocTypeId);
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            return message;
        }

        //public string DeleteRoledocTypeFeat(long roleDocTypeId)
        //{
        //    string message = "";
        //    try
        //    {
        //        _roledocTypefeatRepository.deleteAllbyRoledocTypeId(roleDocTypeId);
        //    }
        //    catch (Exception e)
        //    {
        //        message = e.Message;
        //    }

        //    return message;
        //}
    }
}