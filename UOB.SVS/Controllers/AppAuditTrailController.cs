﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using Treemas.Credential.Model;
using UOB.SVS.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using UOB.SVS.Resources;
using System.Globalization;
using System.Text.RegularExpressions;
using Microsoft.Reporting.WebForms;
using System.IO;
using SpreadsheetLight;
using System.Data;

namespace UOB.SVS.Controllers
{
    public class AppAuditTrailController : PageController
    {
        private IUserRepository _userRepository;
        private IApplicationRepository _appRepository;
        private IAppAuditTrailRepository _auditRepository;

        public AppAuditTrailController(ISessionAuthentication sessionAuthentication, IUserRepository userRepository,
            IApplicationRepository appRepository, IAppAuditTrailRepository auditRepository) : base(sessionAuthentication)
        {
            this._userRepository = userRepository;
            this._appRepository = appRepository;
            this._auditRepository = auditRepository;

            Settings.ModuleName = "AppAuditTrail";
            Settings.Title = "Application Audit Trail";
        }
        protected override void Startup()
        {

        }

        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            CultureInfo culture = CultureInfo.CreateSpecificCulture("id-ID");
            try
            {
                AuditTrailFilter auditFilter = new AuditTrailFilter();
                auditFilter.FunctionName = Request.QueryString.GetValues("FunctionName")[0].IsNullOrEmpty() ? "" : Request.QueryString.GetValues("FunctionName")[0];
                auditFilter.ObjectName = Request.QueryString.GetValues("ObjectName")[0].IsNullOrEmpty() ? "" : Request.QueryString.GetValues("ObjectName")[0];
                auditFilter.ActionName = Request.QueryString.GetValues("ActionName")[0].IsNullOrEmpty() ? "" : Request.QueryString.GetValues("ActionName")[0];
                auditFilter.ActionDateEnd = !Request.QueryString["ActionDateEnd"].IsNullOrEmpty() ? DateTime.ParseExact(Request.QueryString.GetValues("ActionDateEnd")[0], "d/M/yyyy", culture) : (DateTime?)null;
                auditFilter.ActionDateStart = !Request.QueryString["ActionDateStart"].IsNullOrEmpty() ? DateTime.ParseExact(Request.QueryString.GetValues("ActionDateStart")[0], "d/M/yyyy", culture) : (DateTime?)null;

                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.   
                PagedResult<AppAuditTrail> data;
                data = _auditRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, auditFilter);
                var items = data.Items.ToList().ConvertAll<AppAuditTrailView>(new Converter<AppAuditTrail, AppAuditTrailView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public AppAuditTrailView ConvertFrom(AppAuditTrail item)
        {
            //var ar = _auditRepository
            AppAuditTrailView returnItem = new AppAuditTrailView();
            returnItem.Id = item.Id;
            returnItem.FunctionName = item.FunctionName;
            returnItem.ObjectName = item.ObjectName;
            returnItem.ObjectValueBefore = item.ObjectValueBefore;
            returnItem.ObjectValueAfter = item.ObjectValueAfter;
            returnItem.Action = item.Action;
            returnItem.TimeStampString = Convert.ToDateTime(item.TimeStamp).ToString("dd/MM/yyyy HH:mm:ss");
            returnItem.UserMaker = item.UserMaker;
            returnItem.UserChecker = item.UserChecker;

            return returnItem;
        }

        [HttpPost]
        public ActionResult Export(AuditTrailView data)
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture("id-ID");
            AuditTrailFilter auditFilter = new AuditTrailFilter();
            auditFilter.FunctionName = data.FunctionName.IsNullOrEmpty() ? "" : data.FunctionName.Trim();// Request.QueryString.GetValues("FunctionName")[0].IsNullOrEmpty() ? "" : Request.QueryString.GetValues("FunctionName")[0];
            auditFilter.ObjectName = data.ObjectName.IsNullOrEmpty() ? "" : data.ObjectName.Trim(); //Request.QueryString.GetValues("ObjectName")[0].IsNullOrEmpty() ? "" : Request.QueryString.GetValues("ObjectName")[0];
            auditFilter.ActionName = data.Action.IsNullOrEmpty() ? "" : data.Action.Trim(); //Request.QueryString.GetValues("ActionName")[0].IsNullOrEmpty() ? "" : Request.QueryString.GetValues("ActionName")[0];
            auditFilter.ActionDateEnd = !data.TimeStampEnd.IsNullOrEmpty() ? DateTime.ParseExact(data.TimeStampEnd, "d/M/yyyy", culture) : (DateTime?)null;
            auditFilter.ActionDateStart = !data.TimeStampStart.IsNullOrEmpty() ? DateTime.ParseExact(data.TimeStampStart, "d/M/yyyy", culture) : (DateTime?)null;

            var auditList = _auditRepository.FindForExport(auditFilter);

            IList<AppAuditTrailView> items = auditList.ToList().ConvertAll<AppAuditTrailView>(new Converter<AppAuditTrail, AppAuditTrailView>(ConvertFrom));

            DataTable dt = items.ListToDataTable();
            List<AuditTrailView> list = new List<AuditTrailView>();

            MemoryStream ms = new MemoryStream();
            using (SLDocument sl = new SLDocument())
            {
                sl.ImportDataTable(1, 1, dt, true);
                sl.SelectWorksheet("Worksheet");
                sl.SaveAs(ms);
            }

            ms.Position = 0;
            string fName = "SVS_Application_Audit_Trail.xls";
            return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fName);
        }

    }
}