﻿using System;
using System.Linq;
using System.DirectoryServices.AccountManagement;
using LinqSpecs;
using System.Collections.Generic;
using System.Transactions;
using Treemas.Base.Utilities;
using Treemas.Base.Globals;
using Treemas.Credential.Interface;
using Treemas.Credential.Model;

namespace Treemas.Credential
{
    public class UserAccount : IUserAccount
    {
        private readonly IUserRepository repo_User;
        private readonly IApplicationRepository repo_App;
        private readonly IUserApplicationRepository repo_userApp;
        private readonly IFunctionRepository repo_Func;
        private readonly IFeatureRepository repo_Feat;
        private readonly IRoleRepository repo_Role;
        private readonly IAuthorizationRepository repo_Author;
        private readonly IMenuRepository repo_Menu;
        private readonly ILoginInfoRepository repo_loginInfo;
        private readonly IRoleFunctionFeatureRepository repo_roleFuncFeat;

        public UserAccount(IUserRepository Repo_User, IApplicationRepository Repo_App,
                            IUserApplicationRepository Repo_userApp, IFunctionRepository Repo_Func,
                            IFeatureRepository Repo_Feat, IRoleRepository Repo_Role,
                            IAuthorizationRepository Repo_Author, IMenuRepository Repo_Menu,
                            ILoginInfoRepository Repo_loginInfo, IRoleFunctionFeatureRepository Repo_roleFuncFeat)
        {
            this.repo_User = Repo_User;
            this.repo_App = Repo_App;
            this.repo_userApp = Repo_userApp;
            this.repo_Func = Repo_Func;
            this.repo_Feat = Repo_Feat;
            this.repo_Role = Repo_Role;
            this.repo_Author = Repo_Author;
            this.repo_Menu = Repo_Menu;
            this.repo_loginInfo = Repo_loginInfo;
            this.repo_roleFuncFeat = Repo_roleFuncFeat;
        }

        public User IsUserAuthentic(string username, string password, string domain)
        {
            string encPassword = Encryption.Instance.EncryptText(password);
            User _user = repo_User.GetOne(new AdHocSpecification<User>(s => s.Username == username));
            if (_user != null)
            {
                if (_user.InActiveDirectory && _user.IsSuperUser == false)
                {
                    if (!IsUserAuthenticActiveDirectory(username, password, domain))
                    {
                        return null;
                    }
                }
                else
                {
                    _user = null;
                    _user = repo_User.GetOne(new AdHocSpecification<User>(s => s.Username == username && s.Password == encPassword));
                }
                if (_user != null)
                {
                    this._FetchApplications(_user);
                    this._FetchRoles(_user);
                }
            }
            return _user;
        }
        public bool IsUserAuthenticActiveDirectory(string username, string password, string domain)
        {
            using (PrincipalContext pc = new PrincipalContext(ContextType.Domain, domain))
            {
                // validate the credentials
                
                return pc.ValidateCredentials(username, password);
            }
        }

        public bool IsUserExistInActiveDirectory(string userName, string domain)
        {
            using (var domainContext = new PrincipalContext(ContextType.Domain, domain))
            {
                using (var foundUser = UserPrincipal.FindByIdentity(domainContext, IdentityType.SamAccountName, userName))
                {
                    return foundUser != null;
                }
            }
        }

        public void UpdateLoginAttempt(string username, int loginAttempt, bool active, DateTime? lastlogin)
        {
            repo_User.UpdateLoginAttempt(username, loginAttempt, active, lastlogin);
        }
        public void SetUserToInactive(string username)
        {
            repo_User.SetUserToInactive(username);
        }
        public User GetSuperUser(string username)
        {
            return repo_User.FindOne(new AdHocSpecification<User>(s => s.Username == username));
        }
        public User CreateSuperUser(string username, string password, string SystemName)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    User newData = new User(0L);
                    newData.Username = username;
                    newData.Password = Encryption.Instance.EncryptText(password);
                    newData.PasswordExpirationDate = DateTime.Now.AddYears(99);
                    newData.AccountValidityDate = DateTime.Now.AddYears(99);
                    newData.FullName = username;
                    //newData.LastName = "";
                    newData.RegNo = "0000";
                    newData.SessionTimeout = 60;
                    newData.LockTimeout = 10;
                    newData.MaximumConcurrentLogin = 1;
                    newData.IsActive = true;
                    newData.InActiveDirectory = false;
                    newData.IsPrinted = true;
                    newData.UpdateMenu = true;
                    newData.Approved = true;

                    newData.ChangedBy = "System";
                    newData.ChangedDate = DateTime.Now;
                    newData.CreatedBy = "System";
                    newData.CreatedDate = DateTime.Now;
                    repo_User.Add(newData);

                    UserApplication uapp = new UserApplication(0L);
                    uapp.Application = SystemName;
                    uapp.Username = username;
                    uapp.IsDefault = true;
                    uapp.CreatedBy = "System";
                    uapp.CreatedDate = DateTime.Now;
                    uapp.ChangedBy = "System";
                    uapp.ChangedDate = DateTime.Now;
                    repo_userApp.Add(uapp);

                    Role role = repo_Role.getRole(1);
                    IList<RoleFunctionFeature> roleFuncFeat = repo_roleFuncFeat.getRoleFuncFeaturebyRole(role.Id);

                    foreach (var item in roleFuncFeat)
                    {
                        AuthorizationFeature feat = repo_Feat.getFeature(item.FeatureId);
                        AuthorizationFunction func = repo_Func.getFunction(item.FunctionId);
                        Authorization auth = new Authorization(0L);
                        auth.Username = uapp.Username;
                        auth.Application = uapp.Application;
                        auth.Role = role.RoleId;
                        auth.Function = func.FunctionId;
                        auth.Feature = feat.Name;
                        auth.QualifierKey = "";
                        auth.QualifierValue = "";
                        auth.ChangedBy = "System";
                        auth.ChangedDate = DateTime.Now;
                        auth.CreatedBy = "System";
                        auth.CreatedDate = DateTime.Now;
                        repo_Author.Add(auth);
                    }
                    scope.Complete();
                    return newData;
                }
            }
            catch (TransactionAbortedException ex)
            {
                return null;
            }
            catch (ApplicationException ex)
            {
                return null;
            }
        }
        public User GetUserAttributes(string username)
        {
            User _user = repo_User.FindOne(new AdHocSpecification<User>(s => s.Username == username));
            if (_user != null)
            {
                this._FetchApplications(_user);
                this._FetchRoles(_user);
            }
            return _user;
        }
        public void SaveLoginInfo(User user, string hostname, string hostIP, string browser, string browserVersion, bool isMobile, string sessionid)
        {
            DateTime now = DateTime.Now;
            SignonLoginInfo info = new SignonLoginInfo(0L);
            info.Username = user.Username;
            info.LoginTime = now;
            info.LastActive = now;
            info.SessionTimeout = user.SessionTimeout;
            info.LockTimeout = user.LockTimeout;
            info.MaximumLogin = user.MaximumConcurrentLogin;
            info.Hostname = hostname;
            info.HostIP = hostIP;
            info.Browser = browser;
            info.BrowserVersion = browserVersion;
            info.IsMobile = isMobile;
            info.SessionId = sessionid;

            repo_loginInfo.Add(info);
        }
        public void RemoveLoginInfo(SignonLoginInfo info)
        {
            repo_loginInfo.LogoutProcess(info);
        }
        public IList<SignonLoginInfo> GetLoginInfos(string username)
        {
            IEnumerable<SignonLoginInfo> _loginInfos = repo_loginInfo.GetLoginInfos(username);
            return _loginInfos.ToList();
        }
        public void setUpdateMenu(long id, bool update)
        {
            repo_User.setUpdateMenu(id, update);
        }
        public void CreateSuperUser(string password)
        {

        }
        public void _FetchMenus(User user)
        {
            // Get all associated roles for each application
            user.CurrentApplication.Menus = repo_Menu.getApplicationMenu(user.CurrentApplication.Id);
        }
        private void _FetchApplications(User user)
        {
            if (!string.IsNullOrEmpty(user.Username))
            {
                IList<UserApplication> list = repo_userApp.getUserApplication(user.Username);
                if (!list.IsNullOrEmpty<UserApplication>())
                {
                    Predicate<ApplicationInfo> criteria = null;
                    IList<string> list2 = new List<string>();
                    string defaultAppId = null;
                    foreach (UserApplication model in list)
                    {
                        list2.Add(model.Application);
                        if (model.IsDefault)
                        {
                            defaultAppId = model.Application;
                        }
                    }

                    IList<Application> list3 = repo_App.getApplications(list2);
                    if (!list3.IsNullOrEmpty<Application>())
                    {
                        IDictionary<string, ApplicationInfo> dictionary = new Dictionary<string, ApplicationInfo>();
                        foreach (Application model2 in list3)
                        {
                            ApplicationInfo info = new ApplicationInfo();
                            info.Id = model2.ApplicationId;
                            info.Description = model2.Description;
                            info.Name = model2.Name;
                            info.Runtime = model2.Runtime;
                            info.Type = (ApplicationType)Enum.Parse(typeof(ApplicationType), model2.Type);
                            dictionary.Add(model2.ApplicationId, info);
                            if (!list2.Contains(model2.ApplicationId))
                            {
                                list2.Add(model2.ApplicationId);
                            }
                        }

                        user.Applications = dictionary.Values.ToList<ApplicationInfo>();
                        if (!string.IsNullOrEmpty(defaultAppId))
                        {
                            if (criteria == null)
                            {
                                criteria = a => a.Id.Equals(defaultAppId);
                            }
                            user.CurrentApplication = user.Applications.FindElement<ApplicationInfo>(criteria);
                        }
                    }
                }
            }
        }

        private void _FetchRoles(User user)
        {
            new List<Role>();
            if (!user.Applications.IsNullOrEmpty<ApplicationInfo>() && !string.IsNullOrEmpty(user.Username))
            {
                Action<Role> action = null;
                IDictionary<string, ApplicationInfo> appMap = new Dictionary<string, ApplicationInfo>();

                // Get all applications
                IList<Application> list = repo_App.FindAll().ToList();
                IList<string> list2 = new List<string>();
                foreach (Application model in list)
                {
                    ApplicationInfo info = new ApplicationInfo();
                    info.Id = model.ApplicationId;
                    info.Name = model.Name;
                    info.Description = model.Description;
                    info.Runtime = model.Runtime;
                    info.Type = (ApplicationType)Enum.Parse(typeof(ApplicationType), model.Type);
                    appMap.Add(model.ApplicationId, info);
                    list2.Add(model.ApplicationId);
                }

                // Get all functions in registered applications
                IList<AuthorizationFunction> list4 = repo_Func.getFunctions(list2);
                IDictionary<string, IList<AuthorizationFunction>> dictionary = new Dictionary<string, IList<AuthorizationFunction>>();
                foreach (AuthorizationFunction model3 in list4)
                {
                    if (!dictionary.ContainsKey(model3.Application))
                    {
                        dictionary.Add(model3.Application, new List<AuthorizationFunction>());
                    }
                    dictionary[model3.Application].Add(model3);
                }

                // Get all features in registered applications
                IList<AuthorizationFeature> list5 = repo_Feat.getFeatures(list2);
                IDictionary<string, IList<AuthorizationFeature>> dictionary2 = new Dictionary<string, IList<AuthorizationFeature>>();
                foreach (AuthorizationFeature model4 in list5)
                {
                    if (!dictionary2.ContainsKey(model4.Application))
                    {
                        dictionary2.Add(model4.Application, new List<AuthorizationFeature>());
                    }
                    dictionary2[model4.Application].Add(model4);
                }

                // Get all associated roles for each application
                IDictionary<string, IList<Role>> dictionary3 = new Dictionary<string, IList<Role>>();
                foreach (ApplicationInfo info2 in user.Applications)
                {
                    IList<Role> list6 = repo_Role.getApplicationRole(info2.Id);

                    if (!list6.IsNullOrEmpty<Role>())
                    {
                        if (action == null)
                        {
                            action = delegate (Role m) {
                                if (appMap.ContainsKey(m._Application))
                                {
                                    //IList<Menu> listmenu = repo_Menu.getApplicationMenu(m._Application);
                                    m.Application = appMap[m._Application];
                                    //m.Menus = listmenu;
                                }
                            };
                        }
                        list6.IterateByAction<Role>(action);
                        dictionary3.Add(info2.Id, list6);
                    }
                }


                // Collect all the authorized functions and features
                IList<Authorization> list7 = repo_Author.getUserAuthorization(user.Username);

                if (!list7.IsNullOrEmpty<Authorization>())
                {
                    using (IEnumerator<Authorization> enumerator6 = list7.GetEnumerator())
                    {
                        Predicate<Role> criteria = null;
                        Predicate<Role> predicate2 = null;
                        Predicate<AuthorizationFunction> predicate3 = null;
                        Predicate<Role> predicate4 = null;
                        Predicate<AuthorizationFunction> predicate5 = null;
                        Predicate<AuthorizationFeature> predicate6 = null;
                        Predicate<Role> predicate7 = null;
                        Predicate<AuthorizationFunction> predicate8 = null;
                        Predicate<AuthorizationFeature> predicate9 = null;
                        Authorization m;
                        while (enumerator6.MoveNext())
                        {
                            Role role;
                            AuthorizationFunction function;
                            AuthorizationFeature feature;
                            m = enumerator6.Current;
                            if (!ObjectExtensions.IsNullOrEmpty(m.Role) && dictionary3.ContainsKey(m.Application))
                            {
                                if (criteria == null)
                                {
                                    criteria = r => r.RoleId.Equals(m.Role);
                                }
                                role = user.Roles.FindElement<Role>(criteria);
                                if (role.IsNull())
                                {
                                    role = dictionary3[m.Application].FindElement<Role>(criteria);
                                    user.Roles.Add(role);
                                }
                            }
                            if (!ObjectExtensions.IsNullOrEmpty(m.Role) && !ObjectExtensions.IsNullOrEmpty(m.Function))
                            {
                                if (predicate2 == null)
                                {
                                    predicate2 = r => r.RoleId.Equals(m.Role);
                                }
                                role = user.Roles.FindElement<Role>(predicate2);
                                if (!role.IsNull() && dictionary.ContainsKey(role.Application.Id))
                                {
                                    if (predicate3 == null)
                                    {
                                        predicate3 = f => f.FunctionId.Equals(m.Function);
                                    }
                                    function = dictionary[role.Application.Id].FindElement<AuthorizationFunction>(predicate3);
                                    if (!function.IsNull())
                                    {
                                        role.Functions.Add(function);
                                    }
                                }
                            }
                            if ((!ObjectExtensions.IsNullOrEmpty(m.Role) && !ObjectExtensions.IsNullOrEmpty(m.Function)) && !ObjectExtensions.IsNullOrEmpty(m.Feature))
                            {
                                if (predicate4 == null)
                                {
                                    predicate4 = r => r.RoleId.Equals(m.Role);
                                }
                                role = user.Roles.FindElement<Role>(predicate4);
                                if ((!role.IsNull() && !role.Functions.IsNullOrEmpty<AuthorizationFunction>()) && dictionary2.ContainsKey(role.Application.Id))
                                {
                                    if (predicate5 == null)
                                    {
                                        predicate5 = f => f.FunctionId.Equals(m.Function);
                                    }
                                    function = role.Functions.FindElement<AuthorizationFunction>(predicate5);
                                    if (predicate6 == null)
                                    {
                                        predicate6 = ft => ft.FeatureId.Equals(m.Feature);
                                    }
                                    feature = dictionary2[role.Application.Id].FindElement<AuthorizationFeature>(predicate6);
                                    if (!feature.IsNull())
                                    {
                                        function.Features.Add(feature);
                                    }
                                }
                            }
                            if ((!ObjectExtensions.IsNullOrEmpty(m.Role) && !ObjectExtensions.IsNullOrEmpty(m.Function)) && (!ObjectExtensions.IsNullOrEmpty(m.Feature) && !ObjectExtensions.IsNullOrEmpty(m.QualifierKey)))
                            {
                                if (predicate7 == null)
                                {
                                    predicate7 = r => r.RoleId.Equals(m.Role);
                                }
                                role = user.Roles.FindElement<Role>(predicate7);
                                if ((!role.IsNull() && !role.Functions.IsNullOrEmpty<AuthorizationFunction>()) && dictionary2.ContainsKey(role.Application.Id))
                                {
                                    if (predicate8 == null)
                                    {
                                        predicate8 = f => f.FunctionId.Equals(m.Function);
                                    }
                                    if (predicate9 == null)
                                    {
                                        predicate9 = ft => ft.FeatureId.Equals(m.Feature);
                                    }
                                    feature = role.Functions.FindElement<AuthorizationFunction>(predicate8).Features.FindElement<AuthorizationFeature>(predicate9);
                                    if (!feature.IsNull())
                                    {
                                        AuthorizationFeatureQualifier item = new AuthorizationFeatureQualifier
                                        {
                                            Key = m.QualifierKey,
                                            Qualifier = m.QualifierValue
                                        };
                                        feature.Qualifiers.Add(item);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
