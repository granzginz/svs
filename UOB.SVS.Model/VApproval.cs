﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;
using Treemas.Base.Globals;
using Treemas.Credential.Model;

namespace UOB.SVS.Model
{
    public class VApproval : EntityString
    {
        protected VApproval() { }
        public VApproval(string id) : base(id)
        {
            this.Id = id;
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }
        public virtual string ApprovalFuntion { get; set; }
        public virtual string RequestType { get; set; }
        public virtual string ApprovalStatus { get; set; }
        public virtual string MakerUser { get; set; }
        public virtual string MakerDate { get; set; }
        public virtual ApprovalType ApprovalTypeEnum
        {
            get
            {
                ApprovalType currentState = (ApprovalType)Enum.Parse(typeof(ApprovalType), this.RequestType);
                return currentState;
            }
        }
    }
}
