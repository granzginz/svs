﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;
using Treemas.Base.Globals;
using Treemas.Credential.Model;

namespace UOB.SVS.Model
{
    public class SignatureSCMaster : Entity
    {
        protected SignatureSCMaster() { }
        public SignatureSCMaster(long id) : base(id)
        {
            this.ID = id;
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.ID };
        }

        public virtual long ID { get; set; }
        public virtual string AccountNo { get; set; }
        public virtual string CustomerName { get; set; }
        public virtual string Note { get; set; }
        public virtual string Signature { get; set; }
        public virtual string KTP { get; set; }
        public virtual string NoteKTP { get; set; }
        public virtual string Pengenal { get; set; }
        public virtual string AddBy { get; set; }
        public virtual DateTime? DateAdd { get; set; }
        public virtual string UpdBy { get; set; }
        public virtual DateTime? DateUpd { get; set; }

        public virtual string DelBy { get; set; }
        public virtual DateTime? DateDel { get; set; }
        public virtual string AppAddBy { get; set; }
        public virtual DateTime? DateAppAdd { get; set; }
        public virtual string AppUpBy { get; set; }
        public virtual DateTime? DateAppUpd { get; set; }
        public virtual string AppDelBy { get; set; }
        public virtual DateTime? DateAppDel { get; set; }
        public virtual string NotAppAddBy { get; set; }
        public virtual DateTime? DateNotAppAdd { get; set; }
        public virtual string NotAppUpdBy { get; set; }
        public virtual DateTime? DateNotAppUpd { get; set; }

        public virtual string NotAppDelBy { get; set; }
        public virtual DateTime? DateNotAppDell { get; set; }
        public virtual string StatusFlag { get; set; }
        public virtual string StatusAktif { get; set; }
        public virtual string OldNote { get; set; }
        public virtual string OldNoteKTP { get; set; }
        public virtual string FSignature { get; set; }
        public virtual string FKTP { get; set; }
        public virtual string FNote { get; set; }
        public virtual string ZSignature { get; set; }
        public virtual string ZOldSignature { get; set; }
        public virtual string ZKTP { get; set; }

        public virtual string ZOldKTP { get; set; }
        public virtual string OldSignature { get; set; }
        public virtual string OldKTP { get; set; }
        public virtual string OldAccNumber { get; set; }


        public virtual string ApprovedBy { get; set; }
        public virtual string ApprovalType { get; set; }
        public virtual ApprovalType ApprovalTypeEnum
        {
            get
            {
                ApprovalType currentState = (ApprovalType)Enum.Parse(typeof(ApprovalType), this.ApprovalType);
                return currentState;
            }
        }

        public virtual DateTime? ApprovedDate { get; set; }
        public virtual string ApprovalStatus { get; set; }
    }
}
