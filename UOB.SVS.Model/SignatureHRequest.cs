﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;
using Treemas.Base.Globals;
using Treemas.Credential.Model;

namespace UOB.SVS.Model
{
    public class SignatureHRequest : EntityString
    {
        protected SignatureHRequest() { }
        public SignatureHRequest(string id) : base(id)
        {
            this.AccountNo = id;
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.AccountNo };
        }

        public virtual string AccountNo { get; set; }
        public virtual string AccountName { get; set; }
        public virtual string AccountType { get; set; }
        public virtual string CIFNumber { get; set; }
        public virtual string BranchCode { get; set; }
        public virtual string Note { get; set; }
        public virtual string RequestType { get; set; }
        public virtual DateTime? RequestDate { get; set; }
        public virtual string RequestUser { get; set; }
        public virtual string RequestReason { get; set; }
        public virtual bool IsRejected { get; set; }
        public virtual IList<SignatureDDocReq> Documents { get; set; }
        public virtual IList<SignatureDSignReq> Signatures { get; set; }
        public virtual IList<SignatureDKTPReq> IdentityCards { get; set; }
        
    }
}
