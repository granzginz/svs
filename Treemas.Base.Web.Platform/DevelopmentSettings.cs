﻿namespace Treemas.Base.Web.Platform
{
    public class DevelopmentSettings
    {
        public static readonly DevelopmentStage STAGE_DEVELOPMENT;
        public static readonly DevelopmentStage STAGE_PRODUCTION;
        public static readonly DevelopmentStage STAGE_QUALITY_ASSURANCE;

        static DevelopmentSettings()
        {
            DevelopmentStage stage = new DevelopmentStage
            {
                Code = "DEV",
                Description = "Development"
            };
            STAGE_DEVELOPMENT = stage;
            DevelopmentStage stage2 = new DevelopmentStage
            {
                Code = "QA",
                Description = "Quality Assurance"
            };
            STAGE_QUALITY_ASSURANCE = stage2;
            DevelopmentStage stage3 = new DevelopmentStage
            {
                Code = "PROD",
                Description = "Production"
            };
            STAGE_PRODUCTION = stage3;
        }

        public DevelopmentStage Stage { get; set; }
    }
}
