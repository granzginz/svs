﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Treemas.Base.Web.Platform
{
    public class SystemConfigurationConstants
    {
        public string DeploymentContext =>
            "Deployment-Context";

        public string DevelopmentStage =>
            "Development-Stage";

        public string HomeFolder =>
            "Home-Folder";

        public string LoginAttempt =>
            "Login-Attempt";

        public string UOBIEncryptionHalfKey =>
            "UOBIEncryptionHalfKey";

        public string UOBIKey =>
            "UOBIKey";

        public string UOBIAppName =>
            "UOBIAppName";

        public string Name =>
            "System";

        public string Portal =>
            "Portal";
    }
}
