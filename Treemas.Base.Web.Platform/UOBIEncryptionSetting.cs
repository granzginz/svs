﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Treemas.Base.Web.Platform
{
    public class UOBIEncryptionSetting
    {
        public UOBIEncryptionSetting() { }
        public string EncryptionHalfKey { get; set; }
        public string UOBIKey { get; set; }
        public string EncryptionAppName { get; set; }

    }
}