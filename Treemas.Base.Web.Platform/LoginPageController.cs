﻿using System;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Collections.Generic;
using Treemas.Credential.Model;
using Treemas.Credential.Interface;
using Treemas.Base.Utilities;
using Treemas.Base.Resources;


namespace Treemas.Base.Web.Platform
{
    public class LoginPageController : PageController
    {
        private IUserAccount _userAccount;
        private ISSOSessionStorage _ssoSessionStorage;
        private IParameterRepository _InquiryParameter;

        public LoginPageController(IUserAccount userAccount, ISessionAuthentication sessionAuthentication, ISSOSessionStorage ssoSessionStorage,
            IParameterRepository InquiryParameter) : base(sessionAuthentication)
        {
            this._userAccount = userAccount;
            this._ssoSessionStorage = ssoSessionStorage;
            this._InquiryParameter = InquiryParameter;
        }
        private void _SaveAuthenticatedUser(User user)
        {
            _userAccount.SaveLoginInfo(user, base.Request.UserHostName, base.Request.UserHostAddress, base.Request.Browser.Browser, base.Request.Browser.Version, base.Request.Browser.IsMobileDevice, base.Session.SessionID);
            user.LoginInfos = _userAccount.GetLoginInfos(user.Username);
            base.Lookup.Add(user);
            base.Session.Timeout = user.SessionTimeout;

            var param = _InquiryParameter.getParameter();
            base.Lookup.Add(param);
        }

        private void _RemoveAuthenticatedUser(User user)
        {
            base.Lookup.Remove(user);
        }

        [HttpPost]
        public MvcHtmlString AjaxLogin(string username, string password)
        {
            base.Lookup.Clear();
            

            if (SystemSettings.Instance.Security.SimulateAuthenticatedSession && !SystemSettings.Instance.Security.SimulatedAuthenticatedUser.IsNull())
            {
                User simulatedAuthenticatedUser = SystemSettings.Instance.Security.SimulatedAuthenticatedUser;
                string str = simulatedAuthenticatedUser.Username;
                string str2 = simulatedAuthenticatedUser.Password;
                if ((!string.IsNullOrEmpty(str) && !string.IsNullOrEmpty(str2)) && (str.Equals(username) && str2.Equals(password)))
                {
                    this._SaveAuthenticatedUser(simulatedAuthenticatedUser);
                    return new MvcHtmlString("true");
                }
            }
            var ss = DependencyResolver.Current;
            
            if (_userAccount != null)
            {
                User user = _userAccount.IsUserAuthentic(username, password, SystemSettings.Instance.Security.DomainServer);
                if (user != null)
                {
                    //override some user settings
                    user.SessionTimeout = SystemSettings.Instance.Security.SessionTimeout;
                    user.MaximumConcurrentLogin = 1;

                    if (!SystemSettings.Instance.Runtime.IsPortal)
                    {
                        user.CurrentApplication = user.Applications.FindElement<ApplicationInfo>(r => r.Id.Equals(SystemSettings.Instance.Name));
                    }
                    this._SaveAuthenticatedUser(user);
                    if (SystemSettings.Instance.Security.EnableSingleSignOn)
                    {
                        string str3 = SSOClient.Instance.IsLoggedIn(user.Username, base.Request.UserHostName, base.Request.UserHostAddress, base.Request.Browser.Browser, base.Request.Browser.Version, base.Request.Browser.IsMobileDevice);
                        if (!string.IsNullOrEmpty(str3))
                        {
                            SSOClient.Instance.Logout(str3);
                        }
                        str3 = SSOClient.Instance.Login(user.Username, password, base.Request.UserHostName, base.Request.UserHostAddress, base.Request.Browser.Browser, base.Request.Browser.Version, base.Request.Browser.IsMobileDevice);
                        if (string.IsNullOrEmpty(str3))
                        {
                            base.Lookup.Remove<User>();
                            return new MvcHtmlString("false");
                        }
                        string str4 = HttpServerUtility.UrlTokenEncode(Encoding.UTF8.GetBytes(str3));
                        HttpCookie cookie = base.Request.Cookies[ApplicationConstants.Instance.SECURITY_COOKIE_SESSIONID];
                        if (cookie.IsNull())
                        {
                            cookie = new HttpCookie(ApplicationConstants.Instance.SECURITY_COOKIE_SESSIONID, str4);
                        }
                        else
                        {
                            cookie.Value = str4;
                        }
                        cookie.Expires = DateTime.Now.AddMinutes((double)(user.SessionTimeout));
                        base.Response.Cookies.Add(cookie);
                        _ssoSessionStorage.Save(str3, base.Lookup);
                        SSOSessionLookupListener.RemoveExistingInstance(base.Lookup);
                        base.Lookup.AddEventListener(new SSOSessionLookupListener(str3, _ssoSessionStorage));
                    }
                    if (user.IsActive == false )
                    {
                        _RemoveAuthenticatedUser(user);
                        return new MvcHtmlString("InactiveUser");
                    }
                     
                    if (user.IsAccountNotUsed() || user.IsValidityExpired())
                    {
                        _RemoveAuthenticatedUser(user);
                        _userAccount.SetUserToInactive(user.Username);
                        return new MvcHtmlString("InactiveUser");
                    }

                    if (user.Roles.Count<=0)
                    {
                        return new MvcHtmlString("RoleNotFound");
                    }
                    CreateMenu(user);
                    _userAccount.UpdateLoginAttempt(user.Username, 0, true, user.LastLogin);
                    return new MvcHtmlString("true");
                }
                else
                {
                    user = _userAccount.GetUserAttributes(username);
                    if (user != null)
                    {
                        user.LoginAttempt += 1;
                        _userAccount.UpdateLoginAttempt(user.Username, user.LoginAttempt, !(user.LoginAttempt >= SystemSettings.Instance.Security.LoginAttempt), null);
                        if (user.LoginAttempt >= SystemSettings.Instance.Security.LoginAttempt)
                        {
                            return new MvcHtmlString("LoginAttemptExceeded");
                        }
                        
                    }
                }
            }
            return new MvcHtmlString("InvalidUserOrPassword");
        }

        public MvcHtmlString AjaxLogout()
        {
            if (!SystemSettings.Instance.Security.EnableAuthentication || SystemSettings.Instance.Security.SimulateAuthenticatedSession)
            {
                return new MvcHtmlString("false");
            }
            User user = base.Lookup.Get<User>();
            bool flag = !user.IsNull();
            if (user != null)
            {
                removeLoginInfo(user);
                base.Lookup.Remove<User>();
                if (SystemSettings.Instance.Security.EnableSingleSignOn)
                {
                    string id = null;
                    if (base.Request.Cookies.AllKeys.Contains<string>(ApplicationConstants.Instance.SECURITY_COOKIE_SESSIONID))
                    {
                        HttpCookie cookie = base.Request.Cookies[ApplicationConstants.Instance.SECURITY_COOKIE_SESSIONID];
                        id = Encoding.UTF8.GetString(HttpServerUtility.UrlTokenDecode(cookie.Value));
                        cookie.Value = null;
                        cookie.Expires = DateTime.Now.AddDays(-1.0);
                        base.Response.Cookies.Add(cookie);
                    }
                    if (SSOClient.Instance.Logout(id))
                    {
                        _ssoSessionStorage.Delete(id);
                    }
                    else
                    {
                        flag = false;
                    }
                }
            }
            return new MvcHtmlString(flag ? "true" : "false");
        }

        private void removeLoginInfo(User user)
        {
            var hostname = base.Request.UserHostName;
            var hostaddress = base.Request.UserHostAddress;
            var browser = base.Request.Browser.Browser;
            var browserVersion = base.Request.Browser.Version;
            var sessionid = base.Session.SessionID;

            var loginInfo = user.LoginInfos.FindElement(x => x.Username == user.Username &&
                                            x.Browser == browser &&
                                            x.HostIP == hostaddress &&
                                            x.Hostname == hostname &&
                                            x.BrowserVersion == browserVersion &&
                                            x.SessionId == sessionid);
            if (!loginInfo.IsNull())
            {
                _userAccount.RemoveLoginInfo(loginInfo);
            }
        }

        [HttpPost]
        public ActionResult Login(string username, string password)
        {
            string loginReturn = this.AjaxLogin(username, password).ToHtmlString();
            if (loginReturn.Equals("true"))
            {
                User user = base.Lookup.Get<User>();
                if(user.IsMaximumConcurrentLoginReached)
                {
                    KickOtherSession(user);
                    //return this.Redirect(base.Descriptor.BaseUrl + "/" + SystemSettings.Instance.Runtime.SwitchSessionController);
                }
                if (user.IsPasswordExpired())
                {
                    ScreenMessages.Submit(ScreenMessage.Info(BaseResources.PasswordExpired));
                    return this.Redirect(base.Descriptor.BaseUrl + "/" + SystemSettings.Instance.Runtime.ChangePasswordController);
                }
                if (user.IsPasswordNeedToBeReset)
                {
                    ScreenMessages.Submit(ScreenMessage.Info(BaseResources.PasswordFirstLogin));
                    return this.Redirect(base.Descriptor.BaseUrl + "/" + SystemSettings.Instance.Runtime.ChangePasswordController);
                }
                if (SystemSettings.Instance.Runtime.IsPortal)
                {
                    return this.Redirect(base.Descriptor.BaseUrl + "/" + SystemSettings.Instance.Runtime.SwitchController);
                }
                else
                {
                    return this.Redirect(base.Descriptor.BaseUrl + "/" + SystemSettings.Instance.Runtime.HomeController);
                }
                
            }

            ScreenMessages.Submit(ScreenMessage.Error(BaseResources.ResourceManager.GetString(loginReturn)));
            ViewData["_tmsScreenMessages"] = ScreenMessages.Pull();
            return View();
            //return this.Redirect(base.Descriptor.BaseUrl + "/" + SystemSettings.Instance.Security.LoginController);
        }

        private void KickOtherSession(User user)
        {
            if (!user.IsNull())
            {
                var hostname = base.Request.UserHostName;
                var hostaddress = base.Request.UserHostAddress;
                var browser = base.Request.Browser.Browser;
                var browserVersion = base.Request.Browser.Version;
                var sessionid = base.Session.SessionID;

                foreach (SignonLoginInfo info in user.LoginInfos)
                {
                    if (!(info.Username == user.Username &&
                            info.Browser == browser &&
                            info.HostIP == hostaddress &&
                            info.Hostname == hostname &&
                            info.SessionId == sessionid &&
                            info.BrowserVersion == browserVersion))
                    {
                        _userAccount.RemoveLoginInfo(info);
                    }
                }
            }
        }

        public ActionResult Logout()
        {
            if (!SystemSettings.Instance.Security.EnableAuthentication || SystemSettings.Instance.Security.SimulateAuthenticatedSession)
            {
                return null;
            }
            if (this.AjaxLogout().ToHtmlString().Equals("false"))
            {
                return this.Redirect(base.Descriptor.BaseUrl + "/" + SystemSettings.Instance.Runtime.HomeController);
            }
            return this.Redirect(base.Descriptor.BaseUrl + "/" + SystemSettings.Instance.Security.LoginController);
        }

        protected override void Startup()
        {
            base.Settings.Title = BaseResources.LoginToYourAccount;
        }


        private void CreateMenu(User user)
        {
            IList<string> lines = new List<string>();
            //FileInfo filemenu = new FileInfo(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, user.ApplicationMenuPath()));

            if (user.UpdateMenu)
            {
                //Role role = user.Roles.FindElement<Role>(x => x._Application.Equals(user.CurrentApplication.Id));
                IList<Role> roles = user.Roles.FindElements<Role>(x => x._Application.Equals(user.CurrentApplication.Id));
                Dictionary<string, string> combinedAuthorizedFunctions = CombinedAuthorizedFunctions(roles);

                _userAccount._FetchMenus(user);
                IList<Menu> menus = user.CurrentApplication.Menus;

                if (menus.Count() > 0)
                {
                    lines = LinesofMenu(menus, combinedAuthorizedFunctions, lines);
                }
                System.IO.File.WriteAllLines(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, user.ApplicationMenuPath()), lines);
                _userAccount.setUpdateMenu(user.Id, false);
            }
        }
        
        private Dictionary<string, string> CombinedAuthorizedFunctions(IList<Role> roles)
        {
            Dictionary<string, string> authorizedFunctions = new Dictionary<string, string>();
            foreach (Role role in roles)
            {
                foreach (AuthorizationFunction func in role.Functions)
                {
                    if (!authorizedFunctions.ContainsKey(func.FunctionId))
                        authorizedFunctions.Add(func.FunctionId, func.Name);
                }
            }
            return authorizedFunctions;
        }

        private IList<string> LinesofMenu(IList<Menu> menus, Dictionary<string, string> functions, IList<string> lines)
        {
            foreach(Menu menu in menus)
                {
                if (!menu.FunctionId.IsNullOrEmpty())
                {
                    if (functions.ContainsKey(menu.FunctionId) || menu.FunctionId.Equals("Home"))
                    {
                        if (menu.MenuIcon.IsNullOrEmpty())
                            lines.Add(string.Format("<li><a href=\"@Html.Treemas().Page.GetControllerUrl(\"{0}\")\"><span>{1}</span></a></li>", menu.FunctionId, menu.MenuName));
                        else
                            lines.Add(string.Format("<li><a href=\"@Html.Treemas().Page.GetControllerUrl(\"{0}\")\"><i class=\"{1}\"></i> <span>{2}</span></a></li>", menu.FunctionId, menu.MenuIcon, menu.MenuName));
                    }
                }
                else
                {
                    if (!menu.ChildMenu.IsNullOrEmpty())
                    {
                        var before = lines.Count;
                        lines = LinesofMenu(menu.ChildMenu, functions, lines);
                        var after = lines.Count;
                        if (after > before)
                        {
                            if (menu.MenuIcon.IsNullOrEmpty())
                                lines.Insert(before, string.Format("<li><a href=\"#\" ><span>{0}</span></a><ul>", menu.MenuName));
                            else
                                lines.Insert(before, string.Format("<li><a href=\"#\" ><i class=\"{0}\"></i> <span>{1}</span></a><ul>", menu.MenuIcon, menu.MenuName));
                            lines.Add("</ul></li>");
                        }
                    }
                    else
                    {
                        if (menu.MenuIcon.IsNullOrEmpty())
                            lines.Add(string.Format("<li><a href=\"@Html.Treemas().Page.GetControllerUrl(\"{0}\")\"><span>{1}</span></a></li>", menu.FunctionId, menu.MenuName));
                        else
                            lines.Add(string.Format("<li><a href=\"@Html.Treemas().Page.GetControllerUrl(\"{0}\")\"><i class=\"{1}\"></i> <span>{2}</span></a></li>", menu.FunctionId, menu.MenuIcon, menu.MenuName));
                    }
                }
            }
            return lines;
        }
    }
}
