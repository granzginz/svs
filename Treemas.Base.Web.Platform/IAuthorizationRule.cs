﻿using System.Web.Routing;

namespace Treemas.Base.Web.Platform
{
    public interface IAuthorizationRule
    {
        AuthorizationRuleState Authorize(RequestContext requestContext);
    }
}
