﻿using System.Web.Mvc;

namespace Treemas.Base.Web.Platform
{
    public class ApplicationRazorExtension : BaseRazorExtension
    {
        public MvcHtmlString Alias =>
            new MvcHtmlString(SystemSettings.Instance.Alias);

        public MvcHtmlString Name =>
            new MvcHtmlString(SystemSettings.Instance.Name);

        public MvcHtmlString OwnerAlias =>
            new MvcHtmlString(SystemSettings.Instance.OwnerAlias);

        public MvcHtmlString OwnerName =>
            new MvcHtmlString(SystemSettings.Instance.OwnerName);
    }
}
