﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Treemas.Base.Configuration;
using Treemas.Base.Configuration.Binder;
using Treemas.Base.Utilities;


namespace Treemas.Base.Web.Platform
{
    public abstract class WebApplication : HttpApplication
    {
        public WebApplication()
        {
        }

        public WebApplication(string name)
        {
            SystemSettings.Instance.Name = name;
        }

        protected void Application_Start()
        {
            // initial default configuration
            this.InitConfiguration();
            this.Startup();
            RouteCollection routes = RouteTable.Routes;
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("{resource}.ashx/{*pathInfo}");
            routes.MapRoute("_default_route_", "{controller}/{action}/{id}", new
            {
                controller = SystemSettings.Instance.Runtime.HomeController,
                action = "Index",
                id = UrlParameter.Optional
            });
        }
        private void InitConfiguration()
        {
            SystemSettings instance = SystemSettings.Instance;
            ApplicationConfigurationCabinet cabinet = ApplicationConfigurationCabinet.Instance;
            string configuration = ApplicationConstants.Instance.Location.Configuration;
            IConfigurationBinder binder = new XmlFileConfigurationBinder(ApplicationConstants.Instance.Configuration.System.Name, configuration);
            cabinet.AddBinder(binder);
            binder.Load();
            ConfigurationItem item = binder.GetConfiguration(ApplicationConstants.Instance.Configuration.System.DeploymentContext);
            if (item == null)
            {
                item = new ConfigurationItem
                {
                    Key = ApplicationConstants.Instance.Configuration.System.DeploymentContext,
                    Value = "",
                    Description = "Relative path for deployment"
                };
                binder.AddConfiguration(item);
            }
            instance.Deployment.Context.Name = item.Value;
            item = binder.GetConfiguration(ApplicationConstants.Instance.Configuration.System.DevelopmentStage);
            if (item == null)
            {
                item = new ConfigurationItem
                {
                    Key = ApplicationConstants.Instance.Configuration.System.DevelopmentStage,
                    Value = DevelopmentSettings.STAGE_DEVELOPMENT.Code,
                    Description = "Development stage"
                };
                binder.AddConfiguration(item);
            }
            string str2 = item.Value.ToUpper();
            if (str2.Equals(DevelopmentSettings.STAGE_DEVELOPMENT.Code))
            {
                instance.Development.Stage = DevelopmentSettings.STAGE_DEVELOPMENT;
            }
            else if (str2.Equals(DevelopmentSettings.STAGE_PRODUCTION))
            {
                instance.Development.Stage = DevelopmentSettings.STAGE_PRODUCTION;
            }
            else if (str2.Equals(DevelopmentSettings.STAGE_QUALITY_ASSURANCE))
            {
                instance.Development.Stage = DevelopmentSettings.STAGE_QUALITY_ASSURANCE;
            }
            else
            {
                DevelopmentStage stage = new DevelopmentStage
                {
                    Code = str2
                };
                instance.Development.Stage = stage;
            }

            item = binder.GetConfiguration(ApplicationConstants.Instance.Configuration.System.HomeFolder);
            if (item == null)
            {
                item = new ConfigurationItem
                {
                    Key = ApplicationConstants.Instance.Configuration.System.HomeFolder,
                    Value = @"C:\Application_Home\" + SystemSettings.Instance.Alias.Replace(' ', '_'),
                    Description = "Home folder path"
                };
                binder.AddConfiguration(item);
            }
            instance.Deployment.HomeFolderLocation = item.Value;

            //item = binder.GetConfiguration(ApplicationConstants.Instance.Configuration.System.LoginAttempt);
            //if (item == null)
            //{
            //    item = new ConfigurationItem
            //    {
            //        Key = ApplicationConstants.Instance.Configuration.System.LoginAttempt,
            //        Value = "3",
            //        Description = "Login Attempt Limit"
            //    };
            //    binder.AddConfiguration(item);
            //}
            //instance.Security.LoginAttempt = Int32.Parse(item.Value);

            item = binder.GetConfiguration(ApplicationConstants.Instance.Configuration.System.Portal);
            if (item == null)
            {
                item = new ConfigurationItem
                {
                    Key = ApplicationConstants.Instance.Configuration.System.Portal,
                    Value = "url-portal-application",
                    Description = "Application Portal Host"
                };
                binder.AddConfiguration(item);
            }
            instance.Runtime.PortalUrl = item.Value;

            item = binder.GetConfiguration(ApplicationConstants.Instance.Configuration.System.UOBIAppName);
            if (item == null)
            {
                item = new ConfigurationItem
                {
                    Key = ApplicationConstants.Instance.Configuration.System.UOBIAppName,
                    Value = "UOBI_ENCRYPTION_APP_NAME",
                    Description = "AppName for Encrypted Master Key Created by UOBI Encryption Tool"
                };
                binder.AddConfiguration(item);
            }
            instance.UOBISetting.EncryptionAppName = item.Value;

            item = binder.GetConfiguration(ApplicationConstants.Instance.Configuration.System.UOBIEncryptionHalfKey);
            if (item == null)
            {
                item = new ConfigurationItem
                {
                    Key = ApplicationConstants.Instance.Configuration.System.UOBIEncryptionHalfKey,
                    Value = "UOBI_ENCRYPTED_VALUE",
                    Description = "Encrypted Value Encrypted Master Key Created by UOBI Encryption Tool"
                };
                binder.AddConfiguration(item);
            }
            instance.UOBISetting.EncryptionHalfKey = item.Value;

            item = binder.GetConfiguration(ApplicationConstants.Instance.Configuration.System.UOBIKey);
            if (item == null)
            {
                item = new ConfigurationItem
                {
                    Key = ApplicationConstants.Instance.Configuration.System.UOBIKey,
                    Value = "UOBI_ENCRYPTION_KEY",
                    Description = "Encryption Key for Encrypted Master Key Created by UOBI Encryption Tool"
                };
                binder.AddConfiguration(item);
            }
            instance.UOBISetting.UOBIKey = item.Value;

            binder.Save();
            //binder = new DifferentialXmlConfigurationBinder(ApplicationConstants.Instance.CONFIGURATION_SINGLE_SIGN_ON_BINDER, instance.Development.Stage.Code, configuration);
            //cabinet.AddBinder(binder);
            //binder.Load();
            //binder.Save();
            //binder = new AssemblyTextFileConfigurationBinder("Encryption", "Treemas.Base.Web.Platform.Configurations", base.GetType().Assembly);
            //cabinet.AddBinder(binder);
            //binder.Load();
            //binder.Save();
            //binder = ApplicationConfigurationCabinet.Instance.GetBinder(ApplicationConstants.Instance.CONFIGURATION_SINGLE_SIGN_ON_BINDER);
            //binder.Load();
            //item = binder.GetConfiguration(ApplicationConstants.Instance.CONFIGURATION_SINGLE_SIGN_ON_URL);
            //if (item == null)
            //{
            //    item = new ConfigurationItem
            //    {
            //        Key = ApplicationConstants.Instance.CONFIGURATION_SINGLE_SIGN_ON_URL,
            //        Value = "url-to-single-sign-on-service",
            //        Description = "Url of Single Sign On Service"
            //    };
            //    binder.AddConfiguration(item);
            //}
            //SystemSettings.Instance.Security.SSOServiceUrl = item.Value;
            //binder.Save();
            binder = new VolatileConfigurationBinder(ApplicationConstants.Instance.DataUploadName);
            ConfigurationItem item7 = new ConfigurationItem
            {
                Key = ApplicationConstants.Instance.DataUploadRootFolder,
                Value = "DataUpload",
                Description = "Data upload home folder"
            };
            binder.AddConfiguration(item7);
            ConfigurationItem item8 = new ConfigurationItem
            {
                Key = ApplicationConstants.Instance.DataUploadValidationResultFolder,
                Value = "Validation_Result",
                Description = "Data upload validation result folder"
            };
            binder.AddConfiguration(item8);
            cabinet.AddBinder(binder);
            binder = new VolatileConfigurationBinder(ApplicationConstants.Instance.Configuration.Session.Name);
            ConfigurationItem item9 = new ConfigurationItem
            {
                Key = ApplicationConstants.Instance.Configuration.Session.HomeFolder,
                Value = "Sessions",
                Description = "Persisted session storage"
            };
            binder.AddConfiguration(item9);

            instance.Security.SSOSessionStoragePath = Path.Combine(instance.Deployment.HomeFolderLocation, ApplicationConstants.Instance.Configuration.Session.Name).CreateDirectoryIfNotExists();
        }
        private void InitMenu()
        {
            
        }
        private void InitLogging()
        {
            LogSettings logging = SystemSettings.Instance.Logging;
            if (logging.Enabled)
            {
                string path = SystemSettings.Instance.Deployment.HomeFolderLocation + @"\Logs";
                logging.FolderLocation = path;
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
            }
        }
        protected virtual void Startup()
        {
        }

    }
}
