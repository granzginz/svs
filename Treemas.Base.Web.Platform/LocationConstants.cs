﻿using System.Web;

namespace Treemas.Base.Web.Platform
{
    public class LocationConstants
    {
        public string Configuration =>
            HttpContext.Current.Server.MapPath("~/Config");

        public string Content =>
            HttpContext.Current.Server.MapPath("~/Content");        
    }
}
