﻿using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Treemas.Credential.Model;
using Treemas.Base.Lookup;
using Treemas.Base.Utilities;

namespace Treemas.Base.Web.Platform
{
    public class RequestFilterAttribute : ActionFilterAttribute
    {
        private ActionResult ApplyBrowserRestriction(HttpRequestBase request)
        {
            HttpBrowserCapabilitiesBase browser = request.Browser;
            BrowserSettings settings = SystemSettings.Instance.Runtime.Browser;
            if (settings.EnableRestriction && (settings.BlockedAgents.Count > 0))
            {
                foreach (BrowserAgent agent in settings.BlockedAgents)
                {
                    if (agent.IsAgentEquals(browser))
                    {
                        if (string.IsNullOrEmpty(settings.BlockingControllerName))
                        {
                            StringBuilder builder = new StringBuilder();
                            builder.AppendLine("<!DOCTYPE html>                                       ");
                            builder.AppendLine("<html>                                                ");
                            builder.AppendLine("  <head>                                              ");
                            builder.AppendLine("      <style type=\"text/css\">                       ");
                            builder.AppendLine("          #message                                    ");
                            builder.AppendLine("          {                                           ");
                            builder.AppendLine("              padding: 20px;                          ");
                            builder.AppendLine("              margin-top: 30px;                       ");
                            builder.AppendLine("              margin-left: auto;                      ");
                            builder.AppendLine("              margin-right: auto;                     ");
                            builder.AppendLine("              width: 100%;                            ");
                            builder.AppendLine("              color: #000000;                         ");
                            builder.AppendLine("              border: 1px solid #525252;              ");
                            builder.AppendLine("              border-radius: 20px;                    ");
                            builder.AppendLine("              -moz-border-radius: 20px;               ");
                            builder.AppendLine("              -webkit-border-radius: 20px;            ");
                            builder.AppendLine("          }                                           ");
                            builder.AppendLine("      </style>                                        ");
                            builder.AppendLine("  </head>                                             ");
                            builder.AppendLine("  <body>                                              ");
                            builder.AppendLine("      <div id=\"message\">                            ");
                            builder.AppendLine("          Your browser is not supported by this application. Unsupported browsers are follow: ");
                            builder.AppendLine("          <ul>                                        ");
                            foreach (BrowserAgent agent2 in settings.BlockedAgents)
                            {
                                builder.AppendLine($"              <li>{agent2.Name} {agent2.Version}</li>");
                            }
                            builder.AppendLine("          </ul>");
                            builder.AppendLine("      </div>");
                            builder.AppendLine("  </body>");
                            builder.AppendLine("</html>");
                            return new ContentResult { Content = builder.ToString() };
                        }
                        return new RedirectToRouteResult(new RouteValueDictionary(new
                        {
                            Controller = settings.BlockingControllerName,
                            Action = "Index"
                        }));
                    }
                }
            }
            return null;
        }

        private ActionResult EvaluateRuntimeMode(string controllerName)
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine("<!DOCTYPE html>                                       ");
            builder.AppendLine("<html>                                                ");
            builder.AppendLine("  <head>                                              ");
            builder.AppendLine("      <style type=\"text/css\">                       ");
            builder.AppendLine("          #message                                    ");
            builder.AppendLine("          {                                           ");
            builder.AppendLine("              padding: 20px;                          ");
            builder.AppendLine("              margin-top: 100px;                      ");
            builder.AppendLine("              margin-left: auto;                      ");
            builder.AppendLine("              margin-right: auto;                     ");
            builder.AppendLine("              width: 100%;                            ");
            builder.AppendLine("              color: #000000;                         ");
            builder.AppendLine("              text-align: center;                     ");
            builder.AppendLine("              border: 1px solid #525252;              ");
            builder.AppendLine("              border-radius: 20px;                    ");
            builder.AppendLine("              -moz-border-radius: 20px;               ");
            builder.AppendLine("              -webkit-border-radius: 20px;            ");
            builder.AppendLine("          }                                           ");
            builder.AppendLine("      </style>                                        ");
            builder.AppendLine("  </head>                                             ");
            builder.AppendLine("  <body>                                              ");
            builder.AppendLine("      <div id=\"message\">{0}</div>                   ");
            builder.AppendLine("  </body>");
            builder.AppendLine("</html>");
            RuntimeSettings runtime = SystemSettings.Instance.Runtime;
            if (runtime.Mode == RuntimeMode.Offline)
            {
                if (string.IsNullOrEmpty(runtime.OfflineModeController))
                {
                    return new ContentResult { Content = builder.Replace("{0}", "The application is currently shutted down, please contact the administrator for further information.").ToString() };
                }
                if (!controllerName.Equals(runtime.OfflineModeController.ToLower() + "controller"))
                {
                    return new RedirectToRouteResult(new RouteValueDictionary(new
                    {
                        Controller = runtime.OfflineModeController,
                        Action = "Index"
                    }));
                }
            }
            else if (runtime.Mode == RuntimeMode.Maintenance)
            {
                if (string.IsNullOrEmpty(runtime.MaintenanceModeController))
                {
                    return new ContentResult { Content = builder.Replace("{0}", "The application is currently under maintenance.").ToString() };
                }
                if (!controllerName.Equals(runtime.MaintenanceModeController.ToLower() + "controller"))
                {
                    return new RedirectToRouteResult(new RouteValueDictionary(new
                    {
                        Controller = runtime.MaintenanceModeController,
                        Action = "Index"
                    }));
                }
            }
            return null;
        }

        private string GetBaseUrl(HttpRequestBase request)
        {
            string scheme = request.Url.Scheme;
            DeploymentContextSettings context = SystemSettings.Instance.Deployment.Context;
            if (context.EmulateSecureProtocol)
            {
                scheme = "https";
            }
            string name = context.Name;
            if (!string.IsNullOrEmpty(name))
            {
                return $"{scheme}://{request.ServerVariables["HTTP_HOST"]}/{name}";
            }
            return $"{scheme}://{request.ServerVariables["HTTP_HOST"]}";
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            PageController controller = (PageController)filterContext.Controller;
            string controllerName = controller.GetType().Name.ToLower();
            HttpSessionStateBase session = filterContext.HttpContext.Session;
            ActionResult result = this.ApplyBrowserRestriction(filterContext.HttpContext.Request);
            
            if (result != null)
            {
                filterContext.Result = result;
            }
            else
            {
                result = this.EvaluateRuntimeMode(controllerName);
                if (result != null)
                {
                    filterContext.Result = result;
                }
                else
                {
                    controller.Authentication.Authenticate(filterContext.RequestContext);
                    if (!controller.Authentication.IsValid)
                    {
                        filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                        {
                            Controller = SystemSettings.Instance.Security.LoginController,
                            Action = "Index"
                        }));
                    }
                    else if (!controller.Authentication.IsAuthorized && !SystemSettings.Instance.Security.IgnoreAuthorization)
                    {
                        if (string.IsNullOrEmpty(SystemSettings.Instance.Security.UnauthorizedController))
                        {
                            ContentResult result2 = new ContentResult
                            {
                                Content = new MvcHtmlString("Unauthorized access.").ToHtmlString()
                            };
                            filterContext.Result = result2;
                        }
                        else
                        {
                            filterContext.Result = new RedirectResult("/" + SystemSettings.Instance.Security.UnauthorizedController);
                        }
                    }
                    else if (controller.Authentication.IsAuthorized)
                    {
                        ILookup lookup = session.Lookup();
                        User user = lookup.Get<User>();
                        if (!user.IsNull())
                        {
                            var hostname = controller.Request.UserHostName;
                            var hostaddress = controller.Request.UserHostAddress;
                            var browser = controller.Request.Browser.Browser;
                            var browserVersion = controller.Request.Browser.Version;
                            var sessionid = controller.Session.SessionID;

                            var info = user.LoginInfos.FindIndex(x => x.Username == user.Username &&
                                                            x.Browser == browser &&
                                                            x.HostIP == hostaddress &&
                                                            x.Hostname == hostname &&
                                                            x.SessionId == sessionid &&
                                                            x.BrowserVersion == browserVersion);
                            if (info <= 0)
                            {
                                lookup.Remove<User>();
                                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                                {
                                    Controller = SystemSettings.Instance.Security.LoginController,
                                    Action = "Logout"
                                }));
                            }
                        }
                    }
                }
            }
        }
    }
}
