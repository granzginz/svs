﻿namespace Treemas.Base.Web.Platform
{
    public interface IHtmlMimeTypeProvider
    {
        string GetMimeType(string extension);
    }
}
