﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Treemas.Base.Utilities;
using Treemas.Base.Lookup;
using Treemas.Credential.Model;

namespace Treemas.Base.Web.Platform
{
    [RequestFilter]
    public abstract class PageController : Controller
    {
        public const string DEFAULT_ACTION = "Index";
        protected byte PAGE_STATE_INIT;

        public PageController(ISessionAuthentication sessionAuthentication)
        {
            this.Settings = new PageSettings(base.GetType());
            this.Descriptor = new PageDescriptor();
            this.Authentication = sessionAuthentication;
            this.Authentication.screenID = this.Settings.ScreenID;
            this.Authentication.descriptor = this.Descriptor;
            this.IgnoreStartup = false;
            this.PageState = this.PAGE_STATE_INIT;
        }

        [HttpPost]
        public ActionResult AjExt(string name)
        {
            AjaxExtensionRegistry instance = AjaxExtensionRegistry.Instance;
            if (!string.IsNullOrEmpty(name) && instance.HasExtension(name))
            {
                return instance.Get(name).Execute(base.Request, base.Response, base.Session);
            }
            return new ContentResult { Content = string.Empty };
        }

        private void ForwardPageParameters(ViewDataDictionary viewData, HttpRequestBase request)
        {
            NameValueCollection @params = request.Params;
            IDictionary<string, string> dictionary = new Dictionary<string, string>();
            foreach (string str in @params.Keys)
            {
                if (!string.IsNullOrEmpty(str) && str.StartsWith("_p"))
                {
                    dictionary.Add(str.Substring(2, str.Length - 2), @params[str]);
                }
            }
            viewData["_tmsForwardedParameters"] = dictionary;
        }

        protected string GetParameter(string key)
        {
            string str = base.Request.Params[key];
            if (str.IsNullOrEmpty() && base.ViewData.ContainsKey("_tmsForwardedParameters"))
            {
                IDictionary<string, string> dictionary = (IDictionary<string, string>)base.ViewData["_tmsForwardedParameters"];
                if (dictionary.ContainsKey(key))
                {
                    str = dictionary[key];
                }
            }
            return str;
        }

        public ActionResult Index()
        {
            if (this.Authentication.IsValid)
            {
                if (!this.IgnoreStartup)
                {
                    this.Startup();
                }
            }
            CollectScreenMessages();
            if (!this.Authentication.RedirectUrl.IsNullOrEmpty())
            {
                return this.Redirect(this.Authentication.RedirectUrl);
            }
            if (base.Request.Browser.IsMobileDevice && SystemSettings.Instance.Runtime.EnableMobileSupport)
            {
                return base.View($"m{this.Settings.IndexPage}");
            }
            return base.View(this.Settings.IndexPage, this.Model);
        }

        public void CollectScreenMessages()
        {
            if (!this.ScreenMessages.IsNull())
            {
                base.ViewData["_tmsScreenMessages"] = this.ScreenMessages.Pull();
            }
        }
        public ActionResult IndexIgnoreStartup()
        {
            this.IgnoreStartup = true;
            return this.Index();
        }

        protected override void Initialize(RequestContext requestContext)
        {
            HttpSessionStateBase session = requestContext.HttpContext.Session;
            if (session.IsNewSession)
            {
                ILookup lookup = new SimpleLookup();
                session["__tms_Lookup__"] = lookup;
                session["__tms_Screen_Message_Pool__"] = new ScreenMessagePool();
            }
            ILookup lookup1 = (ILookup)session["__tms_Lookup__"];
            this.Descriptor.Initialize(requestContext);
            this.ForwardPageParameters(base.ViewData, requestContext.HttpContext.Request);
            base.Initialize(requestContext);
            base.ViewData["User"] = UserHelper;
            base.ViewData["_tmsAuthenticatedUser"] = UserHelper;
            this.Settings.AttachToRequest(base.ViewData);
            this.Descriptor.AttachToRequest(base.ViewData);
        }

        //public FileResult PullFile(string section, string name)
        //{
        //    string path = Path.Combine(SystemSettings.Instance.Deployment.HomeFolderLocation, section, name);
        //    string mimeType = ObjectRegistry.Instance.Get<IHtmlMimeTypeProvider>().GetMimeType(Path.GetExtension(name));
        //    return new FileStreamResult(new FileStream(path, FileMode.Open), mimeType);
        //}

        //public FileResult PullImage(string section, string name) =>
        //    this.PullFile(section + Path.DirectorySeparatorChar + "Images", name);

        //public FileResult PullUserPicture(string name) =>
        //    this.PullImage("Users", name.Replace('.', '_') + ".png");

        protected void ResetIndexPage()
        {
            string name = base.GetType().Name;
            this.Settings.IndexPage = name.Substring(0, name.IndexOf("Controller"));
        }

        protected virtual void Startup()
        {
        }

        public ISessionAuthentication Authentication { get; private set; }

        public PageDescriptor Descriptor { get; private set; }

        public bool IgnoreStartup { get; set; }

        public ILookup Lookup =>
            ((base.Session["__tms_Lookup__"] != null) ? (ILookup)base.Session["__tms_Lookup__"] : null);

        public object Model { get; set; }

        public byte PageState { get; set; }

        public ScreenMessagePool ScreenMessages =>
            ((ScreenMessagePool)base.Session["__tms_Screen_Message_Pool__"]);

        public PageSettings Settings { get; private set; }

        public User UserHelper
        {
            get
            {
                User u = (Lookup != null) ? this.Lookup.Get<User>() : null;
                return u;
            }
        }

        public Menu MenuHelper
        {
            get
            {
                Menu m = (Lookup != null) ? this.Lookup.Get<Menu>() : null;
                return m;
            }
        }
    }
}
