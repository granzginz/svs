﻿using System;
using System.IO;
using System.Web;
using Treemas.Base.Configuration;

namespace Treemas.Base.Web.Platform
{
    public class ApplicationConfigurationCabinet : ConfigurationCabinet
    {
        private static ApplicationConfigurationCabinet instance = new ApplicationConfigurationCabinet();

        private ApplicationConfigurationCabinet() : base("Application Configuration")
        {
            string path = HttpContext.Current.Server.MapPath("~/Config");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
        }

        public static ApplicationConfigurationCabinet Instance =>
            instance;
    }
}
