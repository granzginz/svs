﻿using System;
using System.Web.Mvc;
using Treemas.Base.Utilities;
using Treemas.Credential.Model;

namespace Treemas.Base.Web.Platform
{
    public class AuthorizationRazorExtension
    {
        public string GetFeatureQualifier(string functionId, string featureId, string key)
        {
            User user = this.Helper.Treemas().Page.User;
            if (this.IsAuthorizedToAccessFunction(functionId) && (user != null))
            {
                string str = functionId.ToLower();
                string str2 = featureId.ToLower();
                if (!user.Roles.IsNullOrEmpty<Role>())
                {
                    foreach (Role role in user.Roles)
                    {
                        if (role.Functions != null)
                        {
                            foreach (AuthorizationFunction function in role.Functions)
                            {
                                if (function.FunctionId.ToLower().Equals(str) && (function.Features != null))
                                {
                                    foreach (AuthorizationFeature feature in function.Features)
                                    {
                                        if (feature.FeatureId.ToLower().Equals(str2) && (feature.Qualifiers != null))
                                        {
                                            foreach (AuthorizationFeatureQualifier qualifier in feature.Qualifiers)
                                            {
                                                if (qualifier.Key.Equals(key))
                                                {
                                                    return qualifier.Qualifier;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return null;
        }

        public string GetScreenFeatureQualifier(string featureId, string key)
        {
            string str = this.Helper.Treemas().Page.ScreenID.ToHtmlString();
            if (this.IsAuthorizedToAccessScreen() && !string.IsNullOrEmpty(str))
            {
                return this.GetFeatureQualifier(str, featureId, key);
            }
            return null;
        }

        public bool IsAuthorizedToAccessFeature(string functionId, string featureId)
        {
            User user = this.Helper.Treemas().Page.User;
            if (this.IsAuthorizedToAccessFunction(functionId) && (user != null))
            {
                string str = functionId.ToLower();
                string str2 = featureId.ToLower();
                if (user.Roles != null)
                {
                    foreach (Role role in user.Roles)
                    {
                        if (role.Functions != null)
                        {
                            foreach (AuthorizationFunction function in role.Functions)
                            {
                                if (function.FunctionId.ToLower().Equals(str) && (function.Features != null))
                                {
                                    foreach (AuthorizationFeature feature in function.Features)
                                    {
                                        if (feature.FeatureId.ToLower().Equals(str2))
                                        {
                                            return true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return false;
        }

        public bool IsAuthorizedToAccessFunction(string functionId)
        {
            User user = this.Helper.Treemas().Page.User;
            if (user != null)
            {
                string str = functionId.ToLower();
                if (user.Roles != null)
                {
                    foreach (Role role in user.Roles)
                    {
                        if (role.Functions != null)
                        {
                            foreach (AuthorizationFunction function in role.Functions)
                            {
                                if (function.FunctionId.ToLower().Equals(str))
                                {
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
            return false;
        }

        public bool IsAuthorizedToAccessQualifier(string functionId, string featureId, string key) =>
            (this.IsAuthorizedToAccessFunction(functionId) && !string.IsNullOrEmpty(this.GetFeatureQualifier(functionId, featureId, key)));

        public bool IsAuthorizedToAccessQualifier(string functionId, string featureId, string key, string value)
        {
            if (!this.IsAuthorizedToAccessFunction(functionId))
            {
                return false;
            }
            string str = this.GetFeatureQualifier(functionId, featureId, key);
            return (!string.IsNullOrEmpty(str) && str.Equals(value));
        }

        public bool IsAuthorizedToAccessScreen()
        {
            string str = this.Helper.Treemas().Page.ScreenID.ToHtmlString();
            return (!string.IsNullOrEmpty(str) && this.IsAuthorizedToAccessFunction(str));
        }

        public bool IsAuthorizedToAccessScreenFeature(string featureId)
        {
            string functionId = this.Helper.Treemas().Page.ScreenID.ToHtmlString();
            return (this.IsAuthorizedToAccessScreen() && this.IsAuthorizedToAccessFeature(functionId, featureId));
        }

        public bool IsAuthorizedToAccessScreenQualifier(string featureId, string key)
        {
            string functionId = this.Helper.Treemas().Page.ScreenID.ToHtmlString();
            return (this.IsAuthorizedToAccessScreen() && this.IsAuthorizedToAccessQualifier(functionId, featureId, key));
        }

        public bool IsAuthorizedToAccessScreenQualifier(string featureId, string key, string value)
        {
            string functionId = this.Helper.Treemas().Page.ScreenID.ToHtmlString();
            return (this.IsAuthorizedToAccessScreen() && this.IsAuthorizedToAccessQualifier(functionId, featureId, key, value));
        }

        public bool IsUserHasRole(string roleId)
        {
            Predicate<Role> criteria = null;
            if (!string.IsNullOrEmpty(roleId))
            {
                User user = this.Helper.Treemas().Page.User;
                if (!user.IsNull() && !user.Roles.IsNullOrEmpty<Role>())
                {
                    if (criteria == null)
                    {
                        criteria = r => r.Id.Equals(roleId);
                    }
                    return !user.Roles.FindElement<Role>(criteria).IsNull();
                }
            }
            return false;
        }

        public HtmlHelper Helper { get; set; }
    }
}
