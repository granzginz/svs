﻿using System;
using System.Web.Mvc;

namespace Treemas.Base.Web.Platform
{
    public class PageSettings
    {
        public PageSettings(Type controllerType)
        {
            this.Title = string.Empty;
            string name = controllerType.Name;
            this.ControllerName = name.Substring(0, name.IndexOf("Controller"));
            this.ScreenID = this.ControllerName;
            this.IndexPage = this.ScreenID;
            this.ModuleName = this.ControllerName;
        }

        public void AttachToRequest(ViewDataDictionary viewData)
        {
            viewData["_tmsScreenTitle"] = this.Title;
            viewData["Title"] = this.Title;
            viewData["_tmsScreenID"] = this.ScreenID;
            viewData["_tmsControllerName"] = this.ControllerName;
            viewData["_tmsModuleName"] = this.ModuleName;
        }

        public string ControllerName { get; set; }
        public string ModuleName { get; set; }

        public string IndexPage { get; set; }

        public string ScreenID { get; set; }

        public string Title { get; set; }
    }
}
