﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Routing;

namespace Treemas.Base.Web.Platform
{
    public interface ISessionAuthentication
    {
        PageDescriptor descriptor { get; set; }
        string screenID { get; set; }
        bool Enabled { get; set; }
        bool IsAuthorized { get; set; }
        bool IsValid { get; set; }
        string RedirectUrl { get; set; }
        void Authenticate(RequestContext requestContext);

    }
}
