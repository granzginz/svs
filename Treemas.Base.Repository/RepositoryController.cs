﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using NHibernate.Util;
using Treemas.Base.Utilities;


namespace Treemas.Base.Repository
{
    public abstract class RepositoryController<T> : RepositoryBase, IRepositoryController<T> where T : IEntity
    {
        public Type GetSubjectType()
        {
            return typeof(T);
        }

        public void Add(T item)
        {
            try
            {
                transact(() => session.Save(item));
            }
            catch (Exception e)
            {
                throw new RepositoryException(e.Message, e);
            }
        }

        public void Save(T item)
        {
            transact(() => session.SaveOrUpdate(item));
        }
        public void SaveOrUpdateAll(IEnumerable<T> items)
        {
            transact(() => items.ForEach(e => session.SaveOrUpdate(e)));
        }
        public bool Contains(T item)
        {
            var ret = transact(() => session.Get<T>(item.Id));

            return !ReferenceEquals(ret, null);
        }

        public int Count
        {
            get
            {
                return transact(() => session.Query<T>().Count());
            }
        }
        public bool Remove(T item)
        {
            transact(() => session.Delete(item));
            return true;
        }

        public IEnumerator<T> GetEnumerator()
        {
            return transact(() => session.Query<T>().Take(1000).GetEnumerator());
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return transact(() => GetEnumerator());
        }
        public IEnumerable<T> FindAll()
        {
            return transact(() => session.Query<T>().ToList());
        }
        public IEnumerable<T> FindAll(Specification<T> specification)
        {
            var query = transact(() => getQuery(specification).ToList());
            return query;
        }

        public T GetOne(Specification<T> specification)
        {
            var query = getQuery(specification);
            return transact(()=>query.SingleOrDefault());
        }

        public T FindOne(Specification<T> specification)
        {
            var query = getQuery(specification);
            return transact(() => query.FirstOrDefault());
        }

        private IQueryable<T> getQuery(Specification<T> specification)
        {
            return session.Query<T>()
            .Where(specification.ToExpression());
        }

    }
}
