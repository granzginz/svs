﻿using System;
using System.Runtime.Serialization;

namespace Treemas.Base.Repository
{
    [Serializable]
    public class RepositoryException : ApplicationException
    {
        public RepositoryException()
        {
        }

        public RepositoryException(Exception root) : base("An exception occured in repository", root)
        {
        }

        public RepositoryException(string message) : base(message)
        {
        }

        protected RepositoryException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public RepositoryException(string message, Exception e)
            : base(message, e)
        {
        }
    }
}
