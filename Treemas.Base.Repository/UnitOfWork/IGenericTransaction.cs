﻿using System;

namespace Treemas.Base.Repository.UnitOfWork
{
    public interface IGenericTransaction : IDisposable
    {
        void Commit();
        void Rollback();
    }
}
