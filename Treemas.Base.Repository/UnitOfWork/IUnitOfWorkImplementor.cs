﻿namespace Treemas.Base.Repository.UnitOfWork
{
    public interface IUnitOfWorkImplementor : IUnitOfWork
    {
        void IncrementUsages();
    }
}
