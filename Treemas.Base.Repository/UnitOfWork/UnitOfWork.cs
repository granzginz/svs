﻿using System;

namespace Treemas.Base.Repository.UnitOfWork
{
    public static class UnitOfWork
    {
        private const string CurrentUnitOfWorkKey = "CurrentUnitOfWork.Key";

        private static IUnitOfWork currentUnitOfWork
        {
            get { return Local.Data[CurrentUnitOfWorkKey] as IUnitOfWork; }
            set { Local.Data[CurrentUnitOfWorkKey] = value; }
        }

        public static IUnitOfWork Current
        {
            get
            {
                var unitOfWork = currentUnitOfWork;
                if (unitOfWork == null)
                    throw new InvalidOperationException("You are not in a unit of work");
                return unitOfWork;
            }
        }

        public static bool IsStarted
        {
            get { return currentUnitOfWork != null; }
        }

        public static IUnitOfWork Start(IUnitOfWorkFactory unitOfWorkFactory)
        {
            if (currentUnitOfWork != null)
                throw new InvalidOperationException("You cannot start more than one unit of work at the same time.");

            var unitOfWork = unitOfWorkFactory.Create();
            currentUnitOfWork = unitOfWork;
            return unitOfWork;
        }

        public static void DisposeUnitOfWork(IUnitOfWorkImplementor unitOfWork)
        {
            currentUnitOfWork = null;
        }
    }
}
