﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.Credential.Model;

namespace Treemas.Credential.Interface
{
    public interface IMenuCrRepository : IBaseRepository<MenuCr>
    {
        IList<MenuCr> getApplicationMenu(string application);
        MenuCr getForEditMode(long id);
        IList<AuthorizationFunction> getFunctions(IList<string> ApplicationIds);
        bool IsDuplicate(string application, string menuId);
        bool IsDuplicateForAdd(string application, string functionid);
        PagedResult<MenuCr> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, ApplicationCRFilter filter);
        IList<MenuCr> FindOnlyParentMenu(string menuID);
        string GetRootMenuId(long id);
        string GetRootMenuId(string id);
        string GetRootMenuNameByParentMenuId(string parentId);

    }
}
