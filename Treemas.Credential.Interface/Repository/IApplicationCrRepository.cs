﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.Credential.Model;

namespace Treemas.Credential.Interface
{
    public interface IApplicationCrRepository: IBaseRepository<ApplicationCr>
    {
        IList<ApplicationCr> getApplications(IList<string> Ids);
        bool IsDuplicate(string applicationid);
        PagedResult<ApplicationCr> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, ApplicationCRFilter filter);
        PagedResult<ApplicationCr> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
        ApplicationCr getApplication(long id);
        ApplicationCr getExistingRequest(string AppID);
        void deleteExistingRequest(string AppID);
        ApplicationCr getChangesRequest();
    }
}
