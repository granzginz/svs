﻿using System.Collections.Generic;
using Treemas.Credential.Model;

namespace Treemas.Credential.Interface
{
    public interface ILoginInfoRepository : IBaseRepository<SignonLoginInfo>
    {
        void EvaluateLoginInfo(IList<SignonLoginInfo> loginInfos);
        SignonLoginInfo GetLoginInfo(long id);
        IList<SignonLoginInfo> GetLoginInfos(string username);
        void LogoutProcess(SignonLoginInfo info);
        SignonLoginInfo GetLoginInfoByData(SignonLoginInfo loginInfo);
    }
}
