﻿using LinqSpecs;
using System.Collections.Generic;
using Treemas.Credential.Model;
using Treemas.Base.Utilities.Queries;

namespace Treemas.Credential.Interface
{
    public interface IRoleDocTypeCrRepository : IBaseRepository<RoleDocTypeCr>
    {
        IList<RoleDocTypeCr> getDocTypes(long RoleId);
        IList<RoleDocTypeCr> getDocTypes2(long RoleId);
        IList<RoleDocTypeCr> getDocTypes(long RoleId,string appStat);
        bool IsDuplicate(long roleid, long DocTypeid);
        PagedResult<RoleDocTypeCr> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<RoleDocTypeCr> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, ApplicationCRFilter filter);
        RoleDocTypeCr getRoleDocType(long id);
        int deleteSelected(long roleId, long DocTypeId);
        RoleDocTypeCr getRoleDocType(long roleId,long docTypeId);
    }
}
