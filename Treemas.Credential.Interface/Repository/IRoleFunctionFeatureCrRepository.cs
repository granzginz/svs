﻿using LinqSpecs;
using System.Collections.Generic;
using Treemas.Credential.Model;
using Treemas.Base.Utilities.Queries;

namespace Treemas.Credential.Interface
{
    public interface IRoleFunctionFeatureCrRepository : IBaseRepository<RoleFunctionFeatureCr>
    {
        IList<RoleFunctionFeatureCr> getRoleFuncFeatures(long RoleFuncId);
        IList<RoleFunctionFeatureCr> getRoleFuncFeatures(long RoleId, long FunctionId);
        bool IsDuplicate(long roleid, long functionid);
        PagedResult<RoleFunctionFeatureCr> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<RoleFunctionFeatureCr> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, ApplicationCRFilter filter);
        RoleFunctionFeatureCr getRoleFuncFeature(long id);
        RoleFunctionFeatureCr getRoleFuncFeature(long rolId, long functionId, long featureId);
        int deleteSelected(long roleId, long functionId, long featureId);
        int deleteAllbyRoleFuncId(long roleFunctId);
        IList<RoleFunctionFeatureCr> getRoleFuncFeaturebyRole(long roleId);
    }
}
