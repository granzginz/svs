﻿using System.Collections.Generic;
using System;
using System.Web.Routing;
using Treemas.Credential.Model;
using Treemas.Base.Utilities.Queries;

namespace Treemas.Credential.Interface
{
    public interface IUserCrRepository : IBaseRepository<UserCr>
    {
        UserCr GetUser(string username);
        UserCr GetUser(string username, ApprovalType type);
        UserCr GetUserforAdmin(string username, ApprovalType type);
        UserCr GetUserById(long id);
        PagedResult<UserCr> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, AuditTrailFilter filter);
        PagedResult<UserCr> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue, AuditTrailFilter filter);
        PagedResult<UserCr> FindAllPagedDate(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, DateTime searchValue);
        PagedResult<UserCr> FindAllPagedApproval(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string user);
        PagedResult<UserCr> FindAllPagedApproval(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue, string user);
        PagedResult<UserCr> FindAllPagedDateApproval(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, DateTime searchValue, string user);
        bool IsDuplicate(string username);
        void UpdateLoginAttempt(string username, int loginAttempt, bool disabled, DateTime? lastlogin);
        void resetPassword(long id, string password);
        void resetPassword(long id, string password, DateTime expiredDate);
        int deleteAll(string username);
        int deleteUserCr(string username, long id);
        void SetUserToInactive(string username);
        IList<PasswordHistory> GetPasswordHistory(string username);
        void SavePasswordHistory(PasswordHistory passwordHistory, PasswordHistory deletedHistory);
        UserCr GetUserForPrint(long id);
        void setApproved(long id, string approvedby, DateTime approveddate, string approvalstatus);
        void setPrinted(long id);
        void setActive(long id, DateTime passexpdate, DateTime accvaldate);
        void setUpdateMenu(long id, bool update);
        void setUpdateMenu(string username, bool update);
        void setUpdateMenu(bool update);
        IList<UserRole> getDistinctUserRoles(IList<UserCr> UserCr);
        IList<UserCr> FindExport(AuditTrailFilter filter);
    }
}
