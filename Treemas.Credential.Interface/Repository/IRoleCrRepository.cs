﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.Credential.Model;

namespace Treemas.Credential.Interface
{
   public interface IRoleCrRepository: IBaseRepository<RoleCr>
    {
        IList<RoleCr> getApplicationRole(string application);
        bool IsDuplicate(string application, string roleid);
        PagedResult<RoleCr> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, ApplicationCRFilter filter);
        RoleCr getRole(long id);
        RoleCr getRole(string id);
        RoleCr getExistingRequest(string roleid);
        void deleteExistingRequest(string roleid);
    }
}
