﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.Credential.Model;

namespace Treemas.Credential.Interface
{
    public interface IFunctionCrRepository : IBaseRepository<AuthorizationFunctionCr>
    {
        IList<AuthorizationFunctionCr> getFunctions(IList<string> ApplicationIds);
        bool IsDuplicate(string application, string functionid);
        PagedResult<AuthorizationFunctionCr> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, ApplicationCRFilter filter);
        AuthorizationFunctionCr getFunction(long id);
        AuthorizationFunctionCr getExistingRequest(string functionId);
        void deleteExistingRequest(string functionId);
    }
}
