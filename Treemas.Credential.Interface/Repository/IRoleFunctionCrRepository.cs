﻿using LinqSpecs;
using System.Collections.Generic;
using Treemas.Credential.Model;
using Treemas.Base.Utilities.Queries;

namespace Treemas.Credential.Interface
{
    public interface IRoleFunctionCrRepository : IBaseRepository<RoleFunctionCr>
    {
        IList<RoleFunctionCr> getFunctions(long RoleId);
        bool IsDuplicate(long roleid, long functionid);
        PagedResult<RoleFunctionCr> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<RoleFunctionCr> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, ApplicationCRFilter filter);
        RoleFunctionCr getRoleFunction(long id);
        RoleFunctionCr getRoleFunction2(long id);
        int deleteSelected(long roleId, long functionId);
        RoleFunctionCr getRoleFunc(long roleId,long funcId);
    }
}
