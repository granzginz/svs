﻿using LinqSpecs;
using System.Collections.Generic;
using Treemas.Credential.Model;
using Treemas.Base.Utilities.Queries;

namespace Treemas.Credential.Interface
{
    public interface IRoleFunctionRepository : IBaseRepository<RoleFunction>
    {
        IList<RoleFunction> getFunctions(long RoleId);
        bool IsDuplicate(long roleid, long functionid);
        PagedResult<RoleFunction> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        RoleFunction getRoleFunction(long id);
        int deleteSelected(long roleId, long functionId);
        RoleFunction getRoleFunc(long roleId,long funcId);
    }
}
