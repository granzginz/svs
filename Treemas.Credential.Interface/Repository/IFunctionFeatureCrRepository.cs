﻿using LinqSpecs;
using System.Collections.Generic;
using Treemas.Credential.Model;
using Treemas.Base.Utilities.Queries;

namespace Treemas.Credential.Interface
{
    public interface IFunctionFeatureCrRepository : IBaseRepository<FunctionFeatureCr>
    {
        FunctionFeatureCr getFunctionFeature(long id);
        FunctionFeatureCr getFunctionFeature(long functionid, long featureid);
        IList<FunctionFeatureCr> getFunctionFeatures(long functionid);
        int deleteSelected(long functionId, long featureid);
        PagedResult<FunctionFeatureCr> FindAllPagedbyFunction(long functionId, int pageNumber, string orderColumn, int itemsPerPage, string orderKey);
        PagedResult<FunctionFeatureCr> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, ApplicationCRFilter filter);
    }
}
