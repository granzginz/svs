﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.Credential.Model;

namespace Treemas.Credential.Interface
{
    public interface IUserAuditTrailRepository : IBaseRepository<UserAuditTrail>
    {
        void SaveAuditTrail(string functionName, object auditedObject, string actionby, string action);
        //IList<UserAuditTrail> FindForExport(AuditTrailFilter filter);
        PagedResult<UserAuditTrail> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue, DateTime start, DateTime end);
        PagedResult<UserAuditTrail> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        //PagedResult<UserAuditTrail> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, AuditTrailFilter filter);
        PagedResult<UserAuditTrail> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
        PagedResult<UserAuditTrail> FindAllPagedDate(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, DateTime searchValue);
        IList<UserAuditTrail> FindAllPagedForExport(string searchColumn, string searchValue, DateTime ActionDateTimeStart, DateTime ActionDateTimeEnd);
    }
}
