﻿using System;
using Treemas.Credential.Model;
using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;

namespace Treemas.Credential.Interface
{
    public interface IAuditTrailLog : IBaseRepository<AuditTrail>
    {
        void SaveAuditTrail(string functionName, object auditedObjectBefore, object auditedObjectAfter, string maker, DateTime? requestDate, string actionby, string action);
        void SaveAuditTrail(string functionName, object auditedObject, string actionby, string action);
        PagedResult<AuditTrail> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<AuditTrail> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
        PagedResult<AuditTrail> FindAllPagedDate(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, DateTime searchValue);
    }
}
