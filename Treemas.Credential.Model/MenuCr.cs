﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.Credential.Model
{
    public class MenuCr : Entity
    {
        protected MenuCr()
        {
        }
        public MenuCr(long id) : base(id)
        {
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }

        public virtual string MenuId { get; set; }
        public virtual string MenuName { get; set; }

        public virtual string Application { get; set; }

        public virtual string MenuUrl { get; set; }
        public virtual string MenuIcon { get; set; }
        public virtual int MenuLevel { get; set; }
        public virtual int MenuOrder { get; set; }
        public virtual bool Active { get; set; }
        public virtual IList<Menu> ChildMenu { get; set; }

        public virtual string ParentMenu { get; set; }

        public virtual string FunctionId { get; set; }

        public virtual string CreatedBy { get; set; }
        public virtual DateTime? CreatedDate { get; set; }
        public virtual string ChangedBy { get; set; }
        public virtual DateTime? ChangedDate { get; set; }
        public virtual bool Approved { get; set; }
        public virtual string ApprovedBy { get; set; }
        public virtual string ApprovalType { get; set; }
        public virtual ApprovalType ApprovalTypeEnum
        {
            get
            {
                ApprovalType currentState = (ApprovalType)Enum.Parse(typeof(ApprovalType), this.ApprovalType);
                return currentState;
            }
        }

        public virtual DateTime? ApprovedDate { get; set; }
        public virtual string ApprovalStatus { get; set; }
    }
}
