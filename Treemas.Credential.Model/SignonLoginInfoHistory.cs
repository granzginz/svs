﻿using System;

namespace Treemas.Credential.Model
{
    public class SignonLoginInfoHistory
    {
        public virtual long Id { get; set; }
        public virtual string SessionId { get; set; }
        public virtual string Username { get; set; }
        public virtual DateTime LoginTime { get; set; }
        public virtual int LockTimeout { get; set; }
        public virtual int MaximumLogin { get; set; }
        public virtual string Hostname { get; set; }
        public virtual string HostIP { get; set; }
        public virtual string Browser { get; set; }
        public virtual string BrowserVersion { get; set; }
        public virtual bool IsMobile { get; set; }
        public virtual int SessionTimeout { get; set; }
    }
}
