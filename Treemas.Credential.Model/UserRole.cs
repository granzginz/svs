﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;
using Treemas.Base.Globals;

namespace Treemas.Credential.Model
{
    public class UserRole 
    {
        public virtual string Username { get; set; }
        public virtual string Role { get; set; }
    }
}
