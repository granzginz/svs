﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.Credential.Model
{
    public class ParameterHistory : Entity
    {
        protected ParameterHistory()
        {

        }
        public ParameterHistory(long id) : base(id)
        {
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }

        public virtual string UserId { get; set; }
        public virtual string ParamId { get; set; }
        public virtual string Password { get; set; }
        //public virtual string DukcapilUrl { get; set; }
        public virtual string StaggingUrl { get; set; }
        //public virtual string ClientIPAddress { get; set; }
        public virtual int SessionTimeout { get; set; }
        public virtual int LoginAttempt { get; set; }
        //public virtual int NIKExpiredDuration { get; set; }
        public virtual string PasswordExp { get; set; }
        //public virtual string Validity { get; set; }
        public virtual string NewUserId { get; set; }
        public virtual string NewPassword { get; set; }
        //public virtual string NewDukcapilUrl { get; set; }
        public virtual string NewStaggingUrl { get; set; }
        //public virtual string NewClientIPAddress { get; set; }
        public virtual int NewSessionTimeout { get; set; }
        public virtual int NewLoginAttempt { get; set; }
        //public virtual int NewNIKExpiredDuration { get; set; }
        public virtual string NewPasswordExp { get; set; }
        //public virtual string NewValidity { get; set; }
        public virtual string NewDomainServer { get; set; }
        public virtual string DomainServer { get; set; }
        public virtual string ApprovalStatus { get; set; }
        public virtual ApprovalStatusState ApprovalStatusStateEnum
        {
            get
            {
                ApprovalStatusState currentState = (ApprovalStatusState)Enum.Parse(typeof(ApprovalStatusState), this.ApprovalStatus);
                return currentState;
            }
        }
        public virtual string ChangedBy { get; set; }
        public virtual DateTime? ChangedDate { get; set; }
        public virtual string ApprovedBy { get; set; }
        public virtual DateTime? ApprovedDate { get; set; }

        public virtual int SizeKTP { get; set; }
        public virtual int SizeSignature { get; set; }
        public virtual int SizeDocument { get; set; }
        public virtual int KeepHistory { get; set; }
        public virtual int KeepAuditTrail { get; set; }
        public virtual int KeepPendingAppr { get; set; }
        public virtual decimal MaxDiffAccount { get; set; }

        public virtual int NewSizeKTP { get; set; }
        public virtual int NewSizeSignature { get; set; }
        public virtual int NewSizeDocument { get; set; }
        public virtual int NewKeepHistory { get; set; }
        public virtual int NewKeepAuditTrail { get; set; }
        public virtual int NewKeepPendingAppr { get; set; }
        public virtual decimal NewMaxDiffAccount { get; set; }

    }
}
