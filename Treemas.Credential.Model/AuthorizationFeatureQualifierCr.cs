﻿
namespace Treemas.Credential.Model
{
    public class AuthorizationFeatureQualifierCr
    {
        public string Key { get; set; }
        public string Qualifier { get; set; }
    }
}
