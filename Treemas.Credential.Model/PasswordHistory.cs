﻿using System;

namespace Treemas.Credential.Model
{
    public class PasswordHistory
    {
        public virtual long Id { get; set; }
        public virtual string Username { get; set; }
        public virtual string Password { get; set; }
        public virtual DateTime ChangePasswordDate { get; set; }
    }
}