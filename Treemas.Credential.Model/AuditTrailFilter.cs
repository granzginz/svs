﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Treemas.Credential.Model
{
    public class AuditTrailFilter
    {
        public DateTime? ActionDateStart { get; set; }
        public DateTime? ActionDateEnd { get; set; }
        public string ActionName { get; set; }
        public string FunctionName { get; set; }
        public string ObjectName { get; set; }
        public string searchColumn { get; set; }
        public string searchValue { get; set; }
    }
}
