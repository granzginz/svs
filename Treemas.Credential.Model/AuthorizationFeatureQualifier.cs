﻿namespace Treemas.Credential.Model
{
    public class AuthorizationFeatureQualifier
    {
        public string Key { get; set; }
        public string Qualifier { get; set; }
    }
}
