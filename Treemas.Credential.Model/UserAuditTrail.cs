﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.Credential.Model
{
    public class UserAuditTrail : Entity
    {
        protected UserAuditTrail()
        {

        }
        public UserAuditTrail(long id) : base(id)
        {
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }

        public virtual DateTime TimeStamp { get; set; }
        public virtual string FunctionName { get; set; }
        public virtual string ObjectName { get; set; }
        public virtual string ObjectValueBefore { get; set; }
        public virtual string ObjectValueAfter { get; set; }
        public virtual string UserMaker { get; set; }
        public virtual DateTime? RequestDate { get; set; }
        public virtual string UserChecker { get; set; }
        public virtual DateTime? ApproveDate { get; set; }
        public virtual string Action { get; set; }
    }
}
