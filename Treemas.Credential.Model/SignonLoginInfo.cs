﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.Credential.Model
{
    public class SignonLoginInfo : Entity
    {
        protected SignonLoginInfo()
        { }
        public SignonLoginInfo(long id) : base(id)
        {

        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }

        public virtual string SessionId { get; set; }
        public virtual string Browser { get; set; }
        public virtual string BrowserVersion { get; set; }
        public virtual string HostIP { get; set; }
        public virtual string Hostname { get; set; }
        public virtual bool IsMobile { get; set; }
        public virtual DateTime LastActive { get; set; }
        public virtual bool Locked { get; set; }
        public virtual DateTime? LockTime { get; set; }
        public virtual int LockTimeout { get; set; }
        public virtual DateTime LoginTime { get; set; }
        public virtual int MaximumLogin { get; set; }
        public virtual int SessionTimeout { get; set; }
        public virtual DateTime? UnlockTime { get; set; }
        public virtual string Username { get; set; }
        public virtual SignonPolicyState PolicyState { get; set; }
    }
}
