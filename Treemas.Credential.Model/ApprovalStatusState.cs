﻿using Treemas.Base.Utilities;

namespace Treemas.Credential.Model
{
    public enum ApprovalStatusState : int
    {
        [StringValue("Request")]
        R = 1,
        [StringValue("Approved")]
        A = 2,
        [StringValue("Declined")]
        D = 3
    }
}
