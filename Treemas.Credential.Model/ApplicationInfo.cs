﻿using System.Collections.Generic;
using Treemas.Base.Globals;

namespace Treemas.Credential.Model
{
    public class ApplicationInfo
    {
        public ApplicationInfo()
        {
            this.RuntimeParameters = new Dictionary<string, string>();
        }

        public string Description { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
        public string Runtime { get; set; }
        public IDictionary<string, string> RuntimeParameters { get; set; }
        public ApplicationType Type { get; set; }
        public virtual IList<Menu> Menus { get; set; }
    }
}
