﻿namespace Treemas.Base.Utilities
{
    public interface IDeleteAction : IAction
    {
        void Delete(object param);
    }
}

