﻿using System.Transactions;

namespace Treemas.Base.Utilities.Pattern
{
    public class TransactionCommandHandler<TCommand> : ICommandHandler<TCommand>
    {
        private readonly ICommandHandler<TCommand> decorated;
        public TransactionCommandHandler(ICommandHandler<TCommand> decorated)
        {
            this.decorated = decorated;
        }
        public void Execute(TCommand command)
        {
            using (var scope = new TransactionScope())
            {
                this.decorated.Execute(command);

                scope.Complete();
            }
        }
    }
}
