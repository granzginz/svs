﻿namespace Treemas.Base.Utilities
{
    public interface IShowAction : IAction
    {
        void Show(object param);
    }
}

