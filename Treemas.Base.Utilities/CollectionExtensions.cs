﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Data;
using System.ComponentModel;

namespace Treemas.Base.Utilities
{
    public static class CollectionExtensions
    {
        public static void AddIfAllowed<T>(this ICollection<T> target, T obj, bool flag)
        {
            if (!((!flag || target.IsNull()) || obj.IsNull()))
            {
                target.Add(obj);
            }
        }

        public static void AddIfConditionAchieved<T>(this ICollection<T> target, T obj, Predicate<T> action)
        {
            if (!target.IsNull() && !obj.IsNull())
            {
                bool flag = false;
                if (action != null)
                {
                    flag = action(obj);
                }
                if (flag)
                {
                    target.Add(obj);
                }
            }
        }

        public static void AddIfNotNull<T>(this ICollection<T> target, T obj)
        {
            if (!(target.IsNull() || obj.IsNull()))
            {
                target.Add(obj);
            }
        }

        public static void AddIfStringIsNotNullOrEmpty<T>(this ICollection<T> target, T obj, string str)
        {
            if (!((target.IsNull() || obj.IsNull()) || str.IsNullOrEmpty()))
            {
                target.Add(obj);
            }
        }

        public static long Count(this IEnumerable target)
        {
            long num = 0L;
            if (!target.IsNull())
            {
                IEnumerator enumerator = target.GetEnumerator();
                if (enumerator.IsNull())
                {
                    return num;
                }
                while (enumerator.MoveNext())
                {
                    num += 1L;
                }
            }
            return num;
        }

        public static int CountElement<T>(this ICollection<T> list, T item)
        {
            int num = 0;
            if (!list.IsNullOrEmpty<T>())
            {
                foreach (T local in list)
                {
                    if (local.Equals(item))
                    {
                        num++;
                    }
                }
            }
            return num;
        }

        public static void EnumerateAsKeyValuePair(this IEnumerable<object> list, Action<object, object> action)
        {
            list.EnumerateAsKeyValuePair("Key", "Value", action);
        }

        public static void EnumerateAsKeyValuePair(this IEnumerable<object> list, string keyField, string valueField, Action<object, object> action)
        {
            if (list != null)
            {
                foreach (object obj2 in list)
                {
                    Type type = obj2.GetType();
                    PropertyInfo property = type.GetProperty(keyField);
                    PropertyInfo info2 = type.GetProperty(valueField);
                    if (!property.IsNull())
                    {
                        action(property.GetValue(obj2, null), info2?.GetValue(obj2, null));
                    }
                }
            }
        }

        public static ICollection<T> Extend<T>(this ICollection<T> target, Action<ICollection<T>> action)
        {
            if (!target.IsNull())
            {
                action(target);
            }
            return target;
        }

        public static IDictionary<T, U> Extend<T, U>(this IDictionary<T, U> target, Action<IDictionary<T, U>> action)
        {
            if (action != null)
            {
                action(target);
            }
            return target;
        }

        public static IList<T> ExtendAsList<T>(this ICollection<T> target, Action<ICollection<T>> action)
        {
            if (!target.IsNull())
            {
                action(target);
            }
            return new List<T>(target);
        }

        public static void FindAgainst<T>(this ICollection<T> target, ICollection<T> other, Func<T, T, bool> criteria, Action<T, T> foundAction)
        {
            if (!target.IsNullOrEmpty<T>() && !other.IsNullOrEmpty<T>())
            {
                foreach (T local in target)
                {
                    bool flag = false;
                    foreach (T local2 in other)
                    {
                        flag = criteria(local, local2);
                        if (flag)
                        {
                            foundAction(local, local2);
                            break;
                        }
                    }
                    if (flag)
                    {
                        break;
                    }
                }
            }
        }

        public static T FindElement<T>(this ICollection<T> list, Predicate<T> criteria)
        {
            if (!list.IsNullOrEmpty<T>())
            {
                foreach (T local in list)
                {
                    if (criteria(local))
                    {
                        return local;
                    }
                }
            }
            return default(T);
        }

        public static IList<T> FindElements<T>(this ICollection<T> list, Predicate<T> criteria)
        {
            IList<T> list2 = null;
            if (!list.IsNullOrEmpty<T>())
            {
                list2 = new List<T>();
                foreach (T local in list)
                {
                    if (criteria(local))
                    {
                        list2.Add(local);
                    }
                }
            }
            return list2;
        }

        public static int FindIndex<T>(this ICollection<T> list, Predicate<T> criteria)
        {
            if (!list.IsNullOrEmpty<T>())
            {
                int num = 0;
                IEnumerator<T> enumerator = list.GetEnumerator();
                while (enumerator.MoveNext())
                {
                    num++;
                    T current = enumerator.Current;
                    if (criteria(current))
                    {
                        return num;
                    }
                }
            }
            return -1;
        }

        public static bool IsElementExists<T>(this ICollection<T> list, Func<T, bool> function)
        {
            bool flag = false;
            if (!list.IsNullOrEmpty<T>())
            {
                foreach (T local in list)
                {
                    flag = function(local);
                    if (flag)
                    {
                        return flag;
                    }
                }
            }
            return flag;
        }

        public static bool IsNotNullAndSizeEquals<T>(this ICollection<T> list, int size) =>
            (!list.IsNullOrEmpty<T>() && (list.Count == size));

        public static bool IsNotNullAndSizeLessThan<T>(this ICollection<T> list, int size) =>
            (!list.IsNullOrEmpty<T>() && (list.Count < size));

        public static bool IsNotNullAndSizeMoreThan<T>(this ICollection<T> list, int size) =>
            (!list.IsNullOrEmpty<T>() && (list.Count > size));

        public static bool IsNullOrEmpty<T>(this ICollection<T> list) =>
            ((list == null) || (list.Count == 0));

        public static bool IsNullOrEmpty<T>(this T[] array) =>
            ((array == null) || (array.Length == 0));

        public static void IterateByAction<T>(this ICollection<T> list, Action<T> action)
        {
            if (!list.IsNullOrEmpty<T>())
            {
                foreach (T local in list)
                {
                    action(local);
                }
            }
        }

        public static void IterateByAction<T>(this ICollection<T> list, Predicate<T> action)
        {
            if (!list.IsNullOrEmpty<T>())
            {
                foreach (T local in list)
                {
                    if (!action(local))
                    {
                        break;
                    }
                }
            }
        }

        public static ICollection<T> IteratedAddition<T>(this ICollection<T> target, int size, Func<int, T> action) =>
            target.IteratedAddition<T>(0, size, action);

        public static ICollection<T> IteratedAddition<T>(this ICollection<T> target, int startIndex, int size, Func<int, T> action)
        {
            if (!target.IsNull() && !action.IsNull())
            {
                for (int i = startIndex; i < size; i++)
                {
                    target.Add(action(i));
                }
            }
            return target;
        }

        public static ICollection<T> Merge<T>(this ICollection<T> target, ICollection<T> source)
        {
            if (!target.IsNull() && !source.IsNullOrEmpty<T>())
            {
                foreach (T local in source)
                {
                    target.Add(local);
                }
            }
            return target;
        }

        public static ICollection<T> Merge<T>(this ICollection<T> target, T[] source)
        {
            if (!target.IsNull() && !source.IsNullOrEmpty<T>())
            {
                foreach (T local in source)
                {
                    target.Add(local);
                }
            }
            return target;
        }

        public static IDictionary<T, U> Merge<T, U>(this IDictionary<T, U> target, IDictionary<T, U> source)
        {
            if (!target.IsNull() && !source.IsNullOrEmpty<KeyValuePair<T, U>>())
            {
                foreach (T local in source.Keys)
                {
                    target.Add(local, source[local]);
                }
            }
            return target;
        }

        public static T[] Merge<T>(this T[] a, T[] b)
        {
            Action<T> action = null;
            Action<T> action2 = null;
            List<T> result = new List<T>();
            if (!a.IsNullOrEmpty<T>())
            {
                if (action == null)
                {
                    action = delegate (T t) {
                        result.Add(t);
                    };
                }
                a.IterateByAction<T>(action);
            }
            if (!b.IsNullOrEmpty<T>())
            {
                if (action2 == null)
                {
                    action2 = delegate (T t) {
                        result.Add(t);
                    };
                }
                b.IterateByAction<T>(action2);
            }
            return result.ToArray();
        }

        public static DataTable ListToDataTable<T>(this IList<T> data)
        {
            DataTable table = new DataTable();

            //special handling for value types and string
            if (typeof(T).IsValueType || typeof(T).Equals(typeof(string)))
            {

                DataColumn dc = new DataColumn("Value");
                table.Columns.Add(dc);
                foreach (T item in data)
                {
                    DataRow dr = table.NewRow();
                    dr[0] = item;
                    table.Rows.Add(dr);
                }
            }
            else
            {
                PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
                foreach (PropertyDescriptor prop in properties)
                {
                    table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
                }
                foreach (T item in data)
                {
                    DataRow row = table.NewRow();
                    foreach (PropertyDescriptor prop in properties)
                    {
                        try
                        {
                            row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                        }
                        catch (Exception ex)
                        {
                            row[prop.Name] = DBNull.Value;
                        }
                    }
                    table.Rows.Add(row);
                }
            }
            return table;
        }
    }
}

