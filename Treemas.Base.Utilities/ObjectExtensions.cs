﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Data.SqlTypes;
using System.Data;

namespace Treemas.Base.Utilities
{
    public static class ObjectExtensions
    {
        public static string CreateDirectoryIfNotExists(this string path)
        {
            if (!(string.IsNullOrEmpty(path) || Directory.Exists(path)))
            {
                Directory.CreateDirectory(path);
            }
            return path;
        }

        public static T Define<T>(this T obj, Action<T> action)
        {
            if (!obj.IsNull() & !action.IsNull())
            {
                action(obj);
            }
            return obj;
        }

        public static T FromBytes<T>(this T obj, byte[] bytes)
        {
            object obj2 = null;
            if (!bytes.IsNullOrEmpty<byte>())
            {
                MemoryStream stream = new MemoryStream();
                stream.Write(bytes, 0, bytes.Length);
                obj2 = obj.FromStream<T>(stream);
                stream.Close();
            }
            return (T)obj2;
        }

        public static T FromStream<T>(this T obj, Stream stream)
        {
            object obj2 = null;
            if (stream != null)
            {
                obj2 = new BinaryFormatter().Deserialize(stream);
            }
            return (T)obj2;
        }

        public static IDictionary<string, object> GetPropertyMap(this object obj)
        {
            IDictionary<string, object> dictionary = new Dictionary<string, object>();
            if (obj != null)
            {
                PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(obj);
                foreach (PropertyDescriptor descriptor in properties)
                {
                    dictionary.Add(descriptor.Name, descriptor.GetValue(obj));
                }
            }
            return dictionary;
        }

        public static bool HasProperty(this object obj, string name) =>
            (!obj.IsNull() && (obj.GetType().GetProperty("name") != null));

        public static bool IsNull(this object obj) =>
            (obj == null);

        public static bool IsNullOrEmpty(this string str) =>
            string.IsNullOrEmpty(str);

        public static void MapPropertyValueFrom<T>(this T dest, object source)
        {
            if (!source.IsNull() && !dest.IsNull())
            {
                PropertyInfo[] properties = dest.GetType().GetProperties();
                PropertyInfo[] infoArray2 = source.GetType().GetProperties();
                Predicate<PropertyInfo> criteria = null;
                foreach (PropertyInfo prop in infoArray2)
                {
                    if (criteria == null)
                    {
                        criteria = p => p.Name.Equals(prop.Name);
                    }
                    PropertyInfo info = properties.FindElement<PropertyInfo>(criteria);
                    if (!info.IsNull())
                    {
                        info.SetValue(dest, prop.GetValue(source, null), null);
                    }
                }
            }
        }

        public static bool StringEquals(this string src, string tg) =>
            (!(src.IsNull() || tg.IsNull()) && src.Equals(tg));

        public static bool StringEqualsIgnoreCase(this string src, string tg) =>
            (!(src.IsNull() || tg.IsNull()) && src.Equals(tg, StringComparison.OrdinalIgnoreCase));

        public static byte[] ToBytes(this object obj)
        {
            Stream stream = obj.ToStream<object>();
            if (stream != null)
            {
                byte[] buffer = stream.ToBytes();
                stream.Close();
            }
            return null;
        }

        public static string ToEmptyStringIfNull(this string str)
        {
            if (!string.IsNullOrEmpty(str))
            {
                return str;
            }
            return string.Empty;
        }

        public static DateTime ToSqlCompatibleValue(this DateTime date)
        {
            if (date.IsNull() || (date == DateTime.MinValue))
            {
                return (DateTime)SqlDateTime.MinValue;
            }
            return date;
        }

        public static Stream ToStream<T>(this T obj)
        {
            if (obj != null)
            {
                MemoryStream serializationStream = new MemoryStream();
                new BinaryFormatter().Serialize(serializationStream, obj);
                return serializationStream;
            }
            return null;
        }

        public static string ToDescription(this Enum en) //ext method
        {
            Type type = en.GetType();
            MemberInfo[] memInfo = type.GetMember(en.ToString());
            if (memInfo != null && memInfo.Length > 0)
            {
                object[] attrs = memInfo[0].GetCustomAttributes(
                                              typeof(StringValueAttribute),
                                              false);
                if (attrs != null && attrs.Length > 0)
                    return ((StringValueAttribute)attrs[0]).StringValue;
            }
            return en.ToString();
        }

        public static string ToUrlQueryString(this object obj, string baseUrl)
        {
            StringBuilder builder = new StringBuilder();
            if (!string.IsNullOrEmpty(baseUrl))
            {
                if (baseUrl.EndsWith("/"))
                {
                    builder.Append(baseUrl.Substring(0, baseUrl.Length - 1));
                }
                else
                {
                    builder.Append(baseUrl);
                }
            }
            if (!obj.IsNull())
            {
                PropertyInfo[] properties = obj.GetType().GetProperties();
                if (!properties.IsNullOrEmpty<PropertyInfo>())
                {
                    builder.Append("?");
                    foreach (PropertyInfo info in properties)
                    {
                        object obj2 = info.GetValue(obj, null);
                        if (!obj2.IsNull())
                        {
                            Type propertyType = info.PropertyType;
                            if (propertyType.IsArray)
                            {
                                Array array = (Array)obj2;
                                for (int i = 0; i < array.Length; i++)
                                {
                                    builder.Append($"{info.Name}={Convert.ToString(array.GetValue(i))}&");
                                }
                            }
                            else if (propertyType.IsAssignableFrom(typeof(IEnumerable)) || propertyType.IsAssignableFrom(typeof(ICollection)))
                            {
                                IEnumerator enumerator;
                                if (propertyType.IsAssignableFrom(typeof(IEnumerable)))
                                {
                                    enumerator = ((IEnumerable)obj2).GetEnumerator();
                                }
                                else
                                {
                                    enumerator = ((ICollection)propertyType).GetEnumerator();
                                }
                                enumerator.MoveNext();
                                object current = enumerator.Current;
                                while (!current.IsNull())
                                {
                                    builder.Append($"{info.Name}={Convert.ToString(current)}&");
                                    enumerator.MoveNext();
                                }
                            }
                            else
                            {
                                builder.Append($"{info.Name}={Convert.ToString(obj2)}&");
                            }
                        }
                    }
                    builder.Remove(builder.Length - 1, 1);
                }
            }
            return builder.ToString();
        }


        public static DateTime? ToDateTime(this string date)
        {
            DateTime outputDate = DateTime.MinValue;
            if (!date.IsNullOrEmpty() && DateTime.TryParse(date, out outputDate))
            {
                return outputDate;
            }
            return null;
        }




    }
}

