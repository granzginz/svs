﻿namespace Treemas.Base.Utilities
{
    public interface IAction
    {
        string GetName();
        void SetName(string name);
    }
}

