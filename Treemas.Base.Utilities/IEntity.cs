﻿namespace Treemas.Base.Utilities
{
    public interface IEntity
    {
        long Id { get; }
    }
}
