﻿using System;
using System.Runtime.Serialization;

namespace Treemas.Base.Utilities
{
    [Serializable]
    public class EncryptionException : ApplicationException
    {
        public EncryptionException()
        {
        }

        public EncryptionException(Exception root) : base("An exception occured in encryption", root)
        {
        }

        public EncryptionException(string message) : base(message)
        {
        }

        protected EncryptionException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public EncryptionException(string message, Exception e)
            : base(message, e)
        {
        }
    }
}
