﻿using System.Collections.Generic;

namespace Treemas.Base.Utilities.Queries
{
    public class PagedResult<T>
    {
        public PagedResult(int pageNumber, int itemsPerPage)
        {
            this.ItemsPerPage = itemsPerPage;
            this.PageNumber = pageNumber;
        }
        public int TotalItems { get; set; }
        public IEnumerable<T> Items { get; set; }
        public int ItemsPerPage { get; set; }
        public int PageNumber { get; set; }
        public bool isNextPage()
        {
            return TotalItems > ItemsPerPage * PageNumber;
        }
        public bool isPreviousPage()
        {
            return PageNumber > 1;
        }
        public int GetTotalPage()
        {
            return TotalItems / ItemsPerPage;
        }
    }
}
