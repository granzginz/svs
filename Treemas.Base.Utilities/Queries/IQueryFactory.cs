﻿
namespace Treemas.Base.Utilities.Queries
{
    public interface IQueryFactory
    {
        TQuery CreateQuery<TQuery>() where TQuery : IQuery;
    }
}
