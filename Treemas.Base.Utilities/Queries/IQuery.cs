﻿
namespace Treemas.Base.Utilities.Queries
{
    public interface IQuery
    {
    }
    public interface IQuery<TResult> : IQuery
    {
        TResult Execute();
    }
    
}
