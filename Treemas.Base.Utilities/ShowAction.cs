﻿using System;

namespace Treemas.Base.Utilities
{
    public class ShowAction : BaseAction, IShowAction, IAction
    {
        public ShowAction(string name, Action<object> action) : base(name)
        {
            this._Action = action;
        }

        public void Show(object param)
        {
            if (this._Action != null)
            {
                this._Action(param);
            }
        }

        protected Action<object> _Action { get; private set; }
    }
}

