﻿using System;

namespace Treemas.Base.Utilities
{
    public class CleanAction : BaseAction, ICleanAction, IAction
    {
        public CleanAction(string name, Action<object> action) : base(name)
        {
            this._Action = action;
        }

        public void Clean(object param)
        {
            if (this._Action != null)
            {
                this._Action(param);
            }
        }

        protected Action<object> _Action { get; private set; }

        public string Name { get; private set; }
    }
}

