﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Treemas.Base.Utilities
{
    public abstract class Entity : Entity<long>, IEntity
    {
        public Entity() : this(0L) { }
        public Entity(long id)
            : base(id)
        { }
    }

    public abstract class Entity<TId>
    {
        protected abstract IEnumerable<object>
           GetAttributesToIncludeInEqualityCheck();

        private TId id;
        public Entity(TId id)
        {
            this.id = id;
        }
        protected static readonly DateTime NullDate = new DateTime(0x76c, 1, 1);
        public virtual TId Id { get { return id; } protected set { id = value; } }
        
        public virtual int RecordVersion { get; protected set; }

        public override bool Equals(object obj)
        {
            return Equals(obj as Entity<TId>);
        }

        private static bool isTransient(Entity<TId> obj)
        {
            return obj != null &&
            Equals(obj.Id, default(TId));
        }

        private Type getUnproxiedType()
        {
            return GetType();
        }

        public virtual bool Equals(Entity<TId> other)
        {
            if (other == null) return false;

            if (ReferenceEquals(this, other)) return true;

            if (isTransient(this) || isTransient(other) || !Equals(Id, other.Id)) return false;

            var otherType = other.getUnproxiedType();
            var thisType = getUnproxiedType();

            if (!(thisType.IsAssignableFrom(otherType) || otherType.IsAssignableFrom(thisType))) return false;

            return GetAttributesToIncludeInEqualityCheck().SequenceEqual(
           other.GetAttributesToIncludeInEqualityCheck());
        }

        public override int GetHashCode()
        {
            return Equals(Id, default(TId)) ? base.GetHashCode() : Id.GetHashCode();
        }
        public static bool operator ==(Entity<TId> entity1, Entity<TId> entity2)
        {
            if ((object)entity1 == null && (object)entity2 == null) { return true; }
            if ((object)entity1 == null || (object)entity2 == null) { return false; }
            return entity1.Id.ToString() == entity2.Id.ToString();
        }

        public static bool operator !=(Entity<TId> entity1, Entity<TId> entity2)
        {
            return (!(entity1 == entity2));
        }
        protected abstract void validate();

    }
}