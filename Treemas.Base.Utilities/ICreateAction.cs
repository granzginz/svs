﻿namespace Treemas.Base.Utilities
{
    public interface ICreateAction : IAction
    {
        void Create(object param);
    }
}

