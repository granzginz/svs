﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;

namespace Treemas.Base.Utilities
{
    public class FtpClient
    {
        public FtpClient()
        {
            this.ReadingBufferSize = 0x400;
        }

        private void _CheckFtpAddress()
        {
            if (string.IsNullOrEmpty(this.FtpAddress))
            {
                throw new Exception("Undefined FTP Address");
            }
        }

        private FtpWebRequest _CreateFtpRequest(string url, string method)
        {
            if (string.IsNullOrEmpty(url))
            {
                return null;
            }
            FtpWebRequest request = (FtpWebRequest) WebRequest.Create(url);
            request.Method = method;
            if (!string.IsNullOrEmpty(this.Username))
            {
                request.Credentials = new NetworkCredential(this.Username, string.IsNullOrEmpty(this.Password) ? string.Empty : this.Password);
            }
            return request;
        }

        private string _GetAbsoluteUrl(string path)
        {
            string ftpAddress = this.FtpAddress;
            if (!string.IsNullOrEmpty(path))
            {
                if (path.StartsWith("/"))
                {
                    ftpAddress = ftpAddress + path;
                }
                else
                {
                    ftpAddress = ftpAddress + "/" + path;
                }
            }
            if (ftpAddress.EndsWith("/"))
            {
                return ftpAddress.Substring(0, ftpAddress.Length - 1);
            }
            return ftpAddress;
        }

        public void CreateDirectory(string path)
        {
            if (!string.IsNullOrEmpty(path))
            {
                this._CheckFtpAddress();
                string str = path;
                if (str.StartsWith("/"))
                {
                    str = str.Substring(1, path.Length);
                }
                string[] strArray = str.Split(new char[] { '/' });
                string parentPath = string.Empty;
                for (int i = 0; i < strArray.Length; i++)
                {
                    bool flag;
                    if (i > 0)
                    {
                        parentPath = parentPath + "/" + strArray[i - 1];
                        flag = this.HasDirectory(parentPath, strArray[i]);
                    }
                    else
                    {
                        parentPath = null;
                        flag = this.HasDirectory(null, strArray[i]);
                    }
                    if (!flag)
                    {
                        FtpWebRequest request = this._CreateFtpRequest(this._GetAbsoluteUrl(parentPath + "/" + strArray[i]), "MKD");
                        FtpWebResponse response = null;
                        try
                        {
                            response = (FtpWebResponse) request.GetResponse();
                        }
                        finally
                        {
                            if (response != null)
                            {
                                response.Close();
                            }
                        }
                    }
                }
            }
        }

        public TemporaryFileStream Download(string path)
        {
            string str = string.Concat(new object[] { Path.GetTempPath(), Path.DirectorySeparatorChar, Path.GetFileNameWithoutExtension(path), "_", Guid.NewGuid().ToString(), Path.GetExtension(path) });
            if (System.IO.File.Exists(str))
            {
                System.IO.File.Delete(str);
            }
            FileStream output = System.IO.File.Create(str, this.ReadingBufferSize);
            this.Download(path, output);
            output.Flush();
            output.Close();
            return new TemporaryFileStream(str, FileMode.Open);
        }

        public void Download(string path, FileStream output)
        {
            if (!string.IsNullOrEmpty(path) && (output != null))
            {
                FtpWebRequest request = this._CreateFtpRequest(this._GetAbsoluteUrl(path), "RETR");
                FtpWebResponse response = null;
                try
                {
                    response = (FtpWebResponse) request.GetResponse();
                    Stream responseStream = response.GetResponseStream();
                    responseStream.CopyTo(output);
                    responseStream.Close();
                }
                finally
                {
                    if (response != null)
                    {
                        response.Close();
                    }
                }
            }
        }

        public void Download(string path, string outputPath)
        {
            string str = outputPath;
            if (str.EndsWith("/"))
            {
                str = str + Path.GetFileName(path);
            }
            else
            {
                str = str + Path.DirectorySeparatorChar + Path.GetFileName(path);
            }
            if (System.IO.File.Exists(outputPath))
            {
                System.IO.File.Delete(outputPath);
            }
            FileStream output = System.IO.File.Create(str);
            this.Download(path, output);
            output.Flush();
            output.Close();
        }

        public bool HasDirectory(string parentPath, string directoryName)
        {
            if (!string.IsNullOrEmpty(directoryName))
            {
                IList<string> list = this.ListDirectories(parentPath);
                if (!list.IsNullOrEmpty<string>())
                {
                    foreach (string str in list)
                    {
                        if (str.EndsWith(directoryName))
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        public IList<string> ListDirectories(string path)
        {
            FtpWebRequest request = this._CreateFtpRequest(this._GetAbsoluteUrl(path), "NLST");
            FtpWebResponse response = null;
            IList<string> list = new List<string>();
            try
            {
                response = (FtpWebResponse) request.GetResponse();
                StreamReader reader = new StreamReader(response.GetResponseStream());
                while (!reader.EndOfStream)
                {
                    list.Add(reader.ReadLine());
                }
                reader.Close();
            }
            finally
            {
                if (response != null)
                {
                    response.Close();
                }
            }
            if (!list.IsNullOrEmpty<string>())
            {
                return list;
            }
            return null;
        }

        public void Upload(FileStream stream, string remotePath)
        {
            this._CheckFtpAddress();
            this.CreateDirectory(remotePath);
            string fileName = remotePath;
            if (!string.IsNullOrEmpty(fileName))
            {
                if (fileName.EndsWith("/"))
                {
                    fileName = fileName + Path.GetFileName(stream.Name);
                }
                else
                {
                    fileName = fileName + "/" + Path.GetFileName(stream.Name);
                }
            }
            else
            {
                fileName = Path.GetFileName(stream.Name);
            }
            FtpWebRequest request = this._CreateFtpRequest(this._GetAbsoluteUrl(fileName), "STOR");
            Stream destination = null;
            try
            {
                destination = request.GetRequestStream();
                stream.CopyTo(destination);
            }
            finally
            {
                if (!destination.IsNull())
                {
                    destination.Close();
                }
            }
        }

        public void Upload(string filePath, string remotePath)
        {
            FileStream stream = System.IO.File.OpenRead(filePath);
            try
            {
                this.Upload(stream, remotePath);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Close();
                }
            }
        }

        public string FtpAddress { get; set; }

        public string Password { get; set; }

        public int ReadingBufferSize { get; set; }

        public string Username { get; set; }
    }
}

