﻿using System.Collections.Generic;
using System.Linq;

namespace Treemas.Base.Configuration
{
    public class ConfigurationCabinet : IConfigurationCabinet
    {
        private IDictionary<string, IConfigurationBinder> binderMap;
        private string label;

        public ConfigurationCabinet(string label)
        {
            this.label = label;
            this.binderMap = new Dictionary<string, IConfigurationBinder>();
        }

        public void AddBinder(IConfigurationBinder binder)
        {
            string label = binder.GetLabel();
            if (this.binderMap.ContainsKey(label))
            {
                this.binderMap[label] = binder;
            }
            else
            {
                this.binderMap.Add(label, binder);
            }
        }

        public IConfigurationBinder GetBinder(string label)
        {
            if (this.binderMap.ContainsKey(label))
            {
                return this.binderMap[label];
            }
            return null;
        }

        public IConfigurationBinder[] GetBinderByConfigurationKey(string key)
        {
            if (this.binderMap.Count > 0)
            {
                IList<IConfigurationBinder> source = new List<IConfigurationBinder>();
                foreach (IConfigurationBinder binder in this.binderMap.Values)
                {
                    ConfigurationItem[] configurations = binder.GetConfigurations();
                    if (configurations != null)
                    {
                        for (int i = configurations.Length - 1; i >= 0; i--)
                        {
                            ConfigurationItem item = configurations[i];
                            if (item.Key.Equals(key))
                            {
                                source.Add(binder);
                                break;
                            }
                        }
                    }
                }
                if (source.Count > 0)
                {
                    return source.ToArray<IConfigurationBinder>();
                }
            }
            return null;
        }

        public IConfigurationBinder[] GetBinders()
        {
            if (this.binderMap.Count > 0)
            {
                return this.binderMap.Values.ToArray<IConfigurationBinder>();
            }
            return null;
        }

        public string GetLabel() => 
            this.label;
    }
}

