﻿using System.Collections.Generic;

namespace Treemas.Base.Configuration
{
    public interface IConfigurationBinder
    {
        void AddConfiguration(ConfigurationItem configuration);
        ConfigurationItem GetConfiguration(string key);
        ConfigurationItem[] GetConfigurations();
        IList<string> GetKeys();
        string GetLabel();
        void Load();
        void MarkAllAsPersisted();
        void MarkAllAsTransient();
        void MarkAsTransient(string key);
        void RemoveConfiguration(string key);
        void RemoveNonPersistedConfigurations();
        void Save();
        void UnmarkAsTransient(string key);
    }
}

