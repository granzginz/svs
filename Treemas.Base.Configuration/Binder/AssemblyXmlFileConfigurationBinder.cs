﻿using System;
using System.IO;
using System.Reflection;

namespace Treemas.Base.Configuration.Binder
{
    public class AssemblyXmlFileConfigurationBinder : BaseXmlFileConfigurationBinder
    {
        private Assembly assembly;
        private string rootNamespace;

        public AssemblyXmlFileConfigurationBinder(string label, string rootNamespace, Assembly assembly) : base(label)
        {
            this.rootNamespace = rootNamespace;
            this.assembly = assembly;
        }

        public override void Load()
        {
            if (!string.IsNullOrEmpty(this.rootNamespace) && (this.assembly != null))
            {
                string name = this.rootNamespace + "." + base.GetLabel() + ".config";
                Stream manifestResourceStream = this.assembly.GetManifestResourceStream(name);
                base.Load(manifestResourceStream);
            }
        }

        public override void Save()
        {
            throw new NotSupportedException();
        }
    }
}

