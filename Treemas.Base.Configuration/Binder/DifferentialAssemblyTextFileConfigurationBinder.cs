﻿using System;
using System.IO;
using System.Reflection;

namespace Treemas.Base.Configuration.Binder
{
    public class DifferentialAssemblyTextFileConfigurationBinder : BaseTextFileConfigurationBinder
    {
        private Assembly assembly;
        private string marker;
        private string namespacePath;

        public DifferentialAssemblyTextFileConfigurationBinder(string label, string marker, string rootNamespace, Assembly assembly) : base(label)
        {
            this.namespacePath = rootNamespace;
            this.assembly = assembly;
            this.marker = marker;
        }

        public override void Load()
        {
            if (!string.IsNullOrEmpty(this.namespacePath) && (this.assembly != null))
            {
                string name = this.namespacePath + "." + base.GetLabel() + ".config";
                if (!string.IsNullOrEmpty(this.marker))
                {
                    name = this.namespacePath + "." + base.GetLabel() + "-" + this.marker + ".config";
                }
                Stream manifestResourceStream = this.assembly.GetManifestResourceStream(name);
                base.Load(manifestResourceStream);
            }
        }

        public override void Save()
        {
            throw new NotSupportedException();
        }
    }
}

