﻿using System.IO;

namespace Treemas.Base.Configuration.Binder
{
    public class TextFileConfigurationBinder : BaseTextFileConfigurationBinder
    {
        private string path;

        public TextFileConfigurationBinder(string label, string path) : base(label)
        {
            this.path = path;
        }

        public override void Load()
        {
            if (!string.IsNullOrEmpty(this.path))
            {
                string path = this.path + @"\" + base.GetLabel() + ".config";
                if (File.Exists(path))
                {
                    Stream stream = File.OpenRead(path);
                    base.Load(stream);
                }
            }
        }

        public override void Save()
        {
            if (!string.IsNullOrEmpty(this.path))
            {
                string path = this.path + @"\" + base.GetLabel() + ".config";
                File.WriteAllText(path, string.Empty);
                Stream stream = File.OpenWrite(path);
                base.Save(stream);
            }
        }
    }
}

