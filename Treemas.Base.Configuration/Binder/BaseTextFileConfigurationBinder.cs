﻿using System.Collections.Generic;
using System.IO;
using Treemas.Base.Configuration;

namespace Treemas.Base.Configuration.Binder
{
    public abstract class BaseTextFileConfigurationBinder : ConfigurationBinder
    {
        public BaseTextFileConfigurationBinder(string label) : base(label)
        {
        }

        public override void Load()
        {
        }

        protected void Load(Stream stream)
        {
            if (stream != null)
            {
                StreamReader reader = new StreamReader(stream);
                try
                {
                    ConfigurationItem configuration = null;
                    ConfigurationItem item2 = null;
                    while (!reader.EndOfStream)
                    {
                        string str = reader.ReadLine().Trim();
                        if (!string.IsNullOrEmpty(str))
                        {
                            if (str.StartsWith("."))
                            {
                                string[] strArray = str.Split(new char[] { '=' });
                                if ((strArray != null) && (strArray.Length > 0))
                                {
                                    string key = strArray[0].Substring(1, strArray[0].Length - 1).Trim();
                                    string str3 = strArray[1].Trim();
                                    if (configuration == null)
                                    {
                                        configuration = new ConfigurationItem();
                                    }
                                    item2 = base.GetConfiguration(key);
                                    if (item2 != null)
                                    {
                                        configuration = item2;
                                    }
                                    configuration.Key = key;
                                    configuration.Value = str3;
                                    base.AddConfiguration(configuration);
                                    configuration = null;
                                }
                            }
                            else if (str.StartsWith("#"))
                            {
                                configuration = new ConfigurationItem {
                                    Description = str.Substring(1, str.Length - 1).Trim()
                                };
                            }
                        }
                    }
                }
                finally
                {
                    reader.Close();
                    stream.Close();
                }
            }
        }

        public override void Save()
        {
        }

        protected void Save(Stream stream)
        {
            if (stream != null)
            {
                StreamWriter writer = new StreamWriter(stream);
                try
                {
                    ConfigurationItem[] configurations = base.GetConfigurations();
                    if (configurations != null)
                    {
                        IList<ConfigurationItem> list = new List<ConfigurationItem>();
                        foreach (ConfigurationItem item in configurations)
                        {
                            if (item.Transient)
                            {
                                list.Add(item);
                            }
                            else
                            {
                                writer.WriteLine($"# {item.Description}");
                                writer.WriteLine($".{item.Key} = {item.Value}");
                                writer.WriteLine();
                            }
                        }
                        foreach (ConfigurationItem item2 in list)
                        {
                            base.RemoveConfiguration(item2.Key);
                        }
                    }
                }
                finally
                {
                    writer.Close();
                    stream.Close();
                }
            }
        }
    }
}

