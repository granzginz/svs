﻿using System;
using System.Collections.Generic;

namespace Treemas.Base.Configuration
{
    public class ConfigurationMasterSlaveBinder : IConfigurationBinder
    {
        private string label;

        public ConfigurationMasterSlaveBinder(string label)
        {
            this.label = label;
            this.Mode = ConfigurationMasterSlaveBinderMode.Master;
        }

        public void AddConfiguration(ConfigurationItem configuration)
        {
            if (this.Mode == ConfigurationMasterSlaveBinderMode.Master)
            {
                if (this.Master != null)
                {
                    this.Master.AddConfiguration(configuration);
                }
            }
            else if (this.Slave != null)
            {
                this.Slave.AddConfiguration(configuration);
            }
        }

        public ConfigurationItem GetConfiguration(string key)
        {
            ConfigurationItem configuration = null;
            if (this.Master != null)
            {
                configuration = this.Master.GetConfiguration(key);
            }
            if ((configuration == null) && (this.Slave != null))
            {
                configuration = this.Slave.GetConfiguration(key);
            }
            return configuration;
        }

        public ConfigurationItem[] GetConfigurations()
        {
            List<ConfigurationItem> list = new List<ConfigurationItem>();
            if (this.Slave != null)
            {
                ConfigurationItem[] configurations = this.Slave.GetConfigurations();
                if (configurations != null)
                {
                    list.AddRange(configurations);
                }
            }
            if (this.Master != null)
            {
                ConfigurationItem[] collection = this.Master.GetConfigurations();
                if (collection != null)
                {
                    List<ConfigurationItem> list2 = new List<ConfigurationItem>();
                    foreach (ConfigurationItem item in collection)
                    {
                        string key = item.Key;
                        foreach (ConfigurationItem item2 in list)
                        {
                            if (item2.Key.Equals(key))
                            {
                                list2.Add(item2);
                            }
                        }
                    }
                    foreach (ConfigurationItem item3 in list2)
                    {
                        list.Remove(item3);
                    }
                    list.AddRange(collection);
                }
            }
            if (list.Count > 0)
            {
                return list.ToArray();
            }
            return null;
        }

        public IList<string> GetKeys()
        {
            List<string> list = new List<string>();
            if (this.Master != null)
            {
                IList<string> keys = this.Master.GetKeys();
                if (keys != null)
                {
                    list.AddRange(keys);
                }
            }
            if (this.Slave != null)
            {
                IList<string> list3 = this.Slave.GetKeys();
                if (list3 != null)
                {
                    foreach (string str in list3)
                    {
                        if (!list.Contains(str))
                        {
                            list.Add(str);
                        }
                    }
                }
            }
            if (list.Count > 0)
            {
                return list;
            }
            return null;
        }

        public string GetLabel() => 
            this.label;

        public void Load()
        {
            if (this.Mode == ConfigurationMasterSlaveBinderMode.Master)
            {
                if (this.Master != null)
                {
                    this.Master.Load();
                }
            }
            else if (this.Slave != null)
            {
                this.Slave.Load();
            }
        }

        public void MarkAllAsPersisted()
        {
            throw new NotImplementedException();
        }

        public void MarkAllAsTransient()
        {
            throw new NotImplementedException();
        }

        public void MarkAsTransient(string key)
        {
            throw new NotImplementedException();
        }

        public void RemoveConfiguration(string key)
        {
            throw new NotImplementedException();
        }

        public void RemoveNonPersistedConfigurations()
        {
            throw new NotImplementedException();
        }

        public void Save()
        {
            if (this.Mode == ConfigurationMasterSlaveBinderMode.Master)
            {
                if (this.Master != null)
                {
                    this.Master.Save();
                }
            }
            else if (this.Slave != null)
            {
                this.Slave.Save();
            }
        }

        public void UnmarkAsTransient(string key)
        {
            throw new NotImplementedException();
        }

        public IConfigurationBinder Master { get; set; }

        public ConfigurationMasterSlaveBinderMode Mode { get; set; }

        public IConfigurationBinder Slave { get; set; }
    }
}

