﻿using System;
using System.IO;
using System.Runtime.Serialization;
using Treemas.Base.Utilities;

namespace Treemas.Base.Web.Service
{
    public class StreamedServiceRuntimeParameter : ServiceRuntimeParameter, IDisposable
    {
        public void Dispose()
        {
            if (!this.Data.IsNull())
            {
                this.Data.Close();
            }
        }

        [DataMember]
        public Stream Data { get; set; }
    }
}

