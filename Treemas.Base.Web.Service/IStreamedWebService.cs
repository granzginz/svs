﻿using System;

namespace Treemas.Base.Web.Service
{
    public interface IStreamedWebService : IDisposable
    {
        StreamedServiceRuntimeResult Execute(StreamedServiceRuntimeParameter parameter);
    }
}

