﻿using System;

namespace Treemas.Base.Web.Service
{
    public enum ServiceStatus
    {
        Success,
        Success_With_Warning,
        Ready,
        Continue,
        Confirmed,
        OK,
        Approved,
        Denied,
        Finished,
        Busy,
        Aborted,
        Error,
        Unavailable
    }
}

