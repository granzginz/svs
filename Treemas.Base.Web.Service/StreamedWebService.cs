﻿using System;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Web;
using Treemas.Base.Utilities;

namespace Treemas.Base.Web.Service
{
    [AspNetCompatibilityRequirements(RequirementsMode=AspNetCompatibilityRequirementsMode.Allowed)]
    public class StreamedWebService : IStreamedWebService, IDisposable
    {
        public StreamedWebService()
        {
            this.Commands = new ServiceCommandPool();
            HttpContext current = HttpContext.Current;
            if (current != null)
            {
                current.Response.BufferOutput = false;
            }
        }

        private StreamedServiceRuntimeResult _ExecuteCommand(StreamedServiceRuntimeParameter parameter)
        {
            if (!parameter.IsNull() && !string.IsNullOrEmpty(parameter.Command))
            {
                IServiceCommand command = this.Commands.GetCommand(parameter.Command);
                if (!command.IsNull() && (command is StreamedServiceCommand))
                {
                    StreamedServiceResult result = ((StreamedServiceCommand) command).Execute(StreamedServiceParameter.Create(parameter));
                    if (result != null)
                    {
                        return result.ToRuntime();
                    }
                }
            }
            return null;
        }

        public void Dispose()
        {
            this.Commands.Clear();
        }

        public StreamedServiceRuntimeResult Execute(StreamedServiceRuntimeParameter parameter)
        {
            StreamedServiceRuntimeResult result2;
            try
            {
                StreamedServiceRuntimeResult result = new StreamedServiceRuntimeResult();
                if (!parameter.IsNull())
                {
                    result = this._ExecuteCommand(parameter);
                }
                result2 = result;
            }
            catch (Exception exception)
            {
                string message;
                if (exception.InnerException != null)
                {
                    message = $"{exception.Message}. {exception.InnerException.Message}";
                }
                else
                {
                    message = exception.Message;
                }
                throw new FaultException(message);
            }
            return result2;
        }

        protected ServiceCommandPool Commands { get; set; }
    }
}

