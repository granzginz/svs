﻿using System;
using System.ServiceModel;

namespace Treemas.Base.Web.Service
{
    [ServiceContract]
    public interface IWebService : IDisposable
    {
        [OperationContract]
        ServiceRuntimeResult Execute(ServiceRuntimeParameter parameter);
    }
}

