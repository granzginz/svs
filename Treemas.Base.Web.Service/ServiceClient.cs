﻿using System;
using System.ServiceModel.Channels;
using Treemas.Base.Utilities;

namespace Treemas.Base.Web.Service
{
    public class ServiceClient : IDisposable
    {
        public ServiceClient(IWebService service)
        {
            this.Service = service;
        }

        public void Dispose()
        {
            if (!this.Service.IsNull())
            {
                ((IChannel) this.Service).Close();
            }
        }

        public virtual ServiceResult Execute(ServiceParameter parameter)
        {
            if (!this.Service.IsNull() && !parameter.IsNull())
            {
                ServiceRuntimeResult result = this.Service.Execute(parameter.ToRuntime());
                if (!result.IsNull())
                {
                    return ServiceResult.Create(result);
                }
            }
            return null;
        }

        public IWebService Service { get; set; }
    }
}

