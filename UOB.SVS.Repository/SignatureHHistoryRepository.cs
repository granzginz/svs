﻿using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using LinqSpecs;
using NHibernate.Linq;
using NHibernate.Criterion;
using Treemas.Base.Repository;
using UOB.SVS.Model;
using UOB.SVS.Repository;
using UOB.SVS.Interface;
using Treemas.Base.Utilities.Queries;
using System;

namespace UOB.SVS.Repository
{
    public class SignatureHHistoryRepository : RepositoryController<SignatureHHistory>, ISignatureHHistoryRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public SignatureHHistoryRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }
        public SignatureHHistory getSignatureHHistory(string id)
        {
            return transact(() => session.QueryOver<SignatureHHistory>()
                                                .Where(f => f.AccountNo == id).SingleOrDefault());
        }

        public IList<SignatureHHistory> getSignatureHHistorys(IList<string> SignatureHHistoryIds)
        {
            return transact(() =>
                   session.QueryOver<SignatureHHistory>()
                          .WhereRestrictionOn(val => val.AccountNo)
                          .IsIn(SignatureHHistoryIds.ToArray()).List());
        }
        public bool IsDuplicate(string accno)
        {
            int rowCount = transact(() => session.QueryOver<SignatureHHistory>()
                                                .Where(f =>f.AccountNo == accno).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public PagedResult<SignatureHHistory> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {
            PagedResult<SignatureHHistory> paged = new PagedResult<SignatureHHistory>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.Query<SignatureHHistory>()
                                                     .OrderByDescending(GetOrderByExpression<SignatureHHistory>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() => session.Query<SignatureHHistory>()
                                                     .OrderBy(GetOrderByExpression<SignatureHHistory>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = Count;
            return paged;
        }
        public PagedResult<SignatureHHistory> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            Specification<SignatureHHistory> specification;

            switch (searchColumn)
            {
                case "AccountNumber":
                    specification = new AdHocSpecification<SignatureHHistory>(s => s.AccountNo.Contains(searchValue));
                    break;
                case "AccountName":
                    specification = new AdHocSpecification<SignatureHHistory>(s => s.AccountName.Contains(searchValue));
                    break;
                case "Note":
                    specification = new AdHocSpecification<SignatureHHistory>(s => s.Note.Contains(searchValue));
                    break;                
                default:
                    specification = new AdHocSpecification<SignatureHHistory>(s => s.AccountNo.Contains(searchValue));
                    break;
            }

            PagedResult<SignatureHHistory> paged = new PagedResult<SignatureHHistory>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.Query<SignatureHHistory>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<SignatureHHistory>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() =>
                          session.Query<SignatureHHistory>()
                                 .Where(specification.ToExpression())
                                 .OrderBy(GetOrderByExpression<SignatureHHistory>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }


            paged.TotalItems = transact(() =>
                         session.Query<SignatureHHistory>()
                                .Where(specification.ToExpression()).Count());

            paged.TotalItems = Count;
            return paged;
        }
        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }

    }
}
