﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using UOB.SVS.Model;

namespace Treemas.Credential.Repository.Mapping
{
    public class DocumentTypeMapping : ClassMapping<DocumentType>
    {
        public DocumentTypeMapping()
        {
            this.Table("DOCUMENT_TYPE");

            Id<long>(x => x.Id, map => { map.Column("ID"); map.Generator(Generators.Identity); });
            Property<string>(x => x.DocType, map => { map.Column("DOC_TYPE"); });
            Property<string>(x => x.Description, map => { map.Column("DESCRIPTION"); });
            Property<DateTime?>(x => x.ChangedDate, map => { map.Column("CHANGED_DATE"); });
            Property<DateTime?>(x => x.CreatedDate, map => { map.Column("CREATED_DATE"); map.Update(false); });
            Property<string>(x => x.ChangedBy, map => { map.Column("CHANGED_BY"); });
            Property<string>(x => x.CreatedBy, map => { map.Column("CREATED_BY"); map.Update(false); });
        }
    }
}
