﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;
using UOB.SVS.Model;

namespace UOB.SVS.Repository.Mapping
{
    public class VFunctionFeatureMapping:ClassMapping<VFunctionFeature>
    {
        public VFunctionFeatureMapping()
        {
            this.Table("FunctionFeatureView");
            Id<long>(x => x.Id,
                map =>
                {
                    map.Column("ID");
                    map.Generator(Generators.Identity);
                });

            Property<long>(x => x.FunctionId,
                map =>
                {
                    map.Column("FUNCTION_ID");
                });

            Property<string>(x => x.FunctionName,
                map =>
                {
                    map.Column("FUNCTION_NAME");
                });

            Property<long>(x => x.FeatureId,
                map =>
                {
                    map.Column("FEATURE_ID");
                });
            Property<string>(x => x.FeatureName,
                map =>
                {
                    map.Column("FEATURE_NAME");
                });

            Property<DateTime?>(x => x.ChangedDate,
                map =>
                {
                    map.Column("CHANGED_DATE");
                });

            Property<DateTime?>(x => x.CreatedDate,
                map =>
                {
                    map.Column("CREATED_DATE");
                    map.Update(false);
                });

            Property<string>(x => x.ChangedBy,
                map =>
                {
                    map.Column("CHANGED_BY");
                });

            Property<string>(x => x.CreatedBy,
                map =>
                {
                    map.Column("CREATED_BY");
                    map.Update(false);
                });
        }
    }
}
