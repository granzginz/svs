﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using UOB.SVS.Model;
using NHibernate;

namespace UOB.SVS.Repository.Mapping
{
    public class SignatureDSignMapping : ClassMapping<SignatureDSign>
    {
        public SignatureDSignMapping()
        {
            Schema("dbo");
            this.Table("SIGNATURE_DETAIL_SIGN");
            //Lazy(true);
            Id<long>(x => x.Id, map => { map.Column("ID"); map.Generator(Generators.Identity); });
            Property<string>(x => x.AccountNo, map => { map.Column("ACCOUNT_NUMBER");});
            Property<string>(x => x.Signature, map => { map.Column("SIGNATURE"); map.Length(4000000); });
            Property<string>(x => x.ImageType, map => { map.Column("ImageType"); });
        }
    }
}
