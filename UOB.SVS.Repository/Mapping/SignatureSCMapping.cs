﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using UOB.SVS.Model;
using NHibernate;

namespace UOB.SVS.Repository.Mapping
{
    public class SignatureSCMapping : ClassMapping<SignatureSCMaster>
    {
        public SignatureSCMapping()
        {
            Schema("dbo");
            this.Table("SIGNATURE");
            //Lazy(true);
            Id<long>(x => x.Id, map => { map.Column("ID"); });
            Property<string>(x => x.AccountNo, map => { map.Column("ACCOUNT_NUMBER"); });
            Property<string>(x => x.CustomerName, map => { map.Column("CUSTOMER_NAME"); });
            Property<string>(x => x.Note, map => { map.Column("NOTE"); });

            Property<string>(x => x.Signature, map => { map.Column("SIGNATURE"); });
            Property<string>(x => x.KTP, map => { map.Column("KTP"); });
            Property<string>(x => x.NoteKTP, map => { map.Column("NOTE_KTP"); });
            Property<string>(x => x.Pengenal, map => { map.Column("PENGENAL"); });
            Property<string>(x => x.AddBy, map => { map.Column("ADD_BY"); });
            Property<DateTime?>(x => x.DateAdd, map => { map.Column("DATE_ADD"); });

            Property<string>(x => x.UpdBy, map => { map.Column("UPD_BY"); });
            Property<DateTime?>(x => x.DateUpd, map => { map.Column("DATE_UPD"); });
            Property<string>(x => x.DelBy, map => { map.Column("DEL_BY"); });
            Property<DateTime?>(x => x.DateDel, map => { map.Column("DATE_DEL"); });
            Property<string>(x => x.AppAddBy, map => { map.Column("APP_ADD_BY"); });
            Property<DateTime?>(x => x.DateAppAdd, map => { map.Column("DATE_APP_ADD"); });

            Property<string>(x => x.AppUpBy, map => { map.Column("APP_UP_BY"); });
            Property<DateTime?>(x => x.DateAppUpd, map => { map.Column("DATE_APP_UPD"); });
            Property<string>(x => x.AppDelBy, map => { map.Column("APP_DEL_BY"); });
            Property<DateTime?>(x => x.DateAppDel, map => { map.Column("DATE_APP_DEL"); });
            Property<string>(x => x.NotAppAddBy, map => { map.Column("NOT_APP_ADD_BY"); });
            Property<DateTime?>(x => x.DateNotAppAdd, map => { map.Column("DATE_NOT_APP_ADD"); });

            Property<string>(x => x.NotAppUpdBy, map => { map.Column("NOT_APP_UPD_BY"); });
            Property<DateTime?>(x => x.DateNotAppUpd, map => { map.Column("DATE_NOT_APP_UPD"); });
            Property<string>(x => x.NotAppDelBy, map => { map.Column("NOT_APP_DEL_BY"); });
            Property<DateTime?>(x => x.DateNotAppDell, map => { map.Column("DATE_NOT_APP_DELL"); });
            Property<string>(x => x.StatusFlag, map => { map.Column("STATUS_FLAG"); });
            Property<string>(x => x.StatusAktif, map => { map.Column("STATUS_AKTIF"); });

            Property<string>(x => x.OldNote, map => { map.Column("OLD_NOTE"); });
            Property<string>(x => x.OldNoteKTP, map => { map.Column("OLD_NOTE_KTP"); });
            Property<string>(x => x.FSignature, map => { map.Column("FSIGNATURE"); });
            Property<string>(x => x.FKTP, map => { map.Column("FKTP"); });
            Property<string>(x => x.FNote, map => { map.Column("FNOTE"); });
            Property<string>(x => x.ZSignature, map => { map.Column("ZSIGNATURE"); });

            Property<string>(x => x.ZOldSignature, map => { map.Column("ZOLD_SIGNATURE"); });
            Property<string>(x => x.ZKTP, map => { map.Column("ZKTP"); });
            Property<string>(x => x.ZOldKTP, map => { map.Column("ZOLD_KTP"); });
            Property<string>(x => x.OldSignature, map => { map.Column("OLD_SIGNATURE"); });
            Property<string>(x => x.OldKTP, map => { map.Column("OLD_KTP"); });
            Property<string>(x => x.OldAccNumber, map => { map.Column("OLD_ACC_NUMBER"); });

            Property<string>(x => x.ApprovedBy, map => { map.Column("APPROVED_BY"); });
            Property<string>(x => x.ApprovalType, map => { map.Column("APPROVAL_TYPE"); });
            Property<DateTime?> (x => x.ApprovedDate, map => { map.Column("APPROVED_DATE"); });
            Property<string>(x => x.ApprovalStatus, map => { map.Column("APPROVAL_STATUS"); });
        }
    }
}
