﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using UOB.SVS.Model;
using NHibernate;

namespace UOB.SVS.Repository.Mapping
{
    public class SignatureHHistoryMapping : ClassMapping<SignatureHHistory>
    {
        public SignatureHHistoryMapping()
        {
            Schema("dbo");
            this.Table("SIGNATURE_HEADER_HISTORY");
            //Lazy(true);
            Id<long>(x => x.RecordId, map => { map.Column("RecordId"); map.Generator(Generators.Identity); });
            Property<string>(x => x.AccountNo, map => { map.Column("AccountNo"); });
            Property<string>(x => x.AccountName, map => { map.Column("AccountName"); });
            Property<string>(x => x.AccountType, map => { map.Column("AccountType"); });
            Property<string>(x => x.CIFNumber, map => { map.Column("CIFNumber"); });
            Property<string>(x => x.BranchCode, map => { map.Column("BranchCode"); });
            Property<string>(x => x.Note, map => { map.Column("Note"); });
            Property<string>(x => x.RequestType, map => { map.Column("RequestType"); });
            Property<DateTime?>(x => x.RequestDate, map => { map.Column("RequestDate"); });
            Property<string>(x => x.RequestUser, map => { map.Column("RequestUser"); });
            Property<string>(x => x.RequestReason, map => { map.Column("RequestReason"); });
            Property<DateTime?>(x => x.ApproveDate, map => { map.Column("ApproveDate"); });
            Property<string>(x => x.ApproveBy, map => { map.Column("ApproveBy"); });
            Property<DateTime?>(x => x.RejectDate, map => { map.Column("RejectDate"); });
            Property<string>(x => x.RejectBy, map => { map.Column("RejectBy"); });
            Property<string>(x => x.RejectReason, map => { map.Column("RejectReason"); });
            
        }
    }
}
