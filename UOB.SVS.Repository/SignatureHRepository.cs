﻿using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using LinqSpecs;
using NHibernate.Linq;
using NHibernate.Criterion;
using Treemas.Base.Repository;
using UOB.SVS.Model;
using UOB.SVS.Repository;
using UOB.SVS.Interface;
using Treemas.Base.Utilities.Queries;
using System;
using Treemas.Base.Utilities;

namespace UOB.SVS.Repository
{
    public class SignatureHRepository : RepositoryControllerString<SignatureHeader>, ISignatureHRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public SignatureHRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }
        public SignatureHeader getSignatureHeader(string id)
        {
            return transact(() => session.QueryOver<SignatureHeader>()
                                                .Where(f => f.AccountNo == id).SingleOrDefault());
        }

        public IList<SignatureHeader> getSignatureHeaders(IList<string> SignatureHeaderIds)
        {
            return transact(() =>
                   session.QueryOver<SignatureHeader>()
                          .WhereRestrictionOn(val => val.AccountNo)
                          .IsIn(SignatureHeaderIds.ToArray()).List());
        }
        public bool IsDuplicate(string accno)
        {
            int rowCount = transact(() => session.QueryOver<SignatureHeader>()
                                                .Where(f =>f.AccountNo == accno).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public PagedResult<SignatureHeader> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey,SVSReqFilter filter)
        {
            //SimpleExpression tanggalStart = Restrictions.Ge("TanggalFPDO", filter.TanggalStart);
            //SimpleExpression tanggalEnd = Restrictions.Le("TanggalFPDO", filter.TanggalEnd);
            Conjunction conjuction = Restrictions.Conjunction();

            //if (!filter.TanggalStart.IsNull()) conjuction.Add(tanggalStart);
            //if (!filter.TanggalEnd.IsNull()) conjuction.Add(tanggalEnd);
            if (!filter.AccountNo.IsNullOrEmpty())
            {
                SimpleExpression accNo = Restrictions.Like("AccountNo", filter.AccountNo, MatchMode.Start);
                conjuction.Add(accNo);
            }
            if (!filter.AccountName.IsNullOrEmpty())
            {
                SimpleExpression accName = Restrictions.Like("AccountName", filter.AccountName, MatchMode.Anywhere);
                conjuction.Add(accName);
            }
            if (!filter.CIFNumber.IsNullOrEmpty())
            {
                SimpleExpression cifNumber = Restrictions.Like("CIFNumber", filter.CIFNumber, MatchMode.Anywhere);
                conjuction.Add(cifNumber);
            }
            if (!filter.Branches.IsNullOrEmpty())
            {
                conjuction.Add(Restrictions.In("BranchCode", filter.Branches.Select(x => x.BranchCode).ToArray()));
            }

            PagedResult<SignatureHeader> paged = new PagedResult<SignatureHeader>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.QueryOver<SignatureHeader>()
                                             .Where(conjuction)
                                             .OrderBy(Projections.Property(orderColumn)).Asc
                                             .Skip((pageNumber) * itemsPerPage)
                                             .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() => session.QueryOver<SignatureHeader>()
                                             .Where(conjuction)
                                             .OrderBy(Projections.Property(orderColumn)).Desc
                                             .Skip((pageNumber) * itemsPerPage)
                                             .Take(itemsPerPage).List());
            }

            paged.TotalItems = transact(() => session.QueryOver<SignatureHeader>()
                                            .Where(conjuction).RowCount());
            return paged;
        }
        public PagedResult<SignatureHeader> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            Specification<SignatureHeader> specification;

            switch (searchColumn)
            {
                case "AccountNo":
                    specification = new AdHocSpecification<SignatureHeader>(s => s.AccountNo.Contains(searchValue));
                    break;
                case "CIFNumber":
                    specification = new AdHocSpecification<SignatureHeader>(s => s.CIFNumber.Contains(searchValue));
                    break;               
                default:
                    specification = new AdHocSpecification<SignatureHeader>(s => s.AccountNo.Contains(searchValue));
                    break;
            }

            PagedResult<SignatureHeader> paged = new PagedResult<SignatureHeader>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.Query<SignatureHeader>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<SignatureHeader>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() =>
                          session.Query<SignatureHeader>()
                                 .Where(specification.ToExpression())
                                 .OrderBy(GetOrderByExpression<SignatureHeader>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }


            paged.TotalItems = transact(() =>
                         session.Query<SignatureHeader>()
                                .Where(specification.ToExpression()).Count());

            paged.TotalItems = Count;
            return paged;
        }
        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }

        public void DeleteSignatureH(string ID)
        {
            string hqlDelete = "delete SignatureHeader where AccountNo = :accountNo";
            int deletedEntities = session.CreateQuery(hqlDelete)
                    .SetString("accountNo", ID)
                    .ExecuteUpdate();
        }

        public void AddKTPDet(SignatureDKTP item)
        {
            transact(() => session.SaveOrUpdate(item));
        }


        public IList<SignatureDKTP> getKTPDet(string accountNo)
        {
            IList<SignatureDKTP> req = transact(() => session.QueryOver<SignatureDKTP>().Where(f => f.AccountNo == accountNo).List());

            return req;
        }

        public void DeleteKTPDet(string ID)
        {
            string hqlDelete = "delete SignatureDKTP where AccountNo = :accountNo";
            int deletedEntities = session.CreateQuery(hqlDelete)
                    .SetString("accountNo", ID)
                    .ExecuteUpdate();
        }

        public void AddSignDet(SignatureDSign item)
        {
            transact(() => session.SaveOrUpdate(item));
        }

        public IList<SignatureDSign> getSignDet(string accountNo)
        {
            IList<SignatureDSign> req = transact(() => session.QueryOver<SignatureDSign>().Where(f => f.AccountNo == accountNo).List());

            return req;
        }

        public void DeleteSignDet(string ID)
        {
            string hqlDelete = "delete SignatureDSign where AccountNo = :accountNo";
            int deletedEntities = session.CreateQuery(hqlDelete)
                    .SetString("accountNo", ID)
                    .ExecuteUpdate();
        }

        public void AddDocDet(SignatureDDoc item)
        {
            transact(() => session.SaveOrUpdate(item));
        }

        public IList<SignatureDDoc> getDocDet(string accountNo)
        {
            IList<SignatureDDoc> req = transact(() => session.QueryOver<SignatureDDoc>().Where(f => f.AccountNo == accountNo).List());

            return req;
        }

        public void DeleteDocDet(string ID)
        {
            string hqlDelete = "delete SignatureDDoc where AccountNo = :accountNo";
            int deletedEntities = session.CreateQuery(hqlDelete)
                    .SetString("accountNo", ID)
                    .ExecuteUpdate();
        }


    }
}
