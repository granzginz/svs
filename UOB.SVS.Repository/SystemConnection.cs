﻿using Treemas.Base.Utilities;
using Treemas.Base.Configuration;
using UOBISecurity;

namespace UOB.SVS.Repository
{
    public class SystemConnection : IConnection
    {
        private ConnectionParam connectionSetting;
        public SystemConnection()
        {

            connectionSetting = new ConnectionParam();
            IConfigurationBinder binder = SystemConfigurationCabinet.Instance.GetBinder("Database");
            CompositeConfigurationItem items = (CompositeConfigurationItem) binder.GetConfiguration("System");


            Encryptor enc = new Encryptor();
            ApplicationRegistryHandler appReg = new ApplicationRegistryHandler();

            string keyReg;
            string appName = items.GetItem("AppName").Value;
            string uobikey = items.GetItem("UOBIKey").Value;
            string config = items.GetItem("DBConfig").Value;
            appName = appName.Trim().Replace(" ", "");
            keyReg = appReg.ReadFromRegistry(@"Software\" + appName, "Key");
            var _ConDetails = enc.Decrypt(config, keyReg, uobikey);
            

            string[] dbsetting = _ConDetails.Split(';');
            string[] dbsettingItem = dbsetting[1].Split('=');

            //ConfigurationItem item = items.GetItem("DBInstance");
            connectionSetting.DBInstance = dbsettingItem[1];

            //item = items.GetItem("DBUser");
            dbsettingItem = dbsetting[3].Split('=');
            connectionSetting.DBUser = dbsettingItem[1];

            //item = items.GetItem("DBPassword");
            dbsettingItem = dbsetting[4].Split('=');
            connectionSetting.DBPassword = dbsettingItem[1];

            //item = items.GetItem("DBName");
            dbsettingItem = dbsetting[2].Split('=');
            connectionSetting.DBName = dbsettingItem[1];
        }
        public ConnectionParam GetConnectionSetting()
        {
            return connectionSetting;
        }
    }
}
