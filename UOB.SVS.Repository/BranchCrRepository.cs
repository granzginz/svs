﻿using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using LinqSpecs;
using NHibernate.Linq;
using NHibernate.Criterion;
using Treemas.Base.Repository;
using UOB.SVS.Model;
using UOB.SVS.Repository;
using UOB.SVS.Interface;
using Treemas.Base.Utilities.Queries;
using System;
using Treemas.Credential.Model;
using Treemas.Base.Utilities;

namespace UOB.SVS.Repository
{
    public class BranchCrRepository : RepositoryController<BranchCr>, IBranchCrRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public BranchCrRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }
        public BranchCr getBranch(string branchCode)
        {
            return transact(() => session.QueryOver<BranchCr>()
                                                .Where(f => f.BranchCode == branchCode).SingleOrDefault());
        }
        public BranchCr getBranch(long id)
        {
            return transact(() => session.QueryOver<BranchCr>()
                                                .Where(f => f.Id == id).SingleOrDefault());
        }

        public IList<BranchCr> getBranchs(IList<string> branchCode)
        {
            return transact(() =>
                   session.QueryOver<BranchCr>()
                          .WhereRestrictionOn(val => val.BranchCode)
                          .IsIn(branchCode.ToArray()).List());
        }
        public bool IsDuplicate(string branchCode)
        {
            int rowCount = transact(() => session.QueryOver<BranchCr>()
                                                .Where(f =>f.BranchCode == branchCode).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public PagedResult<BranchCr> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {
            PagedResult<BranchCr> paged = new PagedResult<BranchCr>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.Query<BranchCr>()
                                                     .OrderByDescending(GetOrderByExpression<BranchCr>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() => session.Query<BranchCr>()
                                                     .OrderBy(GetOrderByExpression<BranchCr>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = Count;
            return paged;
        }
        public PagedResult<BranchCr> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            Specification<BranchCr> specification;

            switch (searchColumn)
            {
                case "BranchCode":
                    specification = new AdHocSpecification<BranchCr>(s => s.BranchCode.Contains(searchValue));
                    break;
                case "MainBranch":
                    specification = new AdHocSpecification<BranchCr>(s => s.MainBranch.Contains(searchValue));
                    break;              
                default:
                    specification = new AdHocSpecification<BranchCr>(s => s.BranchCode.Contains(searchValue));
                    break;
            }

            PagedResult<BranchCr> paged = new PagedResult<BranchCr>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.Query<BranchCr>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<BranchCr>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() =>
                          session.Query<BranchCr>()
                                 .Where(specification.ToExpression())
                                 .OrderBy(GetOrderByExpression<BranchCr>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }


            paged.TotalItems = transact(() =>
                         session.Query<BranchCr>()
                                .Where(specification.ToExpression()).Count());

            paged.TotalItems = Count;
            return paged;
        }
        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }

        public PagedResult<BranchCr> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, ApplicationCRFilter filter)
        {
            SimpleExpression status = Restrictions.Eq("ApprovalStatus", filter.ApprovalStatus);
            SimpleExpression apprtype = Restrictions.Eq("ApprovalType", filter.ApprovalType);

            Conjunction conjuction = Restrictions.Conjunction();
            if (!filter.ApprovalStatus.Trim().IsNullOrEmpty()) conjuction.Add(status);
            if (!filter.ApprovalType.Trim().IsNullOrEmpty()) conjuction.Add(apprtype);

            PagedResult<BranchCr> paged = new PagedResult<BranchCr>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.QueryOver<BranchCr>().Where(conjuction)
                                                     .OrderBy(Projections.Property(orderColumn)).Asc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() => session.QueryOver<BranchCr>().Where(conjuction)
                                                     .OrderBy(Projections.Property(orderColumn)).Desc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }

            paged.TotalItems = paged.Items.Count();
            return paged;
        }
    }
}
