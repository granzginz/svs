﻿using LinqSpecs;
using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Treemas.Base.Repository;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using UOB.SVS.Interface;
using UOB.SVS.Model;

namespace UOB.SVS.Repository
{
   public class VFunctionFeatureRepository : RepositoryController<VFunctionFeature>, IVFunctionFeatureRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public VFunctionFeatureRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }

        public PagedResult<VFunctionFeature> FindAllPagedbyFunction(long functionId, int pageNumber, string orderColumn, int itemsPerPage, string orderKey, string searchColumn, string searchValue)
        {
            SimpleExpression FunctId = Restrictions.Eq("FunctionId", functionId);
            SimpleExpression FeatName = Restrictions.Like("FeatureName", searchValue, MatchMode.Anywhere);

            Conjunction conjuction = Restrictions.Conjunction();
            if (!functionId.ToString().Trim().IsNullOrEmpty()) conjuction.Add(FunctId);
            if (!searchValue.Trim().IsNullOrEmpty()) conjuction.Add(FeatName);


            PagedResult<VFunctionFeature> paged = new PagedResult<VFunctionFeature>(pageNumber, itemsPerPage);

            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.QueryOver<VFunctionFeature>()
                                 .Where(conjuction)
                                 .OrderBy(Projections.Property(orderColumn)).Asc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() =>
                          session.QueryOver<VFunctionFeature>()
                                 .Where(conjuction)
                                 .OrderBy(Projections.Property(orderColumn)).Desc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }
            paged.TotalItems = transact(() =>
                          session.QueryOver<VFunctionFeature>()
                                 .Where(x => x.FunctionId == functionId)
                                .RowCount());

            return paged;
        }
    }
}
