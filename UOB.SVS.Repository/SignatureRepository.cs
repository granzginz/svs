﻿using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using LinqSpecs;
using NHibernate.Linq;
using NHibernate.Criterion;
using Treemas.Base.Repository;
using UOB.SVS.Model;
using UOB.SVS.Repository;
using UOB.SVS.Interface;
using Treemas.Base.Utilities.Queries;
using System;

namespace UOB.SVS.Repository
{
    public class SignatureRepository : RepositoryController<SignatureMaster>, ISignatureRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public SignatureRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }
        public SignatureMaster getSignatureMaster(string id)
        {
            return transact(() => session.QueryOver<SignatureMaster>()
                                                .Where(f => f.AccountNo == id).SingleOrDefault());
        }

        public IList<SignatureMaster> getSignatureMasters(IList<string> SignatureMasterIds)
        {
            return transact(() =>
                   session.QueryOver<SignatureMaster>()
                          .WhereRestrictionOn(val => val.AccountNo)
                          .IsIn(SignatureMasterIds.ToArray()).List());
        }
        public bool IsDuplicate(string accno)
        {
            int rowCount = transact(() => session.QueryOver<SignatureMaster>()
                                                .Where(f =>f.AccountNo == accno).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public PagedResult<SignatureMaster> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {
            PagedResult<SignatureMaster> paged = new PagedResult<SignatureMaster>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.Query<SignatureMaster>()
                                                     .OrderByDescending(GetOrderByExpression<SignatureMaster>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() => session.Query<SignatureMaster>()
                                                     .OrderBy(GetOrderByExpression<SignatureMaster>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = Count;
            return paged;
        }
        public PagedResult<SignatureMaster> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            Specification<SignatureMaster> specification;

            switch (searchColumn)
            {
                case "AccountNumber":
                    specification = new AdHocSpecification<SignatureMaster>(s => s.AccountNo.Contains(searchValue));
                    break;
                case "CustomerName":
                    specification = new AdHocSpecification<SignatureMaster>(s => s.CustomerName.Contains(searchValue));
                    break;
                case "Note":
                    specification = new AdHocSpecification<SignatureMaster>(s => s.Note.Contains(searchValue));
                    break;                
                default:
                    specification = new AdHocSpecification<SignatureMaster>(s => s.AccountNo.Contains(searchValue));
                    break;
            }

            PagedResult<SignatureMaster> paged = new PagedResult<SignatureMaster>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.Query<SignatureMaster>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<SignatureMaster>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() =>
                          session.Query<SignatureMaster>()
                                 .Where(specification.ToExpression())
                                 .OrderBy(GetOrderByExpression<SignatureMaster>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }


            paged.TotalItems = transact(() =>
                         session.Query<SignatureMaster>()
                                .Where(specification.ToExpression()).Count());

            paged.TotalItems = Count;
            return paged;
        }
        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }

    }
}
