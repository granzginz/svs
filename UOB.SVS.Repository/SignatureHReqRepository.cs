﻿using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using LinqSpecs;
using NHibernate.Linq;
using NHibernate.Criterion;
using Treemas.Base.Repository;
using UOB.SVS.Model;
using UOB.SVS.Repository;
using UOB.SVS.Interface;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using System;
using Treemas.Credential.Model;
using System.Data;
using NHibernate.Transform;

namespace UOB.SVS.Repository
{
    public class SignatureHReqRepository : RepositoryControllerString<SignatureHRequest>, ISignatureHReqRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public SignatureHReqRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }
        public SignatureHRequest getSignatureHReq(string id)
        {
            return transact(() => session.QueryOver<SignatureHRequest>()
                                                .Where(f => f.AccountNo == id).SingleOrDefault());
        }

        public IList<SignatureHRequest> getSignatureHReqList(IList<string> SignatureHReqIds)
        {
            return transact(() =>
                   session.QueryOver<SignatureHRequest>()
                          .WhereRestrictionOn(val => val.AccountNo)
                          .IsIn(SignatureHReqIds.ToArray()).List());
        }
        public bool IsDuplicate(string accno)
        {
            int rowCount = transact(() => session.QueryOver<SignatureHRequest>()
                                                .Where(f => f.AccountNo == accno && f.IsRejected == false).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        
        public PagedResult<SignatureHRequest> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, SVSReqFilter filter, User user )
        {
            Conjunction conjuction = Restrictions.Conjunction();
            SimpleExpression reqType = Restrictions.Eq("RequestType", filter.RequestType);

            if (!filter.RequestType.IsNullOrEmpty()) conjuction.Add(reqType);

            if (!filter.AccountNo.IsNullOrEmpty())
            {
                SimpleExpression accNo = Restrictions.Like("AccountNo", filter.AccountNo, MatchMode.Anywhere);
                conjuction.Add(accNo);
            }
            if (!filter.AccountName.IsNullOrEmpty())
            {
                SimpleExpression accName = Restrictions.Like("AccountName", filter.AccountName, MatchMode.Anywhere);
                conjuction.Add(accName);
            }
            //if (!filter.Branches.IsNullOrEmpty())
            //{
            //    conjuction.Add(Restrictions.In("BranchCode", filter.Branches.Select(x => x.BranchCode).ToArray()));
            //}

            if (filter.RequestType.IsNullOrEmpty()) filter.RequestType = ""; ;

            if (filter.AccountNo.IsNullOrEmpty())
            {
                filter.AccountNo = "";
            }
            if (filter.AccountName.IsNullOrEmpty())
            {
                filter.AccountName = "";
            }

            if (!filter.ApprovalType.IsNullOrEmpty() && filter.ApprovalType.ToLower() == "approvesvs")
            {
                SimpleExpression isRejected = Restrictions.Eq("IsRejected", 0);
                conjuction.Add(isRejected);
            }
            if (!filter.RequestUser.IsNullOrEmpty())
            {
                SimpleExpression requestBy = Restrictions.Eq("RequestUser", filter.RequestUser);
                conjuction.Add(requestBy);
            }

            switch (orderColumn)
            {
                case "RequestTypeString":
                    orderColumn = "RequestType";
                    break;
                case "RequestDateString":
                    orderColumn = "RequestDate";
                    break;
                default:
                    break;
            }

            PagedResult<SignatureHRequest> paged = new PagedResult<SignatureHRequest>(pageNumber, itemsPerPage);

            //Alias
            SignatureHRequest a = null;
            User b = null;

            

            //.SetResultTransformer(Transformers.AliasToBean<EmployeeView>())


            string sql = "";

            if (orderKey.ToLower() == "asc")
            {

                //sql = "select AccountNo,AccountName,AccountType,CIFNumber,BranchCode,Note" +
                //        ",RequestType,RequestDate,RequestUser,RequestReason,IsRejected" +
                //        " from SIGNATURE_HEADER_REQUEST a join sec_user b on a.RequestUser = b.USERNAME" +
                //        " where " + Convert.ToString(conjuction) + " and RequestUser = '" + user.Username + "'" +
                //        " order by " + Projections.Property(orderColumn).ToString() + " asc ";

                sql = "select AccountNo,AccountName,AccountType,CIFNumber,BranchCode,Note"+
                      ",RequestType,RequestDate,RequestUser,RequestReason,IsRejected"+
                      " from SIGNATURE_HEADER_REQUEST a"+
                      " inner join (select * from SEC_USER"+
                                   " where BRANCH_CODE in (select a.BRANCH_CODE from BRANCH a"+
                                   " inner join SEC_USER b on a.BRANCH_CODE = b.BRANCH_CODE"+
                                   " where USERNAME = '" + user.Username + "')" +
						 " ) b on a.RequestUser = b.USERNAME"+
                      " where AccountNo like '%"+ filter.AccountNo.Trim() + "%' and AccountName like '%"+ filter.AccountName + "%' and RequestType like '%" + filter.RequestType + "%' and IsRejected = 0" +
                      " order by " + Projections.Property(orderColumn).ToString() + " asc ";

                NHibernate.IQuery myQuery = session.CreateSQLQuery(sql).SetResultTransformer(Transformers.AliasToBean<SignatureHRequest>());

                paged.Items = myQuery.List<SignatureHRequest>().Take(itemsPerPage);


                //session.QueryOver<SignatureHRequest>(() => a)
                //                             .JoinAlias(() => a.user ,() => b)
                //                             .Where(conjuction).And(() => a.BranchCode == b.BranchCode)
                //                             .OrderBy(Projections.Property(orderColumn)).Asc
                //                             .Skip((pageNumber) * itemsPerPage)
                //                             .Take(itemsPerPage).List());
            }
            else
            {
                //sql = "select AccountNo,AccountName,AccountType,CIFNumber,BranchCode,Note" +
                //        ",RequestType,RequestDate,RequestUser,RequestReason,IsRejected" +
                //        " from SIGNATURE_HEADER_REQUEST a join sec_user b on a.RequestUser = b.USERNAME" +
                //        " where " + Convert.ToString(conjuction) + " and RequestUser = '" + user.Username + "'" +
                //        " order by " + Projections.Property(orderColumn).ToString() + " desc ";

                sql = "select AccountNo,AccountName,AccountType,CIFNumber,BranchCode,Note" +
                      ",RequestType,RequestDate,RequestUser,RequestReason,IsRejected" +
                      "from SIGNATURE_HEADER_REQUEST a" +
                      "inner join (select * from SEC_USER" +
                                   "where BRANCH_CODE in (select a.BRANCH_CODE from BRANCH a" +
                                   "inner join SEC_USER b on a.BRANCH_CODE = b.BRANCH_CODE" +
                                   "where USERNAME = '" + user.Username + "')" +
                         ") b on a.RequestUser = b.USERNAME" +
                      " where AccountNo like '%" + filter.AccountNo.Trim() + "%' and AccountName like '%" + filter.AccountName + "%' and RequestType like '%" + filter.RequestType + "%' and IsRejected = 0" +
                      " order by " + Projections.Property(orderColumn).ToString() + " desc ";
                //IList<SignatureHRequest> coverages = session.CreateSQLQuery(sql).List<SignatureHRequest>();

                NHibernate.IQuery myQuery = session.CreateSQLQuery(sql).SetResultTransformer(Transformers.AliasToBean<SignatureHRequest>());

                paged.Items = myQuery.List<SignatureHRequest>().Take(itemsPerPage);

                //paged.Items = transact(() => session.QueryOver<SignatureHRequest>(() => a)
                //                             .JoinAlias(() => a.user, () => b)
                //                             .Where(conjuction).And(() => a.BranchCode == b.BranchCode)
                //                             .OrderBy(Projections.Property(orderColumn)).Desc
                //                             .Skip((pageNumber) * itemsPerPage)
                //                             .Take(itemsPerPage).List());
            }

            //string sqlCount = "select AccountNo " +
            //            " from SIGNATURE_HEADER_REQUEST a join sec_user b on a.RequestUser = b.USERNAME" +
            //            " where " + Convert.ToString(conjuction) + " and RequestUser = '" + user.Username + "'" +
            //            " order by " + Projections.Property(orderColumn).ToString() + " asc ";

            string sqlCount = "select AccountNo,AccountName,AccountType,CIFNumber,BranchCode,Note" +
                      ",RequestType,RequestDate,RequestUser,RequestReason,IsRejected" +
                      " from SIGNATURE_HEADER_REQUEST a" +
                      " inner join (select * from SEC_USER" +
                                   " where BRANCH_CODE in (select a.BRANCH_CODE from BRANCH a" +
                                   " inner join SEC_USER b on a.BRANCH_CODE = b.BRANCH_CODE" +
                                   " where USERNAME = '" + user.Username + "')" +
                         ") b on a.RequestUser = b.USERNAME" +
                      " where AccountNo like '%" + filter.AccountNo.Trim() + "%' and AccountName like '%" + filter.AccountName + "%' and RequestType like '%" + filter.RequestType + "%' and IsRejected = 0" +
                      " order by " + Projections.Property(orderColumn).ToString() + " asc ";

            NHibernate.IQuery myQuery2 = session.CreateSQLQuery(sqlCount).SetResultTransformer(Transformers.AliasToBean<SignatureHRequest>());

            paged.TotalItems = myQuery2.List<SignatureHRequest>().Count();
            //transact(() => session.QueryOver<SignatureHRequest>(() => a)
            //                             .JoinAlias(() => a.user, () => b)
            //                             .Where(conjuction).And(() => a.BranchCode == b.BranchCode).RowCount());
            return paged;
        }

        public PagedResult<SignatureHRequest> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, SVSReqFilter filter)
        {
            Conjunction conjuction = Restrictions.Conjunction();
            SimpleExpression reqType = Restrictions.Eq("RequestType", filter.RequestType);
            if (!filter.RequestType.IsNullOrEmpty()) conjuction.Add(reqType);

            if (!filter.AccountNo.IsNullOrEmpty())
            {
                SimpleExpression accNo = Restrictions.Like("AccountNo", filter.AccountNo, MatchMode.Start);
                conjuction.Add(accNo);
            }
            if (!filter.AccountName.IsNullOrEmpty())
            {
                SimpleExpression accName = Restrictions.Like("AccountName", filter.AccountName, MatchMode.Anywhere);
                conjuction.Add(accName);
            }
            if (!filter.Branches.IsNullOrEmpty())
            {
                conjuction.Add(Restrictions.In("BranchCode", filter.Branches.Select(x => x.BranchCode).ToArray()));
            }

            if (!filter.ApprovalType.IsNullOrEmpty() && filter.ApprovalType.ToLower() == "approvesvs")
            {
                SimpleExpression isRejected = Restrictions.Eq("IsRejected", false);
                conjuction.Add(isRejected);
            }

            if (!filter.RequestUser.IsNullOrEmpty())
            {
                SimpleExpression requestBy = Restrictions.Eq("RequestUser", filter.RequestUser);
                conjuction.Add(requestBy);
            }

            if (orderColumn == "RequestTypeString") orderColumn = "RequestType";
            if (orderColumn == "RequestDateString") orderColumn = "RequestDate";

            PagedResult<SignatureHRequest> paged = new PagedResult<SignatureHRequest>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.QueryOver<SignatureHRequest>()
                                             .Where(conjuction)
                                             .OrderBy(Projections.Property(orderColumn)).Asc
                                             .Skip((pageNumber) * itemsPerPage)
                                             .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() => session.QueryOver<SignatureHRequest>()
                                             .Where(conjuction)
                                             .OrderBy(Projections.Property(orderColumn)).Desc
                                             .Skip((pageNumber) * itemsPerPage)
                                             .Take(itemsPerPage).List());
            }

            paged.TotalItems = transact(() => session.QueryOver<SignatureHRequest>()
                                            .Where(conjuction).RowCount());
            return paged;
        }

        public PagedResult<SignatureHRequest> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {
            PagedResult<SignatureHRequest> paged = new PagedResult<SignatureHRequest>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.Query<SignatureHRequest>()
                                                     .OrderByDescending(GetOrderByExpression<SignatureHRequest>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() => session.Query<SignatureHRequest>()
                                                     .OrderBy(GetOrderByExpression<SignatureHRequest>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = paged.Items.Count();
            return paged;
        }
        public PagedResult<SignatureHRequest> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            Specification<SignatureHRequest> specification;

            switch (searchColumn)
            {
                case "AccountNumber":
                    specification = new AdHocSpecification<SignatureHRequest>(s => s.AccountNo.Contains(searchValue));
                    break;
                case "AccountName":
                    specification = new AdHocSpecification<SignatureHRequest>(s => s.AccountName.Contains(searchValue));
                    break;
                case "Note":
                    specification = new AdHocSpecification<SignatureHRequest>(s => s.Note.Contains(searchValue));
                    break;
                default:
                    specification = new AdHocSpecification<SignatureHRequest>(s => s.AccountNo.Contains(searchValue));
                    break;
            }

            PagedResult<SignatureHRequest> paged = new PagedResult<SignatureHRequest>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.Query<SignatureHRequest>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<SignatureHRequest>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() =>
                          session.Query<SignatureHRequest>()
                                 .Where(specification.ToExpression())
                                 .OrderBy(GetOrderByExpression<SignatureHRequest>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }


            paged.TotalItems = transact(() =>
                         session.Query<SignatureHRequest>()
                                .Where(specification.ToExpression()).Count());

            paged.TotalItems = paged.Items.Count();
            return paged;
        }

        public PagedResult<SignatureHRequest> FindAllPagedCreate(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, SVSReqFilter filter)
        {
            Conjunction conjuction = Restrictions.Conjunction();
            SimpleExpression reqType = Restrictions.Eq("RequestType", filter.RequestType);
            if (!filter.RequestType.IsNullOrEmpty()) conjuction.Add(reqType);

            if (!filter.AccountNo.IsNullOrEmpty())
            {
                SimpleExpression accNo = Restrictions.Like("AccountNo", filter.AccountNo, MatchMode.Start);
                conjuction.Add(accNo);
            }
            if (!filter.AccountName.IsNullOrEmpty())
            {
                SimpleExpression accName = Restrictions.Like("AccountName", filter.AccountName, MatchMode.Anywhere);
                conjuction.Add(accName);
            }
            if (!filter.Branches.IsNullOrEmpty())
            {
                conjuction.Add(Restrictions.In("BranchCode", filter.Branches.Select(x => x.BranchCode).ToArray()));
            }

            if (!filter.RequestUser.IsNullOrEmpty())
            {
                SimpleExpression requestBy = Restrictions.Eq("RequestUser", filter.RequestUser);
                conjuction.Add(requestBy);
            }

            PagedResult<SignatureHRequest> paged = new PagedResult<SignatureHRequest>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.QueryOver<SignatureHRequest>()
                                             .Where(conjuction)
                                             .And(x => x.RequestDate.Value.Date == DateTime.Now.Date)
                                             .OrderBy(Projections.Property(orderColumn)).Asc
                                             .Skip((pageNumber) * itemsPerPage)
                                             .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() => session.QueryOver<SignatureHRequest>()
                                             .Where(conjuction)
                                             .And(x => x.RequestDate.Value.Date == DateTime.Now.Date)
                                             .OrderBy(Projections.Property(orderColumn)).Desc
                                             .Skip((pageNumber) * itemsPerPage)
                                             .Take(itemsPerPage).List());
            }

            paged.TotalItems = transact(() => session.QueryOver<SignatureHRequest>()
                                            .Where(conjuction)
                                             .And(x => x.RequestDate.Value.Date == DateTime.Now.Date)
                                             .RowCount());
            return paged;
        }

        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }

        public void DeleteSignatureHReq(string ID)
        {
            string hqlDelete = "delete SignatureHRequest where AccountNo = :accountNo";
            int deletedEntities = session.CreateQuery(hqlDelete)
                    .SetString("accountNo", ID)
                    .ExecuteUpdate();
        }

        public void AddKTPReqD(SignatureDKTPReq item)
        {
            transact(() => session.Save(item));
        }

        public IList<SignatureDKTPReq> getKTPReqD(string accountNo)
        {
            IList<SignatureDKTPReq> req = transact(() => session.QueryOver<SignatureDKTPReq>().Where(f => f.AccountNo == accountNo).List());

            return req;
        }

        public void DeleteKTPReqD(string ID)
        {
            string hqlDelete = "delete SignatureDKTPReq where AccountNo = :accountNo";
            int deletedEntities = session.CreateQuery(hqlDelete)
                    .SetString("accountNo", ID)
                    .ExecuteUpdate();
        }

        public void DeleteKTPReqD(long ID)
        {
            string hqlDelete = "delete SignatureDKTPReq where ID = :ID";
            int deletedEntities = session.CreateQuery(hqlDelete)
                    .SetDouble("ID", ID)
                    .ExecuteUpdate();
        }

        public void AddSignReqD(SignatureDSignReq item)
        {
            transact(() => session.Save(item));
        }

        public IList<SignatureDSignReq> getSignReqD(string accountNo)
        {
            IList<SignatureDSignReq> req = transact(() => session.QueryOver<SignatureDSignReq>().Where(f => f.AccountNo == accountNo).List());

            return req;
        }

        public void DeleteSignReqD(string ID)
        {
            string hqlDelete = "delete SignatureDSignReq where AccountNo = :accountNo";
            int deletedEntities = session.CreateQuery(hqlDelete)
                    .SetString("accountNo", ID)
                    .ExecuteUpdate();
        }

        public void DeleteSignReqD(long ID)
        {
            string hqlDelete = "delete SignatureDSignReq where ID = :ID";
            int deletedEntities = session.CreateQuery(hqlDelete)
                    .SetDouble("ID", ID)
                    .ExecuteUpdate();
        }

        public void AddDocReqD(SignatureDDocReq item)
        {
            transact(() => session.Save(item));
        }

        public IList<SignatureDDocReq> getDocReqD(string accountNo)
        {
            IList<SignatureDDocReq> req = transact(() => session.QueryOver<SignatureDDocReq>().Where(f => f.AccountNo == accountNo).List());

            return req;
        }

        public void DeleteDocReqD(string ID)
        {
            string hqlDelete = "delete SignatureDDocReq where AccountNo = :accountNo";
            int deletedEntities = session.CreateQuery(hqlDelete)
                    .SetString("accountNo", ID)
                    .ExecuteUpdate();
        }

        public void DeleteDocReqD(long ID)
        {
            string hqlDelete = "delete SignatureDDocReq where ID = :ID";
            int deletedEntities = session.CreateQuery(hqlDelete)
                    .SetDouble("ID", ID)
                    .ExecuteUpdate();
        }
    }
}
