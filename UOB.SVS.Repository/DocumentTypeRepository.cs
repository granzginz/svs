﻿using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using LinqSpecs;
using NHibernate.Linq;
using NHibernate.Criterion;
using Treemas.Base.Repository;
using UOB.SVS.Model;
using UOB.SVS.Repository;
using UOB.SVS.Interface;
using Treemas.Base.Utilities.Queries;
using System;

namespace UOB.SVS.Repository
{
    public class DocumentTypeRepository : RepositoryController<DocumentType>, IDocumentTypeRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public DocumentTypeRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }
        public DocumentType getDocumentType(long id)
        {
            return transact(() => session.QueryOver<DocumentType>()
                                                .Where(f => f.Id == id).SingleOrDefault());
        }

        public IList<DocumentType> getDocumentTypes(IList<string> branchCode)
        {
            return transact(() =>
                   session.QueryOver<DocumentType>()
                          .WhereRestrictionOn(val => val.DocType)
                          .IsIn(branchCode.ToArray()).List());
        }
        public bool IsDuplicate(string branchCode)
        {
            int rowCount = transact(() => session.QueryOver<DocumentType>()
                                                .Where(f =>f.DocType == branchCode).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public PagedResult<DocumentType> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {
            PagedResult<DocumentType> paged = new PagedResult<DocumentType>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.Query<DocumentType>()
                                                     .OrderByDescending(GetOrderByExpression<DocumentType>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() => session.Query<DocumentType>()
                                                     .OrderBy(GetOrderByExpression<DocumentType>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = Count;
            return paged;
        }
        public PagedResult<DocumentType> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            SimpleExpression specification;
            specification = Restrictions.Like(searchColumn, searchValue, MatchMode.Anywhere);

            PagedResult<DocumentType> paged = new PagedResult<DocumentType>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.QueryOver<DocumentType>()
                                 .Where(specification)
                                 .OrderBy(Projections.Property(orderColumn)).Asc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() =>
                          session.QueryOver<DocumentType>()
                                 .Where(specification)
                                 .OrderBy(Projections.Property(orderColumn)).Desc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }


            paged.TotalItems = transact(() =>
                         session.QueryOver<DocumentType>()
                                .Where(specification)
                                .RowCount());
            return paged;
        }

        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }

    }
}
