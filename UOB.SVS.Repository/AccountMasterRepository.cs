﻿using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using LinqSpecs;
using NHibernate.Linq;
using NHibernate.Criterion;
using Treemas.Base.Repository;
using UOB.SVS.Model;
using UOB.SVS.Repository;
using UOB.SVS.Interface;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using System;
using Treemas.Credential.Model;

namespace UOB.SVS.Repository
{
    public class AccountMasterRepository : RepositoryController<AccountMaster>, IAccountMasterRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public AccountMasterRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }
        public AccountMaster getAccount(string AccountNo)
        {
            //return transact(() => session.QueryOver<AccountMaster>()
            //                                    .Where(f => f.AccountNo == AccountNo).SingleOrDefault());

            return FindOne(new AdHocSpecification<AccountMaster>(s => s.AccountNo == AccountNo));
        }

        public AccountMaster getAccountbyCIF(string CIFno)
        {
            return FindOne(new AdHocSpecification<AccountMaster>(s => s.CIFNo == CIFno));
        }

        public PagedResult<AccountMaster> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, SVSReqFilter filter ,User user)
        {
            SimpleExpression accountNo = Restrictions.Eq("AccountNo", filter.AccountNo);
            SimpleExpression cifNo = Restrictions.Eq("CIFNo", filter.CIFNumber);

            Conjunction conjuction = Restrictions.Conjunction();
            if (!filter.AccountNo.Trim().IsNullOrEmpty()) conjuction.Add(accountNo);
            if (!filter.CIFNumber.Trim().IsNullOrEmpty()) conjuction.Add(cifNo);
            
            
            PagedResult<AccountMaster> paged = new PagedResult<AccountMaster>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {

                if (user.BranchCode != "000")
                {
                    paged.Items = transact(() => session.QueryOver<AccountMaster>()
                                                                .Where(conjuction).And(x => x.BranchCode == user.BranchCode)
                                                                .OrderBy(Projections.Property(orderColumn)).Asc
                                                                .Skip((pageNumber) * itemsPerPage)
                                                                .Take(itemsPerPage).List());
                }
                else
                {
                    paged.Items = transact(() => session.QueryOver<AccountMaster>()
                                            .Where(conjuction)
                                            .OrderBy(Projections.Property(orderColumn)).Asc
                                            .Skip((pageNumber) * itemsPerPage)
                                            .Take(itemsPerPage).List());
                }

               
            }
            else
            {
                if (user.BranchCode != "000")
                {
                    paged.Items = transact(() => session.QueryOver<AccountMaster>()
                                                                .Where(conjuction).And(x => x.BranchCode == user.BranchCode)
                                                                .OrderBy(Projections.Property(orderColumn)).Desc
                                                                .Skip((pageNumber) * itemsPerPage)
                                                                .Take(itemsPerPage).List());
                }
                else
                {
                    paged.Items = transact(() => session.QueryOver<AccountMaster>()
                                            .Where(conjuction)
                                            .OrderBy(Projections.Property(orderColumn)).Desc
                                            .Skip((pageNumber) * itemsPerPage)
                                            .Take(itemsPerPage).List());
                }
            }

            if (user.BranchCode != "000")
            {
                paged.TotalItems = transact(() => session.QueryOver<AccountMaster>()
                                          .Where(conjuction).RowCount());
            }
            else
            {
                paged.TotalItems = transact(() => session.QueryOver<AccountMaster>()
                                          .Where(conjuction).And(x => x.BranchCode == user.BranchCode).RowCount());
            }

              
            return paged;
        }

    }
}
